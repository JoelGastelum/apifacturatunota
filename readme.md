Proyecto: Factura tu nota V1
Version laravel: 5.8.30
Autores:

Cambiar version de php
alias php="/usr/local/bin/php71"
ARCHIVO NUEVO
Pasos de instalación:
	1) Ejecutar comando composer install
	2) Configurar base de datos con nombre apifacturatunota
	3) Crea usuario de base de datos
	4) Ejecutar comando php artisan storage:link (Crear enlace simbolico a storage)

Comandos de laravel comunes
	- php artisan route:list (Listado de rutas)
	- php artisan migrate:refresh (Permite realizar roolback a toda la bd y volver a ejecutar todas las migraciones)
	- php artisan db:seed (Ejecuta los datos semillas)
	- php artisan krlove:generate:model User --table-name=user (Permite generar los modelos a partir de la db)
	- php artisan storage:link (Crear enlace simbolico a storage)
	- php artisan make:migration add_esvendedor_usuarios_table (Crear migracion)
	- php artisan migrate (Ejecuta las migraciones pendientes)
	- php artisan migrate:rollback (Retornar antes de la ultima ejecucion de una migración)
	- php artisan route:cache
	- php artisan config:clear
	- php artisan view:clear
	- php artisan cache:clear

Paquetes extras instalados
	1) Collective Html Helper composer require laravelcollective/html
	2) phpmailer
	3) econea/nusoap
	4) composer require mercadopago/dx-php
