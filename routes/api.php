<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/login','ClienteController@login')->name('user.login');

Route::post('user/loginwithtoken','ClienteController@LoginWithToken')->name('user.loginwithtoken');

Route::post('user/clientes','ClienteController@clientes')->name('user.cliente');

Route::post('clientes/save','ClienteController@saveCliente')->name('clientes.save');

Route::post('cliente/newviaje','ClienteController@newViaje')->name('clientes.newviaje');

Route::post('cliente/misviajes','ClienteController@misViajes')->name('clientes.misviajes');

Route::post('cliente/activarviaje','ClienteController@ActivarViaje')->name('clientes.activarviaje');

Route::post('cliente/cambiarestatus','ClienteController@cambiarEstatus')->name('clientes.cambiarestatus');


Route::post('cliente/saveexterno','ClienteController@saveExterno')->name('clientes.saveexterno');

Route::post('clientes/misfacturas','ClienteController@misfacturas')->name('clientes.misfacturas');

Route::post('clientes/delete','ClienteController@DeleteCliente')->name('clientes.delete');

Route::post('clientes/edit','ClienteController@EditCliente')->name('clientes.edit');
                                           
Route::post('nota/buscarticket','ClienteController@buscarTicket')->name('nota.buscar');

Route::post('clientes/saverecomendado','RecomendadoController@store')->name('clientes.saverecomendado');

Route::post('clientes/enviarfacturas','EmpresaController@EnviarFacturas')->name('clientes.enviarfacturas');

Route::post('clientes/misrecomendados','RecomendadoController@buscarRecomendados')->name('clientes.misrecomendados');

Route::post('empresa/login','EmpresaController@login')->name('empresa.login');

Route::post('empresa/info','EmpresaController@infoEmpresa')->name('empresa.info');

Route::post('empresa/savenota','EmpresaController@saveNota')->name('empresa.savenota');

Route::post('empresa/facturar','EmpresaController@facturar')->name('empresa.facturar');

Route::get('empresa/descargarpdf/{id}','EmpresaController@downloadPDF')->name('empresa.descargar');

Route::get('empresa/listopdf/{id}','EmpresaController@PdfListo')->name('empresa.descargar');

Route::post('/empresa/enviarfactura','EmpresaController@MandarCorreo')->name('empresa.mandarcorreo');

Route::get('empresa/downloadpdfxid/{id}/{iduser}','EmpresaController@downloadPDFXId')->name('empresa.downloadpdfxid');


