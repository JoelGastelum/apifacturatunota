<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
})->name('welcome');

/*CONTACTANOS*/
	Route::post('/enviarcontacto', 'PagesController@enviarcontacto')->name('enviarcontacto');


/*SECCION DE ESTABLECIMIENTO*/
	Route::get('/establecimiento/{codigopromocion?}','PagesController@establecimiento')->name('establecimiento');
		Route::post('/verificarcodigopromocion', 'PagesController@verificarcodigopromocion')->name('verificarcodigopromocion');
		Route::post('/buscarprecioplanseleccionado', 'PagesController@buscarprecioplanseleccionado')->name('buscarprecioplanseleccionado');
		Route::post('/buscarplanseleccionado', 'PagesController@buscarplanseleccionado')->name('buscarplanseleccionado');
		Route::post('/revisarrfcenorden', 'PagesController@revisarrfcenorden')->name('revisarrfcenorden');
		Route::post('/seleccionarordenanterior', 'PagesController@seleccionarordenanterior')->name('seleccionarordenanterior');
		Route::post('/compraplan', 'PagesController@compraplan')->name('compraplan');
		/*RUTAS DE MERCADO PAGO*/
		Route::match(array('GET', 'POST'), '/mercadopago_gracias', 'PagesController@mercadopago_gracias')->name('mercadopago.gracias');
		Route::match(array('GET', 'POST'), '/mercadopago_pendiente', 'PagesController@mercadopago_pendiente')->name('mercadopago.pendiente');
		Route::match(array('GET', 'POST'), '/mercadopago_fallido', 'PagesController@mercadopago_fallido')->name('mercadopago.fallido');

/*SECCION DE CONSUMIDOR*/
	Route::get('/consumidor/{codigopromocion?}', 'RegistrateController@consumidor')->name('consumidor');
		Route::post('/verificarusuarioocorreo', 'RegistrateController@verificarusuarioocorreo')->name('verificarusuarioocorreo');
		Route::get('/codigoconfirmacion/{codigoconfirmacion}', 'RegistrateController@codigoconfirmacion')->name('codigoconfirmacion');
		Route::get('/espereconfirmacion/{iduser}', 'RegistrateController@espereconfirmacion')->name('espereconfirmacion');
		Route::get('/usuarioconfirmado/{iduser}/{idremisionencode?}', 'RegistrateController@usuarioconfirmado')->name('usuarioconfirmado');
		Route::post('/reenviarcorreoconfirmacion', 'RegistrateController@reenviarcorreoconfirmacion')->name('reenviarcorreoconfirmacion');
		Route::post('/sconsumidor', 'RegistrateController@sconsumidor')->name('sconsumidor');

/*GANA CON NOSOTROS*/
	Route::get('/gana/{codigopromocion?}', 'RegistrateController@gana')->name('gana');

/*PERMITE FACTURAR EN WEB*/
	Route::get('facturar/{nota}','FacturarController@facturar')->name('facturar');
	Route::post('facturar/verificarrfc','FacturarController@verificarrfc')->name('facturar.verificarrfc');
	Route::post('facturar/guardarcliente','FacturarController@guardarcliente')->name('facturar.guardarcliente');
	Route::post('empresa/facturar','EmpresaController@facturar')->name('empresa.facturar');/*HECHO POR JOEL*/
	Route::get('empresa/descargarcomprobantepdfxid/{id}/{rfc?}','EmpresaController@descargarcomprobantepdfxid')->name('empresa.descargarcomprobantepdfxid');/*NO SE SI SE USE*/
	Route::get('empresa/descargarcomprobantepdfxidweb/{id}/{rfc?}','EmpresaController@descargarcomprobantepdfxidweb')->name('empresa.descargarcomprobantepdfxidweb');
	Route::get('empresa/comprobante/{id}','EmpresaController@descargarcomprobantepdfxidafuera')->name('empresa.comprobante');
	Route::get('empresa/descargarcomprobantexmlxid/{id}','EmpresaController@descargarcomprobantexmlxid')->name('empresa.descargarcomprobantexmlxid');

/*CONSULTA Y LECTURA DE QR*/
	// Route::get('consultaqr/{tokenlogin?}/{rfc?}','ConsultaController@consultaqr')->name('consultaqr');
	Route::get('consultacodigo','ConsultaController@consultacodigo')->name('consultacodigo');
	Route::post('sconsultacodigo','ConsultaController@sconsultacodigo')->name('sconsultacodigo');
	Route::get('consultaqr/{idcliente?}','ConsultaController@consultaqr')->name('consultaqr');
	Route::get('leerqr','ConsultaController@leerqr')->name('leerqr');


/**INCIO DE SESION*/
	Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::get('/forgot-password', function () {
		return view('auth.reset');
	})->name('password.request');

	Route::post('/enviarPass', 'Auth\LoginController@enviarPass')->name('enviarPass');

	Route::post('/buscartoken', 'Auth\LoginController@buscartoken')->name('buscartoken');
	Route::post('/eliminartoken', 'UsuariosController@eliminartoken')->name('eliminartoken');
	Route::post('/login', 'Auth\LoginController@login');
	Route::post('/loginauto', 'Auth\LoginController@loginauto')->name('loginauto');

/*AUTENTICACION*/
Route::middleware(['auth','permisos','estaactivo','web'])->group(function () {
	Route::get('contribuyentes/visualizarpdf/{idcomprobante}/{rfc?}/{idviaje?}','ContribuyentesController@visualizarpdf')->name('contribuyentes.visualizarpdf');

	Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
	Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/users/privacidad', 'PrivacidadesController@privacidad')->name('users.privacidad');
	Route::post('/users/sprivacidad', 'PrivacidadesController@sprivacidad')->name('users.sprivacidad');
	Route::get('/users/cambiarcontrasena', 'PrivacidadesController@cambiarcontrasena')->name('usuarios.cambiarcontrasena');
	Route::post('/users/saveContra', 'PrivacidadesController@saveContra')->name('usuarios.saveContra');

	//TIPO USUARIO = 1 (AFILIADO)
		//ORDENES DE INSCRIPCIONES POR APROBAR
			Route::get('/ordenesporaprobar/index', 'OrdenesporaprobarController@index')->name('ordenesporaprobar.index');
				Route::post('/ordenesporaprobar/loaddata', 'OrdenesporaprobarController@loaddata')->name('ordenesporaprobar.loaddata');
			Route::get('/ordenesporaprobar/mostrar/{id}', 'OrdenesporaprobarController@mostrar')->name('ordenesporaprobar.mostrar');
			Route::get('/ordenesporaprobar/aprobar/{id}', 'OrdenesporaprobarController@aprobar')->name('ordenesporaprobar.aprobar');
				Route::post('/ordenesporaprobar/saprobar', 'OrdenesporaprobarController@saprobar')->name('ordenesporaprobar.saprobar');

		//COMISIONES
			//POR PAGAR
				Route::get('recomendados/comisiones','RecomendadoController@comisionesadmin')->name('recomendados.comisiones');
				Route::post('recomendados/pagos','RecomendadoController@realizarpagos')->name('recomendados.pagos');
				Route::post('recomendados/savepago','RecomendadoController@savePago')->name('recomendados.savepago');

			//PAGADAS
				Route::get('pagos/index','PagosController@index')->name('pagos.index');
				Route::post('pagos/loaddata','PagosController@loaddata')->name('pagos.loaddata');

	//TIPO USUARIO = 2 (ADMINISTRADOR)
		//HOME MODAL DE CONFIGURACION
			Route::post('/home/guardarpasos', 'HomeController@guardarpasos')->name('home.guardarpasos');


		//CAPTURAS
			//CREAR NOTA
				Route::get('/crearnotas/index', 'CrearnotaController@index')->name('crearnotas.index');
				Route::post('/crearnotas/update', 'CrearnotaController@update')->name('crearnotas.update');
				Route::post('/crearnotas/buscarconceptosdenota', 'CrearnotaController@buscarconceptosdenota')->name('crearnotas.buscarconceptosdenota');

		//LISTADOS
			//NOTAS
				Route::get('/notas/index', 'NotasController@index')->name('notas.index');
				Route::post('/notas/loaddata', 'NotasController@loaddata')->name('notas.loaddata');
				Route::get('/notas/create', 'NotasController@create')->name('notas.create');
				Route::get('/notas/visualizar/{id}', 'NotasController@visualizar')->name('notas.visualizar');
				Route::post('/notas/buscarconceptosdenotacreada', 'NotasController@buscarconceptosdenotacreada')->name('notas.buscarconceptosdenotacreada');
				Route::put('/notas/{id}', 'NotasController@update')->name('notas.update');
				Route::post('/notas/eliminar/{id}', 'NotasController@eliminar')->name('notas.eliminar');

			//FACTURAS
				Route::get('/facturas/index', 'FacturasController@index')->name('facturas.index');
				Route::post('/facturas/loaddata', 'FacturasController@loaddata')->name('facturas.loaddata');
				Route::get('/facturas/create', 'FacturasController@create')->name('facturas.create');
				Route::get('/facturas/visualizar/{id}', 'FacturasController@visualizar')->name('facturas.visualizar');
				Route::post('/facturas/buscarconceptosdenotacreada', 'FacturasController@buscarconceptosdenotacreada')->name('facturas.buscarconceptosdenotacreada');
				Route::put('/facturas/{id}', 'FacturasController@update')->name('facturas.update');
				Route::post('/facturas/eliminar/{id}', 'FacturasController@eliminar')->name('facturas.eliminar');

		//CONFIGURACIÓN
			//FACTURACION
				Route::get('/facturas/configuracion', 'FacturasController@Configuracion')->name('facturas.configuracion');
				Route::post('/facturas/actualizarconf', 'FacturasController@actualizarconf')->name('facturas.actualizarconf');

			//CONCEPTOS
				Route::get('/conceptos/index', 'ConceptosController@index')->name('conceptos.index');
				Route::post('/conceptos/loaddata', 'ConceptosController@loaddata')->name('conceptos.loaddata');
				Route::get('/conceptos/create', 'ConceptosController@create')->name('conceptos.create');
				Route::get('/conceptos/edit/{id}', 'ConceptosController@edit')->name('conceptos.edit');
				Route::put('/conceptos/{id}', 'ConceptosController@update')->name('conceptos.update');
				Route::post('/conceptos/eliminar/{id}', 'ConceptosController@eliminar')->name('conceptos.eliminar');
				Route::post('/conceptos/buscarunidadessat', 'ConceptosController@buscarunidadessat')->name('conceptos.buscarunidadessat');
				Route::post('/conceptos/buscarcodigoproductosat', 'ConceptosController@buscarcodigoproductosat')->name('conceptos.buscarcodigoproductosat');

			//SERIES
				Route::get('/series/index', 'SeriesController@index')->name('series.index');
				Route::post('/series/loaddata', 'SeriesController@loaddata')->name('series.loaddata');
				Route::get('/series/create', 'SeriesController@create')->name('series.create');
				Route::get('/series/edit/{id}', 'SeriesController@edit')->name('series.edit');
				Route::put('/series/{id}', 'SeriesController@update')->name('series.update');
				Route::post('/series/eliminar/{id}', 'SeriesController@eliminar')->name('series.eliminar');

			//CERTIFICADOS
				Route::get('/certificados/index', 'CertificadosController@index')->name('certificados.index');
				Route::post('/certificados/loaddata', 'CertificadosController@loaddata')->name('certificados.loaddata');
				Route::get('/certificados/create', 'CertificadosController@create')->name('certificados.create');
				Route::get('/certificados/edit/{id}', 'CertificadosController@edit')->name('certificados.edit');
				Route::put('/certificados/{id}', 'CertificadosController@update')->name('certificados.update');
				Route::post('/certificados/eliminar/{id}', 'CertificadosController@eliminar')->name('certificados.eliminar');

			//SUCURSALES
				Route::get('/sucursales/index', 'SucursalesController@index')->name('sucursales.index');
				Route::post('/sucursales/loaddata', 'SucursalesController@loaddata')->name('sucursales.loaddata');
				Route::get('/sucursales/create', 'SucursalesController@create')->name('sucursales.create');
				Route::get('/sucursales/edit/{id}', 'SucursalesController@edit')->name('sucursales.edit');
				Route::put('/sucursales/{id}', 'SucursalesController@update')->name('sucursales.update');
				Route::post('/sucursales/eliminar/{id}', 'SucursalesController@eliminar')->name('sucursales.eliminar');

			//USUARIOS
				Route::get('/usuarios/index', 'UsuariosController@index')->name('usuarios.index');
				Route::post('/usuarios/loaddata', 'UsuariosController@loaddata')->name('usuarios.loaddata');
				Route::get('/usuarios/create', 'UsuariosController@create')->name('usuarios.create');
				Route::get('/usuarios/edit/{id}', 'UsuariosController@edit')->name('usuarios.edit');
				Route::put('/usuarios/{id}', 'UsuariosController@update')->name('usuarios.update');
				Route::post('/usuarios/eliminar/{id}', 'UsuariosController@eliminar')->name('usuarios.eliminar');

			//COMPRA PLAN
				Route::get('/comprarplan/index', 'ComprarplanController@index')->name('comprarplan.index');
				Route::post('/comprarplan/comprarplan', 'ComprarplanController@comprarplan')->name('comprarplan.comprarplan');

	//TIPO USUARIO = 4 (CONSUMIDOR CLIENTE)
		//GANA DINERO CON NOSOTROS
			Route::get('/ganadineroconnosotros', 'GanardineroController@index')->name('ganardinero.index');
			Route::get('/terminoscondiciones', 'GanardineroController@terminoscondiciones')->name('ganardinero.terminoscondiciones');
			Route::post('/sterminoscondiciones', 'GanardineroController@sterminoscondiciones')->name('ganardinero.sterminoscondiciones');

	//TIPO USUARIO = 4 Y  5  (CONSUMIDOR CLIENTE)
		//CONTRIBUYENTES
			Route::get('/contribuyentes', 'HomeController@index')->name('contribuyentes');
			Route::post('/contribuyentes/eliminarqr', 'ContribuyentesController@eliminarqr')->name('contribuyentes.eliminarqr');
			Route::post('/contribuyentes/buscarviajes', 'ContribuyentesController@buscarviajes')->name('contribuyentes.buscarviajes');
			Route::post('/contribuyentes/editarviaje', 'ContribuyentesController@editarviaje')->name('contribuyentes.editarviaje');
			Route::post('/contribuyentes/seditarviaje', 'ContribuyentesController@seditarviaje')->name('contribuyentes.seditarviaje');
			Route::post('/contribuyentes/cerrarviaje', 'ContribuyentesController@cerrarviaje')->name('contribuyentes.cerrarviaje');
			Route::post('/contribuyentes/eliminarviaje', 'ContribuyentesController@eliminarviaje')->name('contribuyentes.eliminarviaje');
			Route::post('/contribuyentes/enviarqr', 'ContribuyentesController@enviarqr')->name('contribuyentes.enviarqr');
			Route::get('/clientes/misviajes/{rfc}/{idviaje?}', 'ClienteController@Viajes')->name('clientes.Viajes');
			Route::get('empresa/listopdf/{id}','EmpresaController@PdfListo')->name('empresa.descargar');
			// Route::post('/contribuyentes/cambiarcampoviaje', 'ContribuyentesController@cambiarcampoviaje')->name('contribuyentes.cambiarcampoviaje');

		//RECOMENDADOS AMIGOS
			Route::get('recomendados/codigogenerico','RecomendadoController@codigogenerico')->name('recomendados.codigogenerico');
			Route::get('recomendados/misbannergenerico','RecomendadoController@misbannergenerico')->name('recomendados.misbannergenerico');
			Route::get('recomendados/amigos','RecomendadoController@amigos')->name('recomendados.amigos');
			Route::post('recomendados/loaddata','RecomendadoController@loaddata')->name('recomendados.loaddata');
			Route::match(array('GET', 'POST'),'recomendados/loaddata2','RecomendadoController@loaddata2')->name('recomendados.loaddata2');
			Route::get('recomendados/mislinks','RecomendadoController@mislinks')->name('recomendados.mislinks');

			Route::get('recomendados/misbanner','RecomendadoController@misbanner')->name('recomendados.misbanner');

	//TIPO USUARIO = 5 (CONSUMIDOR VENDEDOR)
		//CLIENTES
			Route::get('/clientes/edit', 'ClienteController@create')->name('clientes.create');
			Route::put('/clientes/{id}', 'ClienteController@update')->name('clientes.update');
			Route::get('/clientes/edit/{id}', 'ClienteController@edit')->name('clientes.edit');

			Route::post('clientes/delete','ClienteController@DeleteCliente')->name('clientes.delete');
			Route::get('clientes/misfacturas/{rfc}/{idviaje?}','ClienteController@verfacturas')->name('clientes.misfacturas');
			Route::post('clientes/buscarfacturas','ClienteController@buscarfacturas')->name('clientes.buscarfacturas');
			Route::post('clientes/misfacturasweb','ClienteController@misfacturas')->name('clientes.facturas');
			Route::get('clientes/facturaexterna/{rfc}/{idviaje?}','ClienteController@facturaexterna')->name('clientes.facturaexterna');
			Route::post('cliente/saveexterno','ClienteController@saveExterno')->name('clientes.saveexterno');
			Route::post('/clientes/activarviajeweb','ClienteController@ActivarViaje')->name('clientes.activarviajeWeb');

		//RECOMENDADOS ESTABLECIMIENTOS
			Route::post('clientes/saverecomendado','RecomendadoController@store')->name('clientes.saverecomendado');
			Route::get('/recomendados/index','RecomendadoController@verfacturas')->name('recomendados.index');
			Route::post('clientes/misrecomendadosweb','RecomendadoController@buscarRecomendados')->name('clientes.misrecomendadosweb');
			Route::post('/contribuyentes/crearqr', 'ContribuyentesController@crearqr')->name('contribuyentes.crearqr');
});
// Route::get('empresa/descargarpdf/{id}','EmpresaController@downloadPDF')->name('empresa.descargar');
 Route::get('/cliente/verexcel/{ids}', 'ClienteController@descargarExcel')->name('cliente.verexcel');
// Route::get('/usuarios/sendwhats', 'ClienteController@sendwhats')->name('usuarios.sendwhats');

