/*2020-12-09*/
	/*ACEPTANDO NULL EN TABLA sucursales CAMPO idsucursalintegra*/
	ALTER TABLE `sucursales` CHANGE `idsucursalintegra` `idsucursalintegra` INT(11) NULL DEFAULT NULL;
	/*ANEXANDO IMAGEN DE AVATAR POR DEFECTO*/
	ALTER TABLE `usuarios` CHANGE `foto_perfil` `foto_perfil` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'img/compartidos/avatar10.jpg';
	UPDATE `usuarios` SET foto_perfil = 'img/compartidos/avatar10.jpg' WHERE foto_perfil is null;

/*2020-12-29*/
	ALTER TABLE `usuarios` CHANGE `login` `login` CHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

/*2021-01-14*/
	/*ANEXANDO DATOS DE TABLA SERVICIOS*/
	UPDATE `servicios` SET `numfolios`=0 WHERE `numfolios`<0;
	UPDATE `servicios` SET idconcepto = 14 WHERE idconcepto=12;
	INSERT INTO `conceptosxdefault` (`id`, `codigo`, `concepto`, `codigounidadsat`, `unidad`, `codigogrupoprodsersat`, `idempresa`, `estatus`)
	VALUES
		('14', '0301', 'concepto subscripcion', '123', 'UN', '123', '1', 'A'),
		('13', '0301', 'concepto paquete', '123', 'UN', '123', '1', 'A');

/*2021-01-15*/
	/*ANEXANDO MAS LIMITE AL CODIGO DE USUARIOS*/
	ALTER TABLE `usuarios` CHANGE `codigopromocion` `codigopromocion` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
	INSERT INTO `tiposusuario` (`id`, `nombre`, `opciondefault`) VALUES ('5', 'Cliente vendedor', '');

/*2021-01-19*/
	/*GENEROS DE SEXO*/
	INSERT INTO `generos` (`id`, `descripcion`, `referirsecomo`, `idusercreated`) VALUES (NULL, 'Mujer', '1', NULL), (NULL, 'Hombre', '2', NULL);
	INSERT INTO `generos` (`id`, `descripcion`, `referirsecomo`, `idusercreated`) VALUES ('3', 'Personalizado', '1', NULL);

/*2021-01-21*/
	/*CONFIGURACIONES DE PAGO*/
	INSERT INTO `configuracionweb` (`id`, `nombre`, `parametros`, `tipo`, `created_at`, `updated_at`) VALUES (NULL, 'cuentabanco', '0102', '3', NULL, NULL), (NULL, 'banco', 'Banco de Mexico', '3', NULL, NULL), (NULL, 'numerodecontacto', '0414', '3', NULL, NULL);
	INSERT INTO `configuracionweb` (`id`, `nombre`, `parametros`, `tipo`, `created_at`, `updated_at`) VALUES (NULL, 'clabeinterbancaria', '0412124', '3', NULL, NULL);
	INSERT INTO `configuracionweb` (`id`, `nombre`, `parametros`, `tipo`, `created_at`, `updated_at`) VALUES (NULL, 'numerotarjeta', '04121124', '3', NULL, NULL);

/*2021-01-22*/
	/*USUARIO SUPERADMIN*/
	INSERT INTO `usuarios` (`id`, `login`, `nombre`, `idgenero`, `fnacimiento`, `password`, `email`, `codigoconfirmacion`, `pwd`, `movil`, `fijo`, `estatus`, `idtipousuario`, `idempresa`, `idsucursal`, `codigopromocion`, `idusuariorecomendon`, `clabeinterbancaria`, `tokenlogin`, `foto_perfil`, `remember_token`, `created_at`) VALUES (NULL, 'superadmin', 'Super administrador', NULL, NULL, '$2y$10$iJK.MVsYJ2HfcKgjZceVs.aSZb/jmBGTaKGg1MsExZPn.0aWMNPCG', 'contacto@fesoluciones.com.mx', '36007232bdefff', '123456', NULL, NULL, 'A', '1', '0', '0', '', NULL, NULL, NULL, 'img/compartidos/avatar1.jpg', 'zHLSSFpcwml6AyWr4QB4wi46WafrGfWngV1k8JwT9pVW0qnW3Ft6rCke34f5', '2021-01-22 14:21:31');

/*2021-02-03*/
	/*COLOCANDO CONFIGURACION PARA INDICAR CUANDO FUE LA ULTIMA VEZ QUE SE EJECUTO LA VERIFICACION DE CODIGOS*/
	INSERT INTO `configuracionweb` (`id`, `nombre`, `parametros`, `tipo`, `created_at`, `updated_at`) VALUES (NULL, 'diaactualizacioncodigovencidos', '2021-02-01', '2', NULL, NULL);

/*2021-02-05*/
	/*COLOCANDO VALOR A TABLA DE MENSAJES*/
	INSERT INTO `mensajestimbrados` (`id`, `cadenadebusqueda`, `cadenalegible`, `created_at`, `updated_at`) VALUES (NULL, 'edicom:soapenv:Server.userException: com.edicom.ediwinws.service.cfdi.CFDiException: CFDI33132:', 'El {{rfc}} proporcionado es incorrecto.', NULL, NULL);
