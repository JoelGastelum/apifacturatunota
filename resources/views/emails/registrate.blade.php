@component('mail::message')
# Bienvenido a Mefactura:
#{{ $data['login'] }}

Hola {{ $data['nombre'] }} bienvenido a <strong>Mefactura</strong>, sitio web donde podrás hacer factura rápidamente. Te enviamos tus datos para ingresar
<br>
Login: <strong>{{ $data['login'] }}</strong>
Pass: <strong>{{ $data['pwd'] }}</strong>
<br>
<br>
Lo invitamos a disfrutar de su cuenta gratuita en
@component('mail::button', ['url' => $data['link']])
Acceda a su cuenta
@endcomponent

Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
