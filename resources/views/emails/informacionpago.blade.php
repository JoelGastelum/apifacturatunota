@component('mail::message')
<p>Mefactura.com te agradece por seleccionarnos como tu opción de facturación para el rfc <strong>{{ $data['rfc']}}</strong>, a continuación se detalla la información de tu pedido: subscripción de <strong>{{ $data['subscripcion']}}</strong> y un <strong>{{ $data['paquete'] }}</strong>, dando un total de <strong>{{ $data['total'] }}</strong> pesos. Para su mayor comodidad le especificamos los medios de pago:
<ul>
  <li>Deposito en <strong>{{ $data['banco'] }}</strong> a la cuenta <strong>{{ $data['cuentabanco'] }}</strong></li>
  <li>Transferencia electrónica a la clabe interbancaria <strong>{{ $data['clabeinterbancaria'] }}</strong></li>
  <li>Pago en oxxo a la tarjeta <strong>{{ $data['numerotarjeta'] }}</strong></li>
</ul>
<p>A nombre de <strong>{{ $data['personanombre'] }}</strong> con R.F.C: <strong>{{ $data['personarfc'] }}</strong>
<br>
<p>Una vez realizada el pago por favor comunícate con nosotros vía whatsapp o llamada al teléfono <strong> {{ $data['numerodecontacto'] }}</strong> para activar su cuenta.</p>

Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
