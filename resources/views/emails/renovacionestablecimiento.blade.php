@component('mail::message')
# Bienvenido a Mefactura:
#{{ $data['login'] }}

Hola {{ $data['nombre'] }} muchas gracias por renovar tu plan en <strong>Mefactura</strong>, sitio web donde podrás hacer factura rápidamente.
<br>
<br>
Si desea facturar su orden de compra lo invitamos hacer click en el siguiente botón
@component('mail::button', ['url' => $data['urlfacturar']])
Facturar
@endcomponent

Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
