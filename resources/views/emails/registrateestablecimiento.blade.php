@component('mail::message')
# Bienvenido a Mefactura:
#{{ $data['login'] }}

Hola {{ $data['nombre'] }} bienvenido a <strong>Mefactura</strong>, sitio web donde podrás hacer factura rápidamente. Te enviamos tus datos para ingresar
<br>
Login: <strong>{{ $data['login'] }}</strong>
Pass: <strong>{{ $data['pwd'] }}</strong>
<br>
<br>
Si desea facturar su orden de compra lo invitamos hacer click en el siguiente botón
@component('mail::button', ['url' => $data['urlfacturar']])
Facturar
@endcomponent
Entra y disfrutar de su cuenta gratuita en
@component('mail::button', ['url' => $data['urllogin']])
Acceda a su cuenta
@endcomponent

Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
