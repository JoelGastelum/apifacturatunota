@component('mail::message')


<p>Haz recibido tus facturas!</p>

<a href='{{$excel}}'><Button style='margin:10px 0 10px 0;color:#ffffff;font-weight:bold;display:inline-block;padding:6px 12px;font-size:16px;text-align:center;cursor:pointer;background-image:none;border:1px solid transparent;border-radius:4px;outline: 0;background-color:#04B431;'>Pulsa para ver tu excel!</Button></a>
<br>
Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
