@component('mail::message')
#Se ha recibido un mensaje de contacto:

Correo electrónico: <strong>{{ $data['email'] }}</strong><br><br>
Nombre: <strong>{{ $data['name'] }}</strong><br><br>
Teléfono: <strong>{{ $data['phone'] }}</strong><br><br>
Asunto: <strong>{{ $data['subject'] }}</strong><br><br>
Mensaje: <strong>{{ $data['message'] }}</strong><br><br>

Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
