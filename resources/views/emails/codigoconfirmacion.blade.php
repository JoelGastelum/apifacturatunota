@component('mail::message')
# Reciba un cordial saludo, estimado:
#{{ $data['login'] }}

Hola {{ $data['nombre'] }} casi has culminado tu registro en <strong>Mefactura</strong>, solo necesitamos que confirmes tu correo haciendo click en el siguiente link.
{{ $data['link'] }}


Estamos para brindarle el mejor servicio de calidad<br>
Mefactura
@endcomponent
