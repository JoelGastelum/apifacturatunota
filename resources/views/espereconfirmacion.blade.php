@extends('plantillas.publica')
@section('content')
  <!-- ======= Header ======= -->
    @include('elementos.public_header')
  <!-- End Header -->

  <main id="main">
    <!-- ======= Registro PARA CONSUMIDORES Section ======= -->
    <section id="registro" class="about-video">
      @if(session('success'))
          <div class="row">
              <div class="container">
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              </div>
          </div>
      @endif
      @if(session('danger'))
          <div class="row">
              <div class="container">
                  <div class="alert alert-danger">
                      {{ session('danger') }}
                  </div>
              </div>
          </div>
      @endif
      <div class="container" data-aos="fade-up" >
        <br>
        <div class="section-title">
          <h2>Gracias por registrarse {{ $objUser->login }} en {{ session()->get('Configuracion.Web.titulo') }}</h2>
          <br>
          <h3>Estas a un paso para completar tu registro, revisa tu correo electrónico y confirma tu usuario</h3>
          <br>
          <h4>Si no has recibido el correo, haz click al siguiente botón para reenviar</h3>
          <div class="col-md-12">
            <div class="text-center">
              <button onclick="reenviarcorreo();" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Reenviar correo</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Registro de Section -->
  </main>

  <!-- ======= Footer ======= -->
    @include('elementos.public_footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
  <!-- FIN DE SWEETALERT2-->
  <script type="text/javascript">
      /**
       * Funcion que permite verificar si correo ya existe
       */
      function reenviarcorreo(){
        $.ajax({
            type: "POST",
            url: "{{ route('reenviarcorreoconfirmacion') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "iduser": {{ $objUser->id }},
            },
            dataType:"json",
            success: function(respuesta) {
              if(respuesta.error==1){
                Swal.fire({
                  type: 'success',
                  title: '',
                  text: respuesta.msj
                });
              }else{
                Swal.fire({
                  type: 'warning',
                  title: '',
                  text: respuesta.msj
                });
              }
            },
            error: function() {
            }
        });
      }
  </script>
@endsection


