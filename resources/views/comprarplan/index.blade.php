@extends('plantillas.privada')
@section('content')
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
          <h4>Planes de subscripción</h4>
          <div class="row">
              <?php foreach ($suscripciones as $suscripcion):?>
                <div class="col-md-6 col-xl-4">
                  <a data-id="{{ $suscripcion->id }}" class="suscripcionblock block block-link-pop block-rounded block-bordered text-center" href="javascript:void(0)">
                      <div class="block-header">
                          <h3 class="block-title">{{ $suscripcion->producto }}</h3>
                      </div>
                      <div class="block-content bg-body-light">
                          <div class="py-2">
                              <p class="h1 font-w700 mb-2">${{ number_format($suscripcion->precio,2,'.',',') }}</p>
                              <p class="h6 text-muted">
                                {{ $suscripcion->duracion }}
                                <?php if($suscripcion->duracion>1):?>
                                  meses
                                <?php else:?>
                                  mes
                                <?php endif;?>
                              </p>
                          </div>
                      </div>
                      <div class="block-content block-content-full bg-body-light">
                          <span class="btn btn-hero-success px-4" onclick="seleccion({{ $suscripcion->id }},2);">Seleccionar</span>
                      </div>
                  </a>
                </div>
                <!--CREANDO VARIABLE CON DATOS-->
                <input
                  type="hidden"
                  id="{{ $suscripcion->id }}"
                  data-producto="{{ $suscripcion->producto }}"
                  data-precio="${{ number_format($suscripcion->precio,2,'.',',') }}"
                  data-duracion="{{ $suscripcion->duracion }}"
                >
              <?php endforeach;?>
          </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
          <h4>Planes de paquete de folios</h4>
          <div class="row">
              <?php foreach ($paquetes as $paquete):?>
                <div class="col-md-6 col-xl-4">
                  <a data-id="{{ $paquete->id }}" class="paqueteblock block block-link-pop block-rounded block-bordered text-center" href="javascript:void(0)">
                      <div class="block-header">
                          <h3 class="block-title">{{ $paquete->producto }}</h3>
                      </div>
                      <div class="block-content bg-body-light">
                          <div class="py-2">
                              <p class="h1 font-w700 mb-2">${{ number_format($paquete->precio,2,'.',',') }}</p>
                              <p class="h6 text-muted">
                                {{ $paquete->numfolios }}
                                folios
                              </p>
                          </div>
                      </div>
                      <div class="block-content block-content-full bg-body-light">
                          <span class="btn btn-hero-success px-4" onclick="seleccion({{ $paquete->id }},1);">Seleccionar</span>
                      </div>
                  </a>
                </div>
                <!--CREANDO VARIABLE CON DATOS-->
                <input
                  type="hidden"
                  id="{{ $paquete->id }}"
                  data-producto="{{ $paquete->producto }}"
                  data-precio="${{ number_format($paquete->precio,2,'.',',') }}"
                  data-numfolios="{{ $paquete->numfolios }}"
                >
              <?php endforeach;?>
          </div>
        </div>
    </div>

    <div id="planseleccionado" class="block block-rounded block-bordered" style="display: none;">
      <div class="block-content block-content-full">
        <h4>Paquete seleccionado</h2>
        <p>Usted ha seleccionado comprar los siguientes paquetes.</p>
        <div class="row">
          <div class="col-md-4 offset-md-2">
            <a id="block_servicio" class="block text-center bg-xsmooth" href="javascript:void(0)" style="display: none;">
                <div class="block-content block-content-full aspect-ratio-4-3 d-flex justify-content-center align-items-center">
                    <div>
                        <div class="font-size-h1 font-w300 text-xsmooth-lighter" id="servicio_producto">19</div>
                        <div class="font-w600 mt-2 text-uppercase text-white-75" id="servicio_precio">Messages</div>
                        <div class="font-w600 mt-2 text-uppercase text-white-75" id="servicio_duracion">Messages</div>
                    </div>
                </div>
            </a>
          </div>
          <div class="col-md-4">
            <a id="block_paquete" class="block text-center bg-gd-lake" href="javascript:void(0)" style="display: none;">
                <div class="block-content block-content-full aspect-ratio-4-3 d-flex justify-content-center align-items-center">
                    <div>
                        <div class="font-size-h1 font-w300 text-xsmooth-lighter" id="paquete_producto">19</div>
                        <div class="font-w600 mt-2 text-uppercase text-white-75" id="paquete_precio">Messages</div>
                        <div class="font-w600 mt-2 text-uppercase text-white-75" id="paquete_numfolios">Messages</div>
                    </div>
                </div>
            </a>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <form id="comprarplanform" action="#" method="post" role="form">
              <div class="row">
                <div class="col-lg-12 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
                  <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" id="comprarplan_idserv" name="idserv" value="-1">
                  <input type="hidden" id="comprarplan_idpaquete" name="idpaquete" value="-1">
                  <input type="hidden" id="comprarplan_metodopago" name="metodopago" value="-1">
                  <input type="hidden" id="comprarplan_idorden" name="idorden" value="-1">
                  <div class="row">
                    <div class="col-12">
                      <div class="text-right">
                        <button type="button" onclick="pagar(1);" id="btn-mercadopago" style="cursor:pointer;background: #F4C400;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                            Pagar con tarjeta de crédito
                        </button>
                        <button type="button" onclick="pagar(2);" id="btn-otrometodos" style="cursor:pointer;background: #063E7B;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                            Pagar con otro métodos
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal de información de pago -->
    <div class="modal" id="modal-infopago" tabindex="-1" role="dialog" aria-labelledby="modal-default-vcenter" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Muchas gracias por querer estar con nosotros</h5>
                  <button type="button" class="close" onclick="cerrarmodalinfopago();">
                      <span aria-hidden="true">×</span>
                  </button>
              </div>
              <div class="modal-body pb-1">
                  <p>Mefactura.com te agradece por seleccionarnos nuevamente como tu opción de facturación para el rfc <strong id="infopago-rfc"></strong>, a continuación se detalla la información de tu pedido: <strong id="infopago-subscripcion"></strong> <strong id="infopago-paquete"></strong>, dando un total de <strong id="infopago-total"></strong> pesos. Para su mayor comodidad le especificamos los medios de pago:
                  <ul>
                    <li>Deposito en <strong id="infopago-banco"></strong> a la cuenta <strong id="infopago-cuentabanco"></strong></li>
                    <li>Transferencia electrónica a la clabe interbancaria <strong id="infopago-clabeinterbancaria"></strong></li>
                    <li>Pago en oxxo a la tarjeta <strong id="infopago-numerotarjeta"></strong></li>
                  </ul>
                  <p>Una vez realizada el pago por favor comunícate con nosotros vía whatsapp o llamada al teléfono <strong id="infopago-numerodecontacto"></strong> para activar su cuenta.</p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-light" onclick="cerrarmodalinfopago();">Cerrar</button>
              </div>
          </div>
      </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <script type="text/javascript">
        /**
         * Función que permite marcar la selección del plan
         */
        function seleccion(idservicio,tipo){
            if(tipo==2){
              //Subscripcion
              $(".suscripcionblock").removeClass("block-themed");
              $(".suscripcionblock").each(function() {
                var id = $(this).attr("data-id");
                if(id==idservicio){
                  $( this ).addClass( "block-themed" );
                }
              });
              $("#comprarplan_idserv").val(idservicio);

              var producto = $("#"+idservicio).attr("data-producto");
              $("#servicio_producto").html(producto);
              var precio = $("#"+idservicio).attr("data-precio");
              $("#servicio_precio").html(precio);
              var duracion = $("#"+idservicio).attr("data-duracion");
              if(parseFloat(duracion)>1){
                duracion = duracion + 'meses';
              }else{
                duracion = duracion + 'mes';
              }
              $("#servicio_duracion").html(duracion);

              $("#planseleccionado").show();
              $("#block_servicio").show();
            }else{
              if(tipo==1){
                //paquete de folio
                $(".paqueteblock").removeClass("block-themed");
                $(".paqueteblock").each(function() {
                  var id = $(this).attr("data-id");
                  if(id==idservicio){
                    $( this ).addClass( "block-themed" );
                  }
                });
              }
              $("#comprarplan_idpaquete").val(idservicio);

              var producto = $("#"+idservicio).attr("data-producto");
              $("#paquete_producto").html(producto);
              var precio = $("#"+idservicio).attr("data-precio");
              $("#paquete_precio").html(precio);
              var numfolios = $("#"+idservicio).attr("data-numfolios");
              numfolios = numfolios + 'folios';
              $("#paquete_numfolios").html(numfolios);

              $("#planseleccionado").show();
              $("#block_paquete").show();
            }
        }

        /**
         * Función que permite hacer submit pero coloca el metodo de pago que se va a usar
         */
        function pagar(metodopago){
          var error=0;

          var comprarplan_idserv = $("#comprarplan_idserv").val();
          var comprarplan_idpaquete = $("#comprarplan_idpaquete").val();
          if(
            (comprarplan_idserv=== "undefined" || comprarplan_idserv=== "")
            && (comprarplan_idpaquete=== "undefined" || comprarplan_idpaquete=== "")
          ){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '',
                text: 'Debe de seleccionar plan obligatoriamente'
              });
          }

          if(error==0){
            $("#comprarplan_metodopago").val(metodopago);
            compraplan();
          }
        }

        /**
         * Permite enviar la información para que se cree la orden de pago
         */
        function compraplan(){
          var idserv = $('#comprarplan_idserv').val();
          var idpaquete = $('#comprarplan_idpaquete').val();
          var metodopago = $('#comprarplan_metodopago').val();
          var idorden = $('#comprarplan_idorden').val();

          //BLOQUEANDO BOTON
          $('#btn-mercadopago').prop("disabled",true);
          $('#btn-mercadopago').css("background-color","#E4AE3096");
          $('#btn-otrometodos').prop("disabled",true);
          $('#btn-otrometodos').css("background-color","rgba(6, 62, 123, 0.49)");

          $.ajax({
              type: "POST",
              url: "{{ route('comprarplan.comprarplan') }}",
              beforeSend: function() {
                  Swal.fire({
                      title: 'Espere por favor',
                      allowOutsideClick: false
                  });
                  Swal.showLoading();
              },
              data: {
                  "_token": "{{ csrf_token() }}",
                  "idserv": idserv,
                  "idpaquete": idpaquete,
                  "metodopago": metodopago,
                  "idorden": idorden,
              },
              dataType:"json",
              async: true,
              success: function(respuesta) {
                Swal.close();
                //DESBLOQUEANDO BOTON
                $('#btn-mercadopago').prop("disabled",false);
                $('#btn-mercadopago').css("background-color","#E4AE30");
                $('#btn-otrometodos').prop("disabled",false);
                $('#btn-otrometodos').css("background-color","rgb(6, 62, 123)");
                if(respuesta.error==0){
                  $('#comprarplan_idorden').val(respuesta.idorden);
                  if(respuesta.metodopago==1){
                    //MERCADO PAGO (REDIRECIONAR)
                    window.location=respuesta.url;
                  }else{
                    if(respuesta.metodopago==2){
                      //OTRO MEDIOS DE PAGO (MOSTRAR MODAL)
                      $('#infopago-cuentabanco').html(respuesta.cuentabanco);
                      $('#infopago-banco').html(respuesta.banco);
                      $('#infopago-numerodecontacto').html(respuesta.numerodecontacto);
                      $('#infopago-clabeinterbancaria').html(respuesta.clabeinterbancaria);
                      $('#infopago-numerotarjeta').html(respuesta.numerotarjeta);

                      $('#infopago-rfc').html(respuesta.rfc);
                      if(respuesta.subscripcion!=""){
                        respuesta.subscripcion = 'subscripción de '+respuesta.subscripcion;
                      }
                      $('#infopago-subscripcion').html(respuesta.subscripcion);

                      if(respuesta.paquete!=""){
                        respuesta.paquete = respuesta.paquete;
                      }
                      $('#infopago-paquete').html(respuesta.paquete);
                      $('#infopago-total').html(respuesta.total);

                      $("#modal-infopago").modal({backdrop: 'static', keyboard: false});
                    }
                  }
                }else{
                  Swal.fire({
                    type: 'warning',
                    title: '',
                    text: respuesta.msj
                  });
                }
              },
              error: function() {
                Swal.close();
              }
          });
        }

        /**
         * Permite cerrar el modal de información de pago y redirecciona a home
         */
        function cerrarmodalinfopago(){
          window.location="{{ route('home') }}";
        }

        $(document).ready(function() {
          <?php if(isset($idserv)):?>
            <?php if($idserv!=-1):?>
              seleccion({{$idserv}},2);
            <?php endif;?>
          <?php endif;?>
          <?php if(isset($idpaquete)):?>
            <?php if($idpaquete!=-1):?>
              seleccion({{$idpaquete}},1);
            <?php endif;?>
          <?php endif;?>
          <?php if(isset($comprarplan_idorden)):?>
            <?php if($comprarplan_idorden!=null):?>
              $("#comprarplan_idorden").val({{$comprarplan_idorden}});
            <?php endif;?>
          <?php endif;?>
          <?php if(isset($msjerror)):?>
            <?php if($msjerror!=null):?>
              Swal.fire({
                type: 'warning',
                title: '',
                text: "{{ $msjerror }}"
              });
            <?php endif;?>
          <?php endif;?>
        });
    </script>
@endsection

