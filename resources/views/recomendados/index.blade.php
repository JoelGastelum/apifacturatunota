@extends('plantillas.privada')
@section('content')
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="form-group {{ $errors->has('viaje') ? ' has-error' : '' }}">
                <div class="input-group">
                    {{
                        Form::select(
                            'estatus',
                            $estatus_recomendados,
                            'T',
                            [
                                'class'=>'form-control ',
                                'id'=>'estatus',
                                'style'=>'width:100%;'
                            ]
                        )
                    }}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group {{ $errors->has('fecha_inicial') ? ' has-error' : '' }}">
                <label for="fecha_inicial">
                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input" id="delButton" name="delButton" checked="">
                        <label class="custom-control-label"  for="delButton">Del</label>
                    </div>
                </label>
                {{
                    Form::date(
                        'fecha_inicial',
                        date('Y-m').'-01',
                        [
                            'class'=>'form-control',
                            'id'=>'fecha_inicial',
                            'name'=>'fecha_inicial',
                            'placeholder'=>'Ingrese fecha inicial',
                        ]
                    )
                }}
            </div>
            @if ($errors->has('fecha_inicial'))
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_inicial') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-6">
            <div class="form-group {{ $errors->has('fecha_final') ? ' has-error' : '' }}" name="fecha_final">
                <label for="fecha_final">
                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input" id="AlButton" name="AlButton" checked="">
                        <label class="custom-control-label"  for="AlButton">Al</label>
                    </div>
                </label>
                {{
                    Form::date(
                        'fecha_final',
                        date('Y-m-d'),
                        [
                            'class'=>'form-control',
                            'id'=>'fecha_final',
                            'name'=>'fecha_final',
                            'placeholder'=>'Ingrese fecha final',
                        ]
                    )
                }}
            </div>
            @if ($errors->has('fecha_final'))
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_final') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div id='recomendados'>
        @foreach($recomendados as $recomendado)
            <div class="card" style="cursor:pointer;" <?php if($recomendado->estatus=='R'):?> onClick="verDatos({{$recomendado->id}},'{{$recomendado->nombre}}','{{$recomendado->direccion}}','{{$recomendado->telefono}}','{{$recomendado->personacontacto}}','{{$recomendado->hexa}}','{{$recomendado->fechaCorte}}','{{$recomendado->estado}}','{{$recomendado->comision}}')"<?php endif;?> >
                <div class="row" style="border-bottom:solid 1px #e7eaf3;padding: 0.75rem;">
                    <div class="col-6">
                        <?php if($recomendado->idrecomendado == null):?>
                            {{$recomendado->nombre}}
                        <?php else:?>
                            {{$recomendado->nombre_padre}}
                        <?php endif;?>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <strong>{{$recomendado->fecha}}</strong>
                        </div>
                    </div>
                    <div class="col-12" style="color:#929ca6;">
                        <b>{{$recomendado->comision}}</b>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <a class="btn-float-primary text-center"  href="#" data-toggle="modal" data-target="#modal-alta">
        <span class="fa fa-plus" style="margin-top:4px;line-height: 1.2;"></span>
    </a>

    <!-- Modal crear-->
    <div class="modal fade" id="modal-alta" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
                <h3 class="block-title">Recomendar establecimiento</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle"></i>
                    </button>
                </div>
            </div>
            <div class="block-content">
              {{ Form::open(['action' => 'RecomendadoController@store','id' => 'AddRecomendado','method' => 'post','role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" name="idusuario" id="idusuario" value="{{auth()->user()->id}}">
                <input type="hidden" name="plataforma" id="plataforma" value="web">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-building"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'nombre',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'nombre',
                                            'placeholder'=>'Nombre de establecimiento',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-map-marker-alt"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'direccion',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'direccion',
                                            'placeholder'=>'Dirección',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-user-alt"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'personacontacto',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'personacontacto',
                                            'placeholder'=>'Nombre de contacto',
                                            'required'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'telefono',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'telefono',
                                            'placeholder'=>'Telefono de contacto',
                                            'required'=>true,
                                            'onkeypress'=>'return AcceptNum(event)',
                                            'maxlength'=>'10'
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fas fa-save"></i> Guardar</button>
                        </div>
                    </div>
                </div>
                <br>
              {{ Form::close()}}
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal recomendar establecimiento-->
    <div class="modal fade" id="modal-datos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-content text-center">
                <img id="qr-image" src="" style="margin-bottom:15px" width="200px" height="200px" class="img-fluid" alt="QR image">
                <div class="row">
                    <div class="col-12 col-md-6 offset-md-3">
                        <div class="text-center" id="codigoHex">

                        </div>
                        <div class="text-center" id="fechaCorte">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-11 offset-1 col-sm-9 offset-sm-3 col-lg-9 offset-lg-3">
                        <div class="form-group">
                            <div class="input-group" id="divCompartir">
                                <div class="input-group-prepend" onClick="compartir('WhatsApp')" >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <i id="button" class="text-success fab fa-whatsapp" style="cursor:pointer;"></i>
                                    </span>
                                </div>
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <a id="botonCompartir" style="cursor:pointer;" href="" data-action="share/whatsapp/share">
                                            <i class="fas fa-share-alt"></i>
                                        </a>
                                    </span>
                                </div>
                                <div class="input-group-prepend" onClick="compartir('Correo')"  >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <a  style="cursor:pointer;">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                    </span>
                                </div>
                                <div class="input-group-prepend" onClick="compartir('Sms')" >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <a  style="cursor:pointer;">
                                            <i class="fas fa-sms"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if ($errors->has('numtel'))
                            <span class="help-block">
                                <strong>{{ $errors->first('numtel') }}</strong>
                            </span>
                        @endif
                        <input type="hidden" value="" id="urlQr">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                              <a  id="comprar" href=""><button class="btn btn-primary">Realizar compra</button></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="button" data-dismiss="modal" onclick="//$('#EditRecomendado').submit();" class="btn btn-sm btn-success"><i class=""></i> Cerrar</button>
                        </div>
                    </div>
                </div>
                <br>
              {{Form::close()}}
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal para compartir -->
    <div class="modal fade" id="modal-compartir" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
              <h3 class="block-title" id="tipo">Recomendar establecimiento</h3>
              <div class="block-options">
                  <button type="button" class="btn-block-option" onClick="cerrarmodalqr()" data-dismiss="modal" aria-label="Close">
                      <i class="fa fa-times-circle"></i>
                  </button>
              </div>
            </div>
            <div class="block-content text-center">
            <input type="hidden" name="tipoEnvio" id="tipoEnvio">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i id='iconCompartir' class="fas fa-user-alt"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'compartir',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'compartir',
                                            'placeholder'=>'',
                                            'required'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="button" onclick="completarEnvio()" class="btn btn-sm btn-success"> Enviar</button>
                        </div>
                    </div>
                </div>
                <br>
              {{Form::close()}}
            </div>
          </div>
        </div>
      </div>
    </div>


    <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
    <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>

    <script>
        <?php if(session('success')): ?>
            window.onload = function funLoad() {
                Swal.fire({
                    type: 'success',
                    title: '',
                    text: 'El código posee 30 días para se consumido'
                });
            }
        <?php endif;?>

        //Funciones para compartir
            /**
             * Funcion que coloca el icono y la acción del formulario para compartir
             */
            function compartir(tipo){
                document.getElementById("compartir").value="";
                $('#tipo').html('Enviar por '+tipo);
                if(tipo=='Sms'){
                    $('#compartir').prop('placeholder','Número teléfono');
                    $('#tipoEnvio').val('Sms');
                    $('#iconCompartir').prop('class','fas fa-sms');
                }
                if(tipo=='Correo'){
                    $('#compartir').prop('placeholder','Correo');
                    $('#tipoEnvio').val('Correo');
                    $('#iconCompartir').prop('class','fa fa-envelope');

                }
                if(tipo=='WhatsApp'){
                    $('#compartir').prop('placeholder','Número de teléfono');
                    $('#tipoEnvio').val('WhatsApp');
                    $('#iconCompartir').prop('class','text-success fab fa-whatsapp');
                }
                $('#modal-compartir').modal("toggle");
            }

            /**
             * Funcion que recibe el formulario de compartir y llama la funcion correspondiente
             */
            function completarEnvio(){
                let tipo=$('#tipoEnvio').val();
                if(tipo=='Sms'){
                    enviarSms();
                }
                if(tipo=='Correo'){
                    mandarACorreo();
                }
                if(tipo=='WhatsApp'){
                    sendMsg();
                }
            }

            /**
             * Permite enviar sms
             */
            function enviarSms(){
                var message = document.getElementById("urlQr").value;

                var phone = document.getElementById("compartir").value;
                if (phone.length < 9 || phone.length < 2){
                    Swal.fire({
                        type: 'warning',
                        title: '',
                        text: 'No se puede enviar a este numero'
                    });
                }else{
                    phone =  phone.replace(/ /gi, "")
                    if (phone.length == 9 ){
                       phone =`34${phone}`
                    }
                    <?php  if($isAndroid==1):?>
                        window.location.href = `sms://+52${phone}?body=${message}`;
                    <?php  else:?>
                        window.location.href = `sms://+52${phone}&body=${message}`;
                    <?php  endif; ?>
               }
            }

            /**
             * Permite mandar un correo para compartir el qr y su código
             */
            function mandarACorreo(){
                var mensaje = document.getElementById("urlQr").value;
                var correo = document.getElementById("compartir").value;
                var datos = new FormData();
                    datos.append( '_token', "{{ csrf_token() }}");
                    datos.append('mensaje',mensaje);
                    datos.append('correo',correo);

                $.ajax({
                    url: '{{url("contribuyentes/enviarqr")}}',
                    type:'POST',
                    data:datos,
                    contentType:false,
                    processData:false,
                    success:function(respuesta){
                        if(respuesta.res){
                            Swal.fire({
                                type: 'success',
                                title: '',
                                text: 'Se envio su correo'
                            });
                            $("#modal-compartir").modal('toggle');
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '',
                                text: 'Hubo un error'
                            });
                        }
                    }
                });
            }

            /**
             * Enviar mensaje por whastapp
             */
            function sendMsg(){
                var message = document.getElementById("urlQr").value;
                var phone = document.getElementById("compartir").value;
                if (phone.length < 9 || phone.length < 2){
                    Swal.fire({
                        type: 'warning',
                        title: '',
                        text: 'No se puede enviar a este numero'
                    });
                }else{
                   phone =  phone.replace(/ /gi, "")
                   if (phone.length == 9 ){
                       phone =`34${phone}`
                   }
                   window.location.href = `https://api.whatsapp.com/send?phone=+52${phone}&text=${message}`
                }
            }

        function verDatos(id,nombre,direccion,contacto,personacontacto,hexa,fechaCorte,estado,comision){
            if(estado=='R'){
                $.ajax({
                    type: "POST",
                    url: "{{ route('contribuyentes.crearqr') }}",
                    data: {
                      "_token": "{{ csrf_token() }}",
                      "iduser": "{{auth()->user()->id}}",
                      "idrecomendado": id,
                    },
                    dataType:"json",
                    async: false,
                    success: function(respuesta) {
                        if(respuesta.error==0){
                            document.getElementById("botonCompartir").href = "whatsapp://send?text="+respuesta.urlQr;
                            var src = respuesta.imagen;
                            var viajes=respuesta.viajes;
                            $('#urlQr').val(respuesta.urlQr);
                            $('#codigoHex').html("Consulta con el código: "+hexa);
                            $("#qr-image").attr('src',src);
                            $("#comprar").attr('href',respuesta.url);
                            $('#idrecomendado').val(id);
                            $('#nombreDato').val(nombre);
                            $('#direccionDato').val(direccion);
                            $('#telefonoDato').val(contacto);
                            $('#fechaCorte').html("Caduca el "+fechaCorte);
                            $('#personacontactoDato').val(personacontacto);
                            $('#modal-datos').modal('show');
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '',
                                text: 'No se consiguio QR, por favor intente de nuevo'
                            });
                        }

                    },
                    error: function() {
                    }
                });
            }else{
                if(estado=='C'){
                    Swal.fire({
                        type: 'success',
                        title: '',
                        text: nombre + " ya es usuario en mefactura.com! tu comision es de: $"+ comision
                    });

                }else if(estado=='D'){
                    Swal.fire({
                        type: 'error',
                        title: '',
                        text: nombre + " ha rechazada la invitación a mefactura.com"
                    });
                }
            }
        }

        $(document).ready(function() {
            $(".custom-control-input").on('change', function() {
                buscarRecomendado();
            });

            $('#fecha_inicial').change(function() {
                buscarRecomendado();

            });
            $('#fecha_final').change(function() {
                buscarRecomendado();

            });
            $('select').change(function() {
                buscarRecomendado();
            });
        });

        function buscarRecomendado(){
            var cards=  document.getElementsByClassName("card");
            $(".card").each(function(){
                $(this).animate({width: 'hide',},{left: '1000px'},"slow");
            },1000000);


            let estatus=$('#estatus').val();
            var datos = new FormData();
            datos.append( '_token', "{{ csrf_token() }}");
            datos.append('idusuario',<?php echo auth()->user()->id  ?>);
            datos.append('estatus',estatus);
            if(!$('#delButton').is(':checked')){
                datos.append('fechainicio','01-01-2000');
            }else{
                datos.append('fechainicio',$('#fecha_inicial').val());
            }
            if(!$('#AlButton').is(':checked')){

                datos.append('fechafin','12-12-2030');
            }else{
                datos.append('fechafin',$('#fecha_final').val());
            }
            var DivFact = document.getElementById("facturas");

            $.ajax({
                url: '{{url("clientes/misrecomendadosweb")}}',
                type:'POST',
                data:datos,
                contentType:false,
                processData:false,
                dataType:"json",
                success:function(respuesta){
                    $(".card").each(function(){
                        $(this).remove('slow');
                    },1000000);

                    $('#recomendados').html(respuesta.html);
                }
            });
        }

        function cerrarmodalqr(){
            $.ajax({
               type: "POST",
               url: "{{ route('contribuyentes.eliminarqr') }}",
               data: {
                "_token": "{{ csrf_token() }}",
                "borraRecomendados":true
               },
               dataType:"json",
               async: false,
               success: function(respuesta) {
               },
               error: function() {
               }
           });
        }
    </script>

    <style>
    	.btn-float-primary{
    		position: fixed;
    		bottom: 20px;
    		right: 20px;
    		box-shadow: 0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0);
    		transform: translate3d(0,0,0);
    		min-width: 0;
    		border-radius: 50%;
    		width: 56px;
    		height: 56px;
    		padding: 0;
    		box-sizing: border-box;
    		cursor: pointer;
    		user-select: none;
    		outline: none;
    		border: none;
    		display: inline-block;
    		white-space: nowrap;
    		text-decoration: none;
    		vertical-align: baseline;
    		font-size: 2rem;
    		font-family: Roboto;
    		font-weight: 500;
    		background-color: #3b5998;
    		color: #ffff;
    	}
        .btn-float-primary:hover {
            color: #ffff;
        }
    </style>
@endsection



