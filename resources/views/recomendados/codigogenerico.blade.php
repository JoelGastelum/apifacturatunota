@extends('plantillas.privada')
@section('content')
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="text-center">
                <img id="qr-image" src="{{ $imagen }}" width="200px" height="200px" class="img-fluid" alt="QR image">
                <br>
                <br>
                <h4>Código de promoción: {{ $codigopromocion }}</h4>
                <br>
                <div class="row">
                    <div class="col-10 offset-2 col-md-10 offset-md-2 col-lg-9 offset-lg-3">
                        <div class="text-center">
                            <div class="input-group">
                                <div class="input-group-prepend" onClick="compartir('WhatsApp')" >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <i id="button" class="text-success fab fa-whatsapp" style="cursor:pointer;"></i>
                                    </span>
                                </div>
                                <div class="input-group-prepend" >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <a id="botonCompartir" style="cursor:pointer;" href="whatsapp://send?text=Te invito a conocer mefactura . com registrarte en el siguiente link {{$ruta}}" data-action="share/whatsapp/share">
                                            <i class="fas fa-share-alt"></i>
                                        </a>
                                    </span>
                                </div>
                                <div class="input-group-prepend" onClick="compartir('Correo')"  >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <a  style="cursor:pointer;">
                                            <i class="fa fa-envelope"></i>
                                        </a>
                                    </span>
                                </div>
                                <div class="input-group-prepend" onClick="compartir('Sms')" >
                                    <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                        <a  style="cursor:pointer;">
                                            <i class="fas fa-sms"></i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-12">
                    <div class="d-none d-md-block">
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="text-center">
                        <br>
                        <strong>Tu link para redes sociales: </strong>
                        <br>
                        <span class="text-primary copy-content-clipboard" id="d1">
                            <?php echo $ruta;?>
                        </span>
                        <a href="javascript:void(0);" title="Copiar código en portapapeles" id="copyClip" data-clipboard-target="#d1"><i class="fa fa-copy"></i></a>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('recomendados.misbannergenerico') }}" class="btn btn-success col-md-12">
                        Ir a mis banner publicitario
                    </a>
                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="modal fade" id="modal-compartir" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
              <h3 class="block-title" id="tipo">Recomendar establecimiento</h3>
              <div class="block-options">
                  <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                      <i class="fa fa-times-circle"></i>
                  </button>
              </div>
            </div>
            <div class="block-content text-center">
            <input type="hidden" name="tipoEnvio" id="tipoEnvio">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i id='iconCompartir' class="fas fa-user-alt"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'compartir',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'compartir',
                                            'placeholder'=>'',
                                            'required'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="button" onclick="completarEnvio()" class="btn btn-sm btn-success"> Enviar</button>
                        </div>
                    </div>
                </div>
                <br>
              {{Form::close()}}
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <script src="{{ asset('plugins/clipboard/clipboard.js') }}"></script>

    <script>
        //Funciones para compartir
            /**
             * Funcion que coloca el icono y la acción del formulario para compartir
             */
            function compartir(tipo){
                document.getElementById("compartir").value="";
                $('#tipo').html('Enviar por '+tipo);
                if(tipo=='Sms'){
                    $('#compartir').prop('placeholder','Número teléfono');
                    $('#tipoEnvio').val('Sms');
                    $('#iconCompartir').prop('class','fas fa-sms');
                }
                if(tipo=='Correo'){
                    $('#compartir').prop('placeholder','Correo');
                    $('#tipoEnvio').val('Correo');
                    $('#iconCompartir').prop('class','fa fa-envelope');

                }
                if(tipo=='WhatsApp'){
                    $('#compartir').prop('placeholder','Número de teléfono');
                    $('#tipoEnvio').val('WhatsApp');
                    $('#iconCompartir').prop('class','text-success fab fa-whatsapp');
                }
                $('#modal-compartir').modal("toggle");
            }

            /**
             * Funcion que recibe el formulario de compartir y llama la funcion correspondiente
             */
            function completarEnvio(){
                let tipo=$('#tipoEnvio').val();
                if(tipo=='Sms'){
                    enviarSms();
                }
                if(tipo=='Correo'){
                    mandarACorreo();
                }
                if(tipo=='WhatsApp'){
                    sendMsg();
                }
            }

            /**
             * Permite enviar sms
             */
            function enviarSms(){
                var message ="Te invito a conocer mefactura. com registrarte en el siguiente link {{ $ruta }}";

                var phone = document.getElementById("compartir").value;
                if (phone.length < 9 || phone.length < 2){
                    Swal.fire({
                        type: 'warning',
                        title: '',
                        text: 'No se puede enviar a este numero'
                    });
                }else{
                    phone =  phone.replace(/ /gi, "")
                    if (phone.length == 9 ){
                       phone =`34${phone}`
                    }
                    <?php  if($isAndroid==1):?>
                        window.location.href = `sms://+52${phone}?body=${message}`;
                    <?php  else:?>
                        window.location.href = `sms://+52${phone}&body=${message}`;
                    <?php  endif; ?>
                    $("#modal-compartir").modal('toggle');
               }
            }

            /**
             * Permite mandar un correo para compartir el qr y su código
             */
            function mandarACorreo(){
                var mensaje = "Te invito a conocer mefactura. com registrarte en el siguiente link {{ $ruta }}";
                var correo = document.getElementById("compartir").value;
                var datos = new FormData();
                    datos.append( '_token', "{{ csrf_token() }}");
                    datos.append('mensaje',mensaje);
                    datos.append('correo',correo);

                $.ajax({
                    url: '{{url("contribuyentes/enviarqr")}}',
                    type:'POST',
                    data:datos,
                    contentType:false,
                    processData:false,
                    success:function(respuesta){
                        if(respuesta.res){
                            Swal.fire({
                                type: 'success',
                                title: '',
                                text: 'Se envio su correo'
                            });
                            $("#modal-compartir").modal('toggle');
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '',
                                text: 'Hubo un error'
                            });
                        }

                    }
                });
            }

            /**
             * Enviar mensaje por whastapp
             */
            function sendMsg(){
                var message = "Te invito a conocer mefactura. com registrarte en el siguiente link {{ $ruta }}";
                var phone = document.getElementById("compartir").value;
                if (phone.length < 9 || phone.length < 2){
                    Swal.fire({
                        type: 'warning',
                        title: '',
                        text: 'No se puede enviar a este numero'
                    });
                }else{
                   phone =  phone.replace(/ /gi, "")
                   if (phone.length == 9 ){
                       phone =`34${phone}`
                   }
                   window.location.href = `https://api.whatsapp.com/send?phone=+52${phone}&text=${message}`;
                   $("#modal-compartir").modal('toggle');
                }
            }
    </script>
@endsection
