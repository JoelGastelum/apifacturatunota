@extends('plantillas.privada')
@section('content')
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif

    <div id='recomendados'>
    {{ Form::open(['action' => 'RecomendadoController@savePago','id' => 'EditForm','method' => 'post','role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign', 'files'=>true]) }}
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="ids" name="ids" value={{$ids}}>
        <input type="hidden" id="iduser" name="iduser" value="{{$user->id}}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('fecha_deposito') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                        {{
                            Form::date(
                                'fecha_deposito',
                                date('Y-m').'-01',
                                [
                                    'class'=>'form-control',
                                    'id'=>'fecha_deposito',
                                    'name'=>'fecha',
                                    'placeholder'=>'Fecha',
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('fecha_deposito'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fecha_deposito') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('referencia') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-tags"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'referencia',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'referencia',
                                    'placeholder'=>'Referencia Bancaria',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('referencia'))
                    <span class="help-block">
                        <strong>{{ $errors->first('referencia') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('total') ? ' has-error' : '' }}">
                    <div class="input-group">
                        {{
                            Form::text(
                                'total',
                                $total,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'total',
                                    'placeholder'=>'Total',
                                    'required'=>true,
                                    'style'=>"text-align:right;",
                                    'onkeypress'=>"return AcceptNumPunto(event);",
                                    'onchange'=>"formatonumero('total');",
                                    'align'=>"right"
                                ]
                            )
                        }}
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-dollar-sign"></i>
                            </span>
                        </div>
                    </div>
                </div>
                @if ($errors->has('total'))
                    <span class="help-block">
                        <strong>{{ $errors->first('total') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('calveinterbancaria') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-money-check"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'calveinterbancaria',
                                $user->clabeinterbancaria,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'calveinterbancaria',
                                    'placeholder'=>'Clave Interbancaria',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('calveinterbancaria'))
                    <span class="help-block">
                        <strong>{{ $errors->first('calveinterbancaria') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group {{ $errors->has('cuenta') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-wallet"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'cuenta',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'cuenta',
                                    'placeholder'=>'Cuenta',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button type="submit" class="btn btn-success" id="btn-guardar">
                        <i class="fas fa-save"></i>
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection



