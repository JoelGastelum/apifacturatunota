@extends('plantillas.privada')
@section('content')
    <div class="row">
        <div class="col-12">
            <h4 class="text-center">
                 Puedes promocionar Mefactura colocando un banner en tu página o blog.
            </h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4">
            <a href="javascript:void(0)" class="widget widget-hover-effect3">
                <div class="text-center">
                    <img src="{{ asset('storage/img/compartidos/banner1_308x148.jpg') }}" style="padding-top: 10px;padding-bottom: 10px; padding-left: 20px;">
                </div>
                <div class="widget-simple themed-background">
                    <div class="text-center">
                        <h3 >
                            <strong>Resolución</strong><br>
                            <small>308 x 148</small><br>
                            <button class="btn btn-xs btn-default" data-toggle="tooltip" title="Copiar Código en Portapapeles" onclick="copiarAlPortapapeles('c1');"><i class="fas fa-copy"></i></button>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-8">
            <hr class="d-block d-sm-none">
            <a href="javascript:void(0)" class="widget widget-hover-effect3">
                <div class="text-center">
                    <img src="{{ asset('storage/img/compartidos/banner1_630x303.jpg') }}" style="padding-top: 10px;padding-bottom: 10px; padding-left: 20px;">
                </div>
                <div class="widget-simple themed-background-amethyst">
                    <div class="text-center">
                        <h3 >
                            <strong>Resolución</strong><br>
                            <small>630 x 303</small><br>
                            <button class="btn btn-xs btn-default" data-toggle="tooltip" title="Copiar Código en Portapapeles" onclick="copiarAlPortapapeles('c2');"><i class="fas fa-copy"></i></button>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <hr class="d-block d-sm-none">
            <a href="javascript:void(0)" class="widget widget-hover-effect3">
                <div class="text-center">
                    <img src="{{ asset('storage/img/compartidos/banner1_920x443.jpg') }}" style="padding-top: 10px;padding-bottom: 10px; padding-left: 20px;">
                </div>
                <div class="widget-simple themed-background-autumn">
                    <div class="text-center">
                        <h3 >
                            <strong>Resolución</strong><br>
                            <small>920 x 443</small><br>
                            <button class="btn btn-xs btn-default" data-toggle="tooltip" title="Copiar Código en Portapapeles" onclick="copiarAlPortapapeles('c3');"><i class="fas fa-copy"></i></button>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <hr class="d-block d-sm-none">
    <div class="row">
        <div class="col-md-12">
            <a href="javascript:void(0)" class="widget widget-hover-effect3">
                <div class="text-center">
                    <img src="{{ asset('storage/img/compartidos/banner1_1350x650.jpg') }}" class="img-fluid">
                </div>
                <div class="widget-simple themed-background-modern">
                    <div class="text-center">
                        <h3 >
                            <strong>Resolución</strong><br>
                            <small>1350 x 650</small><br>
                            <button class="btn btn-xs btn-default" data-toggle="tooltip" title="Copiar Código en Portapapeles" onclick="copiarAlPortapapeles('c4');"><i class="fas fa-copy"></i></button>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <script>
        var c1 = '<a href="<?php echo $ruta;?>"><img src="<?php echo 'https:'.$_SERVER['HTTP_HOST'].'/storage/img/compartidos/banner1_308x148.jpg';?>"></a>';
        var c2 = '<a href="<?php echo $ruta;?>"><img src="<?php echo 'https:'.$_SERVER['HTTP_HOST'].'/storage/img/compartidos/banner1_630x303.jpg';?>"></a>';
        var c3 = '<a href="<?php echo $ruta;?>"><img src="<?php echo 'https:'.$_SERVER['HTTP_HOST'].'/storage/img/compartidos/banner1_920x443.jpg';?>"></a>';
        var c4 = '<a href="<?php echo $ruta;?>"><img src="<?php echo 'https:'.$_SERVER['HTTP_HOST'].'/storage/img/compartidos/banner1_1350x650.jpg';?>"></a>';

        /**
         * Funcion que permite copiar un texto al portapapel
         */
        function copiarAlPortapapeles(element) {
            //creamos un input que nos ayudara a guardar el texto temporalmente
            var $temp = $("<input>");
            //lo agregamos a nuestro body
            $("body").append($temp);
            //agregamos en el atributo value del input el contenido html encontrado
            //en el td que se dio click
            //y seleccionamos el input temporal

            if(element=='c1'){
                var contenido = c1;
            }
            if(element=='c2'){
                var contenido = c2;
            }
            if(element=='c3'){
                var contenido = c3;
            }
            if(element=='c4'){
                var contenido = c4;
            }

            $temp.val(contenido).select();
            //ejecutamos la funcion de copiado
            document.execCommand("copy");
            //eliminamos el input temporal
            $temp.remove();
        }
    </script>
@endsection
