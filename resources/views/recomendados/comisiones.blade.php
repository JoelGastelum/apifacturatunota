@extends('plantillas.privada')
@section('content')
    <!-- BOOSTRAP DATEPICKER-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                language: 'es',
                format: "dd-mm-yyyy",
            });
        });
    </script>

    <!--SELECT 2-->
    <link rel="stylesheet" href="{{ asset('privada/js/plugins/select2/css/select2.min.css') }}">
    <script src="{{ asset('privada/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>jQuery(function(){ Dashmix.helpers('select2'); });</script>


    <!-- ANEXANDO NAVEGACION -->
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <button type="submit" onclick="document.getElementById('registroform').submit();" class="btn btn-sm btn-info">
                    <i class="fas fa-file-invoice-dollar"></i> Pagar
                </button>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            @if(session('success'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('warning'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('danger'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    </div>
                </div>
            @endif
            <form id="registroform" action="{{ route('recomendados.pagos') }}" method="POST" >
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
           
            <div id="tabladatos" class="table2 table-responsive" style="font-size: 12px;">
            </div>
            
            
            </form>
        </div>
    </div>
 

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <script>
        var ObjTabla=new myDataTable('tabladatos',{
            height:450,
            columns:[
                {data:'id',visible:false},
                {data:'nombre',title:'Usuario',filter:true,visible:true},
                {data:'nombrecomercial',title:'Empresa',filter:true,visible:true},
                {data:'rfc',title:'RFC',filter:true,visible:true},
                {data:'fechaComp',title:'Fecha Compra',filter:true,visible:true},
                {data:'comision',title:'Comision',filter:true,visible:true ,style:"text-align:right"},
                {data:'acciones',title:'Pagar',filter:false,visible:true},
            ],
            instanceVarName:'ObjTabla',
            responsive:true
        });
        
         function seleccionar(idusuario,idrecomendado){
             let usuariochecked;
             let numerochecks=0;
            $("input[type=checkbox]:checked").each(function(){
                if($(this).is(':checked')){
                    numerochecks++;
                    usuariochecked=this.value;

                }
            });
         //   let idusuarioNvo=usuariochecked.split('-');
           
            if(numerochecks==1){
                console.log("entra");
                $('input[class!=checkbox'+idusuario+']').prop('readonly',true);
            }
            if(numerochecks==0){
                $('input[type=checkbox]').prop('readonly',false);
            }   
        }
         
    
        /**
         * Funcion que permite filtrar el listado
         */
        function filtrar(){
            $.ajax({
                type: "POST",
                url: "{{ route('recomendados.loaddata2') }}",
                dataType: 'json',
                beforeSend: function() {
                    Swal.fire({
                        title: 'Cargando, por favor espere',
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    'estatus':$("#estatus").val(),
                    'fecha_inicial':$("#fecha_inicial").val(),
                    'fecha_final':$("#fecha_final").val(),
                },
                success: function(respuesta) {
                    Swal.close();
                    ObjTabla.initializeTable(respuesta.data);
                },
                error: function(jqXHR, exception) {
                    Swal.close();
                    if(jqXHR.status!=419 && jqXHR.status!=401){
                        Swal.fire({
                            type: 'error',
                            title: '¡Ha ocurrido un error!',
                            text: '¡Ha ocurrido un error, por favor intente de nuevo!'
                        });
                    }else{
                        Swal.fire({
                            type: 'warning',
                            title: '¡Su sesión expiró!',
                        });
                        setTimeout(
                            function(){
                                window.location.href='{{ route('login')}}'
                            },
                            3000
                        );
                    }
                }
            });
        }

        $(document).ready(function() {
            filtrar();
        });
    </script>

    <style>
    
    input[type='checkbox'][readonly]{
    pointer-events: none;
}
</style>
@endsection

