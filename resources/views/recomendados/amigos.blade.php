@extends('plantillas.privada')
@section('content')
    <div class="row" style="display:none;">
        <div class="col-6">
            <div class="form-group {{ $errors->has('fecha_inicial') ? ' has-error' : '' }}">
                <label for="fecha_inicial">
                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input" id="delButton" name="delButton" checked="">
                        <label class="custom-control-label"  for="delButton">Del</label>
                    </div>
                </label>
                {{
                    Form::date(
                        'fecha_inicial',
                        date('Y-m').'-01',
                        [
                            'class'=>'form-control',
                            'id'=>'fecha_inicial',
                            'name'=>'fecha_inicial',
                            'placeholder'=>'Ingrese fecha inicial',
                        ]
                    )
                }}
            </div>
            @if ($errors->has('fecha_inicial'))
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_inicial') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-6">
            <div class="form-group {{ $errors->has('fecha_final') ? ' has-error' : '' }}" name="fecha_final">
                <label for="fecha_final">
                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input" id="AlButton" name="AlButton" checked="">
                        <label class="custom-control-label"  for="AlButton">Al</label>
                    </div>
                </label>
                {{
                    Form::date(
                        'fecha_final',
                        date('Y-m-d'),
                        [
                            'class'=>'form-control',
                            'id'=>'fecha_final',
                            'name'=>'fecha_final',
                            'placeholder'=>'Ingrese fecha final',
                        ]
                    )
                }}
            </div>
            @if ($errors->has('fecha_final'))
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_final') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div id='contenido'>

    </div>
    <br>
    <br>
    <br>
    <br>
    <a class="btn-float-info text-center" href="{{ route('recomendados.mislinks') }}">
        <span class="fa fa-share" style="margin-top:4px;line-height: 1.2;"></span>
    </a>
<script>
    $(document).ready(function() {
        buscarAmigos();
    });

    $(".custom-control-input").on('change', function() {
        buscarAmigos();
    });
    $('#fecha_inicial').change(function() {
        buscarAmigos();
    });
    $('#fecha_final').change(function() {
        buscarAmigos();
    });

    function buscarAmigos(){
        var cards=  document.getElementsByClassName("card");
        $(".card").each(function(){
            $(this).animate({width: 'hide',},{left: '1000px'},"slow");
        },1000000);


        var datos = new FormData();
        datos.append( '_token', "{{ csrf_token() }}");
        datos.append('idusuario',<?php echo auth()->user()->id  ?>);
        if(!$('#delButton').is(':checked')){
            datos.append('fechainicio','01-01-2000');
        }else{
            datos.append('fechainicio',$('#fecha_inicial').val());
        }
        if(!$('#AlButton').is(':checked')){
            datos.append('fechafin','12-12-2030');
        }else{
            datos.append('fechafin',$('#fecha_final').val());
        }

        $.ajax({
            url: '{{ route("recomendados.loaddata") }}',
            type:'POST',
            data:datos,
            contentType:false,
            processData:false,
            dataType:"json",
            success:function(respuesta){
                $(".card").each(function(){
                    $(this).remove('slow');
                },1000000);

                $("#contenido").html(respuesta.html);
            }
        });
    }
</script>

<style>
    .btn-float-info{
        position: fixed;
        bottom: 20px;
        right: 20px;
        box-shadow: 0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0);
        transform: translate3d(0,0,0);
        min-width: 0;
        border-radius: 50%;
        width: 56px;
        height: 56px;
        padding: 0;
        box-sizing: border-box;
        cursor: pointer;
        user-select: none;
        outline: none;
        border: none;
        display: inline-block;
        white-space: nowrap;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 2rem;
        font-family: Roboto;
        font-weight: 500;
        background-color: #5aa8f2;
        color: #ffff;
    }
  .btn-float-info:hover {
    color: #ffff;
  }
</style>
@endsection



