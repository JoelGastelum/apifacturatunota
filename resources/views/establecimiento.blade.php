@extends('plantillas.publica')
@section('content')
  <!-- ======= Header ======= -->
    @include('elementos.public_header')
  <!-- End Header -->

  <main id="main">
    <!-- ======= Inicio Section ======= -->
    <section class="d-flex align-items-center">
      <div class=" position-relative" data-aos="fade-up" data-aos-delay="100">
        <div>
          <img src="{{ asset('publica/img/establecimientos.jpeg') }}" alt="{{ session()->get('Configuracion.Web.titulo') }}" class="img-fluid">
        </div>
      </div>
    </section>
    <!-- End Inicio -->

    <!-- ======= PARA TU ESTABLECIMIENTO de Section ======= -->
    <section id="about-video" class="about-video">
      <div class="container" data-aos="fade-up" >
        <div class="section-title">
          <h2>{{ session()->get('Configuracion.Web.titulo') }} para tu establecimiento</h2>
        </div>
        <div class="row">
          <div class="col-lg-6 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
            <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
          </div>
          <div class="col-lg-6 pt-3 pt-lg-0 content" data-aos="zoom-in" data-aos-delay="100">
            <h3>Resuelve la petición de tu cliente en 3 segundos.</h3>
            <p class="font-italic">
              {{ session()->get('Configuracion.Web.titulo') }} es la manera más fácil y rápida de otorgar facturas a tus clientes.
            </p>
            <ul>
              <li><i class="bx bx-check-double"></i> Abre la aplicacion desde tu celular.</li>
              <li><i class="bx bx-check-double"></i> Captura el folio de venta.</li>
              <li><i class="bx bx-check-double"></i> Captura el importe del consumo.</li>
              <li><i class="bx bx-check-double"></i> Listo.</li>
            </ul>
            <p style="display: none;">
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum
            </p>
          </div>
        </div>
      </div>
    </section>
    <!-- End PARA TU ESTABLECIMIENTO de Section -->

    <!-- ======= Planes Section ======= -->
    <form id="forma" name="forma" method="post" enctype="multipart/form-data">
        <input id="idserv" name="idserv" type="hidden" value="-1" />
        <input id="idpaquete" name="idpaquete" type="hidden" value="-1" />
        <section id="planes" class="pricing">
          <div class="container" data-aos="fade-up">
            <div class="section-title">
              <h2>Planes</h2>
              <p>A continuación se presentan planes para que puedas elegir a tu gusto.</p>
              <br>
              <div class="form-row">
                <div class="col-md-4 form-group offset-md-4">
                  <?php $typecodigopromocion="hidden";?>
                  <?php if($codigopromocion==null):?>
                    <hr>
                    <?php $typecodigopromocion="text";?>
                  <?php endif;?>
                  <input type="{{ $typecodigopromocion }}" autocomplete="off" name="codigopromocion" class="form-control" id="codigopromocion" placeholder="Código de promoción" onchange="verificarcodigopromocion();" value="{{ $codigopromocion }}"/>
                  <div class="validate"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <?php echo $html;?>
            </div>
            <div class="row" id="filareferido" style="display: none;">
              <div class="col-md-12">
                <br>
                <div class="text-right" id="filareferido_contenido" style="color:red;font-size: 12px;">

                </div>
              </div>
              <br>
            </div>
          </div>
        </section>
    </form>
    <!-- End Planes Section -->

    <!-- ======= Preguntas frecuentes Section ======= -->
    <section id="planseleccionado" class="pricing" style="display: none;">
      <div class="container" data-aos="fade-up">
        <hr>
        <div class="section-title">
          <h2>Paquete seleccionado</h2>
          <p>Usted ha seleccionado comprar el siguiente paquete, pero necesitamos los siguientes datos para completar la compra.</p>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-4" id="planseleccionado_container">

              </div>
              <div class="col-md-8">
                <form id="comprarplanform" action="{{ route('compraplan') }}" method="post" role="form">
                  <div class="row">
                    <div class="col-lg-12 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
                      <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" id="comprarplan_idorden" name="idorden" value="-1">
                      <input type="hidden" id="comprarplan_metodopago" name="metodopago" value="1">
                      <input type="hidden" id="comprarplan_idserv" name="idserv" value="-1">
                      <input type="hidden" id="comprarplan_idpaquete" name="idpaquete" value="-1">
                      <input type="hidden" id="comprarplan_codigo" name="codigo" value="-1">
                      <div class="form-row">
                        <div class="col-md-12 form-group">
                          <input type="text" name="razonsocial" autocomplete="off" class="form-control" id="razonsocial" placeholder="Razón social" required/>
                          <div class="validate"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-12 form-group">
                          <input onchange="verificarrfc(1);" autocomplete="off" type="text" name="rfc" class="form-control" id="rfc" placeholder="RFC" required/>
                          <div class="validate"></div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="col-md-12 form-group">
                          <input onchange="verificarcorreo();" autocomplete="off" type="text" name="email" class="form-control" id="email" placeholder="Correo electrónico" required/>
                          <div class="validate"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-12">
                          <div class="text-right">
                            <button type="button" onclick="pagar(1);" id="btn-mercadopago" style="background: #F4C400;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                                Pagar con tarjeta de crédito
                            </button>
                            <button type="button" onclick="pagar(2);" id="btn-otrometodos" style="background: #063E7B;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                                Pagar con otro métodos
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Preguntas frecuentes Section -->
  </main>

  <!-- Modal de información de pago -->
  <div class="modal" id="modal-infopago" tabindex="-1" role="dialog" aria-labelledby="modal-default-vcenter" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Muchas gracias por querer estar con nosotros</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pb-1">
                <p>
                  Mefactura.com te agradece por seleccionarnos como tu opción de facturación para el rfc <strong id="infopago-rfc"></strong>, a continuación se detalla la información de tu pedido: subscripción de <strong id="infopago-subscripcion"></strong> y un <strong id="infopago-paquete"></strong>, dando un total de <strong id="infopago-total"></strong> pesos. Para su mayor comodidad le especificamos los medios de pago:
                </p>
                <ul>
                  <li>Deposito en <strong id="infopago-banco"></strong> a la cuenta <strong id="infopago-cuentabanco"></strong></li>
                  <li>Transferencia electrónica a la clabe interbancaria <strong id="infopago-clabeinterbancaria"></strong></li>
                  <li>Pago en oxxo a la tarjeta <strong id="infopago-numerotarjeta"></strong></li>
                </ul>
                <p>A nombre de <strong id="infopago-personanombre"></strong> con R.F.C: <strong id="infopago-personarfc"></strong>
                <br>
                <p>Una vez realizada el pago por favor comunícate con nosotros vía whatsapp o llamada al teléfono <strong id="infopago-numerodecontacto"></strong> para activar su cuenta.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
  </div>

  <!-- Modal de detección de orden de pago -->
  <div class="modal" id="modal-ordenpagoanteriorpago" tabindex="-1" role="dialog" aria-labelledby="modal-default-vcenter" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ya existe orden anterior</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pb-1">
                <p>Mefactura.com ha detectado que anteriormente usted poseía una orden para el rfc <strong id="ordenpagoanteriorpago-rfc"></strong>, a continuación se detalla la información de su orden anterior: subscripción de <strong id="ordenpagoanteriorpago-subscripcion"></strong> y un <strong id="ordenpagoanteriorpago-paquete"></strong>, dando un total de <strong id="ordenpagoanteriorpago-total"></strong> pesos.
                <br>
                <div class="row">
                  <div class="col-12">
                    <div class="text-center">
                      <button onclick="seleccionarordenanterior();" class="btn btn-success">
                        <i class="far fa-check-circle"></i> Seleccionar orden existente
                      </button>
                      <button onclick="$('#modal-ordenpagoanteriorpago').modal('hide');" class="btn btn-primary">
                        <i class="far fa-times-circle"></i> Crear nueva orden
                      </button>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
  </div>


  <!-- ======= Footer ======= -->
    @include('elementos.public_footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
  <!-- FIN DE SWEETALERT2-->
  <script type="text/javascript">
    /*PLANES*/
        var enviarformulario=1;

        /**
         * Permite verificar si codigo colocado esta vigente
         */
        function verificarcodigopromocion(auto=0){
          $("#filareferido").hide();
          $("#codigopromocionaplica").val(0);
          var codigopromocion = $("#codigopromocion").val();
          if(codigopromocion!="" && codigopromocion!='undefined'){
            $.ajax({
                type: "POST",
                url: "{{ route('verificarcodigopromocion') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "codigo": codigopromocion
                },
                dataType:"json",
                success: function(respuesta) {
                  if(respuesta.respuesta==0){

                      $(".condescuento").hide();
                      $(".sindescuento").show();
                      if(auto==0){
                        Swal.fire({
                          type: 'warning',
                          title: '',
                          text: 'Código no válido'
                        });
                      }
                  }else{
                      $("#filareferido").show();
                      $("#filareferido_contenido").html(respuesta.usuario);
                      $(".sindescuento").hide();
                      $(".condescuento").show();
                  }
                },
                error: function() {
                }
            });
          }else{
            $(".condescuento").hide();
            $(".sindescuento").show();
          }
        }

        /**
         * Funcion que se ejecutara al seleccionar un plan de folios, permite mostrar el precio del paquete y subscripcion seleccionado antes de comprar
         */
        function actualizarprecio(idsubscripcion,idpaquete){
          var codigopromocion = $("#codigopromocion").val();
          $.ajax({
              type: "POST",
              url: "{{ route('buscarprecioplanseleccionado') }}",
              data: {
                  "_token": "{{ csrf_token() }}",
                  "idserv": idsubscripcion,
                  "idpaquete": idpaquete,
                  "codigopromocion": codigopromocion,
              },
              dataType:"json",
              success: function(respuesta) {
                $("#total_"+idsubscripcion).html(respuesta.total);
              },
              error: function() {
              }
          });
        }

        /**
         * Funcion que se ejecuta al darle click comprar
         */
        function buscarplanseleccionado(id){
            var idserv = id;
            var codigopromocion = $("#codigopromocion").val();

            //VERIFICAR QUE HAYA SELECCIONADO UN PAQUETE PARA ESA SUBSCRIPCION
            var seleccionadopaquete=0;
            $(".pa_"+idserv).each(function( index ) {
                // console.log($( this ));
                if($( this ).is(':checked')) {
                  idpaquete = $( this ).val();
                  seleccionadopaquete=1;
                }
            });

            if(seleccionadopaquete==1){
              $.ajax({
                  type: "POST",
                  url: "{{ route('buscarplanseleccionado') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "idserv": idserv,
                      "idpaquete": idpaquete,
                      "codigopromocion": codigopromocion,
                  },
                  dataType:"json",
                  success: function(respuesta) {
                    $("#comprarplan_idserv").val(idserv);
                    $("#comprarplan_idpaquete").val(idpaquete);
                    $("#comprarplan_codigo").val(codigopromocion);

                    $("#planseleccionado").show();
                    $("#planseleccionado_container").html(respuesta.html);

                    $('html,body').animate({
                      scrollTop: $('#planseleccionado_container').offset().top
                    }, 2000);
                  },
                  error: function() {
                  }
              });
            }else{
              Swal.fire({
                type: 'warning',
                title: '',
                text: 'Debe de seleccionar un paquete para la subscripción seleccionada'
              });
            }
        }

        /**
         * Funcion que permite verificar si correo ya existe
         */
        function verificarcorreo(){
            var error=0;
            var email = $("#email").val();
            if(email=== "undefined" || email=== ""){
                error=1;
            }

            if(error==0){
              $('#btn-mercadopago').prop("disabled",true);
              $('#btn-mercadopago').css("background-color","#E4AE3096");
              $('#btn-otrometodos').prop("disabled",true);
              $('#btn-otrometodos').css("background-color","rgba(6, 62, 123, 0.49)");
              $.ajax({
                  type: "POST",
                  url: "{{ route('verificarusuarioocorreo') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "tipo": 'email',
                      "idtipousuario": '2',
                      "email": email,
                  },
                  dataType:"json",
                  async: false,
                  success: function(respuesta) {
                    if(respuesta.error==1){
                      Swal.fire({
                        type: 'warning',
                        title: respuesta.title,
                        text: respuesta.msj
                      });
                      $("#email").addClass("campoerror");
                      $("#email").removeClass("camposuccess");
                      $("#email").focus();
                      enviarformulario = 0;
                    }else{
                      $("#email").addClass("camposuccess");
                      $("#email").removeClass("campoerror");
                      $('#btn-mercadopago').prop("disabled",false);
                      $('#btn-mercadopago').css("background-color","#E4AE30");
                      $('#btn-otrometodos').prop("disabled",false);
                      $('#btn-otrometodos').css("background-color","rgb(6, 62, 123)");
                      enviarformulario = 1;
                    }
                  },
                  error: function() {
                  }
              });
            }
        }

        /**
         * Funcion que permite verificar si rfc ya existe
         */
        function verificarrfc(buscarordenesanteriores=0){
            var error=0;
            var rfc = $("#rfc").val();
            if(rfc=== "undefined" || rfc=== ""){
                error=1;
            }

            if(error==0){
              $('#btn-mercadopago').prop("disabled",true);
              $('#btn-mercadopago').css("background-color","#E4AE3096");
              $('#btn-otrometodos').prop("disabled",true);
              $('#btn-otrometodos').css("background-color","rgba(6, 62, 123, 0.49)");
              $.ajax({
                  type: "POST",
                  url: "{{ route('verificarusuarioocorreo') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "tipo": 'rfc',
                      "rfc": rfc,
                  },
                  dataType:"json",
                  async: false,
                  success: function(respuesta) {
                    if(respuesta.error==1){
                      Swal.fire({
                        type: 'warning',
                        title: respuesta.title,
                        text: respuesta.msj
                      });
                      $("#rfc").addClass("campoerror");
                      $("#rfc").removeClass("camposuccess");
                      $("#rfc").focus();
                      enviarformulario = 0;
                    }else{
                      if(buscarordenesanteriores==1){
                        //BUSCAR SI EL RFC EXISTE EN ORDENES EN ESPERA
                        $.ajax({
                            type: "POST",
                            url: "{{ route('revisarrfcenorden') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "rfc": rfc,
                            },
                            dataType:"json",
                            async: false,
                            success: function(respuesta) {
                              if(respuesta.conseguido==1){
                                //ABRIR MODAL MOSTRANDO QUE SI PUEDE SELECCIONAR PLAN
                                $('#ordenpagoanteriorpago-rfc').html(respuesta.rfc);
                                $('#ordenpagoanteriorpago-subscripcion').html(respuesta.subscripcion);
                                $('#ordenpagoanteriorpago-paquete').html(respuesta.paquete);
                                $('#ordenpagoanteriorpago-total').html(respuesta.total);

                                $("#comprarplan_idorden").val(respuesta.idorden);

                                $('#modal-ordenpagoanteriorpago').modal();
                              }

                            },
                            error: function() {
                            }
                        });
                      }


                      $("#rfc").addClass("camposuccess");
                      $("#rfc").removeClass("campoerror");
                      $('#btn-mercadopago').prop("disabled",false);
                      $('#btn-mercadopago').css("background-color","#E4AE30");
                      $('#btn-otrometodos').prop("disabled",false);
                      $('#btn-otrometodos').css("background-color","rgb(6, 62, 123)");
                      enviarformulario = 1;
                    }
                  },
                  error: function() {
                  }
              });
            }
        }
          /**
           * Funcion que permite al usuario escoger la orden anterior que no termino de pagar
           * Actualizar la pagina con los datos de la orden
           */
           function seleccionarordenanterior(){
              var idorden = $("#comprarplan_idorden").val();
              $.ajax({
                  type: "POST",
                  url: "{{ route('seleccionarordenanterior') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "idorden": idorden,
                  },
                  dataType:"json",
                  async: false,
                  beforeSend: function() {
                      Swal.fire({
                          title: 'Espere por favor',
                          allowOutsideClick: false
                      });
                      Swal.showLoading();
                  },
                  success: function(respuesta) {
                    if(respuesta.error==0){
                      $("#planseleccionado_container").html(respuesta.html);
                      $("#comprarplan_idserv").val(respuesta.idserv);
                      $("#comprarplan_idpaquete").val(respuesta.idpaquete);
                      Swal.close();
                      $('#modal-ordenpagoanteriorpago').modal('hide');
                    }else{
                      Swal.close();
                    }
                  },
                  error: function() {
                      Swal.close();
                  }
              });
           }

        /**
         * Función que permite hacer submit pero coloca el metodo de pago que se va a usar
         */
        function pagar(metodopago){
          var error=0;

          var email = $("#email").val();
          if(email=== "undefined" || email=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '',
                text: 'Debe de anexar un correo electrónico'
              });
          }
          var rfc = $("#rfc").val();
          if(rfc=== "undefined" || rfc=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '',
                text: 'Debe de anexar un rfc'
              });
          }
          var razonsocial = $("#razonsocial").val();
          if(razonsocial=== "undefined" || razonsocial=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '',
                text: 'Debe de anexar una razón social'
              });
          }

          if(error==0){
            $("#comprarplan_metodopago").val(metodopago);
            compraplan();
          }
        }

        /**
         * Permite enviar la información para que se cree la orden de pago
         */
        function compraplan(){
          verificarcorreo();
          if(enviarformulario == 1){
            verificarrfc();
            if(enviarformulario == 1){
              var idorden = $('#comprarplan_idorden').val();
              var metodopago = $('#comprarplan_metodopago').val();
              var idserv = $('#comprarplan_idserv').val();
              var idpaquete = $('#comprarplan_idpaquete').val();
              var codigo = $('#comprarplan_codigo').val();
              var razonsocial = $('#razonsocial').val();
              var rfc = $('#rfc').val();
              var email = $('#email').val();

              //BLOQUEANDO BOTON
              $('#btn-mercadopago').prop("disabled",true);
              $('#btn-mercadopago').css("background-color","#E4AE3096");
              $('#btn-otrometodos').prop("disabled",true);
              $('#btn-otrometodos').css("background-color","rgba(6, 62, 123, 0.49)");

              $.ajax({
                  type: "POST",
                  url: "{{ route('compraplan') }}",
                  beforeSend: function() {
                      Swal.fire({
                          title: 'Espere por favor',
                          allowOutsideClick: false
                      });
                      Swal.showLoading();
                  },
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "idorden": idorden,
                      "metodopago": metodopago,
                      "idserv": idserv,
                      "idpaquete": idpaquete,
                      "codigo": codigo,
                      "razonsocial": razonsocial,
                      "rfc": rfc,
                      "email": email
                  },
                  dataType:"json",
                  async: true,
                  success: function(respuesta) {
                    Swal.close();
                    //DESBLOQUEANDO BOTON
                    $('#btn-mercadopago').prop("disabled",false);
                    $('#btn-mercadopago').css("background-color","#E4AE30");
                    $('#btn-otrometodos').prop("disabled",false);
                    $('#btn-otrometodos').css("background-color","rgb(6, 62, 123)");
                    if(respuesta.error==0){
                      $('#comprarplan_idorden').val(respuesta.idorden);
                      if(respuesta.metodopago==1){
                        //MERCADO PAGO (REDIRECIONAR)
                        window.location=respuesta.url;
                      }else{
                        if(respuesta.metodopago==2){
                          //OTRO MEDIOS DE PAGO (MOSTRAR MODAL)
                          $('#infopago-cuentabanco').html(respuesta.cuentabanco);
                          $('#infopago-banco').html(respuesta.banco);
                          $('#infopago-numerodecontacto').html(respuesta.numerodecontacto);
                          $('#infopago-clabeinterbancaria').html(respuesta.clabeinterbancaria);
                          $('#infopago-numerotarjeta').html(respuesta.numerotarjeta);
                          $('#infopago-personarfc').html(respuesta.personarfc);
                          $('#infopago-personanombre').html(respuesta.personanombre);

                          $('#infopago-rfc').html(respuesta.rfc);
                          $('#infopago-subscripcion').html(respuesta.subscripcion);
                          $('#infopago-paquete').html(respuesta.paquete);
                          $('#infopago-total').html(respuesta.total);

                          $("#modal-infopago").modal();
                        }
                      }
                    }else{
                      Swal.fire({
                        type: 'warning',
                        title: '',
                        text: respuesta.msj
                      });
                    }
                  },
                  error: function() {
                    Swal.close();
                  }
              });
            }
          }
        }

        $(document).ready(function() {
            <?php if($codigopromocion!=null):?>
                verificarcodigopromocion(1);
            <?php endif;?>
            //CASOS PARA MOSTRAR LOS ERRORES DE MERCADO PAGO
            <?php if(isset($comprarplan_idorden)):?>
              <?php if($comprarplan_idorden!=null):?>
                $("#comprarplan_idorden").val({{ $comprarplan_idorden }});
                seleccionarordenanterior();
                $("#planseleccionado").show();
              <?php endif;?>
            <?php endif;?>
            <?php if(isset($msjerror)):?>
              <?php if($msjerror!=null):?>
                Swal.fire({
                  type: 'warning',
                  title: '',
                  text: "{{ $msjerror }}"
                });
              <?php endif;?>
            <?php endif;?>
            <?php if(isset($razonsocial)):?>
              <?php if($razonsocial!=null):?>
                $("#razonsocial").val('{{ $razonsocial }}');
              <?php endif;?>
            <?php endif;?>
            <?php if(isset($rfc)):?>
              <?php if($rfc!=null):?>
                $("#rfc").val('{{ $rfc }}');
              <?php endif;?>
            <?php endif;?>
            <?php if(isset($email)):?>
              <?php if($email!=null):?>
                $("#email").val('{{ $email }}');
              <?php endif;?>
            <?php endif;?>
        });
  </script>
@endsection
