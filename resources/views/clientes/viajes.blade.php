@extends('plantillas.privada')
@section('content')
    <style>
        .movleft{
            transform: translateX(-100px);
            position:relative;
            background-color:#e6ebf4;
        }
        .movleft::after{
            position:absolute;
            z-index: -1;
            content: '\f303';
            font-family: 'Font Awesome 5 Free';
            top:0;
            height: 100%;
            /*width: 50%;*/
            right: -55px;
            font-weight: bold;
            background-color:#0665d0;
            padding-top: 25px;
            padding-left: 25px;
            padding-right: 25px;
            cursor:pointer;
            color:white;
        }

        .movright{
            transform: translateX(0px);
        }
    </style>

    <!--HAMMER JS-->
    <script src="https://hammerjs.github.io/dist/hammer.js"></script>
    <script src="{{ asset('plugins/jquery-hammer/jquery.hammer.js') }}"></script>
    <script src="{{ asset('plugins/jquery-hammer/PreventGhostClick.js') }}"></script>

    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div style="float:right;margin-bottom:20px">
                <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input" id="abiertos" onClick="filtrar('abiertos')" name="abiertos" checked="">
                    <label class="custom-control-label"  for="abiertos">Abiertos</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                    <input type="checkbox" class="custom-control-input" id="cerrados" onClick="filtrar('cerrados')" name="cerrados" checked="">
                    <label class="custom-control-label"  for="cerrados">Cerrados</label>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12" id='tablaViajes'>
        <?php
            echo $listaviajes;
        ?>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <a class="btn-float-primary text-center" onclick="editarviaje(0);" href="#">
        <span class="fa fa-plus" style="margin-top:4px;line-height: 1.2;"></span>
    </a>
    <div class="modal" id="modal-editarviaje" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <div class="block-options">
                            <button type="button" class="btn-block-option" style="padding-left: 0;padding-right: 1.25rem;" onclick="cerramodaleditandoviaje();">
                                <i class="fas fa-chevron-left"></i>
                            </button>
                        </div>
                        <h3 class="block-title" id="modal-editarviaje-titulo"></h3>
                    </div>
                    <div class="block-content">
                        {{ Form::hidden('me_idviaje',null,['id'=>'me_idviaje'])}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-signature"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::text(
                                                'me_descripcion',
                                                null,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'me_descripcion',
                                                    'placeholder'=>'Nombre',
                                                    'required'=>true
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'me_presupuesto',
                                                0.00,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'me_presupuesto',
                                                    'style'=>"text-align:right;",
                                                    'align'=>"right",
                                                    'onkeypress'=>"return AcceptNumPunto(event);",
                                                    'onchange'=>"formatonumero('me_presupuesto');"
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-dollar-sign"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" id="btn-cerrarviaje"class="btn col-12 btn-sm btn-dark" onclick="cerrarviaje();"><i class="fas fa-lock"></i> Cerrar viaje</button>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-success" onclick="guardarviaje();"><i class="fas fa-save"></i> Guardar</button>
                        <button type="button" id="btn-eliminarviaje"class="btn btn-sm btn-danger" onclick="eliminarviaje();"><i class="far fa-calendar-minus"></i> Eliminar</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SWEETALERT2-->
<link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
<script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<!-- FIN DE SWEETALERT2-->
<!-- JQUERY NUMBER-->
<script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>


<script>
/*FUNCIONES PARA HAMMER.JS*/
// $("#row_todos").hammer().bind("panleft", $('#edit_todos').show());
$(document).ready(function(){
    var nohacerclick=0;
    $(".row_viajes").each(function(){
        var self=this;
        $(this).hammer().bind("swipeleft", function(ev) {
            $(".row_viajes").removeClass("movleft");
            // console.log(self);
            $(self).addClass("movleft");
            // $(self).attr('data-movido','1');
        });
        $(this).hammer().bind("swiperight", function(ev) {
            $(self).removeClass("movleft");
            nohacerclick=1;
        });
    });

    $(".row_viajes").click(function(e){
        // console.log('ex '+e.clientX);
        // console.log('left '+$(this).offset().left);
        // console.log('width '+$(this).width());
        if(nohacerclick==1){
            nohacerclick=0;
        }else{
            if(
                $(this).hasClass("movleft")
            ){
                if(
                    e.clientX > ($(this).width()+$(this).offset().left)
                ){
                    // console.log('botoncito rojo');
                    var data_idviaje = $(this).attr('data-idviaje');
                    editarviaje(data_idviaje);
                }else{
                    // $(this).removeClass("movleft");
                    // var data_movido = $(this).attr('data-movido');
                    // if(data_movido==1){
                        // $(this).removeClass("movleft");
                        // $(this).attr('data-movido','0');
                    // }
                    // console.log('click al row ');
                    // $('#row_todos').removeClass("movleft");
                }
            }else{
                // console.log(this);
                var data_location = $(this).attr('data-location');
                window.location=data_location;
                // console.log('click normal');
            }

        }
    });
});




/*FUNCIONES PARA HAMMER.JS*/
// var myElements = document.getElementsByClassName('row_viajes');
// var myElements = document.getElementById('tablaViajes');
// // console.log(myElements);
// // var mc = [];
// mc = new Hammer(myElements,{
//             drag_max_touches: 0,
//             prevent_default: true,
//             scale_treshold: 0,
//         }
// );
// mc.on("panleft", function(ev) {
//     // var touches = ev.gesture.touches;
//     console.log(ev.target);
//     console.log(ev.scale);
//     // console.log($(this));
//     // console.log(i);
//     // console.log(myElements);
//     // elementico.className += " movleft";
//     // $(ev.target).addClass("movleft");
// });

// var i_mc = 0;
//for(var i=0;i<myElements.length;i++){
    // console.log(myElements[i]);
    // var elementico = myElements[i];
    // mc[i] = new Hammer(elementico);
    // mc[i].on("panleft", function(ev) {
    //     console.log(ev.target);
    //     // console.log($(this));
    //     // console.log(i);
    //     // console.log(myElements);
    //     // elementico.className += " movleft";
    //     $(ev.target).addClass("movleft");
    // });
    // i_mc=i_mc+1;
//}

// // create a simple instance
// // by default, it only adds horizontal recognizers
// var mc = new Hammer(myElement);
// mc.on("panleft", function(ev) {
//     $('#row_todos').addClass("movleft");
//     // $('#row_todos').addClass("movright");
//     // $('#edit_todos').show();
//     // alert("HOLAAA");
//     // $('#edit_todos').css("left",'-80px');
// });

// $("#row_todos").click(function(e){
//     // console.log('ex '+e.clientX);
//     // console.log('left '+$(this).offset().left);
//     // console.log('width '+$(this).width());
//     if(
//         $("#row_todos").hasClass("movleft")
//     ){
//         if(
//             e.clientX > ($(this).width()+$(this).offset().left)
//         ){
//             console.log('botoncito rojo');
//         }else{
//             console.log('click al row');
//             // $('#row_todos').removeClass("movleft");
//         }
//     }else{
//         console.log('click normal');
//     }


//     // if (
//     //     e.clientX > $(this).offset().left + 90 &&
//     //     e.clientY < $(this).offset().top + 10
//     // ) {
//     //     alert('click');
//     // }
// });

// listen to events...
// mc.on("swipeleft", function(ev) {
//     $('#row_todos').addClass("mov");
//     $('#row_todos').addClass("mov");
//     // $('#row_todos').html("HOLa");
//     // $('#edit_todos').show();
//     // alert("HOLAAA");
//     // $('#edit_todos').css("left",'-80px');
// });






function formatonumero(inputid){
        var value = $('#'+inputid).val();
        value = $.number(value, 2, '.', ',');
        $('#'+inputid).val(value);
    }

function filtrar(tipo){


    $('.'+tipo).toggle('slow');
}

function editarviaje(idviaje){
                $("#btn-cerrarviaje").hide();
                $("#btn-eliminarviaje").hide();
                $.ajax({
                    type: "POST",
                    url: "{{ route('contribuyentes.editarviaje') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "idviaje": idviaje,
                        "idcontribuyente": {{ $idcontribuyente }},
                    },
                    dataType:"json",
                    success: function(respuesta) {
                        $('#modal-viajes').modal('toggle');

                        //EDITAR VIAJE
                        if(respuesta.nuevo==1){
                            $("#modal-editarviaje-titulo").html('Crear nuevo viaje');
                        }else{
                            $("#modal-editarviaje-titulo").html('Editar viaje');
                        }
                        $('#me_idviaje').val(respuesta.id);
                        $('#me_descripcion').val(respuesta.descripcion);
                        $('#me_presupuesto').val(respuesta.presupuesto);

                        if(idviaje>0 && respuesta.estatus==1){
                            //ESTA ABIERTO Y ES VIEJO
                            $("#btn-cerrarviaje").show();
                        }
                        if(idviaje>0 && respuesta.canti==0){
                            //SE PUEDE ELIMINAR Y ES VIEJO
                            $("#btn-eliminarviaje").show();
                        }
                        $('#modal-editarviaje').modal({backdrop: 'static', keyboard: false});
                    },
                    error: function() {
                    }
                });
            }


            function guardarviaje(){
                var error=0;
                var idviaje= $('#me_idviaje').val();
                var presupuesto= $('#me_presupuesto').val();
                if(presupuesto=== "undefined" || presupuesto=== ""){
                    error=1;
                    Swal.fire({
                        type: 'warning',
                        title: '¡Importe!',
                        text: '¡Debe de anexar un valor para el importe!'
                    });
                }
                var descripcion= $('#me_descripcion').val();
                if(descripcion=== "undefined" || descripcion=== ""){
                    error=1;
                    Swal.fire({
                        type: 'warning',
                        title: '¡Importe!',
                        text: '¡Debe de anexar un valor para la descripcion!'
                    });
                }

                if(error==0){
                    $.ajax({
                        type: "POST",
                        url: "{{ route('contribuyentes.seditarviaje') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "idviaje": idviaje,
                            "presupuesto": presupuesto,
                            "descripcion": descripcion,
                            "idcontribuyente": {{ $idcontribuyente }},
                            "rfcDefault" : '{{$rfcDefault}}'
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            console.log(respuesta);
                            if(respuesta.error==1){
                                Swal.fire({
                                    type: 'error',
                                    title: '¡No se pudo guardar!',
                                    text: '¡Ha ocurrido un error no se pudo guardar el viaje!'
                                });
                            }else{
                                $('#tablaViajes').append(respuesta.html);
                                cerramodaleditandoviaje();
                            }
                        },
                        error: function() {
                        }
                    });
                }
            }

            function cerrarviaje(){
                var idviaje= $('#me_idviaje').val();
                Swal.fire({
                    title: '¿Estas seguro que desea cerrar el viaje?',
                    text: "Usted está por cerrar el viaje",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'No',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Sí, estoy seguro!"
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('contribuyentes.cerrarviaje') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "idviaje": idviaje
                            },
                            dataType:"json",
                            success: function(respuesta) {


                                $('#tr'+idviaje).prop('class','cerrados');
                                if(!$('#cerrados').is(':checked')){
                                    $('#tr'+idviaje).toggle('slow');
                                }

                                $('#viajeCerrado'+idviaje).prop('class','fa fa-lock');
                                cerramodaleditandoviaje();
                            },
                            error: function() {
                            }
                        });
                    }
                });
            }


            function cerramodaleditandoviaje(){
                $('#modal-editarviaje').modal('toggle');

            }


</script>


    <style>
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        border-radius: 5px; /* 5px rounded corners */
    }

    /* Add rounded corners to the top left and the top right corner of the image */
    img {
        border-radius: 5px 5px 0 0;
    }

    .btn-float-primary{
        position: fixed;
        bottom: 20px;
        right: 20px;
        box-shadow: 0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0);
        transform: translate3d(0,0,0);
        min-width: 0;
        border-radius: 50%;
        width: 56px;
        height: 56px;
        padding: 0;
        box-sizing: border-box;
        cursor: pointer;
        user-select: none;
        outline: none;
        border: none;
        display: inline-block;
        white-space: nowrap;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 2rem;
        font-family: Roboto;
        font-weight: 500;
        background-color: #3b5998;
        color: #ffff;
    }

    .btn-float-primary:hover {
      color: #ffff;
    }
</style>
    @endsection
