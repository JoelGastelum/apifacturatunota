@extends('plantillas.privada')
@section('content')
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row" >
        <div class="col-12">
            <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full d-flex align-items-center justify-content-between">
                    <div class="item item-circle bg-body-light">
                        <i class="fas fa-plane text-primary"></i>
                    </div>
                    <p class="font-size-lg font-w600 mb-0" id="tituloviajes">
                        {{$nombreViaje}}
                    </p>
                    <div class="ml-3 text-center">
                        <p class="font-size-lg font-w600 mb-0" onclick="compartir()">
                            <i class="fas fa-envelope text-primary"></i>
                            <br>
                            Compartir
                        </p>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-6">
            <div class="form-group {{ $errors->has('fecha_inicial') ? ' has-error' : '' }}">
                <label for="fecha_inicial">
                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input" id="delButton" name="delButton" checked="">
                        <label class="custom-control-label"  for="delButton">Del</label>
                    </div>
                </label>
                {{
                    Form::date(
                        'fecha_inicial',
                        date('Y-m').'-01',
                        [
                            'class'=>'form-control',
                            'id'=>'fecha_inicial',
                            'name'=>'fecha_inicial',
                            'placeholder'=>'Ingrese fecha inicial',
                        ]
                    )
                }}
            </div>
            @if ($errors->has('fecha_inicial'))
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_inicial') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-6">
            <div class="form-group {{ $errors->has('fecha_final') ? ' has-error' : '' }}" name="fecha_final">
                <label for="fecha_final">
                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                        <input type="checkbox" class="custom-control-input" id="AlButton" name="AlButton" checked="">
                        <label class="custom-control-label"  for="AlButton">Al</label>
                    </div>
                </label>
                {{
                    Form::date(
                        'fecha_final',
                        date('Y-m-d'),
                        [
                            'class'=>'form-control',
                            'id'=>'fecha_final',
                            'name'=>'fecha_final',
                            'placeholder'=>'Ingrese fecha final',
                        ]
                    )
                }}
            </div>
            @if ($errors->has('fecha_final'))
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_final') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div id='facturas'>
    </div>
    <br>
    <br>
    <br>
    <?php if($cerrado==0):?>
        <a class="btn-float-primary text-center" href="{{ route('clientes.facturaexterna',array($rfcDefault,$idviaje)) }}">
            <span class="fa fa-plus" style="margin-top:4px;line-height: 1.2;"></span>
        </a>
    <?php endif;?>

    <!-- Modal -->
    <div class="modal" id="modal-viajes" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Viajes</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times-circle"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <table class="table table-vcenter">
                            <thead>
                                <tr>
                                    <th style="width: 85%;">Viaje</th>
                                    <th style="width: 15%;text-align:center">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-hero-light dropdown-toggle"  style="padding-top: 1px; padding-left: 10px; padding-right: 5px; padding-bottom: 1px;" id="dropdown-default-hero-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdown-default-hero-dark" style="font-size: 14px;">
                                                <a class="dropdown-item" href="javascript:void(0)" onclick="buscarviajes(0);"><span id="filtadoviaje_0"><?php if($configfiltroviajes==0):?><i class="fas fa-check"></i><?php endif;?></span> Todos</a>
                                                <a class="dropdown-item" href="javascript:void(0)" onclick="buscarviajes(1);"><span id="filtadoviaje_1"><?php if($configfiltroviajes==1):?><i class="fas fa-check"></i><?php endif;?></span> Solo abiertos <i class="fas fa-lock-open" style="color:#ca2;cursor:pointer;"></i></a>
                                                <a class="dropdown-item" href="javascript:void(0)" onclick="buscarviajes(2);"><span id="filtadoviaje_2"><?php if($configfiltroviajes==2):?><i class="fas fa-check"></i><?php endif;?></span> Solo cerrados <i class="fas fa-lock" style="color:#5471ca;cursor:pointer;"></i></a>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="viajes_tbody">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modal-editarviaje" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <div class="block-options">
                            <button type="button" class="btn-block-option" style="padding-left: 0;padding-right: 1.25rem;" onclick="cerramodaleditandoviaje();">
                                <i class="fas fa-chevron-left"></i>
                            </button>
                        </div>
                        <h3 class="block-title" id="modal-editarviaje-titulo"></h3>
                    </div>
                    <div class="block-content">
                        {{ Form::hidden('me_idviaje',null,['id'=>'me_idviaje'])}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-signature"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::text(
                                                'me_descripcion',
                                                null,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'me_descripcion',
                                                    'placeholder'=>'Nombre',
                                                    'required'=>true
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'me_presupuesto',
                                                0.00,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'me_presupuesto',
                                                    'style'=>"text-align:right;",
                                                    'align'=>"right",
                                                    'onkeypress'=>"return AcceptNumPunto(event);",
                                                    'onchange'=>"formatonumero('me_presupuesto');"
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-dollar-sign"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" id="btn-cerrarviaje"class="btn col-12 btn-sm btn-dark" onclick="cerrarviaje();"><i class="fas fa-lock"></i> Cerrar viaje</button>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-success" onclick="guardarviaje();"><i class="fas fa-save"></i> Guardar</button>
                        <button type="button" id="btn-eliminarviaje"class="btn btn-sm btn-danger" onclick="eliminarviaje();"><i class="far fa-calendar-minus"></i> Eliminar</button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="ids" >
    <div class="modal fade" id="modal-compartir" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
              <h3 class="block-title" id="tipo">Recomendar establecimiento</h3>
              <div class="block-options">
                  <button type="button" class="btn-block-option" onClick="cerrarmodalqr()" data-dismiss="modal" aria-label="Close">
                      <i class="fa fa-times-circle"></i>
                  </button>
              </div>
            </div>
            <div class="block-content text-center">
            <input type="hidden" name="tipoEnvio" id="tipoEnvio">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i id='iconCompartir' class="fas fa-user-alt"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'compartir',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'compartir',
                                            'placeholder'=>'Ingrese correo',
                                            'required'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="button" onclick="completarEnvio()" class="btn btn-sm btn-success"> Enviar</button>
                        </div>
                    </div>
                </div>
                <br>
              {{Form::close()}}
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END Modal -->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
<script>
    //FUNCIONALIDAD DEL MODAL DE VIAJES
        /**
         * Funcion que permite cargar un modal con los viajes y buscara los viajes y la funcionalidad
         * @return {[type]} [description]
         */
        function abrirmodalviajes(){
            var configfiltroviajes = $("#idconfigfiltroviajes").val();
            buscarviajes(configfiltroviajes);
            $('#modal-viajes').modal({backdrop: 'static', keyboard: false});
        }



        function compartir(){
            $('#modal-compartir').modal("toggle");
        }


    function completarEnvio(){


        var datos = new FormData();
            datos.append( '_token', "{{ csrf_token() }}");
            datos.append('idusuario',"{{auth()->user()->id}}");
            datos.append('correo',$('#compartir').val());
            datos.append('idfacturas',$('#ids').val());
        $.ajax({
                url: '{{url("api/clientes/enviarfacturas")}}',
                type:'POST',
                data:datos,
                contentType:false,
                processData:false,
                success:function(respuesta){
                    if(respuesta.res){
                        Swal.fire({
                        type: 'Success',
                        title: '',
                        text: 'Se envio su correo'
                    });
                    }else{
                        Swal.fire({
                        type: 'warning',
                        title: '',
                        text: 'Hubo un error'
                    });
                    }

                }
            });
    }
        /**
         * Funcion que permite buscar los viajes del usuario
         */
        function buscarviajes(configfiltroviajes){
            //COLOCAR EL CHECK EN EL FILTRADO SELECCIONADO O EL POR DEFECTO
                if(configfiltroviajes==0){
                    //COLOCAR MARCADOS TODOS
                    $("#filtadoviaje_0").html('<i class="fas fa-check"></i>');
                    $("#filtadoviaje_1").html('');
                    $("#filtadoviaje_2").html('');
                }
                if(configfiltroviajes==1){
                    //COLOCAR MARCADOS ABIERTOS
                    $("#filtadoviaje_0").html('');
                    $("#filtadoviaje_1").html('<i class="fas fa-check"></i>');
                    $("#filtadoviaje_2").html('');
                }
                if(configfiltroviajes==2){
                    //COLOCAR MARCADOS CERRADO
                    $("#filtadoviaje_0").html('');
                    $("#filtadoviaje_1").html('');
                    $("#filtadoviaje_2").html('<i class="fas fa-check"></i>');
                }
                $("#idconfigfiltroviajes").val(configfiltroviajes);

            var idviajes = $("#idviaje").val();//VIAJES QUE ESTAN SELECCIONADO ANTES DE BUSCAR
            $.ajax({
                type: "POST",
                url: "{{ route('contribuyentes.buscarviajes') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "iduser": {{ auth()->user()->id }},
                    "idviajes": idviajes,
                    "configfiltroviajes": configfiltroviajes,
                    "idcontribuyente": {{ $idcontribuyente }},
                },
                dataType:"json",
                success: function(respuesta) {
                    $("#viajes_tbody").html(respuesta.html);
                },
                error: function() {
                }
            });
        }

        //MODAL DE EDITANDO VIAJE
            /**
             * Funcion que permite cambiar o  descripcion de un viaje
             */
            function editarviaje(idviaje){
                $("#btn-cerrarviaje").hide();
                $("#btn-eliminarviaje").hide();
                $.ajax({
                    type: "POST",
                    url: "{{ route('contribuyentes.editarviaje') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "idviaje": idviaje,
                        "idcontribuyente": {{ $idcontribuyente }},
                    },
                    dataType:"json",
                    success: function(respuesta) {
                        $('#modal-viajes').modal('toggle');

                        //EDITAR VIAJE
                        if(respuesta.nuevo==1){
                            $("#modal-editarviaje-titulo").html('Crear nuevo viaje');
                        }else{
                            $("#modal-editarviaje-titulo").html('Editar viaje');
                        }
                        $('#me_idviaje').val(respuesta.id);
                        $('#me_descripcion').val(respuesta.descripcion);
                        $('#me_presupuesto').val(respuesta.presupuesto);

                        if(idviaje>0 && respuesta.estatus==1){
                            //ESTA ABIERTO Y ES VIEJO
                            $("#btn-cerrarviaje").show();
                        }
                        if(idviaje>0 && respuesta.canti==0){
                            //SE PUEDE ELIMINAR Y ES VIEJO
                            $("#btn-eliminarviaje").show();
                        }
                        $('#modal-editarviaje').modal({backdrop: 'static', keyboard: false});
                    },
                    error: function() {
                    }
                });
            }

            /**
             * Funcion que permite ejecutar ajax para guardar y editar un viaje
             */
            function guardarviaje(){
                var error=0;
                var idviaje= $('#me_idviaje').val();
                var presupuesto= $('#me_presupuesto').val();
                if(presupuesto=== "undefined" || presupuesto=== ""){
                    error=1;
                    Swal.fire({
                        type: 'warning',
                        title: '¡Importe!',
                        text: '¡Debe de anexar un valor para el importe!'
                    });
                }
                var descripcion= $('#me_descripcion').val();
                if(descripcion=== "undefined" || descripcion=== ""){
                    error=1;
                    Swal.fire({
                        type: 'warning',
                        title: '¡Importe!',
                        text: '¡Debe de anexar un valor para la descripcion!'
                    });
                }

                if(error==0){
                    $.ajax({
                        type: "POST",
                        url: "{{ route('contribuyentes.seditarviaje') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "idviaje": idviaje,
                            "presupuesto": presupuesto,
                            "descripcion": descripcion,
                            "idcontribuyente": {{ $idcontribuyente }},
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            if(respuesta.error==1){
                                Swal.fire({
                                    type: 'error',
                                    title: '¡No se pudo guardar!',
                                    text: '¡Ha ocurrido un error no se pudo guardar el viaje!'
                                });
                            }else{
                                cerramodaleditandoviaje();
                            }
                        },
                        error: function() {
                        }
                    });
                }
            }

            /**
             * Funcion que permite cambiar o  descripcion de un viaje
             */
            function cerramodaleditandoviaje(){
                $('#modal-editarviaje').modal('toggle');
                abrirmodalviajes();
            }

            /**
             * Funcion que permite cerrar el viaje
             */
            function cerrarviaje(){
                var idviaje= $('#me_idviaje').val();
                Swal.fire({
                    title: '¿Estas seguro que desea cerrar el viaje?',
                    text: "Usted está por cerrar el viaje",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'No',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Sí, estoy seguro!"
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('contribuyentes.cerrarviaje') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "idviaje": idviaje
                            },
                            dataType:"json",
                            success: function(respuesta) {
                                cerramodaleditandoviaje();
                            },
                            error: function() {
                            }
                        });
                    }
                });
            }

            /**
             * Funcion que permite eliminar el viaje
             */
            function eliminarviaje(){
                var idviaje= $('#me_idviaje').val();
                Swal.fire({
                    title: '¿Estas seguro que desea eliminar el viaje?',
                    text: "Usted está por eliminar el viaje",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'No',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Sí, estoy seguro!"
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('contribuyentes.eliminarviaje') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "idviaje": idviaje
                            },
                            dataType:"json",
                            success: function(respuesta) {
                                cerramodaleditandoviaje();
                            },
                            error: function() {
                            }
                        });
                    }
                });
            }

        /**
         * Funcion que permite seleccionar los id de viajes para el filtrado
         */
        function seleccionarviaje(idviaje,nombreviaje){
            $("#idviaje").val(idviaje);

            var mensaje=nombreviaje;
            $("#tituloviajes").html(mensaje);

            $('#modal-viajes').modal('toggle');
            buscarFactura();
        }

    /*
     * Funcion que permite darle formato de moneda al input
     */
    function formatonumero(inputid){
        var value = $('#'+inputid).val();
        value = $.number(value, 2, '.', ',');
        $('#'+inputid).val(value);
    }



    /**
     * Permite redireccionar a mostar el pdf
     */
    function verFactura(id){
        window.location=("{{url('contribuyentes/visualizarpdf')}}/"+id+"/{{ $rfcDefault }}"+"/{{ $idviaje }}");
    }

    $(".custom-control-input").on('change', function() {
        buscarFactura();
    });
    $('#fecha_inicial').change(function() {
        buscarFactura();
    });
    $('#fecha_final').change(function() {
        buscarFactura();
    });

    $(document).ready(function() {
        buscarFactura();
    });
    function filtroViaje(val) {
      alert("new input value: " + val);
    }

    function verificaCampos(){
        if($('#descripcion').val()==""){
            return alert("Ingrese un nombre valido");
        }
        if($('#presupuesto').val()==""){
            return alert("Ingrese un presupuesto valido");
        }

        $('#EditForm').submit();
    }

    /**
     * Permite buscar las facturas
     */
    function buscarFactura(){

        var datos = new FormData();
        idviaje=-1;
        <?php
        if($idviaje!=null){
        ?>
        idviaje={{$idviaje}};
        <?php
        }
        ?>
        datos.append( '_token', "{{ csrf_token() }}");
        datos.append('rfc','<?php  echo $rfcDefault?>');
        datos.append('idcliente',<?php echo auth()->user()->id  ?>);
        datos.append('idcontribuyente',<?php echo $idcontribuyente  ?>);
        datos.append('idviaje',idviaje);
        if(!$('#delButton').is(':checked')){
            datos.append('nuevafecha','01-01-2000');
        }else{
            datos.append('nuevafecha',$('#fecha_inicial').val());
        }
        if(!$('#AlButton').is(':checked')){
            datos.append('fecha','12-12-2030');
        }else{
            datos.append('fecha',$('#fecha_final').val());
        }

        $.ajax({
            url: '{{url("clientes/buscarfacturas")}}',
            type:'POST',
            data:datos,
            contentType:false,
            processData:false,
            dataType:"json",
            beforeSend: function() {
                Swal.fire({
                    title: 'Cargando',
                    allowOutsideClick: false
                });
                Swal.showLoading();
            },
            success:function(respuesta){
                Swal.close();

               $('#ids').val(respuesta.ids);
                $('#facturas').html(respuesta.html);
            }
        });
    }

    function verificaCampos(){
        if($('#descripcion').val()==""){
            return alert("Ingrese un nombre valido");
        }
        if($('#presupuesto').val()==""){
            return alert("Ingrese un presupuesto valido");
        }

        $('#EditForm').submit();
    }
</script>

<!-- SWEETALERT2-->
<link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
<script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<!-- FIN DE SWEETALERT2-->
<!-- JQUERY NUMBER-->
<script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
<!-- FIN DE JQUERY NUMBER-->

<style>
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        border-radius: 5px; /* 5px rounded corners */
    }

    /* Add rounded corners to the top left and the top right corner of the image */
    img {
        border-radius: 5px 5px 0 0;
    }

    .btn-float-primary{
        position: fixed;
        bottom: 20px;
        right: 20px;
        box-shadow: 0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0);
        transform: translate3d(0,0,0);
        min-width: 0;
        border-radius: 50%;
        width: 56px;
        height: 56px;
        padding: 0;
        box-sizing: border-box;
        cursor: pointer;
        user-select: none;
        outline: none;
        border: none;
        display: inline-block;
        white-space: nowrap;
        text-decoration: none;
        vertical-align: baseline;
        font-size: 2rem;
        font-family: Roboto;
        font-weight: 500;
        background-color: #3b5998;
        color: #ffff;
    }

    .btn-float-primary:hover {
      color: #ffff;
    }
</style>
@endsection
