@extends('plantillas.privada')
@section('content')
    <?php if($objCliente->id>0):?>
        <!--FORMULARIO PARA ELIMINAR CLIENTE-->
        <form method="POST" action="{{route('clientes.delete')}}" id="FormDelete{{$objCliente->id}}" class="d-inline">
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="idcliente" id="idcliente" value="{{$objCliente->id}}">
            <input type="hidden" name="plataforma" id="plataforma" value="web">
        </form>
    <?php endif;?>
    {{ Form::model($objCliente, ['action' => ['ClienteController@update', $objCliente->id>0?$objCliente->id:-1], 'id' => 'EditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('rfc') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-id-card"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'rfc',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'rfc',
                                    'placeholder'=>'RFC',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('rfc'))
                    <span class="help-block">
                        <strong>{{ $errors->first('rfc') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-user-alt"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'nombre',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'nombre',
                                    'placeholder'=>'Nombre o Razón Social',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group required {{ $errors->has('correo') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-envelope"></i>
                            </span>
                        </div>
                        {{
                            Form::email(
                                'correo',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'correo',
                                    'placeholder'=>'Correo',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('correo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('correo') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('calle') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-map-marker-alt"></i>
                            </span>
                        </div>
                        {{
                            Form::textarea(
                                'calle',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'calle',
                                    'placeholder'=>'Domicilio',
                                    'rows'=>4
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('calle'))
                    <span class="help-block">
                        <strong>{{ $errors->first('calle') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('localidad') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-globe-americas"></i>
                            </span>
                        </div>
                        {{
                            Form::textarea(
                                'localidad',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'localidad',
                                    'placeholder'=>'Localidad',
                                    'rows'=>4
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('localidad'))
                    <span class="help-block">
                        <strong>{{ $errors->first('localidad') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <?php if($objCliente->id>0):?>
            <!--EDITAR-->
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit"><i class="far fa-save"></i> Guardar</button>
                        <button class="btn btn-danger" type="button" onclick="eliminar({{$objCliente->id}})"><i class="far fa-trash-alt"></i> Eliminar</button>
                    </div>
                </div>
            </div>
        <?php else:?>
            <!--NUEVO-->
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit"><i class="far fa-save"></i> Guardar</button>
                    </div>
                </div>
            </div>
        <?php endif;?>
    {{ Form::close() }}

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <!-- JQUERY VALIDATE-->
        <script src="{{ asset('plugins/jquery-validate/jquery.validate.js') }}"></script>
    <!-- FIN DE JQUERY VALIDATE-->
    <script type="text/javascript">
        $(document).ready(function() {
            // $("#EditForm").validate();
        });

        /**
         * Funcion para guardar formulario haciendo submit
         */
        function guardar(){
            $('#EditForm').submit();
        }

        /**
         * Permite eliminar el cliente
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        function eliminar(id){
            Swal.fire({
                title: '¿Estas seguro que desea eliminar?',
                text: "Usted está por eliminar",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Sí, estoy seguro!"
            }).then((result) => {
                if (result.value) {
                    $('#FormDelete'+id).submit();
                }
            });
        }

    </script>
    <style>
        p.note {
          font-size: 1rem;
          color: red;
        }

        /*input {
          border-radius: 5px;
          border: 1px solid #ccc;
          padding: 4px;
          font-family: 'Lato';
          width: 300px;
          margin-top: 10px;
        }*/

        label {
          width: 300px;
          font-weight: bold;
          display: inline-block;
          margin-top: 20px;
        }

        label span {
          font-size: 1rem;
        }

        label.error {
            color: red;
            font-size: 1rem;
            display: block;
            margin-top: 5px;
        }

        input.error {
            border: 1px solid red;
            font-weight: 300;
            color: red;
        }
    </style>

@endsection

