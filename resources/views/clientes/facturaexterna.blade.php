@extends('plantillas.privada')
@section('content')
    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

    {{ Form::open(['action' => 'ClienteController@saveExterno','id' => 'EditForm','method' => 'post','role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign', 'files'=>true]) }}
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="plataforma" name="plataforma" value="web">
        <input type="hidden" id="idcliente" name="idcliente" value="{{$rfcDefault}}">
        <input type="hidden" id="idusuario" name="idusuario" value='{{auth()->user()->id}}'>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('total') ? ' has-error' : '' }}">
                    <div class="input-group">
                        {{
                            Form::text(
                                'total',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'total',
                                    'placeholder'=>'Total',
                                    'required'=>true,
                                    'style'=>"text-align:right;",
                                    'onkeypress'=>"return AcceptNumPunto(event);",
                                    'onchange'=>"formatonumero('total');",
                                    'align'=>"right"
                                ]
                            )
                        }}
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-dollar-sign"></i>
                            </span>
                        </div>
                    </div>
                </div>
                @if ($errors->has('total'))
                    <span class="help-block">
                        <strong>{{ $errors->first('total') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('concepto') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-prescription-bottle"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'concepto',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'concepto',
                                    'placeholder'=>'Concepto',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('concepto'))
                    <span class="help-block">
                        <strong>{{ $errors->first('concepto') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('fecha_inicial') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                        {{
                            Form::date(
                                'fecha_inicial',
                                date('Y-m').'-01',
                                [
                                    'class'=>'form-control',
                                    'id'=>'fecha_inicial',
                                    'name'=>'fecha',
                                    'placeholder'=>'Fecha',
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('fecha_inicial'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fecha_inicial') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('foto') ? ' has-error' : '' }}">
                    <input id="foto" name="foto" type="file" class="file" multiple="" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Anexe comprobante" data-allowed-file-extensions='["xml","png","jpeg","jpg"]'>
                    <script>
                        $("#foto").fileinput({
                            showPreview: false,
                            showUpload: false,
                            dropZoneEnabled: false,
                            language: "es",
                            theme: "fas",
                            allowedFileExtensions: ["xml","png","jpeg","jpg"],
                            maxFileCount: 1
                        });
                    </script>
                    @if ($errors->has('foto'))
                        <span class="help-block">
                            <strong>{{ $errors->first('foto') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <?php  if($idviaje==-1) { ?>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('idviaje') ? ' has-error' : '' }}">
                    {{ Form::label('idviaje', 'Viaje') }}
                    <div class="input-group">
                        <span  onClick="editarviaje(0)" class="input-group-text">
                            <i class="fas fa-plus"></i>
                        </span>
                        <?php

                        ?>
                        {{

                            Form::select(
                                'idviaje',
                                $viajes,
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'idviaje',
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('idviaje'))
                    <span class="help-block">
                        <strong>{{ $errors->first('idviaje') }}</strong>
                    </span>
                @endif
            </div>
            <?php  }else{  ?>
                <input type="hidden" name="idviaje" id="idviaje" value="{{$idviaje}}" >

                <?php  }  ?>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button type="submit" class="btn btn-success" id="btn-guardar">
                        <i class="fas fa-save"></i>
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    {{ Form::close() }}
    <div class="modal" id="modal-editarviaje" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <div class="block-options">
                            <button type="button" class="btn-block-option" style="padding-left: 0;padding-right: 1.25rem;" onclick="cerramodaleditandoviaje();">
                                <i class="fas fa-chevron-left"></i>
                            </button>
                        </div>
                        <h3 class="block-title" id="modal-editarviaje-titulo"></h3>
                    </div>
                    <div class="block-content">
                        {{ Form::hidden('me_idviaje',null,['id'=>'me_idviaje'])}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-signature"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::text(
                                                'me_descripcion',
                                                null,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'me_descripcion',
                                                    'placeholder'=>'Nombre',
                                                    'required'=>true
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'me_presupuesto',
                                                0.00,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'me_presupuesto',
                                                    'style'=>"text-align:right;",
                                                    'align'=>"right",
                                                    'onkeypress'=>"return AcceptNumPunto(event);",
                                                    'onchange'=>"formatonumero('me_presupuesto');"
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-dollar-sign"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" id="btn-cerrarviaje"class="btn col-12 btn-sm btn-dark" onclick="cerrarviaje();"><i class="fas fa-lock"></i> Cerrar viaje</button>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-success" onclick="guardarviaje();"><i class="fas fa-save"></i> Guardar</button>
                        <button type="button" id="btn-eliminarviaje"class="btn btn-sm btn-danger" onclick="eliminarviaje();"><i class="far fa-calendar-minus"></i> Eliminar</button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <script type="text/javascript">
        $("#EditForm").submit(function(){
            $("#btn-guardar").attr('disabled',true);
            Swal.fire({
                title: 'Guardando, por favor espere',
                allowOutsideClick: false
            });
            Swal.showLoading();
        });


            function editarviaje(idviaje){
                $("#btn-cerrarviaje").hide();
                $("#btn-eliminarviaje").hide();
                $.ajax({
                    type: "POST",
                    url: "{{ route('contribuyentes.editarviaje') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "idviaje": idviaje,
                        "idcontribuyente": {{ $idcontribuyente }},
                    },
                    dataType:"json",
                    success: function(respuesta) {
                        $('#modal-viajes').modal('toggle');

                        //EDITAR VIAJE
                        if(respuesta.nuevo==1){
                            $("#modal-editarviaje-titulo").html('Crear nuevo viaje');
                        }



                        if(idviaje>0 && respuesta.estatus==1){
                            //ESTA ABIERTO Y ES VIEJO
                            $("#btn-cerrarviaje").show();
                        }
                        if(idviaje>0 && respuesta.canti==0){
                            //SE PUEDE ELIMINAR Y ES VIEJO
                            $("#btn-eliminarviaje").show();
                        }
                        $('#modal-editarviaje').modal({backdrop: 'static', keyboard: false});
                    },
                    error: function() {
                    }
                });
            }

            function cerramodaleditandoviaje(){
                $('#modal-editarviaje').modal('toggle');

            }
             function guardarviaje(){
                var error=0;
                var idviaje= $('#me_idviaje').val();
                var presupuesto= $('#me_presupuesto').val();
                if(presupuesto=== "undefined" || presupuesto=== ""){
                    error=1;
                    Swal.fire({
                        type: 'warning',
                        title: '¡Importe!',
                        text: '¡Debe de anexar un valor para el importe!'
                    });
                }
                var descripcion= $('#me_descripcion').val();
                if(descripcion=== "undefined" || descripcion=== ""){
                    error=1;
                    Swal.fire({
                        type: 'warning',
                        title: '¡Importe!',
                        text: '¡Debe de anexar un valor para la descripcion!'
                    });
                }

                if(error==0){
                    $.ajax({
                        type: "POST",
                        url: "{{ route('contribuyentes.seditarviaje') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "idviaje": idviaje,
                            "presupuesto": presupuesto,
                            "descripcion": descripcion,
                            "idcontribuyente": {{ $idcontribuyente }},
                            "rfcDefault" : '{{$rfcDefault}}'
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            select = document.getElementById("idviaje");
                            option = document.createElement("option");
                            option.value = respuesta.viaje.id;
                            option.text = respuesta.viaje.descripcion;
                        select.appendChild(option);
                            console.log(respuesta);
                            if(respuesta.error==1){
                                Swal.fire({
                                    type: 'error',
                                    title: '¡No se pudo guardar!',
                                    text: '¡Ha ocurrido un error no se pudo guardar el viaje!'
                                });
                            }else{
                                $('#tablaViajes').append(respuesta.html);
                                cerramodaleditandoviaje();
                            }
                        },
                        error: function() {
                        }
                    });
                }
            }








        /*
         * Funcion que permite darle formato de moneda al input
         */
        function formatonumero(inputid){
            var value = $('#'+inputid).val();
            value = $.number(value, 2, '.', ',');
            $('#'+inputid).val(value);
        }
        $(document).ready(function() {

            select = document.getElementById("idviaje");
            option = document.createElement("option");
            option.value = 0;
            option.text = 'Sin viaje';
            select.appendChild(option);
        });
    </script>
@endsection



