@extends('plantillas.privada')
@section('content')
    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>


    <link rel="stylesheet" href="{{ asset('privada/js/plugins/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('privada/js/plugins/slick-carousel/slick-theme.css') }}">
    <!-- Page JS Plugins -->
    <script src="{{ asset('privada/js/plugins/slick-carousel/slick.min.js') }}"></script>
    <script src="{{ asset('privada/js/be_comp_onboarding.min.js') }}"></script>

    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>Bienvenido: <?=auth()->user()->nombre;?></h1>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <?php if(auth()->user()->idtipousuario==2):?>
        <?php if($planvencido==1):?>
            <?php if($diadiferencias>=0):?>
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 my-2">Su plan de membresía se vencera en {{ $diadiferencias }} días</h3>
                    <p class="mb-0">
                        Su plan está por caducar, por favor renueve adquiriendo un nuevo plan haciendo un click <a href="{{ route('comprarplan.index') }}" class="text-info"> Aquí</a>
                    </p>
                </div>
            <?php else:?>
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 my-2">Se ha vencido su membresía</h3>
                    <p class="mb-0">
                        Su plan de membresía ha caducado, por favor renueve adquiriendo un nuevo plan haciendo un click <a href="{{ route('comprarplan.index') }}" class="text-info"> Aquí</a>
                    </p>
                </div>
            <?php endif;?>
            <br>
        <?php endif;?>
        <?php if($foliosvencidos==1):?>
            <?php if($folio_disponibles>=0):?>
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 my-2">Su plan de folio está por agotarse</h3>
                    <p class="mb-0">
                        Su plan de folio está por acabarse le queda disponible {{ $folio_disponibles }}, por favor renueve adquiriendo un nuevo plan haciendo un click <a href="{{ route('comprarplan.index') }}" class="text-info"> Aquí</a>
                    </p>
                </div>
            <?php else:?>
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 my-2">Se plan de folio se ha acabado</h3>
                    <p class="mb-0">
                        Su plan de folio se ha gastado, por favor renueve adquiriendo un nuevo plan haciendo un click <a href="{{ route('comprarplan.index') }}" class="text-info"> Aquí</a>
                    </p>
                </div>
            <?php endif;?>
            <br>
        <?php endif;?>
        <!--ZONAS DE DESCARGA-->
        <div class="row">
            <?php if(env('APK_AND_CLI')!=0 || env('APK_AND_EMP')!=0):?>
                <div class="col-sm-6 col-md-3 col-lg-4">
                    <div class="block block-rounded bg-image" style="background-image: url('{{ asset('privada/media/photos/android-1.jpg') }}');">
                        <div class="block-content">
                            <p class="text-white text-uppercase font-size-sm font-w700 text-center mt-2 mb-4">
                                App para establecimiento (android)
                            </p>
                            <?php if(env('APK_AND_EMP')!=0):?>
                                <a class="block block-rounded block-link-shadow bg-black-25 mb-2" href="https://drive.google.com/uc?export=download&id={{ env('APK_AND_EMP') }}" target="_blank">
                                    <div class="block-content block-content-sm block-content-full d-flex align-items-center justify-content-between">
                                        <div class="mr-3">
                                            <p class="text-white font-size-h3 font-w300 mb-0">
                                                Google Drive
                                            </p>
                                            <p class="text-white-75 mb-0">
                                                451 <small>Descargas</small>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-2x text-gray fas fa-download"></i>
                                        </div>
                                    </div>
                                </a>
                            <?php endif;?>
                            <?php if(env('APK_AND_CLI')!=0):?>
                                <a class="block block-rounded block-link-shadow bg-black-25 mb-2" href="{{ asset('storage/apk/empresa.apk') }}" download="Empresa_apk" target="_blank">
                                    <div class="block-content block-content-sm block-content-full d-flex align-items-center justify-content-between">
                                        <div class="mr-3">
                                            <p class="text-white font-size-h3 font-w300 mb-0">
                                                Server mefactura.com
                                            </p>
                                            <p class="text-white-75 mb-0">
                                                262 <small>Descargas</small>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-2x text-gray fas fa-download"></i>
                                        </div>
                                    </div>
                                </a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <?php if(env('APK_IOS_CLI')!=0 || env('APK_IOS_EMP')!=0):?>
                <div class="col-sm-6 col-md-3 col-lg-4">
                    <div class="block block-rounded bg-image" style="background-image: url('{{ asset('privada/media/photos/apple_ios-1.jpg') }}');">
                        <div class="block-content">
                            <p class="text-white text-uppercase font-size-sm font-w700 text-center mt-2 mb-4">
                                Zona de descarga (IOS)
                            </p>
                            <?php if(env('APK_IOS_EMP')!=0):?>
                                <a class="block block-rounded block-link-shadow bg-black-25 mb-2" href="https://drive.google.com/uc?export=download&id={{ env('APK_IOS_EMP') }}" target="_blank">
                                    <div class="block-content block-content-sm block-content-full d-flex align-items-center justify-content-between">
                                        <div class="mr-3">
                                            <p class="text-white font-size-h3 font-w300 mb-0">
                                                Establecimientos
                                            </p>
                                            <p class="text-white-75 mb-0">
                                                262 <small>Descargas</small>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-2x text-gray fas fa-download"></i>
                                        </div>
                                    </div>
                                </a>
                            <?php endif;?>
                            <?php if(env('APK_IOS_CLI')!=0):?>
                                <a class="block block-rounded block-link-shadow bg-black-25 mb-2" href="https://drive.google.com/uc?export=download&id={{ env('APK_IOS_CLI') }}" target="_blank">
                                    <div class="block-content block-content-sm block-content-full d-flex align-items-center justify-content-between">
                                        <div class="mr-3">
                                            <p class="text-white font-size-h3 font-w300 mb-0">
                                                Clientes
                                            </p>
                                            <p class="text-white-75 mb-0">
                                                451 <small>Descargas</small>
                                            </p>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-2x text-gray fas fa-download"></i>
                                        </div>
                                    </div>
                                </a>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            <?php endif;?>
        </div>
    <?php endif;?>

    <?php if(auth()->user()->idtipousuario==2):?>
        <?php if((int)$empresa->pasowizard<4):?>
            <!-- Onboarding Modal -->
            <div class="modal fade" id="modal-onboarding" tabindex="-1" role="dialog" aria-labelledby="modal-onboarding" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content rounded overflow-hidden bg-image" style="background-image: url('{{ asset('storage/img/compartidos/pasos1.jpg') }}');">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="p-3 text-right text-md-left text-danger-light" style="cursor:pointer;" onclick="cerrarwizard();">
                                    <i class="fa fa-share text-danger-light mr-1"></i> Configurar más tarde
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="bg-white shadow-lg">
                                    <div class="js-slider slick-dotted-inner" data-dots="true" data-arrows="false" data-infinite="false">
                                        <div class="p-4">
                                            <i class="fas fa-tools fa-3x text-muted my-4"> Configuración </i>
                                            <h3 class="font-size-h3 font-w300 mb-2">Paso 1 de 4 - Datos Generales</h3>
                                            <form class="mb-3">
                                                <div class="form-group">
                                                    <label for="nombrecomercial">Nombre comercial</label>
                                                    <input type="text" class="form-control" id="nombrecomercial" name="nombrecomercial" placeholder="Nombre comercial" value={{ $empresa->nombrecomercial }}>
                                                </div>
                                                <div class="form-group">
                                                    <label for="regimen">Régimen fiscal</label>
                                                    <div class="input-group">
                                                        {{
                                                            Form::select(
                                                                'regimen',
                                                                $regimenfiscales,
                                                                $empresa->regimen,
                                                                [
                                                                    'class'=>'form-control ',
                                                                    'id'=>'regimen',
                                                                    'placeholder'=>'Régimen fiscal'
                                                                ]
                                                            )
                                                        }}
                                                    </div>
                                                </div>
                                            </form>
                                            <button type="button" class="btn btn-hero btn-primary mb-4" onclick="guardarpasos(1);">
                                                Siguiente<i class="fa fa-arrow-right ml-1"></i>
                                            </button>
                                        </div>
                                        <div class="slick-slide p-4">
                                            <i class="fas fa-tools fa-3x text-muted my-4"> Configuración </i>
                                            <h3 class="font-size-h3 font-w300 mb-2">Paso 2 de 4 - Registrar certificado</h3>
                                            <form class="mb-3">
                                                <div class="form-group">
                                                    <label for="archivokey">Llave de CSD (.key)</label>
                                                    <div class="input-group">
                                                        <input id="archivokey" name="archivokey" type="file" class="file" data-show-upload="false" data-show-preview="false" data-msg-placeholder="Llave de CSD (.key)" data-allowed-file-extensions='["key"]' data-toggle="popover" data-placement = "top" >
                                                        <script>
                                                            $("#archivokey").fileinput({
                                                                language: "es",
                                                                theme: "fas",
                                                                allowedFileExtensions: ["key"],
                                                                showPreview: false,
                                                                /*maxFileSize: 2048,
                                                                minImageWidth: 50,
                                                                minImageHeight: 500,
                                                                maxImageWidth: 1024,
                                                                maxImageHeight: 1024,*/
                                                                maxFileCount: 1
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="form-group {{ $errors->has('archivocertificado') ? ' has-error' : '' }}">
                                                    <label for="archivocertificado">Certificado CSD (.cer)</label>
                                                    <div class="input-group">
                                                        <input id="archivocertificado" name="archivocertificado" type="file" class="file"  data-show-upload="false"  data-show-preview="false" data-msg-placeholder="Certificado de CSD (.cer)" data-allowed-file-extensions='["cer"]' data-toggle="popover" data-placement = "top" >
                                                        <script>
                                                            $("#archivocertificado").fileinput({
                                                                language: "es",
                                                                theme: "fas",
                                                                allowedFileExtensions: ["cer"],
                                                                /*maxFileSize: 2048,
                                                                minImageWidth: 50,
                                                                minImageHeight: 500,
                                                                maxImageWidth: 1024,
                                                                maxImageHeight: 1024,*/
                                                                maxFileCount: 1
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">Contraseña</label>
                                                    <input type="password" class="form-control" id="password" placeholder="Contraseña">
                                                </div>
                                            </form>
                                            <button type="button" class="btn btn-hero btn-danger mb-4" onclick="jQuery('.js-slider').slick('slickGoTo', 0);">
                                                <i class="fa fa-arrow-left ml-1"></i> Anterior
                                            </button>
                                            <button type="button" class="btn btn-hero btn-primary mb-4" onclick="guardarpasos(2);">
                                                Siguiente <i class="fa fa-arrow-right ml-1"></i>
                                            </button>
                                        </div>
                                        <div class="slick-slide p-4">
                                            <i class="fas fa-tools fa-3x text-muted my-4"> Configuración </i>
                                            <h3 class="font-size-h3 font-w300 mb-2">Paso 3 de 4 - Registrar serie</h3>
                                            <form class="mb-3">
                                                <div class="form-group">
                                                    <label for="serie">Serie</label>
                                                    <input type="text" class="form-control" id="serie" name="serie" placeholder="Serie">
                                                </div>
                                                <div class="form-group">
                                                    <label for="folio_actual">Folio actual</label>
                                                    <input type="text" class="form-control" id="folio_actual" name="folio_actual" placeholder="Folio actual">
                                                </div>
                                            </form>
                                            <button type="button" class="btn btn-hero btn-danger mb-4" onclick="jQuery('.js-slider').slick('slickGoTo', 1);">
                                                <i class="fa fa-arrow-left ml-1"></i> Anterior
                                            </button>
                                            <button type="button" class="btn btn-hero btn-primary mb-4" onclick="guardarpasos(3);">
                                                Siguiente <i class="fa fa-arrow-right ml-1"></i>
                                            </button>
                                        </div>
                                        <div class="slick-slide p-4">
                                            <i class="fas fa-tools fa-3x text-muted my-4"> Configuración </i>
                                            <h3 class="font-size-h3 font-w300 mb-2">Paso 4 de 4 - Registrar Surcursal</h3>
                                            <form class="mb-3">
                                                <div class="form-group">
                                                    <label for="sucursal_nombre">Nombre</label>
                                                    <input type="text" class="form-control" id="sucursal_nombre" name="sucursal_nombre" placeholder="Sucursal" value="Matriz">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sucursal_domicilio">Domicilio</label>
                                                    <textarea id="sucursal_domicilio" name="sucursal_domicilio" rows="4" class="form-control" placeholder="Domicilio"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sucursal_codigopostal">Código postal</label>
                                                    <input type="text" class="form-control" id="sucursal_codigopostal" name="sucursal_codigopostal" placeholder="Código Postal">
                                                </div>
                                            </form>
                                            <button type="button" class="btn btn-hero btn-success mb-4" onclick="guardarpasos(4);">
                                                Finalizar <i class="fa fa-check ml-1"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Onboarding Modal -->
        <?php endif;?>
    <?php endif;?>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <script type="text/javascript">
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', () => {
                navigator.serviceWorker.register('{{ route('welcome')  }}/sw2.js');
            });
        }

        <?php if(auth()->user()->idtipousuario==2):?>
            /**
             * Permite guardar los pasos del modal
             *
             */
            function guardarpasos(paso){
                if(paso==1){
                    //NOMBRE COMERCIAL Y REGIMEN
                    var error=0;
                    var nombrecomercial = $("#nombrecomercial").val();
                    if(nombrecomercial=== "undefined" || nombrecomercial=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Nombre comercial!',
                            text: '¡Debe de anexar un valor para el nombre comercial!'
                        });
                    }
                    var regimen = $("#regimen").val();
                    if(regimen=== "undefined" || regimen=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Regimen!',
                            text: '¡Debe de anexar un valor para regimen!'
                        });
                    }

                    if(error==0){
                        //PERMITE GUARDAR NOMBRE Y REGIMEN
                        $.ajax({
                          type: "POST",
                          url: "{{ route('home.guardarpasos') }}",
                          data: {
                              "_token": "{{ csrf_token() }}",
                              "paso": 1,
                              "nombrecomercial": nombrecomercial,
                              "regimen": regimen,
                          },
                          dataType:"json",
                          success: function(respuesta) {
                            if(respuesta.error==1){
                              Swal.fire({
                                type: 'warning',
                                title: '',
                                text: respuesta.msj
                              });
                            }else{
                                jQuery('.js-slider').slick('slickGoTo', 1);
                            }
                          },
                          error: function() {
                          }
                        });
                    }
                }
                if(paso==2){
                    //CERTIFICADOS
                    var error=0;
                    var archivocer_val = $('#archivocertificado').val();
                    if(archivocer_val=== "undefined" || archivocer_val=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Certificado!',
                            text: '¡Debe de cargar un archivo para certificado!'
                        });
                    }
                    var archivokey_val = $('#archivokey').val();
                    if(archivokey_val=== "undefined" || archivokey_val=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Key!',
                            text: '¡Debe de cargar un archivo para llave!'
                        });
                    }
                    var password = $('#password').val();
                    if(password=== "undefined" || password=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Contraseña!',
                            text: '¡Debe de anexar un valor para contraseña!'
                        });
                    }
                    if(error==0){
                        var datos= new FormData();
                        datos.append( '_token', "{{ csrf_token() }}");
                        datos.append('paso', 2);
                        datos.append('archivocer', $('#archivocertificado')[0].files[0]);
                        datos.append('archivokey', $('#archivokey')[0].files[0]);
                        datos.append('password',$('#password').val());

                        //PERMITE GUARDAR CERTIFICADOS
                        $.ajax({
                            type: "POST",
                            url: "{{ route('home.guardarpasos') }}",
                            data:datos,
                            contentType:false,
                            processData:false,
                            dataType:"json",
                            success: function(respuesta) {
                                if(respuesta.error==1){
                                  Swal.fire({
                                    type: 'warning',
                                    title: '',
                                    text: respuesta.msj
                                  });
                                }else{
                                    jQuery('.js-slider').slick('slickGoTo', 2);
                                }
                            },
                            error: function() {
                            }
                        });
                    }
                }
                if(paso==3){
                    //SERIE
                    var error=0;
                    var serie = $("#serie").val();
                    if(serie=== "undefined" || serie=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Serie!',
                            text: '¡Debe de anexar un valor para la serie!'
                        });
                    }
                    var folio_actual = $("#folio_actual").val();
                    if(folio_actual=== "undefined" || folio_actual=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Folio actual!',
                            text: '¡Debe de anexar un valor para folio actual!'
                        });
                    }

                    if(error==0){
                        //PERMITE GUARDAR NOMBRE Y REGIMEN
                        $.ajax({
                            type: "POST",
                            url: "{{ route('home.guardarpasos') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "paso": 3,
                                "serie": serie,
                                "folio_actual": folio_actual,
                            },
                            dataType:"json",
                            success: function(respuesta) {
                                if(respuesta.error==1){
                                    Swal.fire({
                                        type: 'warning',
                                        title: '',
                                        text: respuesta.msj
                                    });
                                }else{
                                    jQuery('.js-slider').slick('slickGoTo', 3);
                                }
                            },
                            error: function() {
                            }
                        });
                    }
                }
                if(paso==4){
                    //SERIE
                    var error=0;
                    var sucursal_nombre = $("#sucursal_nombre").val();
                    if(sucursal_nombre=== "undefined" || sucursal_nombre=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Sucursal!',
                            text: '¡Debe de anexar un valor para nombre!'
                        });
                    }
                    var sucursal_domicilio = $("#sucursal_domicilio").val();
                    if(sucursal_domicilio=== "undefined" || sucursal_domicilio=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Domicilio!',
                            text: '¡Debe de anexar un valor para domicilio!'
                        });
                    }
                    var sucursal_codigopostal = $("#sucursal_codigopostal").val();
                    if(sucursal_codigopostal=== "undefined" || sucursal_codigopostal=== ""){
                        error=1;
                        Swal.fire({
                            type: 'warning',
                            title: '¡Código Postal!',
                            text: '¡Debe de anexar un valor para código postal!'
                        });
                    }

                    if(error==0){
                        //PERMITE GUARDAR SUCURSAL
                        $.ajax({
                            type: "POST",
                            url: "{{ route('home.guardarpasos') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "paso": 4,
                                "nombre": sucursal_nombre,
                                "domicilio": sucursal_domicilio,
                                "codigopostal": sucursal_codigopostal,
                            },
                            dataType:"json",
                            success: function(respuesta) {
                                if(respuesta.error==1){
                                    Swal.fire({
                                        type: 'warning',
                                        title: '',
                                        text: respuesta.msj
                                    });
                                }else{
                                    $("#modal-onboarding").modal('hide');
                                }
                            },
                            error: function() {
                            }
                        });
                    }
                }
            }

            /**
             * Permite cerrar el modal, si es así cerrar sesión
             */
            function cerrarwizard(){
                Swal.fire({
                    title: '¿Estas seguro que desea salir?',
                    text: "Usted está por salir de la configuración",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'No',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Sí, estoy seguro!"
                }).then((result) => {
                    if (result.value) {
                        cerrarsesion();
                    }
                });
            }
        <?php endif;?>


        $(document).ready(function() {
            <?php if(auth()->user()->idtipousuario==2):?>
                <?php if((int)$empresa->pasowizard<4):?>
                    jQuery(".js-slider:not(.js-slider-enabled)").each(function (e, a) {
                        var t = jQuery(a);
                        t.addClass("js-slider-enabled").slick({
                            arrows: t.data("arrows") || !1,
                            dots: false,
                            slidesToShow: t.data("slides-to-show") || 1,
                            centerMode: t.data("center-mode") || !1,
                            autoplay: t.data("autoplay") || !1,
                            autoplaySpeed: t.data("autoplay-speed") || 3e3,
                            initialSlide:{{(int)$empresa->pasowizard }},
                            touchMove:false,
                            accessibility:false,
                            draggable:false,
                            infinite: void 0 === t.data("infinite") || t.data("infinite"),
                        });
                    });
                    $('#modal-onboarding').modal({backdrop: 'static', keyboard: false});
                <?php endif; ?>
            <?php endif; ?>
        });
  </script>

@endsection
