@extends('plantillas.publica')
@section('content')
  <!-- ======= Header ======= -->
    @include('elementos.public_header')
  <!-- End Header -->

  <main id="main">
    <!-- ======= Inicio Section ======= -->
    <section id="inicio" class="d-flex align-items-center">
      <div class=" position-relative" data-aos="fade-up" data-aos-delay="100">
        <div>
          <img src="{{ asset('publica/img/hero-bg.jpg') }}" alt="{{ session()->get('Configuracion.Web.titulo') }}" class="img-fluid">
        </div>
        <div id="herop" class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <a href="{{ route('establecimiento') }}" class="btn-get-started scrollto">mefactura Establecimientos</a>
                <a href="{{ route('consumidor') }}" class="btn-get-started scrollto">mefactura Consumidores</a>
                <!--<a href="{{ route('gana') }}" class="btn-get-started scrollto">¡Ganar dinero Ya!</a>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Inicio -->

    <!-- ======= Palabras publicitarias Section ======= -->
    <section id="about" class="about" >
      <div class="container" data-aos="fade-up" style="padding-top: 5px 0;">
        <div class="row content text-center">
          <div class="col-lg-12 ">
             <!--    <h2 style="color: #5e5e5e;margin: 10px 0 0 0; font-size: 20px;"> Sube tu nota desde tu celular introduciendo solo 2 datos (folio e importe del consumo).</h2>
                <h2 style="color: #5e5e5e;margin: 10px 0 0 0; font-size: 20px;"> Ofrece a tus clientes una plataforma para que ellos se generen su factura.</h2> -->
            <ul>
              <li style="color: #5e5e5e;margin: 20px 0 0 0; font-size: 20px; padding-top: 5px 0;">
                <!-- <i class="ri-check-double-line">                   -->
                <!-- </i>  -->
              Sube tu nota desde tu celular introduciendo solo 2 datos (folio e importe del consumo).</li>
              <li style="color: #5e5e5e;margin: 20px 0 0 0; font-size: 20px;">
                <!-- <i class="ri-check-double-line">              -->
                <!-- </i>  -->
              Ofrece a tus clientes una plataforma para que ellos se generen su factura.
            </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- End Palabras publicitarias Section -->

    <!-- ======= Indicadores Section ======= -->
    <section id="counts" class="counts section-bg">
      <div class="container">
        <div class="row justify-content-end">
          <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
            <div class="count-box">
              <span data-toggle="counter-up">10</span>
              <p>Años ofreciendo soluciones en CFDI</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
            <div class="count-box">
              <span data-toggle="counter-up">3500</span>
              <p>Contribuyentes emitiendo comprobantes</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
            <div class="count-box">
              <span data-toggle="counter-up">8</span>
              <p>Soluciones CFDI</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
            <div class="count-box">
              <span data-toggle="counter-up">5</span>
              <p>Millones de CFDI emitidos</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Indicadores Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials" style="display: none;">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Testimonios</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
        <div class="owl-carousel testimonials-carousel">
          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="{{ asset('publica/img/testimonials/testimonials-1.jpg') }}" class="testimonial-img" alt="">
            <h3>Saul Goodman</h3>
            <h4>Ceo &amp; Founder</h4>
          </div>
          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="{{ asset('publica/img/testimonials/testimonials-2.jpg') }}" class="testimonial-img" alt="">
            <h3>Sara Wilsson</h3>
            <h4>Designer</h4>
          </div>
          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="{{ asset('publica/img/testimonials/testimonials-3.jpg') }}" class="testimonial-img" alt="">
            <h3>Jena Karlis</h3>
            <h4>Store Owner</h4>
          </div>
          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="{{ asset('publica/img/testimonials/testimonials-4.jpg') }}" class="testimonial-img" alt="">
            <h3>Matt Brandon</h3>
            <h4>Freelancer</h4>
          </div>
          <div class="testimonial-item">
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
            <img src="{{ asset('publica/img/testimonials/testimonials-5.jpg') }}" class="testimonial-img" alt="">
            <h3>John Larson</h3>
            <h4>Entrepreneur</h4>
          </div>
        </div>
      </div>
    </section>
    <!-- End Testimonials Section -->

    <!-- ======= Tutoriales Section ======= -->
    <section id="tutoriales" class="testimonials" style="display: none;">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Tutoriales</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
        <div class="owl-carousel testimonials-carousel">
          <div class="testimonial-item">
            <p>
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </p>
              <h3>Cómo subir una nota</h3>
          </div>
          <div class="testimonial-item">
            <p>
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </p>
              <h3>Cómo generarse la factura</h3>
          </div>
          <div class="testimonial-item">
            <p>
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </p>
              <h3>Tutorial3</h3>
          </div>
          <div class="testimonial-item">
            <p>
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </p>
              <h3>Tutorial4</h3>
          </div>
          <div class="testimonial-item">
            <p>
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </p>
              <h3>Tutorial5</h3>
          </div>
        </div>
      </div>
    </section>
    <!-- End Tutorial Section -->

    <!-- ======= Requerimientos Section ======= -->
    <section id="requerimientos" class="services section-bg" style="display: none;">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Requerimientos</h2>
          <p>¿Qué necesitas para emitir facturas con {{ session()->get('Configuracion.Web.titulo') }}?</p>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-6 align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box iconbox-blue">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,521.0016835830174C376.1290562159157,517.8887921683347,466.0731472004068,529.7835943286574,510.70327084640275,468.03025145048787C554.3714126377745,407.6079735673963,508.03601936045806,328.9844924480964,491.2728898941984,256.3432110539036C474.5976632858925,184.082847569629,479.9380746630129,96.60480741107993,416.23090153303,58.64404602377083C348.86323505073057,18.502131276798302,261.93793281208167,40.57373210992963,193.5410806939664,78.93577620505333C130.42746243093433,114.334589627462,98.30271207620316,179.96522072025542,76.75703585869454,249.04625023123273C51.97151888228291,328.5150500222984,13.704378332031375,421.85034740162234,66.52175969318436,486.19268352777647C119.04800174914682,550.1803526380478,217.28368757567262,524.383925680826,300,521.0016835830174"></path>
                </svg>
                <!-- <i class="bx bxs-user-rectangle"></i> -->
                <i class="bx bxs-user-check"></i>
              </div>
              <h4><a href="">Registro</a></h4>
              <p>Darte de alta en {{ session()->get('Configuracion.Web.titulo') }}</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box iconbox-red">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,532.3542879108572C369.38199826031484,532.3153073249985,429.10787420159085,491.63046689027357,474.5244479745417,439.17860296908856C522.8885846962883,383.3225815378663,569.1668002868075,314.3205725914397,550.7432151929288,242.7694973846089C532.6665558377875,172.5657663291529,456.2379748765914,142.6223662098291,390.3689995646985,112.34683881706744C326.66090330228417,83.06452184765237,258.84405631176094,53.51806209861945,193.32584062364296,78.48882559362697C121.61183558270385,105.82097193414197,62.805066853699245,167.19869350419734,48.57481801355237,242.6138429142374C34.843463184063346,315.3850353017275,76.69343916112496,383.4422959591041,125.22947124332185,439.3748458443577C170.7312796277747,491.8107796887764,230.57421082200815,532.3932930995766,300,532.3542879108572"></path>
                </svg>
                <i class="bx bx-file"></i>
              </div>
              <h4><a href="">Datos Fiscales</a></h4>
              <p>Captura la información Fiscal de tu establecimiento</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box iconbox-yellow">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,503.46388370962813C374.79870501325706,506.71871716319447,464.8034551963731,527.1746412648533,510.4981551193396,467.86667711651364C555.9287308511215,408.9015244558933,512.6030010748507,327.5744911775523,490.211057578863,256.5855673507754C471.097692560561,195.9906835881958,447.69079081568157,138.11976852964426,395.19560036434837,102.3242989838813C329.3053358748298,57.3949838291264,248.02791733380457,8.279543830951368,175.87071277845988,42.242879143198664C103.41431057327972,76.34704239035025,93.79494320519305,170.9812938413882,81.28167332365135,250.07896920659033C70.17666984294237,320.27484674793965,64.84698225790005,396.69656628748305,111.28512138212992,450.4950937839243C156.20124167950087,502.5303643271138,231.32542653798444,500.4755392045468,300,503.46388370962813"></path>
                </svg>
                <i class="bx bx-award"></i>
              </div>
              <h4><a href="">Certificado de sellos digital</a></h4>
              <p>Subir los Certificados de sello digital</p>
            </div>
          </div>
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box iconbox-yellow">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,503.46388370962813C374.79870501325706,506.71871716319447,464.8034551963731,527.1746412648533,510.4981551193396,467.86667711651364C555.9287308511215,408.9015244558933,512.6030010748507,327.5744911775523,490.211057578863,256.5855673507754C471.097692560561,195.9906835881958,447.69079081568157,138.11976852964426,395.19560036434837,102.3242989838813C329.3053358748298,57.3949838291264,248.02791733380457,8.279543830951368,175.87071277845988,42.242879143198664C103.41431057327972,76.34704239035025,93.79494320519305,170.9812938413882,81.28167332365135,250.07896920659033C70.17666984294237,320.27484674793965,64.84698225790005,396.69656628748305,111.28512138212992,450.4950937839243C156.20124167950087,502.5303643271138,231.32542653798444,500.4755392045468,300,503.46388370962813"></path>
                </svg>
                <i class="bx bx-layer"></i>
              </div>
              <h4><a href="">Nemo Enim</a></h4>
              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box iconbox-red">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,532.3542879108572C369.38199826031484,532.3153073249985,429.10787420159085,491.63046689027357,474.5244479745417,439.17860296908856C522.8885846962883,383.3225815378663,569.1668002868075,314.3205725914397,550.7432151929288,242.7694973846089C532.6665558377875,172.5657663291529,456.2379748765914,142.6223662098291,390.3689995646985,112.34683881706744C326.66090330228417,83.06452184765237,258.84405631176094,53.51806209861945,193.32584062364296,78.48882559362697C121.61183558270385,105.82097193414197,62.805066853699245,167.19869350419734,48.57481801355237,242.6138429142374C34.843463184063346,315.3850353017275,76.69343916112496,383.4422959591041,125.22947124332185,439.3748458443577C170.7312796277747,491.8107796887764,230.57421082200815,532.3932930995766,300,532.3542879108572"></path>
                </svg>
                <i class="bx bx-slideshow"></i>
              </div>
              <h4><a href="">Dele Cardo</a></h4>
              <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box iconbox-teal">
              <div class="icon">
                <svg width="100" height="100" viewBox="0 0 600 600" xmlns="http://www.w3.org/2000/svg">
                  <path stroke="none" stroke-width="0" fill="#f5f5f5" d="M300,566.797414625762C385.7384707136149,576.1784315230908,478.7894351017131,552.8928747891023,531.9192734346935,484.94944893311C584.6109503024035,417.5663521118492,582.489472248146,322.67544863468447,553.9536738515405,242.03673114598146C529.1557734026468,171.96086150256528,465.24506316201064,127.66468636344209,395.9583748389544,100.7403814666027C334.2173773831606,76.7482773500951,269.4350130405921,84.62216499799875,207.1952322260088,107.2889140133804C132.92018162631612,134.33871894543012,41.79353780512637,160.00259165414826,22.644507872594943,236.69541883565114C3.319112789854554,314.0945973066697,72.72355303640163,379.243833228382,124.04198916343866,440.3218312028393C172.9286146004772,498.5055451809895,224.45579914871206,558.5317968840102,300,566.797414625762"></path>
                </svg>
                <i class="bx bx-arch"></i>
              </div>
              <h4><a href="">Divera Don</a></h4>
              <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
            </div>
          </div> -->
        </div>
      </div>
    </section>
    <!-- End Requerimientos Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta" style="display:none;">
      <div class="container" data-aos="zoom-in">
        <div class="text-center">
          <h3>Llamada a la acción</h3>
          <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <a class="cta-btn" href="#">Llamada a la acción</a>
        </div>
      </div>
    </section>
    <!-- End Cta Section -->

    <!-- ======= Preguntas frecuentes Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Preguntas Frecuentes</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>
        <div class="faq-list">
          <ul>
            <!--Pregunta 1-->
            <li data-aos="fade-up">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#faq-list-1"> Qué son los Certificados de sello digital?  <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-parent=".faq-list">
                <p>
                  Son Archivos emitidos por el Sat, que se utilizan generar comprobantes fiscales digitales.
                </p>
                <p>
                  Son las credenciales del contribuyente para que pueda emitir CFDIs con un proveedor.
                </p>
              </div>
            </li>
            <!--Pregunta 2-->
            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-2" class="collapsed"> Cómo saber el vencimiento de mis certificados?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                <p>
                  En el portal FacturaTuNota se muestra la vigencia, y notifica al usuario cuando se aproxima el vencimiento.
                </p>
              </div>
            </li>
            <!--Pregunta 3-->
            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-3" class="collapsed"> Los Folios adquiridos tienen vigencia?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                <p>
                  No, nuestros paquetes de folios tienen vigencia ilimitada, con nosotros los folios no se vencen.
                </p>
              </div>
            </li>
            <!--Pregunta 4-->
            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-4" class="collapsed"> Qué haría cuando se me terminen los folios? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                <p>
                  Será necesario adquirir un nuevo paquete de folios, mismos que pueden ser adquiridos desde el portal de FacturaTuNota y realizar el pago en línea.
                </p>
              </div>
            </li>
            <!--Pregunta 5-->
            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-5" class="collapsed"> Cómo saber cuántos folios me quedan? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                <p>
                  El portal muestra en su menú el número de folios que quedan disponibles.
                </p>
              </div>
            </li>
            <!--Pregunta 6-->
            <li data-aos="fade-up" data-aos-delay="500">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-6" class="collapsed"> Debo pagar una anualidad por el servicio? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-6" class="collapse" data-parent=".faq-list">
                <p>
                  Sí, es necesario cubrir una cuota anual de $999 (novecientos noventa y nueve pesos), la cual cubre el alojamiento del portal en la web, el alojamiento de los comprobantes y las actualizaciones que sean necesarias.
                </p>
              </div>
            </li>
            <!--Pregunta 7-->
            <li data-aos="fade-up" data-aos-delay="600">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#faq-list-7" class="collapsed"> Por cuanto tiempo permanecen las facturas disponibles? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-7" class="collapse" data-parent=".faq-list">
                <p>
                  La ley obliga a los proveedores de factura, el mantener disponibles los comprobantes de sus clientes en un periodo mínimo de 6 meses, con nuestros servicios los comprobantes están disponibles de manera permanente, como un beneficio más de la anualidad.
                </p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
    <!-- End Preguntas frecuentes Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Contacto</h2>
          <!--    <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>
        <div>
          <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3621.225267892409!2d-107.43780058499792!3d24.821968784072112!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjTCsDQ5JzE5LjEiTiAxMDfCsDI2JzA4LjIiVw!5e0!3m2!1ses!2sve!4v1599736415906!5m2!1ses!2sve" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="row mt-5">
          <div class="col-lg-4">
            <div class="info">
              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Localización:</h4>
                <p>Calle de la tuna 3666, Fracc San Florencio, Culiacan Sinaloa, CP 80058</p>
              </div>
              <div class="email">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <p>contacto{{ '@'.session()->get('Configuracion.Web.titulo') }}.com</p>
              </div>
              <div class="phone">
                <i class="icofont-whatsapp"></i>
                <h4>Teléfonos:</h4>
                <p>6672 93 86 74</p>
                <p>6672 856 453</p>
              </div>
            </div>
          </div>
          <div class="col-lg-8 mt-5 mt-lg-0">
            <form action="#" method="post" role="form" class="php-email-form">
              <input type="hidden" id="valido" value="0">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Correo electrónico" data-rule="email" data-msg="Por favor introduzca una dirección de correo electrónico válida" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Nro. de Télefono" data-rule="minlen:11" data-msg="Ingrese nro. teléfonico válido" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Ingrese al menos 8 caracteres de asunto" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" id="message" name="message" rows="5" data-rule="required" data-msg="Por favor escribe algo para nosotros" placeholder="Mensaje"></textarea>
                <div class="validate"></div>
              </div>
              <div class="col-md-12">
                <div class="text-center">
                  <div class="g-recaptcha" data-sitekey="6Lfl0jsaAAAAAOhcqxjd_wL7GbZCXV6qvoxYsFXe" data-callback="finalizadocaptcha" data-expired-callback="nofinalizadocaptcha"></div>
                </div>
              </div>
              <div class="mb-3">
                <div id="loading" class="loading">Cargando</div>
                <div id="error-message" class="error-message"></div>
                <div id="sent-message" class="sent-message">Tu mensaje ha sido enviado. ¡Gracias!</div>
              </div>
              <div class="text-center"><button type="button" onclick="enviarcontacto();" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Enviar Mensaje</button></div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- End Contact Section -->
  </main>

  <!-- ======= Footer ======= -->
    @include('elementos.public_footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
  <!-- FIN DE SWEETALERT2-->
  <script type="text/javascript">
      var finalizadocaptcha = function(response) {
        $('#valido').val(1);
      };
      var nofinalizadocaptcha = function(response) {
        $('#valido').val(0);
      };

      /**
       * Funcion que permite verificar si correo ya existe
       */
      function enviarcontacto(){
          var error=0;
          var name = $("#name").val();
          if(name=== "undefined" || name=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '¡Nombre!',
                text: '¡Debe ingresar un nombre!'
              });
          }
          var email = $("#email").val();
          if(email=== "undefined" || email=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '¡Correo electrónico!',
                text: '¡Debe ingresar un email!'
              });
          }else{
              var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
              if ( !expr.test(email) ){
                error=1;
                Swal.fire({
                  type: 'warning',
                  title: '¡Correo electrónico!',
                  text: '¡Debe ingresar un email válido!'
                })
              }
          }
          var phone = $("#phone").val();
          if(phone=== "undefined" || phone=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '¡Teléfono!',
                text: '¡Debe ingresar un teléfono!'
              });
          }
          var subject = $("#subject").val();
          if(subject=== "undefined" || subject=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '¡Asunto!',
                text: '¡Debe ingresar un asunto!'
              });
          }
          var message = $("#message").val();
          if(message=== "undefined" || message=== ""){
              error=1;
              Swal.fire({
                type: 'warning',
                title: '¡Mensaje!',
                text: '¡Debe ingresar un mensaje!'
              });
          }

          var valido = $('#valido').val();
          if(valido==0){
            error=1;
            Swal.fire({
              type: 'warning',
              title: '¡Captcha!',
              text: '¡Debe completa el captcha!'
            });
          }


          if(error==0){
            $.ajax({
                type: "POST",
                url: "{{ route('enviarcontacto') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "email": email,
                    "name":name,
                    "phone":phone,
                    "subject":subject,
                    "message":message,
                },
                dataType:"json",
                beforeSend: function() {
                  $('#loading').show();
                  $('#error-message').hide();
                  $('#sent-message').hide();
                },
                success: function(respuesta) {
                  $('#loading').hide();
                  if(respuesta.error==1){
                    $('#sent-message').hide();
                    $('#error-message').html(respuesta.mensaje);
                    $('#error-message').show();
                  }else{
                    $('#sent-message').hide();
                    $('#error-message').hide();
                    $('#sent-message').show();
                  }
                },
                error: function() {
                  $('#loading').hide();
                  $('#sent-message').hide();
                  $('#error-message').html("No pudo enviarse el mensaje, intente de nuevo más tarde");
                  $('#error-message').show();
                }
            });
          }
      }

      $(document).ready(function() {
        $("#valido").val(0);
      });
  </script>

  <script src="https://www.google.com/recaptcha/api.js"></script>
@endsection


