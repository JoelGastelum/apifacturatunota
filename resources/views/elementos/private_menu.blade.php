            <nav id="sidebar" aria-label="Main Navigation">
                <!-- Side Header (mini Sidebar mode) -->
                <div class="smini-visible-block">
                    <div class="content-header bg-header-dark">
                        <!-- Logo -->
                        <a class="link-fx font-size-lg text-white" href="{{ route('home') }}">
                            <span class="text-white-75"> <?php echo session()->get('Configuracion.Web.abreviatura');?></span>
                        </a>
                        <!-- END Logo -->
                    </div>
                </div>
                <!-- END Side Header (mini Sidebar mode) -->

                <!-- Side Header (normal Sidebar mode) -->
                <div class="smini-hidden">
                    <div class="content-header justify-content-lg-center bg-header-dark">

                        <a class="navbar-brand text-center" href="{{ route('home') }}">
                            <span class="text-center" style="display: inline-block;line-height: 2em;position: relative;vertical-align: middle;width: 8.0em;font-size: 22px;">
                                <img class="img-fluid" src="{{ asset('storage/'.session()->get('Configuracion.Web.logo')) }}"/>
                            </span>
                        </a>
                        <!-- END Logo -->

                        <!-- Options -->
                        <div class="d-lg-none">
                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <a class="text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                                <i class="fa fa-times-circle"></i>
                            </a>
                            <!-- END Close Sidebar -->
                        </div>
                        <!-- END Options -->
                    </div>
                </div>
                <!-- END Side Header (normal Sidebar mode) -->

                <!-- Side Actions -->
                <div class="content-side content-side-full text-center bg-body-light">
                    <div class="smini-hide">
                        <img class="img-avatar" src="{{ asset('storage/'.auth()->user()->foto_perfil) }}" alt="Avatar">
                        <div class="mt-3 font-w600">
                            <?php
                                echo auth()->user()->login;
                            ?>
                            <?php if(auth()->user()->idtipousuario!=4):?>
                                <?php if(isset(auth()->user()->empresa->nombrecomercial)):?>
                                    <br>
                                    <?php echo (auth()->user()->empresa->nombrecomercial);?>
                                <?php endif;?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
                <!-- END Side Actions -->

                <!-- Menu de Navigación a la derecha -->
                    <div class="content-side content-side-full">
                        <ul class="nav-main">
                            <!-- Escritorio -->
                                <?php if(auth()->user()->idtipousuario!=4 && auth()->user()->idtipousuario!=5):?>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link <?php echo (Route::currentRouteName() == 'home') ? 'active' : ''; ?>" href="{{ route('home') }}">
                                            <i class="nav-main-link-icon fas fa-tachometer-alt"></i>
                                            <span class="nav-main-link-name">Escritorio</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                            <!-- Fin de Escritorio -->
                            <?php echo session()->get('Menu');?>
                        </ul>
                    </div>
                <!-- Fin de Menu de Navigación a la derecha -->
            </nav>
