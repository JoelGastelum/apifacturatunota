<header id="header" class="fixed-top">
  <div class="container d-flex align-items-center">
    <!-- <a href="index.html"><img class="logo mr-auto" src="assets/img/logo.png" alt="" width="100" height="17"></a> -->
    <!-- <h1 class="logo mr-auto"><a href="index.html">Factura tu nota</a></h1> -->
    <h1 class="logo mr-auto" style="">
      <a href="{{ route('welcome') }}">
        <img src="{{ asset('publica/img/logo.png') }}" alt="{{ session()->get('Configuracion.Web.titulo') }}" class="responsive" >
      </a>
    </h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
    <nav class="nav-menu d-none d-lg-block" id="menu-header">
      <ul>
        <li><a href="{{ route('welcome') }}/#inicio">Inicio</a></li>
        <li><a href="{{ route('welcome') }}/#contact">Contacto</a></li>
        <li><a href="{{ route('login') }}">Iniciar sesión</a></li>
      </ul>
    </nav><!-- .nav-menu -->
    <!--<a href="#about-video" class="get-started-btn scrollto">Empezar</a>-->
  </div>
</header>
