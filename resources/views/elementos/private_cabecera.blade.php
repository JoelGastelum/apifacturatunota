                <header id="page-header">
                    <!-- Header Content -->
                    <div class="content-header">
                        <!-- Left Section -->
                        <div>
                            <!-- Toggle Sidebar -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                            <?php
                                if(isset($rutaAnt)){
                            ?>
                                <a href="{{$rutaAnt}}">
                                    <button  type="button" class="btn btn-dual mr-1" >
                                        <i class="fas fa-chevron-left"></i>
                                    </button>
                                </a>
                            <?php
                               }else{
                            ?>
                                <button id="boton_esconder_menu" type="button" class="btn btn-dual mr-1" data-toggle="layout" data-action="sidebar_toggle">
                                    <i class="fa fa-fw fas fa-bars"></i>
                                </button>
                            <?php
                               }
                            ?>
                            <!-- END Toggle Sidebar -->
                            <?php if(isset($tituloencabezado)):?>
                                <strong style="color:#fff;">{{ $tituloencabezado }}</strong>
                            <?php endif;?>
                        </div>
                        <!-- END Left Section -->

                        <!-- Right Section -->
                        <div>
                            <?php if(isset($accionencabezado)):?>
                                <?php if(count($accionencabezado)>0):?>
                                    <?php foreach ($accionencabezado as $accion):?>
                                        <i style="cursor:pointer;font-size: 1.3em;" class="{{$accion['icono']}}" onclick="{{$accion['funcion']}}"></i>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endif;?>

                            <?php if(auth()->user()->idtipousuario==2):?>
                                <span class="badge badge-info badge-pill"><div class="d-none d-sm-block">Membresía:</div> {{ date('d-m-Y',strtotime(auth()->user()->empresa->fechavigencia)) }}</span>
                                <?php if(isset(auth()->user()->empresa->folioscomprados) && isset(auth()->user()->empresa->foliosconsumidos)):?>
                                    <?php $class_folios='success';?>
                                    <?php if(((int)auth()->user()->empresa->folioscomprados - (int)auth()->user()->empresa->foliosconsumidos)<=20):?>
                                        <?php $class_folios='warning';?>
                                        <?php if(((int)auth()->user()->empresa->folioscomprados - (int)auth()->user()->empresa->foliosconsumidos)<=5):?>
                                                <?php $class_folios='danger';?>
                                        <?php endif;?>
                                    <?php endif;?>
                                    <span class="badge badge-{{$class_folios}} badge-pill"><div class="d-none d-sm-block">Folios disponibles:</div> {{ ((int)auth()->user()->empresa->folioscomprados - (int)auth()->user()->empresa->foliosconsumidos) }}</span>
                                <?php endif;?>
                            <?php endif;?>

                            <!-- BOTON DE PERFIL -->
                            <div class="dropdown d-inline-block">
                                <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="far fa-fw fa-user-circle"></i>
                                    <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown">
                                    <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                                        <img class="img-avatar img-avatar48 img-avatar-thumb" src="{{ asset('storage/'.auth()->user()->foto_perfil) }}" alt="Avatar">
                                        <div class="pt-2">
                                            <a class="text-white font-w600" href="javascript:void(0)">
                                                <?php
                                                    echo auth()->user()->login;
                                                ?>
                                                <?php if(isset(auth()->user()->empresa->nombrecomercial)):?>
                                                    <br>
                                                    <?php echo (auth()->user()->empresa->nombrecomercial);?>
                                                <?php endif;?>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="p-2">
                                        <a class="dropdown-item" href="{{route('users.privacidad')}}">
                                            <i class="fa fa-fw fa-cog mr-1"></i> Mi perfil
                                        </a>
                                        <a class="dropdown-item" href="{{route('usuarios.cambiarcontrasena')}}">
                                            <i class="fas fa-fw fa-key mr-1"></i> Cambiar contraseña
                                        </a>
                                        <div role="separator" class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#" onclick="cerrarsesion();">
                                            <i class="fas fa-fw fa-sign-out-alt mr-1"></i> Cerrar Sesión
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- FIN DE BOTON DE PERFIL -->
                        </div>
                        <!-- END Right Section -->
                    </div>
                    <!-- END Header Content -->

                    <!-- Header Loader -->
                    <div id="page-header-loader" class="overlay-header bg-primary-dark">
                        <div class="content-header">
                            <div class="w-100 text-center">
                                <i class="fa fa-fw fa-2x fa-sun fa-spin text-white"></i>
                            </div>
                        </div>
                    </div>
                    <!-- END Header Loader -->
                    <script>
                        function cerrarsesion(){
                            $.ajax({
                                type: "POST",
                                url: "{{ route('eliminartoken') }}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "remember_token": "{{ auth()->user()->remember_token }}",
                                },
                                dataType:"json",
                                success: function(respuesta) {
                                    localStorage.removeItem('remember_token');
                                    // event.preventDefault();
                                    document.getElementById('logout-form').submit();
                                },
                                error: function() {
                                }
                            });
                        }
                    </script>
                </header>
