            <footer class="bg-body-light" style="display:none">
                <div class="row" style="margin-right: 0px;">
                    <div class="col-12 text-center">
                        <p style="padding-top: 30px;">
                            Copyright &copy; <?php echo date("Y").' '.session()->get('Configuracion.Web.nombre');?>. Todos los Derechos Reservados.<br>
            				Desarrollado por <a href="http://fesoluciones.com.mx/" target="_blank">Fe Soluciones</a>
                       </p>
                      </div>
                </div>
            </footer>
