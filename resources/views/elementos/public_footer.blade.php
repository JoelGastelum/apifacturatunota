<footer id="footer" style="display: none;">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 footer-contact">
          <h3>{{ session()->get('Configuracion.Web.titulo') }}</h3>
          <p>
            Calle de la tuna 3666,<br>
            Fracc San Florencio,<br>
            Culiacan Sinaloa, CP 80058 <br><br>
            <strong>Teléfonos:</strong> 6672 93 86 74<br>
            <strong>Teléfonos:</strong> 6672 856 453<br>
            <strong>Email:</strong> contacto{{ '@'.session()->get('Configuracion.Web.titulo') }}.com<br>
          </p>
        </div>
        <div class="col-lg-2 col-md-6 footer-links">
          <h4>Enlaces útiles</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#inicio">Inicio</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#about-video">Para tí</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#requerimientos">Requerimientos</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#registro">Registrate</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#planes">Planes</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#leerqr">Lectora QR</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('welcome') }}/#contact">Contacto</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-6 footer-links">
          <h4>Nuestros servicios</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Diseño web</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Desarrollo web</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Gestión de productos</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Márketing</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Diseño gráfico</a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-6 footer-newsletter">
          <h4>Suscríbase a nuestro boletín</h4>
          <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
          <form action="" method="post">
            <input type="email" name="email"><input type="submit" value="Subscribe">
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="container d-md-flex py-4" style="position:sticky;">
    <div class="mr-md-auto text-center text-md-left">
      <div class="copyright">
        {{ date("Y") }} &copy; Copyright <strong><span>{{ session()->get('Configuracion.Web.titulo') }}</span></strong>. Todos los derechos reservados
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/onepage-multipurpose-bootstrap-template/ -->
        Desarrollado por <a href="http://fesoluciones.com.mx/" target="_blank">Fe Soluciones</a>
      </div>
    </div>
    <div class="social-links text-center text-md-right pt-3 pt-md-0">
      <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
      <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
      <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
      <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
      <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
    </div>
  </div>
</footer>
