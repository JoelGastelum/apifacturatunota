@extends('plantillas.publica')
@section('content')
    <!-- ======= Header ======= -->
      @include('elementos.public_header')
    <!-- End Header -->

    <!-- ======= Login ======= -->
    <section id="contact" class="contact" style="display: none;">
      <br>
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Iniciar sesión</h2>
        </div>
        <div class="row mt-5">
          <div class="col-lg-8 mt-5 mt-lg-0 offset-lg-2">
            <form action="{{ route('login') }}" method="post" role="form">
              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="text" name="login" class="form-control" id="login" placeholder="Nombre de usuario" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                  <div class="validate"></div>
                </div>
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" required/>
                  <div class="validate"></div>
                </div>
              </div>
              <?php if(session('danger')):?>
                 <div class="mb-3">
                    <div class="error-message" style="color: #fff;background: #ed3c0d;text-align: left;padding: 15px;font-weight: 600;">
                     <?php echo session('danger');?>
                    </div>
                 </div>
              <?php endif;?>
              <?php if(session('success')):?>
                 <div class="mb-3">
                    <div class="error-message" style="color: #fff;background: #53B031;text-align: left;padding: 15px;font-weight: 600;">
                     <?php echo session('success');?>
                    </div>
                 </div>
              <?php endif;?>
              <div class="text-center">
                <button type="submit" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                  Iniciar sesión
                </button>
                <a href="{{ route('password.request') }}">
                  <button type="button" style="margin-top:10px; background: #17188C;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                    Recuperar mi contraseña
                  </button>
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!--FORMULARIO ESCONDIDO PARA INICIAR SESION-->
    <form id="form-loginauto" action="{{ route('loginauto') }}" method="POST" style="display: none;">
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id="iduser" name="iduser" value="-1">
        <input type="hidden" id="idremember_token" name="remember_token" value="-1">
    </form>

    <!-- End Login -->

    <!-- ======= Footer ======= -->
      @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <script>
      /**
       * Verificar si existe un valor en el localstorage para autologueo
       */
      function buscartoken(){
        // localStorage.setItem('tokenlogin', '505e5a863c4781e99ea3289fc7444c4911200a3534c6555f009591abdaa3ddb5f1a6591797129cd1f54e2b9a1cb0d4f780cf046641c0ddfcdf1847594f02be7f');
        var remember_token = localStorage.getItem('remember_token');
        // console.log(localStorage.getItem('remember_token'));
        // $("#tokenver").html(remember_token);
        if(remember_token!=null){
          $.ajax({
            type: "POST",
            url: "{{ route('buscartoken') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "remember_token": remember_token,
            },
            dataType:"json",
            success: function(respuesta) {
              if(respuesta.encontrado==1){
                if(respuesta.idtipousuario!=2){
                  //INICIAR SESION AUTOMATICAMENTE
                  $("#iduser").val(respuesta.iduser);
                  $("#idremember_token").val(remember_token);
                  $("#form-loginauto").submit();
                }else{
                  $("#contact").show();
                }
              }else{
                $("#contact").show();
              }
            },
            error: function() {
            }
          });
        }else{
          $("#contact").show();
        }
      }

      $(document).ready(function(){
        buscartoken();
      });
    </script>
@endsection
