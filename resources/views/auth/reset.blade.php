@extends('plantillas.publica')
@section('content')
    <!-- ======= Header ======= -->
    @include('elementos.public_header')
      <br>
      <br>
      <br>
      <br>
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Recuperar Contraseña</h2>
        </div>
        <div class="row mt-5">
          <div class="col-lg-8 mt-5 mt-lg-0 offset-lg-2">
          <form action="{{ route('enviarPass') }}" method="post" role="form">
              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="email" name="correo" class="form-control" id="correo" placeholder="Correo del usuario"  required/>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="login" name="login" class="form-control" id="login" placeholder="Login del usuario"  required/>
                  <div class="validate"></div>
                </div>
              </div>
              <?php if(session('danger')):?>
                 <div class="mb-3">
                    <div class="error-message" style="color: #fff;background: #ed3c0d;text-align: left;padding: 15px;font-weight: 600;">
                     <?php echo session('danger');?>
                    </div>
                 </div>
              <?php endif;?>
              <div class="text-center">
                <button type="submit" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Enviar Correo</button>
              </div>
            </form>
          </div>
        </div>
      </div>

@include('elementos.public_footer')
@endsection
