@extends('plantillas.privada')
@section('content')
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('ordenesporaprobar.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::model($objOrden, ['action' => ['OrdenesporaprobarController@saprobar'], 'id' => 'EditForm','method' => 'post','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="id" name="id" value="{{ $objOrden->id }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group  {{ $errors->has('metodopago') ? ' has-error' : '' }}">
                            {{ Form::label('metodopago', 'Método de pago') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'metodopago',
                                        $metodopago[$objOrden->metodopago],
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'metodopago',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('metodopago'))
                            <span class="help-block">
                                <strong>{{ $errors->first('metodopago') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  {{ $errors->has('updated_at') ? ' has-error' : '' }}">
                            {{ Form::label('updated_at', 'Fecha') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'updated_at',
                                        date('d-m-Y',strtotime($objOrden->updated_at)),
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'updated_at',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('updated_at'))
                            <span class="help-block">
                                <strong>{{ $errors->first('updated_at') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group  {{ $errors->has('titulo') ? ' has-error' : '' }}">
                            {{ Form::label('titulo', 'Título') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'titulo',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'titulo',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('titulo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('titulo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group  {{ $errors->has('razonsocial') ? ' has-error' : '' }}">
                            {{ Form::label('razonsocial', 'Razón Social') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'razonsocial',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'razonsocial',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('razonsocial'))
                            <span class="help-block">
                                <strong>{{ $errors->first('razonsocial') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('rfc') ? ' has-error' : '' }}">
                            {{ Form::label('rfc', 'Rfc') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'rfc',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'rfc',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('rfc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('rfc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">
                            {{ Form::label('email', 'Email') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'email',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'email',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('folio') ? ' has-error' : '' }}">
                            {{ Form::label('folio', 'Folio') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'folio',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'folio',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('folio'))
                            <span class="help-block">
                                <strong>{{ $errors->first('folio') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('duracion') ? ' has-error' : '' }}">
                            {{ Form::label('duracion', 'Duración') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'duracion',
                                        $objOrden->duracion .' meses',
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'duracion',
                                            'disabled'=>true,
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('duracion'))
                            <span class="help-block">
                                <strong>{{ $errors->first('duracion') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('iva_monto') ? ' has-error' : '' }}">
                            {{ Form::label('iva_monto', 'IVA') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'iva_monto',
                                        number_format($objOrden->iva_monto,2,'.',','),
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'iva_monto',
                                            'disabled'=>true,
                                            'style'=>'text-align:right;',
                                            'align'=>"right"
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('iva_monto'))
                            <span class="help-block">
                                <strong>{{ $errors->first('iva_monto') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('subtotal') ? ' has-error' : '' }}">
                            {{ Form::label('subtotal', 'Subtotal') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'subtotal',
                                        number_format($objOrden->subtotal,2,'.',','),
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'subtotal',
                                            'disabled'=>true,
                                            'style'=>'text-align:right;',
                                            'align'=>"right"
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('subtotal'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subtotal') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group  {{ $errors->has('total') ? ' has-error' : '' }}">
                            {{ Form::label('total', 'Total') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'total',
                                        number_format($objOrden->total,2,'.',','),
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'total',
                                            'disabled'=>true,
                                            'style'=>'text-align:right;',
                                            'align'=>"right"
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('total'))
                            <span class="help-block">
                                <strong>{{ $errors->first('total') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th style="width: 40%;">Servicio</th>
                                        <th style="width: 20%;text-align: right;">Iva</th>
                                        <th style="width: 20%;text-align: right;">Subtotal</th>
                                        <th style="width: 20%;text-align: right;">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $iva_monto=0;
                                        $subtotal=0;
                                        $total=0;
                                    ?>
                                    <?php foreach ($objOrden->ordenesdetalle as $ordenesdetalle): ?>
                                        <tr>
                                            <td>
                                                <?php
                                                    if(isset($ordenesdetalle->servicio->producto)){
                                                        echo $ordenesdetalle->servicio->producto;
                                                    }
                                                ?>
                                            </td>
                                            <td style="text-align: right;">
                                                <?php $iva_monto += $ordenesdetalle->iva_monto;?>
                                                {{ number_format($ordenesdetalle->iva_monto,2,'.',',') }}
                                            </td>
                                            <td style="text-align: right;">
                                                <?php $subtotal += $ordenesdetalle->subtotal;?>
                                                {{ number_format($ordenesdetalle->subtotal,2,'.',',') }}
                                            </td>
                                            <td style="text-align: right;">
                                                <?php $total += $ordenesdetalle->total;?>
                                                {{ number_format($ordenesdetalle->total,2,'.',',') }}
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                                <tfooter>
                                    <tr>
                                        <td style="text-align: right;">
                                            Total
                                        </td>
                                        <td style="text-align: right;">
                                            {{ number_format($iva_monto,2,'.',',') }}
                                        </td>
                                        <td style="text-align: right;">
                                            {{ number_format($subtotal,2,'.',',') }}
                                        </td>
                                        <td style="text-align: right;">
                                            {{ number_format($total,2,'.',',') }}
                                        </td>
                                    </tr>
                                </tfooter>
                            </table>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <!-- NOTIFICACIONES-->
        <script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
        <script>jQuery(function(){ Dashmix.helpers('notify'); });</script>
    <!-- FIN DE NOTIFICACIONES-->
    <script type="text/javascript">
        $(document).ready(function() {
        });
    </script>
@endsection

