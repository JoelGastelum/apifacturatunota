@extends('plantillas.privada')
@section('content')
    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

   <!-- Mensajes-->
        @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <br>
    <br>
    <div class="">
        <div class="block-content block-content-full">
            {{ Form::model($User, ['action' => ['PrivacidadesController@sprivacidad'], 'id' => 'UserEditForm','method' => 'post','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                {!! Form::hidden('id', $User->id) !!}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-3">
                        <div class="text-center">
                            <img style="border-radius: 45%;" class="img-fluid" data-toggle="modal" data-target="#modal-img" src="{{ asset('storage/'.$User->foto_perfil) }}" alt="Avatar">
                            <div class="h4">{{$User->login}}</div>
                        </div>
                    </div>
                </div>
              <!--  <div class="row">
                    <div class="col-md-12">
                        <div class="text-left">
                           <a href="{{route('usuarios.cambiarcontrasena')}}"> 
                                <button type="button"  class="btn btn-warning btn-sm">
                                    <li class="fas fa-file-signature"></li>
                                    Cambiar Contraseña
                                </button>
                            </a>
                            <button type="button"   class="btn btn-primary btn-sm">
                                <li class="far fa-images"></li>
                            </button>
                        </div>
                       
                    </div>
                </div>-->
                <h2 class="content-heading">Información Personal</h2>
                <div class="">
                    <div class="col-md-12">
                        <div class="form-group required{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            {{ Form::label('nombre', 'Nombre') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-user"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'nombre',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'nombre',
                                            'required'=>true,
                                            'placeholder'=>'Nombre',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                        ]
                                    )
                                }}
                            </div>
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            {{ Form::label('email', 'Email') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                </div>
                                {{
                                    Form::email(
                                        'email',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'email',
                                            'placeholder'=>'Email',
                                            'minlength'=>'3',
                                            'maxlength'=>'255',
                                        ]
                                    )
                                }}
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group {{ $errors->has('movil') ? ' has-error' : '' }}">
                            {{ Form::label('movil', 'Móvil') }}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fas fa-mobile-alt"></i>
                                    </span>
                                </div>
                                {{
                                    Form::text(
                                        'movil',
                                        null,
                                        [
                                            'class'=>'form-control',
                                            'id'=>'movil',
                                            'placeholder'=>'Móvil',
                                        ]
                                    )
                                }}
                            </div>
                            @if ($errors->has('movil'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('movil') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="boton">
                                <li class=" fas fa-save"></li>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>

    <div class="modal fade" id="modal-img" tabindex="-1" role="dialog" aria-labelledby="modal-block-fadein" aria-hidden="true">
        <div class="modal-dialog modal-dialog-fadein modal-lg" role="document">
            <div class="modal-content" id="capacitydetail_contenido">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">
                            Cambiar Foto
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('foto_perfil') ? ' has-error' : '' }}">
                                <input id="foto_perfil" name="foto_perfil" type="file" class="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione un avatar" data-allowed-file-extensions='["jpg","jpeg","png","gif"]'>
                                <script>
                                    $("#foto_perfil").fileinput({
                                        language: "es",
                                        theme: "fas",
                                        allowedFileExtensions: ["jpg","jpeg", "png", "gif"],
                                        /*maxFileSize: 3072,
                                        minImageWidth: 232,
                                        minImageHeight: 155,
                                        maxImageWidth: 650,
                                        maxImageHeight: 433,*/
                                        maxFileCount: 1
                                    });
                                </script>
                                @if ($errors->has('foto_perfil'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('foto_perfil') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row" style="margin-bottom:20px">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-success" id="boton">
                                            <li class=" fas fa-save"></li>
                                            Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

            {{ Form::close() }}



            
            <!-- SWEETALERT2-->
                <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->
            <script type="text/javascript">
                /**
                 * Permite mostrar contrasena visualmente
                 *
                 */
                function mostrarPassword(){
                    var pass = document.getElementById("password");
                    var recontrasena = document.getElementById("recontrasena");
                    if((pass.type == "password")&&(recontrasena.type == "password")){
                        pass.type = "text";
                        recontrasena.type = "text";
                        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    }else{
                        pass.type = "password";
                        recontrasena.type = "password";
                        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    }
                }

                $('#password').keypress(function(event){
                    if($('#password').val().length<1){
                        $('#boton').prop("disabled",true);
                        $('#alertPasswordCanti').removeClass('d-none');
                    }else{
                        $('#alertPasswordCanti').addClass('d-none');
                        $('#boton').prop("disabled",false);
                    }
                });
                $('#recontrasena').focusout(function(event){
                    if($('#password').val()!=$('#recontrasena').val()){
                        Swal.fire({
                          type: 'warning',
                          title: 'Password do not match!',
                          text: 'The supplied passwords do not match, please check!'
                        });
                        $('#boton').prop("disabled",true);
                    }else{
                        $('#alertPassword').addClass('d-none');
                        $('#boton').prop("disabled",false);
                    }
                });

                $(document).ready(function() {
                });
            </script>
        </div>
    </div>
@endsection


<!--
                    <div class="col-md-5">
                        <div class="form-group{{ $errors->has('foto_perfil') ? ' has-error' : '' }}">
                            <input id="foto_perfil" name="foto_perfil" type="file" class="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione un avatar" data-allowed-file-extensions='["jpg","jpeg","png","gif"]'>
                            <script>
                                $("#foto_perfil").fileinput({
                                    language: "es",
                                    theme: "fas",
                                    allowedFileExtensions: ["jpg","jpeg", "png", "gif"],
                                    /*maxFileSize: 3072,
                                    minImageWidth: 232,
                                    minImageHeight: 155,
                                    maxImageWidth: 650,
                                    maxImageHeight: 433,*/
                                    maxFileCount: 1
                                });
                            </script>
                            @if ($errors->has('foto_perfil'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_perfil') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    -->