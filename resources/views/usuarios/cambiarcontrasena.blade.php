@extends('plantillas.privada')
@section('content')
    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
<div class="  ">
    <div class="block-content block-content-full">
        <div class="col-md-6">
            {{ Form::model($User, ['action' => ['PrivacidadesController@saveContra'], 'id' => 'UserEditForm','method' => 'post','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
            {!! Form::hidden('id', $User->id) !!}
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        {{ Form::label('passwordact', 'Password Actual') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-key"></i>
                                </span>
                            </div>
                            {{
                                Form::password(
                                    'passwordact',
                                    [
                                        'class'=>'form-control',
                                        'id'=>'passwordact',
                                        'placeholder'=>'Password Actual',
                                        'minlength'=>'1',
                                        'maxlength'=>'255',
                                    ]
                                )
                            }}
                            <div class="input-group-prepend" onclick="mostrarPassword();">
                                <span class="input-group-text"  style="cursor:pointer">
                                    <i class="fa fa-eye-slash icon"></i>
                                </span>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        {{ Form::label('password', 'Password') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-key"></i>
                                </span>
                            </div>
                            {{
                                Form::password(
                                    'password',
                                    [
                                        'class'=>'form-control',
                                        'id'=>'password',
                                        'placeholder'=>'Password',
                                        'minlength'=>'1',
                                        'maxlength'=>'255',
                                    ]
                                )
                            }}
                            <div class="input-group-prepend" onclick="mostrarPassword();">
                                <span class="input-group-text"  style="cursor:pointer">
                                    <i class="fa fa-eye-slash icon"></i>
                                </span>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('recontrasena') ? ' has-error' : '' }}">
                        {{ Form::label('recontrasena', 'Confirme Password') }}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-key"></i>
                                </span>
                            </div>
                            {{
                                Form::password(
                                    'recontrasena',
                                    [
                                        'class'=>'form-control',
                                        'id'=>'recontrasena',
                                        'placeholder'=>'Confirme password',
                                        'minlength'=>'1',
                                        'maxlength'=>'255',
                                    ]
                                )
                            }}
                            <div class="input-group-prepend"  onclick="mostrarPassword();"> 
                                <span class="input-group-text" style="cursor:pointer">
                                    <i class="fa fa-eye-slash icon"></i>
                                </span>
                            </div>
                        </div>
                        @if ($errors->has('recontrasena'))
                            <span class="help-block">
                                <strong>{{ $errors->first('recontrasena') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div id="alertPasswordCanti" class="col-sm-12 d-none">
                        <div class="alert alert-danger"><div>Contraseña no segura.</div></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                            <button type="Submit"  class="btn btn-success btn-sm">
                                <li class="fas fa-save"></li>
                                Guardar
                            </button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<!-- SWEETALERT2-->
<link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
                <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
            <!-- FIN DE SWEETALERT2-->
            <script type="text/javascript">
                /**
                 * Permite mostrar contrasena visualmente
                 *
                 */
                function mostrarPassword(){
                    var pass = document.getElementById("password");
                    var passact = document.getElementById("passwordact");
                    var recontrasena = document.getElementById("recontrasena");
                    if((pass.type == "password")&&(recontrasena.type == "password")&&(passact.type == "password")){
                        pass.type = "text";
                        passact.type = "text";
                        recontrasena.type = "text";
                        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
                    }else{
                        pass.type = "password";
                        passact.type = "password";
                        recontrasena.type = "password";
                        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
                    }
                }

                $('#password').keypress(function(event){
                    if($('#password').val().length<1){
                        $('#boton').prop("disabled",true);
                        $('#alertPasswordCanti').removeClass('d-none');
                    }else{
                        $('#alertPasswordCanti').addClass('d-none');
                        $('#boton').prop("disabled",false);
                    }
                });
                $('#recontrasena').focusout(function(event){
                    if($('#password').val()!=$('#recontrasena').val()){
                        Swal.fire({
                          type: 'warning',
                          title: 'Password do not match!',
                          text: 'The supplied passwords do not match, please check!'
                        });
                        $('#boton').prop("disabled",true);
                    }else{
                        $('#alertPassword').addClass('d-none');
                        $('#boton').prop("disabled",false);
                    }
                });

                $(document).ready(function() {
                });
            </script>

    @endsection

