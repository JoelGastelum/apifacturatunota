@extends('plantillas.privada')
@section('content')
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('usuarios.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::model($objUser, ['action' => ['UsuariosController@update', $objUser->id>0?$objUser->id:-1], 'id' => 'EditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('nombre') ? ' has-error' : '' }}">
                            {{ Form::label('nombre', 'Nombre') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'nombre',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'nombre',
                                            'placeholder'=>'Ingrese un nombre',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('idsucursal') ? ' has-error' : '' }}">
                            {{ Form::label('idsucursal', 'Sucursal') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idsucursal',
                                        $sucursales,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idsucursal',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idsucursal'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idsucursal') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('login') ? ' has-error' : '' }}">
                            {{ Form::label('login', 'Login') }}
                            <div class="input-group">
                                <?php $readonly=false;?>
                                <?php if($objUser->id>0):?>
                                    <?php $readonly=true;?>
                                <?php endif;?>
                                {{
                                    Form::text(
                                        'login',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'login',
                                            'placeholder'=>'Ingrese login',
                                            'required'=>true,
                                            'onchange'=>'verificarlogin();',
                                            'readonly'=>$readonly
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('login'))
                            <span class="help-block">
                                <strong>{{ $errors->first('login') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('pwd') ? ' has-error' : '' }}">
                            {{ Form::label('pwd', 'Password') }}
                            <div class="input-group">
                                <input type="password" class="form-control" id="pwd" placeholder="Ingrese password" <?php if($objUser->id<=0){echo 'required';};?> name="pwd">
                            </div>
                        </div>
                        @if ($errors->has('pwd'))
                            <span class="help-block">
                                <strong>{{ $errors->first('pwd') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="btn-save">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <script type="text/javascript">
        /**
        * Funcion que permite verificar si login ya existe
        */
        function verificarlogin(){
            var error=0;
            var login = $("#login").val();
            if(login=== "undefined" || login=== ""){
              error=1;
            }

            if(error==0){
                $('#btn-save').prop("disabled",true);
                $.ajax({
                    type: "POST",
                    url: "{{ route('verificarusuarioocorreo') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "tipo": 'login',
                        "login": login,
                    },
                    dataType:"json",
                    success: function(respuesta) {
                      if(respuesta.error==1){
                        Swal.fire({
                          type: 'warning',
                          title: respuesta.title,
                          text: respuesta.msj
                        });
                        $("#login").addClass("campoerror");
                        $("#login").removeClass("camposuccess");
                        $("#login").focus();
                      }else{
                        $("#login").addClass("camposuccess");
                        $("#login").removeClass("campoerror");
                        $('#btn-save').prop("disabled",false);
                      }
                    },
                    error: function() {
                    }
                });
            }
        }

        $(document).ready(function() {
        });
    </script>
@endsection

