@extends('plantillas.privada')
@section('content')
    <!--SELECT 2-->
    <link rel="stylesheet" href="{{ asset('privada/js/plugins/select2/css/select2.min.css') }}">
    <script src="{{ asset('privada/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>jQuery(function(){ Dashmix.helpers('select2'); });</script>

    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('conceptos.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::model($objConcepto, ['action' => ['ConceptosController@update', $objConcepto->id>0?$objConcepto->id:-1], 'id' => 'EditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="codigo" id="codigo" value="0">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('concepto') ? ' has-error' : '' }}">
                            {{ Form::label('concepto', 'Concepto') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'concepto',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'concepto',
                                            'placeholder'=>'Ingrese concepto',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('concepto'))
                            <span class="help-block">
                                <strong>{{ $errors->first('concepto') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('unidad') ? ' has-error' : '' }}">
                            {{ Form::label('unidad', 'Unidad') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'unidad',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'unidad',
                                            'placeholder'=>'Ingrese una unidad',
                                            'required'=>true,

                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('unidad'))
                            <span class="help-block">
                                <strong>{{ $errors->first('unidad') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('codigounidadsat') ? ' has-error' : '' }}">
                            {{ Form::label('codigounidadsat', 'Unidad SAT') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'codigounidadsat',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'codigounidadsat',
                                            'placeholder'=>'Ingrese Unidad SAT',
                                            'required'=>true,
                                            'style'=>'text-transform:uppercase',
                                            'maxlength'=>5,
                                            'onchange'=>'buscarunidadessatdirecta();'
                                        ]
                                    )
                                }}
                                <div class="input-group-prepend" style="cursor:pointer;" onclick="abrimodalunidadessat();">
                                    <span class="input-group-text">
                                        <i id="button" class="text-info far fa-list-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if ($errors->has('codigounidadsat'))
                            <span class="help-block">
                                <strong>{{ $errors->first('codigounidadsat') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('codigogrupoprodsersat') ? ' has-error' : '' }}">
                            {{ Form::label('codigogrupoprodsersat', 'Código producto SAT') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'codigogrupoprodsersat',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'codigogrupoprodsersat',
                                            'placeholder'=>'Ingrese código producto SAT',
                                            'required'=>true,
                                            'maxlength'=>20,
                                            'onchange'=>'buscarcodigoproductosatdirecta();'
                                        ]
                                    )
                                }}
                                <div class="input-group-prepend" style="cursor:pointer;" onclick="abrimodalcodigoproductosat();">
                                    <span class="input-group-text">
                                        <i id="button" class="text-info far fa-list-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        @if ($errors->has('codigogrupoprodsersat'))
                            <span class="help-block">
                                <strong>{{ $errors->first('codigogrupoprodsersat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('tasaiva') ? ' has-error' : '' }}">
                            {{ Form::label('tasaiva', 'Tasa Iva') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'tasaiva',
                                        ['0.16'=>'16%','0.08'=>'8%','0.0'=>'Sin Iva'],
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'tasaiva',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('tasaiva'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tasaiva') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('tasahospedaje') ? ' has-error' : '' }}">
                            {{ Form::label('tasahospedaje', 'Tasa Hospedaje') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'tasahospedaje',
                                        ['0.00'=>'Sin Impuesto','0.03'=>'3%'],
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'tasahospedaje',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('tasahospedaje'))
                            <span class="help-block">
                                <strong>{{ $errors->first('tasahospedaje') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">

                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="btn-guardar">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!--UNIDAD SAT-->
    <div class="modal" id="modal-unidadessat" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Unidades SAT</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times-circle"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required {{ $errors->has('texto-unidadessat') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'texto-unidadessat',
                                                null,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'texto-unidadessat',
                                                    'placeholder'=>'Ingrese una descripción o clave',
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend" style="cursor:pointer;" onclick="buscarunidadessat();">
                                            <span class="input-group-text">
                                                <i id="button" class="text-info fas fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" style="height:250px;overflow-y:auto;">
                                    <table class="table table-sm table-vcenter">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width: 20%;">Clave</th>
                                                <th class="text-left" style="width: 80%;">Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody-unidadessat">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times-circle"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--CODIGO PRODUCTO SAT-->
    <div class="modal" id="modal-codigoproductosat" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Código producto SAT</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-times-circle"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required {{ $errors->has('texto-codigoproductosat') ? ' has-error' : '' }}">
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'texto-codigoproductosat',
                                                null,
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'texto-codigoproductosat',
                                                    'placeholder'=>'Ingrese una descripción o clave',
                                                ]
                                            )
                                        }}
                                        <div class="input-group-prepend" style="cursor:pointer;" onclick="buscarcodigoproductosat();">
                                            <span class="input-group-text">
                                                <i id="button" class="text-info fas fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive" style="height:250px;overflow-y:auto;">
                                    <table class="table table-sm table-vcenter">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="width: 20%;">Clave</th>
                                                <th class="text-left" style="width: 80%;">Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody-codigoproductosat">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">
                            <i class="fa fa-times-circle"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <script type="text/javascript">
        //UNIDAD SAT
            $("#texto-unidadessat").keypress(function(e) {
                if (e.which == 13) {
                    buscarunidadessat();
                    return false;
                }
            });

            /**
             * Verificar si clave colocado directamente en el campo es valido
             */
            function buscarunidadessatdirecta(){
                $('#btn-guardar').attr("disabled",true);

                var error = 0;
                var codigounidadsat = $("#codigounidadsat").val();
                if(codigounidadsat=== "undefined" || codigounidadsat=== ""){
                    error = 1;
                }

                if(error==0){
                    $.ajax({
                        url: "{{ route('conceptos.buscarunidadessat') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "codigounidadsat": codigounidadsat,
                            "tipobusqueda":2,
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            if(respuesta.error==0){
                                $('#btn-guardar').attr("disabled",false);
                            }else{
                                Swal.fire({
                                    type: 'warning',
                                    title: '¡No se consiguio!',
                                    text: '¡No se ha conseguido la clave ingresada en las unidades del SAT!'
                                });
                            }
                        },
                        error: function() {
                        }
                    });
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: '¡Sin valor!',
                        text: '¡Debe de anexar un valor para campo de búsqueda!'
                    });
                }
            }

            /**
             * Mostrar modal
             */
            function abrimodalunidadessat(){
                $("#modal-unidadessat").modal();
            }

            /**
             * Armar tabla
             */
            function buscarunidadessat(){
                var error = 0;
                var texto = $("#texto-unidadessat").val();
                if(texto=== "undefined" || texto=== ""){
                    error = 1;
                }

                if(error==0){
                    $.ajax({
                        url: "{{ route('conceptos.buscarunidadessat') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "texto": texto,
                            "tipobusqueda":1,
                        },
                        dataType:"json",
                        beforeSend: function() {
                            Swal.fire({
                                title: 'Buscando por favor espere',
                                allowOutsideClick: false
                            });
                            Swal.showLoading();
                        },
                        success: function(respuesta) {
                            Swal.close();
                            $("#tbody-unidadessat").html(respuesta.html);
                        },
                        error: function() {
                            Swal.close();
                            $("#tbody-unidadessat").html('');
                        }
                    });
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: '¡Sin valor!',
                        text: '¡Debe de anexar un valor para campo de búsqueda!'
                    });
                }
            }

            /**
             * Seleccion de la unidad
             */
            function asignaunidadsat(clave){
                $('#btn-guardar').attr("disabled",false);
                $("#codigounidadsat").val(clave);
                $('#modal-unidadessat').modal('toggle');
            }

        //CODIGO PRODUCTO SAT
            $("#texto-codigoproductosat").keypress(function(e) {
                if (e.which == 13) {
                    buscarcodigoproductosat();
                    return false;
                }
            });

            /**
             * Verificar si clave colocado directamente en el campo es valido
             */
            function buscarcodigoproductosatdirecta(){
                $('#btn-guardar').attr("disabled",true);

                var error = 0;
                var codigogrupoprodsersat = $("#codigogrupoprodsersat").val();
                if(codigogrupoprodsersat=== "undefined" || codigogrupoprodsersat=== ""){
                    error = 1;
                }

                if(error==0){
                    $.ajax({
                        url: "{{ route('conceptos.buscarcodigoproductosat') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "codigogrupoprodsersat": codigogrupoprodsersat,
                            "tipobusqueda":2,
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            if(respuesta.error==0){
                                $('#btn-guardar').attr("disabled",false);
                            }else{
                                Swal.fire({
                                    type: 'warning',
                                    title: '¡No se consiguio!',
                                    text: '¡No se ha conseguido la clave ingresada en las código productos del SAT!'
                                });
                            }
                        },
                        error: function() {
                        }
                    });
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: '¡Sin valor!',
                        text: '¡Debe de anexar un valor para campo de búsqueda!'
                    });
                }
            }

            /**
             * Mostrar modal
             */
            function abrimodalcodigoproductosat(){
                $("#modal-codigoproductosat").modal();
            }

            /**
             * Armar tabla
             */
            function buscarcodigoproductosat(){
                var error = 0;
                var texto = $("#texto-codigoproductosat").val();
                if(texto=== "undefined" || texto=== ""){
                    error = 1;
                }

                if(error==0){
                    $.ajax({
                        url: "{{ route('conceptos.buscarcodigoproductosat') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "texto": texto,
                            "tipobusqueda":1,
                        },
                        dataType:"json",
                        beforeSend: function() {
                            Swal.fire({
                                title: 'Buscando por favor espere',
                                allowOutsideClick: false
                            });
                            Swal.showLoading();
                        },
                        success: function(respuesta) {
                            Swal.close();
                            $("#tbody-codigoproductosat").html(respuesta.html);
                        },
                        error: function() {
                            Swal.close();
                            $("#tbody-codigoproductosat").html('');
                        }
                    });
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: '¡Sin valor!',
                        text: '¡Debe de anexar un valor para campo de búsqueda!'
                    });
                }
            }

            /**
             * Seleccion de la unidad
             */
            function asignacodigoproductosat(clave){
                $('#btn-guardar').attr("disabled",false);
                $("#codigogrupoprodsersat").val(clave);
                $('#modal-codigoproductosat').modal('toggle');
            }


        function remplazar(elemento){
            let texto = elemento.value
            texto = texto.split(/[^A-Za-z\#\&]+/g)
            texto = texto.join("")
            elemento.value = texto
            }

        $(document).ready(function() {
        });
    </script>
@endsection

