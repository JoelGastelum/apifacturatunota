@extends('plantillas.privada')
@section('content')
    <!-- BOOSTRAP DATEPICKER-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                language: 'es',
                format: "dd-mm-yyyy",
            });
        });
    </script>

    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <?php if(isset($arraybtn['create'])):?>
                    <a href="{{ route('sucursales.create')}}" class="btn btn-sm btn-success">
                        <i class="fas fa-plus"></i> Crear
                    </a>
                <?php endif;?>
            </div>
        </div>
    </div>
    <br>

    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            @if(session('success'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('warning'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('danger'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    </div>
                </div>
            @endif
            <div id="tabladatos" class="table2 table-responsive" style="font-size: 12px;">
            </div>
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <script>
        function eliminar(id){
            Swal.fire({
                title: '¿Estas seguro que desea eliminar?',
                text: "Usted está por eliminar",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Sí, estoy seguro!"
            }).then((result) => {
                if (result.value) {
                    $('#FormDelete'+id).submit();
                }
            });
        }

        var ObjTabla=new myDataTable('tabladatos',{
            height:450,
            columns:[
                {data:'id',visible:false},
                {data:'acciones',title:'Acciones',filter:false,visible:true},
                {data:'nombre',title:'Nombre',filter:true,visible:true}
            ],
            instanceVarName:'ObjTabla',
            responsive:true
        });

        /**
         * Funcion que permite filtrar el listado
         */
        function filtrar(){
            $.ajax({
                type: "POST",
                url: "{{ route('sucursales.loaddata') }}",
                dataType: 'json',
                beforeSend: function() {
                    Swal.fire({
                        title: 'Cargando, por favor espere',
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    'estatus':$("#estatus").val(),
                    'fecha_inicial':$("#fecha_inicial").val(),
                    'fecha_final':$("#fecha_final").val(),
                },
                success: function(respuesta) {
                    Swal.close();
                    ObjTabla.initializeTable(respuesta.data);
                },
                error: function(jqXHR, exception) {
                    Swal.close();
                    if(jqXHR.status!=419 && jqXHR.status!=401){
                        Swal.fire({
                            type: 'error',
                            title: '¡Ha ocurrido un error!',
                            text: '¡Ha ocurrido un error, por favor intente de nuevo!'
                        });
                    }else{
                        Swal.fire({
                            type: 'warning',
                            title: '¡Su sesión expiró!',
                        });
                        setTimeout(
                            function(){
                                window.location.href='{{ route('login')}}'
                            },
                            3000
                        );
                    }
                }
            });
        }

        $(document).ready(function() {
            filtrar();
        });
    </script>
@endsection

