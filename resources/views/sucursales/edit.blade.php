@extends('plantillas.privada')
@section('content')
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('sucursales.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::model($objSucursal, ['action' => ['SucursalesController@update', $objSucursal->id>0?$objSucursal->id:-1], 'id' => 'EditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required {{ $errors->has('nombre') ? ' has-error' : '' }}">
                            {{ Form::label('nombre', 'Nombre') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'nombre',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'nombre',
                                            'placeholder'=>'Ingrese un nombre',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required {{ $errors->has('domicilio') ? ' has-error' : '' }}">
                            {{ Form::label('domicilio', 'Domicilio') }}
                            <div class="input-group">
                                {{
                                    Form::textarea(
                                        'domicilio',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'domicilio',
                                            'placeholder'=>'Ingrese un domiciolio',
                                            'rows'=>4,
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('domicilio'))
                            <span class="help-block">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group required {{ $errors->has('idserieremision') ? ' has-error' : '' }}">
                            {{ Form::label('idserieremision', 'Serie de Remisión') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idserieremision',
                                        $series,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idserieremision',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idserieremision'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idserieremision') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required {{ $errors->has('idserie') ? ' has-error' : '' }}">
                            {{ Form::label('idserie', 'Serie Factura') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idserie',
                                        $series,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idserie',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idserie'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idserie') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-3" style="display: none;">
                        <div class="form-group required {{ $errors->has('idserienc') ? ' has-error' : '' }}">
                            {{ Form::label('idserienc', 'Serie Nota Crédito') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idserienc',
                                        $series,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idserienc',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idserienc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idserienc') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-3" style="display: none;">
                        <div class="form-group required {{ $errors->has('idseriepago') ? ' has-error' : '' }}">
                            {{ Form::label('idseriepago', 'Serie Comprobante de pago') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idseriepago',
                                        $series,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idseriepago',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idseriepago'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idseriepago') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required {{ $errors->has('codigopostal') ? ' has-error' : '' }}">
                            {{ Form::label('codigopostal', 'Código Postal') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'codigopostal',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'codigopostal',
                                            'placeholder'=>'Ingrese un código postal',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('codigopostal'))
                            <span class="help-block">
                                <strong>{{ $errors->first('codigopostal') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="btn-guardar">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <script type="text/javascript">
        $(document).ready(function() {
        });
    </script>
@endsection

