@extends('plantillas.publica')
@section('content')
    <!-- ======= Header ======= -->
      @include('elementos.public_header')
    <!-- End Header -->

    <!-- ======= register ======= -->
    <section id="contact" class="contact">
      <br>
        @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <br>
      <br>
      <div class="container" data-aos="fade-up">
        <div class="section-title">
          <h2>Registrate</h2>
        </div>
        <div class="row mt-3 justify-content-md-center">
          <div class="col-lg-10 mt-3 mt-lg-0">
            <form id="registroform" action="{{ route('sregistrate') }}" method="post" role="form" autocomplete="off">
              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" autocomplete="off" name="nombre" class="form-control" id="nombre" placeholder="Ingrese nombre" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                </div>
                <div class="col-md-6 form-group">
                  <input onchange="verificarcorreo();" autocomplete="off" type="email" name="email" class="form-control" id="email" placeholder="Ingrese correo electrónico" size="30" required/>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" class="form-control" autocomplete="off" name="movil" id="movil" placeholder="Nro.telefónico móvil"/>
                </div>
                <div class="col-md-6 form-group">
                  <input type="text" class="form-control" autocomplete="off" name="fijo" id="fijo" placeholder="Nro.telefónico fijo"/>
                </div>
              </div>
              <hr>
              <h3>Cuenta</h3>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input onchange="verificarlogin();" autocomplete="off" type="text" name="login" class="form-control" id="login" placeholder="Login de usuario" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control" autocomplete="off" name="pwd" id="pwd" placeholder="Contraseña" required/>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control" autocomplete="off" name="repwd" id="repwd" placeholder="Confirmar contraseña" required/>
                  <div class="validate"></div>
                </div>
              </div>
                <?php $typecodigopromocion="hidden";?>
                <?php if($codigopromocion==null):?>
                  <hr>
                  <?php $typecodigopromocion="text";?>
                <?php endif;?>
              <div class="form-row">
                  <div class="col-md-12 form-group">
                    <input onchange="verificarcodigo();"  autocomplete="off" type="{{$typecodigopromocion}}" class="form-control" placeholder="Código promoción" name="codigopromocion" id="codigopromocion" value="{{$codigopromocion}}"/>
                  </div>
              </div>
              <?php if(session('danger')):?>
                 <div class="mb-3">
                    <div class="error-message" style="color: #fff;background: #ed3c0d;text-align: left;padding: 15px;font-weight: 600;">
                     <?php echo session('danger');?>
                    </div>
                 </div>
              <?php endif;?>
              <div class="text-center">
                <button type="submit" id="btn-save" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Aceptar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- End Login -->

    <!-- ======= Footer ======= -->
      @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <script type="text/javascript">
        /**
         * Funcion que permite verificar si correo ya existe
         */
        function verificarcorreo(){
            var error=0;
            var email = $("#email").val();
            if(email=== "undefined" || email=== ""){
                error=1;
            }

            if(error==0){
              //DISABLED BOTON SAVE
              $('#btn-save').prop("disabled",true);
              $('#btn-save').css("background-color","#E4AE3096");
              $.ajax({
                  type: "POST",
                  url: "{{ route('verificarusuarioocorreo') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "tipo": 'email',
                      "email": email,
                  },
                  dataType:"json",
                  success: function(respuesta) {
                    if(respuesta.error==1){
                      Swal.fire({
                        type: 'warning',
                        title: respuesta.title,
                        text: respuesta.msj
                      });
                      $("#email").addClass("campoerror");
                      $("#email").removeClass("camposuccess");
                      $("#email").focus();
                    }else{
                      $("#email").addClass("camposuccess");
                      $("#email").removeClass("campoerror");
                      $('#btn-save').prop("disabled",false);
                      $('#btn-save').css("background-color","#E4AE30");
                    }
                  },
                  error: function() {
                  }
              });
            }
        }

        /**
         * Funcion que permite verificar si login ya existe
         */
        function verificarlogin(){
            var error=0;
            var login = $("#login").val();
            if(login=== "undefined" || login=== ""){
                error=1;
            }

            if(error==0){
              //DISABLED BOTON SAVE
              $('#btn-save').prop("disabled",true);
              $('#btn-save').css("background-color","#E4AE3096");

              $.ajax({
                  type: "POST",
                  url: "{{ route('verificarusuarioocorreo') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "tipo": 'login',
                      "login": login,
                  },
                  dataType:"json",
                  success: function(respuesta) {
                    if(respuesta.error==1){
                      Swal.fire({
                        type: 'warning',
                        title: respuesta.title,
                        text: respuesta.msj
                      });
                      $("#login").addClass("campoerror");
                      $("#login").removeClass("camposuccess");
                      $("#login").focus();
                    }else{
                      $("#login").addClass("camposuccess");
                      $("#login").removeClass("campoerror");
                      $('#btn-save').prop("disabled",false);
                      $('#btn-save').css("background-color","#E4AE30");
                    }
                  },
                  error: function() {
                  }
              });
            }
        }

        /**
         * Funcion que permite verificar si codigo existe
         */
        function verificarcodigo(){
            var error=0;
            var codigopromocion = $("#codigopromocion").val();
            if(codigopromocion=== "undefined" || codigopromocion=== ""){
                error=1;
            }

            if(error==0){
              $('#btn-save').prop("disabled",true);
              $('#btn-save').css("background-color","#E4AE3096");
              $.ajax({
                  type: "POST",
                  url: "{{ route('verificarusuarioocorreo') }}",
                  data: {
                      "_token": "{{ csrf_token() }}",
                      "tipo": 'codigopromocion',
                      "codigopromocion": codigopromocion,
                  },
                  dataType:"json",
                  success: function(respuesta) {
                    if(respuesta.error==1){
                      Swal.fire({
                        type: 'warning',
                        title: '',
                        text: respuesta.msj
                      });
                      $("#codigopromocion").addClass("campoerror");
                      $("#codigopromocion").removeClass("camposuccess");
                      $("#codigopromocion").focus();
                    }else{
                      $("#codigopromocion").addClass("camposuccess");
                      $("#codigopromocion").removeClass("campoerror");
                      $('#btn-save').prop("disabled",false);
                      $('#btn-save').css("background-color","#E4AE30");
                    }
                  },
                  error: function() {
                  }
              });
            }
        }


        $("#registroform").submit(function(){
          $('#btn-save').prop("disabled",true);
          $('#btn-save').css("background-color","#E4AE3096");
          if($('#pwd').val()!=$('#repwd').val()){
                Swal.fire({
                  type: 'warning',
                  title: '¡Contraseña no coinciden!',
                  text: '¡Las contraseñas suministradas no coinciden, por favor verifique!'
                });
                $('#btn-save').prop("disabled",false);
                $('#btn-save').css("background-color","#E4AE30");
                return false;
            }else{
                return true;
            }
        });

        $('#repwd').focusout(function(event){
            if($('#pwd').val()!=$('#repwd').val()){
                Swal.fire({
                  type: 'warning',
                  title: '¡Contraseña no coinciden!',
                  text: '¡Las contraseñas suministradas no coinciden, por favor verifique!'
                });
                $('#btn-save').prop("disabled",true);
            }else{
                $('#btn-save').prop("disabled",false);
            }
        });

        $(document).ready(function() {
        });
    </script>
@endsection
