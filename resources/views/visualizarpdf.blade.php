@extends('plantillas.privada')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <?php if($isandroid==0):?>
                <iframe id="objpdf" type="application/pdf" src="{{route('empresa.descargarcomprobantepdfxidweb',[$idcomprobante,$rfc])}}" width="100%" height="600px">
                </iframe>
            <?php else:?>
                <img src="{{route('empresa.descargarcomprobantepdfxidweb',[$idcomprobante,$rfc])}}" class="img-fluid">
            <?php endif;?>
        </div>
    </div>
<script>
</script>
@endsection
