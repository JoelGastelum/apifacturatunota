@extends('plantillas.privada')
@section('content')
    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="90">
        <span class="font-size-sm font-w600">90%</span>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <p class="mb-0">Estas a solo un paso para ganar con mefactura.com, <strong>solos debes completar el formulario y aceptar los términos</strong>.</p>
            </div>
        </div>
    </div>


    {{ Form::model($objUser, ['action' => ['GanardineroController@sterminoscondiciones'], 'id' => 'EditForm','method' => 'post','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-user-tag"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'nombre',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'nombre',
                                    'placeholder'=>'Nombre',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('nombre'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('clabeinterbancaria') ? ' has-error' : '' }}">
                    <div class="input-group">
                        {{
                            Form::text(
                                'clabeinterbancaria',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'clabeinterbancaria',
                                    'placeholder'=>'Clabe interbancaria o Número de tarjeta',
                                    'onkeypress'=>"return AcceptNum(event);",
                                    'maxlength'=>'18',
                                    'required'=>true
                                ]
                            )
                        }}
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-university"></i>
                            </span>
                        </div>
                    </div>
                </div>
                @if ($errors->has('clabeinterbancaria'))
                    <span class="help-block">
                        <strong>{{ $errors->first('clabeinterbancaria') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('clabeinterbancaria') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-calendar-alt"></i>
                            </span>
                        </div>
                        {{
                            Form::date(
                                'fnacimiento',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'fnacimiento',
                                    'placeholder'=>'Fecha nacimiento',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('clabeinterbancaria'))
                    <span class="help-block">
                        <strong>{{ $errors->first('clabeinterbancaria') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('movil') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-mobile-alt"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'movil',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'movil',
                                    'placeholder'=>'Número telefónico movil',
                                    'onkeypress'=>"return AcceptNum(event);",
                                    'maxlength'=>'10',
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('movil'))
                    <span class="help-block">
                        <strong>{{ $errors->first('movil') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-envelope"></i>
                            </span>
                        </div>
                        {{
                            Form::email(
                                'email',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'email',
                                    'placeholder'=>'Correo electrónico',
                                    'disabled'=>true,
                                    'required'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-md-4">
                <div class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-restroom"></i>
                            </span>
                        </div>
                        {{
                            Form::select(
                                'idgenero',
                                $generos,
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'idgenero',
                                    'required'=>true,
                                    'onchange'=>'creargenero();'
                                ]
                            )
                        }}
                    </div>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row" id="row_genero" style="display: none;">
            <div class="col-md-6">
                <div class="form-group required {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-female"></i>
                            </span>
                        </div>
                        {{
                            Form::text(
                                'descripcion',
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'descripcion',
                                    'placeholder'=>'¿Nombre genero?',
                                    'required'=>false,
                                    'disabled'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group required {{ $errors->has('referirsecomo') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-male"></i>
                            </span>
                        </div>
                        {{
                            Form::select(
                                'referirsecomo',
                                array(1 =>'Mujer',2=>' Hombre'),
                                null,
                                [
                                    'class'=>'form-control ',
                                    'id'=>'referirsecomo',
                                    'placeholder'=>'¿Referirse como?',
                                    'required'=>false,
                                    'disabled'=>true
                                ]
                            )
                        }}
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
            <input type="checkbox" data-aceptar="0" class="custom-control-input" id="checkbox-termino" name="checkbox-termino" onchange="cambiarbotondisabled();">
            <label class="custom-control-label" for="checkbox-termino">Aceptar los <span style="cursor:pointer;color:#3b5998;" onclick="abrirmodal();">terminos y condiciones</span></label>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <button id="btn-save" class="btn btn-success" type="submit" disabled="true">
                        <i class="far fa-save"></i> Guardar
                    </button>
                </div>
            </div>
        </div>
    {{ Form::close() }}

    <!-- Modal -->
    <div class="modal" id="modal-termino" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Terminos y condiciones</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" onclick="$('#modal-termino').modal('hide');">
                                <i class="fa fa-times-circle"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>
                            El actual aviso y panel de información legal tiene como fin dar a conocer el control de acceso sujeto y la utilización del servicio del sitio web Comerciorapido.com (en adelante el Sitio Web), que SEOINVERSIÓN coloca en disposición de visitantes e interesados en el contenido que allí se publica, así como aplicaciones, herramientas y eventualidades que deben estar en conocimiento del usuario.

                        </p>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-light" onclick="$('#modal-termino').modal('hide');">
                            Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    /**
     * Funcion que permite crear generos
     */
    function creargenero(){
        var value = $("#idgenero").val();
        if(value=={{ env('IDGENEROPERSONALIZADO') }}){
            $("#row_genero").show();
            $("#descripcion").attr("disabled",false);
            $("#referirsecomo").attr("disabled",false);

            $("#descripcion").attr("required",true);
            $("#referirsecomo").attr("required",true);
        }else{
            $("#row_genero").hide();
            $("#descripcion").attr("disabled",true);
            $("#referirsecomo").attr("disabled",true);

            $("#descripcion").attr("required",false);
            $("#referirsecomo").attr("required",false);
        }
    }

    /**
     * Funcion de cambiar el ciclo
     */
    function cambiarbotondisabled(){
        var aceptar = $("#checkbox-termino").attr('data-aceptar');
        if(aceptar==0){
            $("#checkbox-termino").attr('data-aceptar',1);
            $("#btn-save").attr('disabled',false);
        }else{
            $("#checkbox-termino").attr('data-aceptar',0);
            $("#btn-save").attr('disabled',true);
        }
    }

    /**
     * Funcion de cambiar el ciclo
     */
    function abrirmodal(){
        $("#modal-termino").modal("show");
    }

    $(document).ready(function() {
    });
</script>
@endsection
