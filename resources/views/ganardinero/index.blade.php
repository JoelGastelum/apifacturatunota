@extends('plantillas.privada')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                <h1 class="font-w400 mt-2 mb-0 mb-sm-2">
                    Gana dinero con nosotros
                </h1>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <div class="text-left">
                <h4>
                    Con <span class="badge badge-pill badge-primary">mefactura.com</span> es muy fácil ganar dinero, solo sigue el siguiente video y aprenderás como.
                </h4>
            </div>
        </div>
    </div>
    <blockquote class="blockquote text-right" style="border-top:1px dashed #e6ebf4;border-bottom:1px dashed #e6ebf4;">
        <p class="mb-0">Para ganar más dinero no debes trabajar más, debes tener mejores ideas.</p>
        <footer class="blockquote-footer">Steve Jobs</footer>
    </blockquote>
    <div class="row">
        <div class="col-12">
            <div class="text-center embed-container">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/lpL5A1Zijcc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                <a href="{{ route('ganardinero.terminoscondiciones') }}" class="btn btn-hero-lg btn-rounded btn-hero-success mr-1 mb-3 col-12">
                    Si quiero ganar dinero
                </a>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function() {
    });
</script>
@endsection
