@extends('plantillas.privada')
@section('content')
    <!-- FILE INPUT-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
    <script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>

    <!-- COLOR PICKER-->
    <link rel="stylesheet" href="{{ asset('privada/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
    <script src="{{ asset('privada/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script>jQuery(function(){ Dashmix.helpers(['colorpicker']); });</script>

    @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <h2>Configuración General </h2>
                    </div>
                </div>
            </div>
            <hr>
            {{ Form::open(['action' => 'FacturasController@actualizarconf','id' => 'EditForm','method' => 'post','enctype'=>'multipart/form-data','role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="mostrarformuescanear" name="mostrarformuescanear" value="{{ $empresa->mostrarformuescanear }}">
                <input type="hidden" id="solicitaconsectivo" name="solicitaconsectivo" value="{{ $empresa->solicitaconsectivo }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>¿Desea mostrar datos del cliente al facturar?</h3>
                                <div class="form-group text-center">
                                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="mostrarformuescanear_1" onchange="cambiacheck('mostrarformuescanear',1);" name="mostrarformuescanear_1" @if($empresa->mostrarformuescanear==1)checked=""@endif>
                                        <label class="custom-control-label" for="mostrarformuescanear_1">Si</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="mostrarformuescanear_0" onchange="cambiacheck('mostrarformuescanear',0);" name="mostrarformuescanear_0" @if($empresa->mostrarformuescanear==0)checked=""@endif>
                                        <label class="custom-control-label" for="mostrarformuescanear_0">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Uso de cfdi default</h3>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select name="cfdidefault" id="cfdidefault" class="form-control">
                                                <option value="-1">Ninguno</option>
                                                @foreach($usoCfdi as $cfdi)
                                                    <option value="{{$cfdi->id}}" @if($empresa->idusocfdidefault==$cfdi->id) selected @endif>{{$cfdi->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>¿Usar consecutivo de nota?</h3>
                                <div class="form-group text-center">
                                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="solicitaconsectivo_1" onchange="cambiacheck('solicitaconsectivo',1);" name="solicitaconsectivo_1" @if($empresa->solicitaconsectivo==1)checked=""@endif>
                                        <label class="custom-control-label" for="solicitaconsectivo_1">Si</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-inline custom-control-primary">
                                        <input type="checkbox" class="custom-control-input" id="solicitaconsectivo_0" onchange="cambiacheck('solicitaconsectivo',0);" name="solicitaconsectivo_0" @if($empresa->solicitaconsectivo==0)checked=""@endif>
                                        <label class="custom-control-label" for="solicitaconsectivo_0">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required {{ $errors->has('avisodiamembresia') ? ' has-error' : '' }}">
                                    {{ Form::label('avisodiamembresia', 'Cantidad de días para avisar vencimiento') }}
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'avisodiamembresia',
                                                $empresa->avisodiamembresia,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'avisodiamembresia',
                                                    'onchange'=>'actualizarMembresia();'
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required {{ $errors->has('avisofolio') ? ' has-error' : '' }}">
                                    {{ Form::label('avisofolio', 'Cantidad de folios para avisar finalización') }}
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'avisofolio',
                                                $empresa->avisofolio,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'avisofolio',
                                                    'onchange'=>'actualizarFolio();'
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <h4>Configuración de pdf</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php $class_logopdf='col-md-12'; ?>
                            <?php if($empresa->logopdf!=null): ?>
                                <?php $class_logopdf='col-md-6'; ?>
                                <div class="{{ $class_logopdf }}">
                                    <div class="text-center">
                                        <div class="h4">Logo actual</div>
                                        <img class="img-fluid" src="{{ asset('storage/'.$empresa->logopdf) }}?nocache={{uniqid()}}" alt="Avatar">
                                    </div>
                                </div>
                            <?php endif;?>
                            <div class="{{ $class_logopdf }}">
                                <div class="form-group">
                                    {{ Form::label('logopdf', 'Logo') }}
                                    <input id="logopdf" name="logopdf" type="file" class="file" data-show-upload="false" data-show-caption="true" data-msg-placeholder="Seleccione una imagen" data-allowed-file-extensions='["jpg"]'>
                                    <script>
                                        $("#logopdf").fileinput({
                                            language: "es",
                                            theme: "fas",
                                            allowedFileExtensions: ["jpg"],
                                            /*maxFileSize: 3072,
                                            minImageWidth: 232,
                                            minImageHeight: 155,
                                            maxImageWidth: 650,
                                            maxImageHeight: 433,*/
                                            maxFileCount: 1
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('wlogopdf') ? ' has-error' : '' }}">
                                    {{ Form::label('wlogopdf', 'Ancho del logo (px)') }}
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'wlogopdf',
                                                $empresa->wlogopdf,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'wlogopdf',
                                                    'placeholder'=>'Ancho del logo (px)',
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                                @if ($errors->has('wlogopdf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('wlogopdf') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('hlogopdf') ? ' has-error' : '' }}">
                                    {{ Form::label('hlogopdf', 'Largo del logo (px)') }}
                                    <div class="input-group">
                                        {{
                                            Form::text(
                                                'hlogopdf',
                                                $empresa->hlogopdf,
                                                [
                                                    'class'=>'form-control ',
                                                    'id'=>'hlogopdf',
                                                    'placeholder'=>'Largo de logo (px)'
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                                @if ($errors->has('hlogopdf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('hlogopdf') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-row required">
                                    <label for="colorfondopdf">Color de fondo</label>
                                    <div class="js-colorpicker input-group" data-format="rgba">
                                        <input type="text" class="form-control" id="colorfondopdf" name="colorfondopdf" value="{{ $empresa->colorfondopdf }}" required="true">
                                        <div class="input-group-append">
                                            <span class="input-group-text colorpicker-input-addon">
                                                <i></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-row required">
                                    <label for="colorletrapdf">Color de letra</label>
                                    <div class="js-colorpicker input-group" data-format="rgba">
                                        <input type="text" class="form-control" id="colorletrapdf" name="colorletrapdf" value="{{ $empresa->colorletrapdf }}" required="true">
                                        <div class="input-group-append">
                                            <span class="input-group-text colorpicker-input-addon">
                                                <i></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="btn-guardar">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
<script>
    /**
     * Permite cambiar el check
     */
    function cambiacheck(idcampo,valuecampo){
        if(idcampo=="mostrarformuescanear"){
            if(valuecampo==0){
                //OPCION NO
                if($('#mostrarformuescanear_0').is(':checked')){
                    $('#mostrarformuescanear_1').prop("checked",false);
                    $("#mostrarformuescanear").val(0);
                }else{
                    $('#mostrarformuescanear_1').prop("checked",true);
                    $("#mostrarformuescanear").val(1);
                }
            }else{
                //OPCION SI
                if($('#mostrarformuescanear_1').is(':checked')){
                    $('#mostrarformuescanear_0').prop("checked",false);
                    $("#mostrarformuescanear").val(1);
                }else{
                    $('#mostrarformuescanear_0').prop("checked",true);
                    $("#mostrarformuescanear").val(0);
                }
            }
        }
        if(idcampo=="solicitaconsectivo"){
            if(valuecampo==0){
                //OPCION NO
                if($('#solicitaconsectivo_0').is(':checked')){
                    $('#solicitaconsectivo_1').prop("checked",false);
                    $("#solicitaconsectivo").val(0);
                }else{
                    $('#solicitaconsectivo_1').prop("checked",true);
                    $("#solicitaconsectivo").val(1);
                }
            }else{
                //OPCION SI
                if($('#solicitaconsectivo_1').is(':checked')){
                    $('#solicitaconsectivo_0').prop("checked",false);
                    $("#solicitaconsectivo").val(1);
                }else{
                    $('#solicitaconsectivo_0').prop("checked",true);
                    $("#solicitaconsectivo").val(0);
                }
            }
        }
    }

    $("#EditForm").submit(function(){
        $("#btn-guardar").attr('disabled',true);
        Swal.fire({
            title: 'Guardando, por favor espere',
            allowOutsideClick: false
        });
        Swal.showLoading();
    });

</script>
@endsection
