@extends('plantillas.privada')
@section('content')
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('facturas.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            <?php //dd($objRemision->estatus);?>
            <?php if($objComprobante->estatus=='A'):?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group  {{ $errors->has('noticket') ? ' has-error' : '' }}">
                            {{ Form::label('noticket', 'Número de ticket') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'noticket',
                                        $objRemision->noticket,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'noticket',
                                            'readonly'=>true,
                                            'onchange'=>'buscarconceptoseimportes();'
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('noticket'))
                            <span class="help-block">
                                <strong>{{ $errors->first('noticket') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  {{ $errors->has('sucursales_nombre') ? ' has-error' : '' }}">
                            {{ Form::label('sucursales_nombre', 'Sucursal') }}
                            <?php $sucursales_nombre='';?>
                            <?php if(isset($objRemision->sucursales->nombre)):?>
                                <?php $sucursales_nombre=$objRemision->sucursales->nombre;?>
                            <?php endif;?>
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'sucursales_nombre',
                                        $sucursales_nombre,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'sucursales_nombre',
                                            'readonly'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('sucursales_nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('sucursales_nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group  {{ $errors->has('formaspago_nombre') ? ' has-error' : '' }}">
                            {{ Form::label('formaspago_nombre', 'Forma de pago') }}
                            <?php $formaspago_nombre='';?>
                            <?php if(isset($objRemision->formaspago->nombre)):?>
                                <?php $formaspago_nombre=$objRemision->formaspago->nombre;?>
                            <?php endif;?>
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'formaspago_nombre',
                                        $objRemision->formaspago->nombre,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'formaspago_nombre',
                                            'readonly'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('formaspago_nombre'))
                            <span class="help-block">
                                <strong>{{ $errors->first('formaspago_nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <?php if($objRemision->estatus=='F'):?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group  {{ $errors->has('clientes_nombre') ? ' has-error' : '' }}">
                                {{ Form::label('clientes_nombre', 'Cliente') }}
                                <?php $clientes_nombre='';?>
                                <?php if(isset($objRemision->cliente->nombre)):?>
                                    <?php $clientes_nombre=$objRemision->cliente->nombre;?>
                                <?php endif;?>
                                <div class="input-group">
                                    {{
                                        Form::text(
                                            'clientes_nombre',
                                            $clientes_nombre,
                                            [
                                                'class'=>'form-control ',
                                                'id'=>'clientes_nombre',
                                                'readonly'=>true
                                            ]
                                        )
                                    }}
                                </div>
                            </div>
                            @if ($errors->has('clientes_nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('clientes_nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                <?php endif;?>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th style="width: 80%;">Concepto</th>
                                        <th style="width: 20%;" class="text-right">Importe</th>
                                    </tr>
                                </thead>
                                <tbody id="tabla_conceptosnota_tbody">
                                </tbody>
                                <tfoot id="tabla_conceptosnota_tfoot">
                                    <tr>
                                        <td style="text-align: right;">
                                            Subtotal
                                        </td>
                                        <td style="text-align: right;">
                                            {{number_format($objRemision->subtotal,2,'.',',')}}
                                        </td>
                                    </tr>
                                    <?php if(isset($objRemision->ieps)):?>
                                        <?php if($objRemision->ieps>0):?>
                                            <tr>
                                                <td style="text-align: right;">
                                                    IEPS
                                                </td>
                                                <td style="text-align: right;">
                                                    {{number_format($objRemision->ieps,2,'.',',')}}
                                                </td>
                                            </tr>
                                        <?php endif;?>
                                    <?php endif;?>
                                    <?php if(isset($objRemision->iva)):?>
                                        <?php if($objRemision->iva>0):?>
                                            <tr>
                                                <td style="text-align: right;">
                                                    IVA
                                                </td>
                                                <td style="text-align: right;">
                                                    {{number_format($objRemision->iva,2,'.',',')}}
                                                </td>
                                            </tr>
                                        <?php endif;?>
                                    <?php endif;?>
                                    <tr>
                                        <td style="text-align: right;">
                                            <strong>Total</strong>
                                        </td>
                                        <td style="text-align: right;">
                                            <strong>{{number_format($objRemision->total,2,'.',',')}}</strong>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            <?php else:?>
                <div class="row">
                    <embed id="objpdf" src="" type="application/pdf" width="100%" height="600px">

                    </embed>
                </div>
            <?php endif;?>
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <!-- NOTIFICACIONES-->
        <script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
        <script>jQuery(function(){ Dashmix.helpers('notify'); });</script>
    <!-- FIN DE NOTIFICACIONES-->
    <script type="text/javascript">
        <?php if($objRemision->estatus=='A'):?>
            /**
             * Permite buscar los conceptos de la nota (remision) por el noticket escrito
             */
            function buscarconceptoseimportes(){
                var idremision = {{$objRemision->id}};

                $.ajax({
                    type: "POST",
                    url: "{{ route('notas.buscarconceptosdenotacreada')}}",
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "idremision": idremision
                    },
                    dataType:"json",
                    success: function(respuesta) {
                        if(respuesta.error==0){
                            $("#tabla_conceptosnota_tbody").html(respuesta.html);
                            sumarimportes();

                            if(respuesta.estatus=="F"){
                                //OBSERVANDO NOTA FACTURADA
                                $.notify({
                                    icon:"fas fa-eye",
                                    title: "",
                                    message: "Usted está observando una nota facturada"
                                },{
                                    type: "info",
                                });
                            }else{
                                //ACTIVA
                                $("#btn-eliminar").show();


                                $.notify({
                                    icon:"fas fa-exclamation",
                                    title: "",
                                    message: "Usted está observando una nota no facturada"
                                },{
                                    type: "info",
                                });
                            }
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '',
                                text: respuesta.error
                            });
                        }
                    },
                    error: function(jqXHR, exception) {
                        if(jqXHR.status!=419){
                            // Swal.fire({
                            //     type: 'error',
                            //     title: 'There is an error!',
                            //     text: 'There is an error, please again later!'
                            // });
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '¡Sesión expirada!',
                            });
                            setTimeout(
                                function(){
                                    window.location.href='{{ route('login')}}'
                                },
                                3000
                            );
                        }
                    }
                });
            }

            var url_base ="{{ route('empresa.facturar') }}";
                                          url_base = url_base.replace('facturar', '');
                                          url = url_base+'descargarcomprobantepdfxid/'+respuesta.idcomprobante;
                                          var embed = '<embed id="objpdf" src="https://docs.google.com/viewerng/viewer?url='+url+'" type="application/pdf" width="100%" height="600px"></object>';
                                          $( "embed" ).replaceWith(embed);
                                          $("#div_pdf").show();

            $(document).ready(function() {
                buscarconceptoseimportes();
            });
        <?php else:?>
            /**
             * Permite buscar el pdf del comprobante si ya esta timbrado
             */
            function buscarpdf(){
                var url_base ="{{ route('empresa.facturar') }}";
                url_base = url_base.replace('facturar', '');
                url = url_base+'descargarcomprobantepdfxid/{{$objComprobante->id}}';
                var embed = '<embed id="objpdf" src="https://docs.google.com/viewerng/viewer?url='+url+'" type="application/pdf" width="100%" height="600px"></object>';
                $( "embed" ).replaceWith(embed);
                $("#div_pdf").show();
            }


            $(document).ready(function() {
                buscarpdf();
            });
        <?php endif;?>
    </script>
@endsection

