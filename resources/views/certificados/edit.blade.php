@extends('plantillas.privada')
@section('content')
    <!-- FILE INPUT-->
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap-fileinput/css/fileinput.min.css') }}">
        <script src="{{ asset('plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-fileinput/js/locales/es.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap-fileinput/themes/fas/theme.min.js') }}"></script>
    <!-- FINFILE INPUT-->

    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('certificados.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::model($objCertificado, ['action' => ['CertificadosController@update', $objCertificado->id>0?$objCertificado->id:-1], 'id' => 'EditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="archivokey">Llave de CSD (.key)</label>
                            <input id="archivokey" name="archivokey" type="file" class="file" data-show-upload="false" data-show-preview="false" data-msg-placeholder="Llave de CSD (.key)" data-allowed-file-extensions='["key"]' data-toggle="popover" data-placement = "top" >
                            <script>
                                $("#archivokey").fileinput({
                                    language: "es",
                                    theme: "fas",
                                    allowedFileExtensions: ["key"],
                                    showPreview: false,
                                    /*maxFileSize: 2048,
                                    minImageWidth: 50,
                                    minImageHeight: 500,
                                    maxImageWidth: 1024,
                                    maxImageHeight: 1024,*/
                                    maxFileCount: 1
                                });
                            </script>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('archivocertificado') ? ' has-error' : '' }}">
                            <label for="archivocertificado">Certificado CSD (.cer)</label>
                            <input id="archivocertificado" name="archivocertificado" type="file" class="file"  data-show-upload="false"  data-show-preview="false" data-msg-placeholder="Certificado de CSD (.cer)" data-allowed-file-extensions='["cer"]' data-toggle="popover" data-placement = "top" >
                            <script>
                                $("#archivocertificado").fileinput({
                                    language: "es",
                                    theme: "fas",
                                    allowedFileExtensions: ["cer"],
                                    /*maxFileSize: 2048,
                                    minImageWidth: 50,
                                    minImageHeight: 500,
                                    maxImageWidth: 1024,
                                    maxImageHeight: 1024,*/
                                    maxFileCount: 1
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" placeholder="Contraseña">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="btn-guardar">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <script type="text/javascript">
        $(document).ready(function() {
        });
    </script>
@endsection

