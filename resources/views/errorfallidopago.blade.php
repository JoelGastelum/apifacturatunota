@extends('plantillas.publica')
@section('content')
<!-- ======= Header ======= -->
    @include('elementos.public_header')
  <!-- End Header -->

  <main id="main">
    <!-- ======= Inicio Section ======= -->
    <section class="d-flex align-items-center" >
      <div class=" position-relative" data-aos="fade-up" data-aos-delay="100">
        <div>
          <img src="{{ asset('publica/img/hero-bg.jpg') }}" alt="{{ session()->get('Configuracion.Web.titulo') }}" class="img-fluid">
        </div>
        <div id="herop" class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="text-center">
                <a style="background-color: #FF7E54;" href="{{ route('establecimiento') }}" class="btn-get-started scrollto">Establecimientos</a>
                <a href="{{ route('consumidor') }}" class="btn-get-started scrollto">Consumidores</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Inicio -->

    <section id="about" class="about" >
      <div class="container" data-aos="fade-up" style="padding-top: 5px 0;">
        <div class="row content text-center">
          <div class="col-lg-12 ">
             <!--    <h2 style="color: #5e5e5e;margin: 10px 0 0 0; font-size: 20px;"> Sube tu nota desde tu celular introduciendo solo 2 datos (folio e importe del consumo).</h2>
                <h2 style="color: #5e5e5e;margin: 10px 0 0 0; font-size: 20px;"> Ofrece a tus clientes una plataforma para que ellos se generen su factura.</h2> -->
            <ul>
              <li style="color: #5e5e5e;margin: 20px 0 0 0; font-size: 20px; padding-top: 5px 0;">
                <!-- <i class="ri-check-double-line">                   -->
                <!-- </i>  -->
                {{ $msj }}.
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <!-- ======= Footer ======= -->
        @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
    <div id="preloader"></div>

    <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
<script>
    //CODIGO JAVASCRIPT PARA PWA
    document.addEventListener("DOMContentLoaded", function(event) {
    	View.ini();
    });
    var View = {
     	ini: function(){
          document.getElementById('button').addEventListener( "click", View.SendMessage);
    	},
    	SendMessage:function(){
    	  var message = document.getElementById("urlQr").value;

    	  var phone = document.getElementById("numtel").value;
    		if (phone.length < 9 || phone.length < 2){
    			View.drawAlertError("Mensaje o TelÃ©fono no pueden ser vacios")
    		}else{
    			phone =	phone.replace(/ /gi, "")
    			if (phone.length == 9 ){
    				phone =`34${phone}`
    			}
    			window.location.href = `https://api.whatsapp.com/send?phone=+52${phone}&text=${message}`
    		}
    	},
    	closeModal: function(){
    		document.getElementById('myModal').style.display = "none";
    	},
    	openModal: function (){
    		document.getElementById('myModal').style.display = "block";
    	},
    	drawAlertError:function(message){
            var element = document.getElementById("notify");
            element.classList.remove("ocultar");
            element.innerHTML = message
            setTimeout(function() {
                document.getElementById("notify").classList.add("ocultar");
            },2000);
        }
    }
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
          navigator.serviceWorker.register('{{ route('welcome')  }}/sw2.js');
        });
    }
</script>
@endsection
