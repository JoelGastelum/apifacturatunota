@extends('plantillas.publica')
@section('content')
    <!-- ======= Header ======= -->
      @include('elementos.public_header')
    <!-- End Header -->

    <!-- ======= leeqr ======= -->
    <section id="contact" class="contact">
        <br>
        @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <br>
        <?php if($error==0):?>
          <div class="container" data-aos="fade-up">
            <div class="section-title">
              <h2>Cliente</h2>
            </div>
            <div class="row mt-3 justify-content-md-center">
              <div class="col-lg-10 mt-3 mt-lg-0">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="text-align:center;width: 30%;">Campo</th>
                                <th style="text-align:left;width: 50%;">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($cliente as $key => $value):?>
                                <?php if($value!=''):?>
                                    <?php if(
                                            $key!='id'
                                            && $key!='idempresa'
                                            && $key!='idusuario'
                                            && $key!='estatus'
                                        ):?>
                                        <tr>
                                            <td style="text-align:center;width: 30%;">
                                                {{strtoupper ($key)}}
                                            </td>
                                            <td style="text-align:left;width: 70%;">
                                                {{$value}}
                                            </td>
                                        </tr>
                                    <?php endif;?>
                                <?php endif;?>
                            <?php endforeach;?>
                        </tbody>
                    </table>
               </div>
            </div>
          </div>
        <?php else:?>
          <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h4>{{$msj}}</h4>
            </div>
        <?php endif;?>
    </section>
    <!-- End Login -->

    <!-- ======= Footer ======= -->
      @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <script type="text/javascript">

        $(document).ready(function() {
        });
    </script>
@endsection
