@extends('plantillas.privada')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="bg-image" style="background-image: url('{{ asset('privada/media/photos/photo22@2x.jpg') }}');">
            <div class="hero bg-white-95 align-items-sm-start">
                <div class="hero-inner">
                    <div class="content content-full">
                        <div class="px-3 py-5 text-center text-sm-right">
                            <h1 class="h2 font-w700 mt-5 mb-3">{{ $msj }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //CODIGO JAVASCRIPT PARA PWA
    document.addEventListener("DOMContentLoaded", function(event) {
    	View.ini();
    });
    var View = {
     	ini: function(){
          document.getElementById('button').addEventListener( "click", View.SendMessage);
    	},
    	SendMessage:function(){
    	  var message = document.getElementById("urlQr").value;

    	  var phone = document.getElementById("numtel").value;
    		if (phone.length < 9 || phone.length < 2){
    			View.drawAlertError("Mensaje o TelÃ©fono no pueden ser vacios")
    		}else{
    			phone =	phone.replace(/ /gi, "")
    			if (phone.length == 9 ){
    				phone =`34${phone}`
    			}
    			window.location.href = `https://api.whatsapp.com/send?phone=+52${phone}&text=${message}`
    		}
    	},
    	closeModal: function(){
    		document.getElementById('myModal').style.display = "none";
    	},
    	openModal: function (){
    		document.getElementById('myModal').style.display = "block";
    	},
    	drawAlertError:function(message){
            var element = document.getElementById("notify");
            element.classList.remove("ocultar");
            element.innerHTML = message
            setTimeout(function() {
                document.getElementById("notify").classList.add("ocultar");
            },2000);
        }
    }
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
          navigator.serviceWorker.register('{{ route('welcome')  }}/sw2.js');
        });
    }
</script>
@endsection
