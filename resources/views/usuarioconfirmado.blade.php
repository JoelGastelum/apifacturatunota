@extends('plantillas.publica')
@section('content')
  <!-- ======= Header ======= -->
    @include('elementos.public_header')
  <!-- End Header -->

  <main id="main">
    <section id="registro" class="about-video">
      @if(session('success'))
          <div class="row">
              <div class="container">
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              </div>
          </div>
      @endif
      @if(session('danger'))
          <div class="row">
              <div class="container">
                  <div class="alert alert-danger">
                      {{ session('danger') }}
                  </div>
              </div>
          </div>
      @endif
      <div class="container" data-aos="fade-up" >
        <br>
        <div class="section-title">
          <h2>Gracias por registrarse {{ $objUser->login }} en {{ session()->get('Configuracion.Web.titulo') }}</h2>
          <br>
          <h3>Te hemos enviado a tu correo las creedenciales de acceso</h3>
          <br>
          <?php if($idremisionencode!=null):?>
            <h4>Si desea facturar su orden de compra lo invitamos hacer click en el siguiente botón:</h4>
            <a href="{{ route('facturar',[$idremisionencode]) }}" class="btn btn-primary">
              Facturar
            </a>
          <?php else:?>
            <a href="{{ route('login')}}" class="btn btn-primary">
              Accede a tu cuenta
            </a>
          <?php endif;?>
      </div>
    </section>
    <!-- End Registro de Section -->
  </main>

  <!-- ======= Footer ======= -->
    @include('elementos.public_footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
  <!-- FIN DE SWEETALERT2-->
  <script type="text/javascript">
  </script>
@endsection


