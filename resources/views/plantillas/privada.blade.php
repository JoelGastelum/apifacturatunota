<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
    <link rel="manifest" href="manifest1.webmanifest">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">-->
        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?php echo session()->get('Configuracion.Web.descripcion');?>">
        <meta name="keywords" content="<?php echo session()->get('Configuracion.Web.palabrasclaves');?>" />
        <meta name="author" content="{{ env('APP_AUTOR') }}">

        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            <?php echo session()->get('Configuracion.Web.titulo');?>
        </title>

        <!-- FONT -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">

        <!-- ESTILOS -->
        <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

        <link rel="stylesheet" id="css-main" href="{{ asset('privada/css/dashmix.css') }}">
        <?php if(auth()->user()->idtipousuario==4):?>
            <link rel="stylesheet" id="css-theme" href="{{ asset('privada/css/themes/xmodern.css') }}">
        <?php elseif(auth()->user()->idtipousuario==5):?>

        <?php else:?>
            <link rel="stylesheet" id="css-theme" href="{{ asset('privada/css/themes/xamarillo.css') }}">
        <?php endif;?>

        <!-- JS-->
            <script src="{{ asset('jquery/jquery-3.3.1.min.js') }}"></script>
            <script src="{{ asset('privada/js/dashmix.core.min.js') }}"></script>
            <script src="{{ asset('privada/js/dashmix.app.min.js') }}"></script>
        <!-- FIN DE JS -->

        <!-- mydatatable-->
            <link rel="stylesheet" href="{{ asset('plugins/mydatatable/mydatatable.css') }}">
            <script src="{{ asset('plugins/mydatatable/mydatatable.js') }}?v=2" ></script>
        <!-- FIN DE mydatatable -->
        <script type="text/javascript">
            function AcceptNum(evt){
                //ACEPTA NUMERO
                var key = evt.which || evt.keyCode;
                return (key <= 13 || (key >= 48 && key <= 57) );
            }
            function AcceptNumPunto(evt){
                //ACEPTA NUMERO
                var key = evt.which || evt.keyCode;
                return (key <= 13 || key <= 46 || (key >= 48 && key <= 57) );
            }
            function AcceptLetra(evt){
                //ACEPTA LETRAS
                var key = evt.which || evt.keyCode;
                if((key!=32) && (key<65) || (key>90) && (key<97) || (key>122 && key != 241 && key != 209 && key != 225 && key != 233 && key != 237 && key != 243 && key != 250 && key != 193 && key != 201 && key != 205 && key != 211 && key != 218)){
                    if(key==0 || key==8 || key==9 || key==17 || key==18 || key==46 || key==37 || key==38 || key==39 || key==40 || key==116){
                        return key;
                    }else{
                        return false;
                    }
                }else{
                    return key;
                }
            }
            function trunc (x, posiciones = 2) {
                //FUNCION PARA TRUNCAR NUMEROS FLOTANTES
                var s = x.toString()
                var l = s.length
                var decimalLength = s.indexOf('.') + 1

                if (l - decimalLength <= posiciones){
                return x
                }
                // Parte decimal del número
                var isNeg  = x < 0
                var decimal =  x % 1
                var entera  = isNeg ? Math.ceil(x) : Math.floor(x)
                // Parte decimal como número entero
                // Ejemplo: parte decimal = 0.77
                // decimalFormated = 0.77 * (10^posiciones)
                // si posiciones es 2 ==> 0.77 * 100
                // si posiciones es 3 ==> 0.77 * 1000
                var decimalFormated = Math.floor(
                Math.abs(decimal) * Math.pow(10, posiciones)
                )
                // Sustraemos del número original la parte decimal
                // y le sumamos la parte decimal que hemos formateado
                var finalNum = entera +
                ((decimalFormated / Math.pow(10, posiciones))*(isNeg ? -1 : 1))

                return finalNum
            }
            $(document).ready(function() {
                // $(".alert-danger").fadeOut(5000);
                $(".alert-success").fadeOut(5000);
            });
        </script>
    </head>
    <body>
        <?php
            $ruta= Route::getCurrentRoute()->getActionName();
            $accion=explode('@', Route::getCurrentRoute()->getActionName())[1];
        ?>
        <!-- Page Container -->
        <div id="page-container" <?php if(auth()->user()->idtipousuario==4):?> style="background-color: #fff;" <?php endif;?> class="sidebar-o side-scroll page-header-fixed page-header-dark main-content-boxed">
            <!-- MENU LATERAL -->
                @include('elementos.private_menu')
            <!-- FIN DE MENU LATERAL -->

            <!-- CABECERA -->
                @include('elementos.private_cabecera')
            <!-- FIN DE CABECERA -->

            <!-- Main Container -->
            <main id="main-container" <?php if(auth()->user()->idtipousuario==4):?> style="background-color: #fff;" <?php endif;?>>
                <!-- CONTENIDO -->
                <div class="content">
                    @yield('content')
                </div>
                <!-- FIN DE CONTENIDO -->
            </main>
            <!-- END Main Container -->

            <!-- FOOTER -->
                @include('elementos.private_footer')
            <!-- FIN DE FOOTER -->
        </div>
    </body>
    <style>

    </style>
    <script>
        $(document).ready(function(){
            // console.log("{{auth()->user()->remember_token}}");
            var remember_token = localStorage.getItem('remember_token');
            // console.log(localStorage.getItem('remember_token'));
            // $("#tokenver").html(tokenlogin);
            // if(remember_token==null){
                // console.log("antes");
                localStorage.setItem('remember_token', "{{auth()->user()->remember_token}}");
                // console.log(localStorage.getItem('remember_token'));
            // }
        });
    </script>
</html>
