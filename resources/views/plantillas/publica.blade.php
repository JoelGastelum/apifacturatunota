<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">

      <title>{{ session()->get('Configuracion.Web.titulo') }}</title>
      <meta name="description" content="<?php echo session()->get('Configuracion.Web.descripcion');?>">
      <meta name="keywords" content="<?php echo session()->get('Configuracion.Web.palabrasclaves');?>" />
      <meta name="author" content="{{ env('APP_AUTOR') }}">

      <!-- Favicons -->
      <link rel="icon" href="{{ asset('favicon.ico') }}">
      <link href="{{ asset('publica/img/mefactura-icon.png') }}" rel="apple-touch-icon">

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

      <!-- Vendor CSS Files -->
      <link href="{{ asset('publica/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('publica/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
      <link href="{{ asset('publica/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
      <link href="{{ asset('publica/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
      <link href="{{ asset('publica/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{ asset('publica/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <link href="{{ asset('publica/vendor/aos/aos.css') }}" rel="stylesheet">

      <!-- Template Main CSS File -->
      <link href="{{ asset('publica/css/style.css') }}" rel="stylesheet">

      <!-- Font Awesome -->
      <link href="{{ asset('fontawesome/css/all.min.css') }}" rel="stylesheet">

      <!-- Vendor JS Files -->
      <script src="{{ asset('publica/vendor/jquery/jquery.min.js') }}"></script>
    </head>
    <body>

      @yield('content')


      <!-- Vendor JS Files -->
      <script src="{{ asset('publica/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/php-email-form/validate.js') }}"></script>
      <script src="{{ asset('publica/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/counterup/counterup.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/venobox/venobox.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
      <script src="{{ asset('publica/vendor/aos/aos.js') }}"></script>

      <!-- Template Main JS File -->
      <script src="{{ asset('publica/js/main.js') }}"></script>

      <!-- Chat -->
      <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '00fcaf5d-7845-41e4-b567-ed86f00719f1', f: true }); done = true; } }; })();</script>

    </body>

    </html>
