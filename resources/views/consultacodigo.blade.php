@extends('plantillas.publica')
@section('content')
    <!-- ======= Header ======= -->
      @include('elementos.public_header')
    <!-- End Header -->

    <!-- ======= leeqr ======= -->
    <section id="contact" class="contact">
        <br>
        @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <div class="container" data-aos="fade-up">
            <div class="section-title">
              <h2>Código del cliente</h2>
            </div>
            <div class="row mt-3 justify-content-md-center">
                <div class="col-lg-10 mt-3 mt-lg-0">
                    <form action="{{ route('sconsultacodigo') }}" id="forma" name="forma" method="post">
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="col-12">
                                <input required="" type="text" autocomplete="off" name="codigocliente" class="form-control" id="codigocliente" placeholder="Código del cliente" value=""/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button type="submit" id="btn-save" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- End Login -->

    <!-- ======= Footer ======= -->
      @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <script type="text/javascript">

        $(document).ready(function() {
        });
    </script>
@endsection
