@extends('plantillas.publica')
@section('content')
  <!-- ======= Header ======= -->
    @include('elementos.public_header')
  <!-- End Header -->

  <main id="main">
    <!-- ======= Inicio Section ======= -->
    <section class="d-flex align-items-center">
      <div class=" position-relative" data-aos="fade-up" data-aos-delay="100">
        <div>
          <img src="{{ asset('publica/img/consumidores.jpeg') }}" alt="{{ session()->get('Configuracion.Web.titulo') }}" class="img-fluid">
        </div>
      </div>
    </section>
    <!-- End Inicio -->

    <!-- ======= Registro PARA CONSUMIDORES Section ======= -->
    <section id="registro" class="about-video">
      @if(session('success'))
          <div class="row">
              <div class="container">
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              </div>
          </div>
      @endif
      <div class="container" data-aos="fade-up" >
        <hr>
        <div class="section-title">
          <h2>Registrate gratuitamente en {{ session()->get('Configuracion.Web.titulo') }}</h2>
        </div>
        <form id="registroform" action="{{ route('sconsumidor') }}" method="post" role="form" autocomplete="off">
          <div class="row">
            <div class="col-lg-6 pt-3 pt-lg-0 content" data-aos="zoom-in" data-aos-delay="100">
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://youtu.be/QtXPAg8Vrlc" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </div>
            <div class="col-lg-6 pt-3 pt-lg-0 content" data-aos="zoom-in" data-aos-delay="100">
              <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
              <a href="https://youtu.be/lpL5A1Zijcc" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 video-box align-self-baseline" data-aos="fade-right" data-aos-delay="100">
              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input onchange="verificarcorreo();" autocomplete="off" type="text" name="email" class="form-control" id="email" placeholder="Correo electrónico" required/>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input onchange="verificarlogin();" autocomplete="off" type="text" name="login" class="form-control" id="login" placeholder="Login de usuario" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control" autocomplete="off" name="pwd" id="pwd" placeholder="Contraseña" required/>
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 form-group">
                  <input type="password" class="form-control" autocomplete="off" name="repwd" id="repwd" placeholder="Confirmar contraseña" required/>
                  <div class="validate"></div>
                </div>
              </div>
                <?php $typecodigopromocion="hidden";?>
                <?php if($codigopromocion==null):?>
                  <hr>
                  <?php $typecodigopromocion="text";?>
                <?php endif;?>
              <div class="form-row">
                  <div class="col-md-12 form-group">
                    <input onchange="verificarcodigo();" autocomplete="off" type="{{$typecodigopromocion}}" class="form-control" placeholder="Código de recomendación" name="codigopromocion" id="codigopromocion" value="{{$codigopromocion}}"/>
                  </div>
              </div>
              <?php if(session('danger')):?>
                 <div class="mb-3">
                    <div class="error-message" style="color: #fff;background: #ed3c0d;text-align: left;padding: 15px;font-weight: 600;">
                     <?php echo session('danger');?>
                    </div>
                 </div>
              <?php endif;?>
            </div>
          </div>
          <br>
          <div class="row" id="filareferido" style="display: none;">
            <div class="col-md-12">
              <div class="text-right" id="filareferido_contenido" style="color:red;font-size: 12px;">

              </div>
            </div>
            <br>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="text-center">
                <button type="submit" id="btn-save" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Registrate</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
    <!-- End Registro de Section -->
  </main>

  <!-- ======= Footer ======= -->
    @include('elementos.public_footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
  <!-- FIN DE SWEETALERT2-->
  <script type="text/javascript">
    /*REGISTRO DE CLIENTE*/
      /**
       * Funcion que permite verificar si correo ya existe
       */
      function verificarcorreo(){
          var error=0;
          var email = $("#email").val();
          if(email=== "undefined" || email=== ""){
              error=1;
          }

          if(error==0){
            $('#btn-save').prop("disabled",true);
            $('#btn-save').css("background-color","#E4AE3096");
            $.ajax({
                type: "POST",
                url: "{{ route('verificarusuarioocorreo') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "tipo": 'email',
                    "idtipousuario": '4,5',
                    "email": email,
                },
                dataType:"json",
                success: function(respuesta) {
                  if(respuesta.error==1){
                    Swal.fire({
                      type: 'warning',
                      title: respuesta.title,
                      text: respuesta.msj
                    });
                    $("#email").addClass("campoerror");
                    $("#email").removeClass("camposuccess");
                    $("#email").focus();
                  }else{
                    $("#email").addClass("camposuccess");
                    $("#email").removeClass("campoerror");
                    $('#btn-save').prop("disabled",false);
                    $('#btn-save').css("background-color","#E4AE30");
                  }
                },
                error: function() {
                }
            });
          }
      }

      /**
       * Funcion que permite verificar si login ya existe
       */
      function verificarlogin(){
          var error=0;
          var login = $("#login").val();
          if(login=== "undefined" || login=== ""){
              error=1;
          }

          if(error==0){
            $('#btn-save').prop("disabled",true);
            $('#btn-save').css("background-color","#E4AE3096");
            $.ajax({
                type: "POST",
                url: "{{ route('verificarusuarioocorreo') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "tipo": 'login',
                    "login": login,
                },
                dataType:"json",
                success: function(respuesta) {
                  if(respuesta.error==1){
                    Swal.fire({
                      type: 'warning',
                      title: respuesta.title,
                      text: respuesta.msj
                    });
                    $("#login").addClass("campoerror");
                    $("#login").removeClass("camposuccess");
                    $("#login").focus();
                  }else{
                    $("#login").addClass("camposuccess");
                    $("#login").removeClass("campoerror");
                    $('#btn-save').prop("disabled",false);
                    $('#btn-save').css("background-color","#E4AE30");
                  }
                },
                error: function() {
                }
            });
          }
      }

      /**
       * Funcion que permite verificar si codigo existe
       */
      function verificarcodigo(){
          $("#filareferido").hide();
          var error=0;
          var codigopromocion = $("#codigopromocion").val();
          if(codigopromocion=== "undefined" || codigopromocion=== ""){
              error=1;
          }

          if(error==0){
            $('#btn-save').prop("disabled",true);
            $('#btn-save').css("background-color","#E4AE3096");
            $.ajax({
                type: "POST",
                url: "{{ route('verificarusuarioocorreo') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "tipo": 'codigopromocion',
                    "codigopromocion": codigopromocion,
                },
                dataType:"json",
                success: function(respuesta) {
                  if(respuesta.error==1){
                    Swal.fire({
                      type: 'warning',
                      title: respuesta.title,
                      text: respuesta.msj
                    });
                    $("#codigopromocion").addClass("campoerror");
                    $("#codigopromocion").removeClass("camposuccess");
                    $("#codigopromocion").focus();
                  }else{
                    $("#codigopromocion").addClass("camposuccess");
                    $("#codigopromocion").removeClass("campoerror");
                    $('#btn-save').css("background-color","#E4AE30");

                    $("#filareferido").show();
                    $("#filareferido_contenido").html(respuesta.usuario);
                  }
                },
                error: function() {
                }
            });
          }
      }

      $("#registroform").submit(function(){
        if($('#pwd').val()!=$('#repwd').val()){
              Swal.fire({
                type: 'warning',
                title: '',
                text: '¡Las contraseñas suministradas no coinciden, por favor verifique!'
              });
              return false;
          }else{
              $('#btn-save').prop("disabled",true);
              $('#btn-save').css("background-color","#E4AE3096");
              return true;
          }
      });

      $('#repwd').focusout(function(event){
          if($('#pwd').val()!=$('#repwd').val()){
              Swal.fire({
                type: 'warning',
                title: '',
                text: '¡Las contraseñas suministradas no coinciden, por favor verifique!'
              });
              $('#btn-save').prop("disabled",true);
              $('#btn-save').css("background-color","#E4AE3096");
          }else{
              $('#btn-save').prop("disabled",false);
              $('#btn-save').css("background-color","#E4AE30");
          }
      });

      $(document).ready(function() {
          <?php if($codigopromocion!=null):?>
              verificarcodigo();
          <?php endif;?>
      });
  </script>
@endsection


