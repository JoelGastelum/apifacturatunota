@extends('plantillas.privada')
@section('content')

    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <button type="button" class="btn btn-sm btn-info" onclick = "$('#filtrado_modal').modal('show');">
                    <i class="fas fa-filter"></i> Filtrar
                </button>
            </div>
        </div>
    </div>
    <br>

    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            @if(session('success'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('warning'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('danger'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    </div>
                </div>
            @endif
            <div id="tabladatos" class="table2 table-responsive" style="font-size: 12px;">
            </div>
        </div>
    </div>

    <div class="modal fade" id="filtrado_modal" tabindex="-1" role="dialog" aria-labelledby="modal-block-fadein" aria-hidden="true">
        <div class="modal-dialog modal-dialog-fadein modal-lg" role="document">
            <div class="modal-content" id="capacitydetail_contenido">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">
                            Filtrar listado
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('fecha_inicial') ? ' has-error' : '' }}">
                                    {{ Form::label('fecha_inicial', 'Fecha inicial') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::date(
                                                'fecha_inicial',
                                                date('Y').'-01-01',
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'fecha_inicial',
                                                    'name'=>'fecha_inicial',
                                                    'placeholder'=>'Fecha inicial',
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                                @if ($errors->has('fecha_inicial'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fecha_inicial') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('fecha_final') ? ' has-error' : '' }}" name="fecha_final">
                                    {{ Form::label('fecha_final', 'Fecha final') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::date(
                                                'fecha_final',
                                                date('Y-m-d'),
                                                [
                                                    'class'=>'form-control',
                                                    'id'=>'fecha_final',
                                                    'name'=>'fecha_final',
                                                    'placeholder'=>'Fecha final',
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                                @if ($errors->has('fecha_final'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fecha_final') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" onclick="filtrar();">Filtrar</button>
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <script>
        var ObjTabla=new myDataTable('tabladatos',{
            height:450,
            columns:[
                {data:'id',visible:false},
                {data:'nombre',title:'Usuario',filter:true,visible:true},
                {data:'fecha',title:'Fecha',filter:true,visible:true},
                {data:'importe',title:'Importe',filter:true,visible:true,style:"text-align:right;"},
                {data:'cuenta',title:'Cuenta',filter:true,visible:true},
                {data:'referencia',title:'Referencia',filter:true,visible:true}
            ],
            instanceVarName:'ObjTabla',
            responsive:true
        });

        /**
         * Funcion que permite filtrar el listado
         */
        function filtrar(){
            var error=0;
            var var_fecha_inicial= $("#fecha_inicial").val();
            if(var_fecha_inicial==''){
                error=1;
            }
            var var_fecha_final= $("#fecha_final").val();
            if(var_fecha_final==''){
                error=1;
            }

            if(error==0){
                $.ajax({
                    type: "POST",
                    url: "{{ route('pagos.loaddata') }}",
                    dataType: 'json',
                    beforeSend: function() {
                        Swal.fire({
                            title: 'Cargando, por favor espere',
                            allowOutsideClick: false
                        });
                        Swal.showLoading();
                    },
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "fecha_inicial": var_fecha_inicial,
                        "fecha_final": var_fecha_final,
                    },
                    success: function(respuesta) {
                        Swal.close();
                        ObjTabla.initializeTable(respuesta.data);
                    },
                    error: function(jqXHR, exception) {
                        Swal.close();
                        if(jqXHR.status!=419 && jqXHR.status!=401){
                            Swal.fire({
                                type: 'error',
                                title: '¡Ha ocurrido un error!',
                                text: '¡Ha ocurrido un error, por favor intente de nuevo!'
                            });
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '¡Su sesión expiró!',
                            });
                            setTimeout(
                                function(){
                                    window.location.href='{{ route('login')}}'
                                },
                                3000
                            );
                        }
                    }
                });
            }
        }

        $(document).ready(function() {
            filtrar();
        });
    </script>
@endsection

