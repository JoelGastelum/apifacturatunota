@extends('plantillas.privada')
@section('content')
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <a class="btn btn-sm btn-warning" href="{{ route('series.index')}}">
                    <i class="fa fa-arrow-left"></i> Volver
                </a>
            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::model($objSerie, ['action' => ['SeriesController@update', $objSerie->id>0?$objSerie->id:-1], 'id' => 'EditForm','method' => 'put','enctype'=>'multipart/form-data', 'role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group required {{ $errors->has('serie') ? ' has-error' : '' }}">
                            {{ Form::label('serie', 'Serie') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'serie',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'serie',
                                            'placeholder'=>'Ingrese una serie',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('serie'))
                            <span class="help-block">
                                <strong>{{ $errors->first('serie') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required {{ $errors->has('folio_actual') ? ' has-error' : '' }}">
                            {{ Form::label('folio_actual', 'Folio Actual') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'folio_actual',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'folio_actual',
                                            'placeholder'=>'Ingrese un folio actual',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('folio_actual'))
                            <span class="help-block">
                                <strong>{{ $errors->first('folio_actual') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group required {{ $errors->has('idcertificadointegra') ? ' has-error' : '' }}">
                            {{ Form::label('idcertificadointegra', 'Certificado') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idcertificadointegra',
                                        $certicados,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idcertificadointegra',
                                            'required'=>true
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idcertificadointegra'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idcertificadointegra') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" id="btn-guardar">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <script type="text/javascript">
        $(document).ready(function() {
        });
    </script>
@endsection

