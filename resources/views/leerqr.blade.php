@extends('plantillas.publica')
@section('content')
    <script src="{{ asset('plugins/html5-qrcode/html5-qrcode.js?v=1') }}"></script>

    <!-- ======= Header ======= -->
      @include('elementos.public_header')
    <!-- End Header -->

    <main id="main">
      <!-- ======= Inicio Section ======= -->
      <section class="d-flex align-items-center">
        <div class=" position-relative" data-aos="fade-up" data-aos-delay="100">
          <div>
            <img src="{{ asset('publica/img/hero-bg.jpg') }}" alt="{{ session()->get('Configuracion.Web.titulo') }}" class="img-fluid">
          </div>
        </div>
      </section>
      <!-- End Inicio -->

      <!-- ======= Login ======= -->
      <section id="contact" class="contact">
        <br>
        <div class="container" data-aos="fade-up">
          <div class="section-title">
            <h2>QR Universal</h2>
          </div>
          <div class="row mt-5">
            <div class="col-lg-4">
              <div class="info">
                <div class="address">
                  <i class="icofont-google-map"></i>
                  <h4>Localización:</h4>
                  <p>Calle de la tuna 3666, Fracc San Florencio, Culiacan Sinaloa, CP 80058</p>
                </div>
                <div class="email">
                  <i class="icofont-envelope"></i>
                  <h4>Email:</h4>
                  <p>contacto@facturatunota.com</p>
                </div>
                <div class="phone">
                  <i class="icofont-whatsapp"></i>
                  <h4>Teléfonos:</h4>
                  <p>6672 93 86 74</p>
                  <p>6672 856 453</p>
                </div>
              </div>
            </div>
            <div class="col-lg-8 mt-5 mt-lg-0">
              <div id="qr-reader" class="text-center" style=""></div>
              <div id="qr-reader-results"></div>
              <br>
              <div class="row">
                <div class="col-md-12">
                  <input type="text" class="form-control" name="codigoqr" id="codigoqr" placeholder="Código QR"/>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-12">
                  <div class="text-center">
                    <button onclick="leercodigoqr();" type="button" id="btn-leerqr" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Leer qr</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End Login -->
    </main>

    <!-- ======= Footer ======= -->
      @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- SWEETALERT2-->
      <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
      <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <script>
        function is_numeric(value) {
            return !isNaN(parseFloat(value)) && isFinite(value);
        }

        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete"
                || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        docReady(function () {
            var resultContainer = document.getElementById('qr-reader-results');
            var lastResult, countResults = 0;
            function onScanSuccess(qrCodeMessage) {
                if (qrCodeMessage !== lastResult) {
                    // console.log(qrCodeMessage);
                    // window.location=qrCodeMessage;
                    location.href=qrCodeMessage;
                }else{
                    mostrarError('Código QR invalido');
                }
            }

            var html5QrcodeScanner = new Html5QrcodeScanner(
                "qr-reader", { fps: 10, qrbox: 250 });
            html5QrcodeScanner.render(onScanSuccess);
        });

        /**
         * Permite leer el codigo escrito y reedireccionar
         */
        function leercodigoqr(){
          var error = 0;
          var codigoqr = $("#codigoqr").val();
          if(codigoqr=== "undefined" || codigoqr=== ""){
              error=1;
              Swal.fire({
                  type: 'warning',
                  title: '¡Código QR!',
                  text: '¡Debe de anexar un valor para código de QR!'
              });
          }

          if(error==0){
            var baseurl = "{{ route('consultaqr') }}"+"/"+codigoqr;
            location.href=baseurl;
          }
        }
    </script>
@endsection
