@extends('plantillas.privada')
@section('content')
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">

            </div>
        </div>
    </div>
    <br>
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            {{ Form::open(['action' => 'CrearnotaController@update','id' => 'EditForm','method' => 'post','role'=>'form','class'=>'form-horizontal','data-smk-icon' => 'glyphicon-remove-sign']) }}
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="idremision" name="idremision" value="-1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('noticket') ? ' has-error' : '' }}">
                            {{ Form::label('noticket', 'Número de ticket') }}
                            <div class="input-group">
                                {{
                                    Form::text(
                                        'noticket',
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'noticket',
                                            'autofocus'=>true,
                                            'placeholder'=>'Ingrese un número de ticket',
                                            'onchange'=>'buscarconceptoseimportes();'
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('noticket'))
                            <span class="help-block">
                                <strong>{{ $errors->first('noticket') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required {{ $errors->has('idsucursal') ? ' has-error' : '' }}">
                            {{ Form::label('idsucursal', 'Sucursal') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idsucursal',
                                        $sucursales,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idsucursal',
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idsucursal'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idsucursal') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group required {{ $errors->has('idformapago') ? ' has-error' : '' }}">
                            {{ Form::label('idformapago', 'Forma de pago') }}
                            <div class="input-group">
                                {{
                                    Form::select(
                                        'idformapago',
                                        $formaspago,
                                        null,
                                        [
                                            'class'=>'form-control ',
                                            'id'=>'idformapago',
                                        ]
                                    )
                                }}
                            </div>
                        </div>
                        @if ($errors->has('idformapago'))
                            <span class="help-block">
                                <strong>{{ $errors->first('idformapago') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="button" class="btn btn-success" id="btn-facturar" style="display:none;" onclick="facturar();">
                                <i class="fas fa-receipt"></i>
                                Facturar
                            </button>
                            <button type="button" class="btn btn-primary" id="btn-guardar" style="display:none;" onclick="guardar();">
                                <i class="fas fa-save"></i>
                                Guardar
                            </button>
                            <button type="button" class="btn btn-danger" id="btn-eliminar" style="display:none;" onclick="eliminar();">
                                <i class="fas fa-trash-alt"></i>
                                Eliminar
                            </button>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-sm table-bordered table-striped table-vcenter">
                                <thead>
                                    <tr>
                                        <th style="width: 80%;">Concepto</th>
                                        <th style="width: 20%;" class="text-right">Importe</th>
                                    </tr>
                                </thead>
                                <tbody id="tabla_conceptosnota_tbody">
                                </tbody>
                                <tfoot id="tabla_conceptosnota_tfoot">
                                    <tr>
                                        <td colspan="2" style="text-align: right;" id="tabla_conceptosnota_tfoot_sumaimporte">

                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <!-- JQUERY NUMBER-->
        <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
    <!-- FIN DE JQUERY NUMBER-->
    <!-- NOTIFICACIONES-->
        <script src="{{ asset('plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
        <script>jQuery(function(){ Dashmix.helpers('notify'); });</script>
    <!-- FIN DE NOTIFICACIONES-->
    <script type="text/javascript">
        /*
         * Funcion que permite darle formato de moneda al input
         */
        function formatonumero(inputid){
            var value = $('#'+inputid).val();
            value = $.number(value, 2, '.', ',');
            $('#'+inputid).val(value);
        }


        /**
         * Permite buscar los conceptos de la nota (remision) por el noticket escrito
         */
        function buscarconceptoseimportes(){
            var nuevo = 0;
            var noticket = $("#noticket").val();
            var idsucursal = $("#idsucursal").val();
            if(noticket=== "undefined" || noticket=== ""){
                nuevo=1;
            }

            $.ajax({
                type: "POST",
                url: "{{ route('crearnotas.buscarconceptosdenota')}}",
                data: {
                        "_token": "{{ csrf_token() }}",
                        "nuevo": nuevo,
                        "noticket": noticket,
                        "idsucursal": idsucursal,
                },
                dataType:"json",
                success: function(respuesta) {
                    $("#tabla_conceptosnota_tbody").html(respuesta.html);
                    $("#idremision").val(respuesta.idremision);
                    sumarimportes();

                    if(respuesta.idremision==-1){
                        $("#btn-facturar").show();
                        $("#btn-guardar").show();
                        $("#btn-eliminar").hide();

                        $(".importes").attr('disabled',false);
                        $("#idsucursal").attr('disabled',false);
                        $("#idformapago").attr('disabled',false);

                        //CREANDO NUEVA NOTA
                        if(!(noticket=== "undefined" || noticket=== "")){
                            $.notify({
                                icon:"fas fa-plus-circle",
                                title: "",
                                message: "Usted está generando una nueva nota"
                            },{
                                type: "success",
                            });
                        }
                    }else{
                        $("#idremision").val(respuesta.idremision);
                        $("#idformapago").val(respuesta.idformapago);
                        if(respuesta.estatus=="F"){
                            //FACTURADA
                            $("#btn-facturar").hide();
                            $("#btn-guardar").hide();
                            $("#btn-eliminar").hide();

                            $(".importes").attr('disabled',true);
                            $("#idsucursal").attr('disabled',true);
                            $("#idformapago").attr('disabled',true);

                            //OBSERVANDO NOTA FACTURADA
                            $.notify({
                                icon:"fas fa-eye",
                                title: "",
                                message: "Usted está observando una nota facturada"
                            },{
                                type: "info",
                            });
                        }else{
                            //ACTIVA
                            $("#btn-facturar").show();
                            $("#btn-guardar").show();
                            $("#btn-eliminar").show();

                            $(".importes").attr('disabled',false);
                            $("#idsucursal").attr('disabled',false);
                            $("#idformapago").attr('disabled',false);

                            //EDITANDO NOTA NO FACTURADA
                            $.notify({
                                icon:"fas fa-exclamation",
                                title: "",
                                message: "Usted está editando una nota no facturada"
                            },{
                                type: "warning",
                            });
                        }
                    }
                },
                error: function(jqXHR, exception) {
                    if(jqXHR.status!=419){
                        // Swal.fire({
                        //     type: 'error',
                        //     title: 'There is an error!',
                        //     text: 'There is an error, please again later!'
                        // });
                    }else{
                        Swal.fire({
                            type: 'warning',
                            title: '¡Sesión expirada!',
                        });
                        setTimeout(
                            function(){
                                window.location.href='{{ route('login')}}'
                            },
                            3000
                        );
                    }
                }
            });
        }

        /**
         * Permite guardar la nota (remision)
         */
        function guardar(){
            var error=0;
            //VERIFICANDO QUE SUMA DE IMPORTES SEAN MAYOR QUE 0
            var suma = 0;
            var conceptos = [];
            $(".importes").each(function(){
                value = $(this).val();
                idconcepto = $(this).attr("data-idconcepto");
                if(value != ""){
                    value = value.replace(',','');
                    suma = suma + parseFloat(value);
                    if(parseFloat(value)>0){
                        fila = {};
                        fila.idconcepto = idconcepto;
                        fila.importe = value;
                        conceptos.push(fila);
                    }
                }
            });
            if(suma==0){
                error=1;
                Swal.fire({
                    type: 'warning',
                    title: '¡Importes!',
                    text: '¡No puede generar una nota con importe igual a cero (0)!'
                });
            }

            var noticket = $("#noticket").val();
            if(noticket=== "undefined" || noticket=== ""){
                error=1;
                Swal.fire({
                    type: 'warning',
                    title: '¡Número de ticket!',
                    text: '¡No puede generar una nota sin número de ticket!'
                });
            }

            var idsucursal = $("#idsucursal").val();
            if(idsucursal=== "undefined" || idsucursal=== ""){
                error=1;
                Swal.fire({
                    type: 'warning',
                    title: '¡Sucursal!',
                    text: '¡No puede generar una nota sin seleccionar una sucursal!'
                });
            }

            var idformapago = $("#idformapago").val();
            if(idformapago=== "undefined" || idformapago=== ""){
                error=1;
                Swal.fire({
                    type: 'warning',
                    title: '¡Forma de pago!',
                    text: '¡No puede generar una nota sin seleccionar una forma de pago!'
                });
            }

            if(error==0){
                var idremision = $("#idremision").val();
                var conceptos_json = JSON.stringify(conceptos);
                $.ajax({
                    type: "POST",
                    url: "{{ route('crearnotas.update')}}",
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "noticket": noticket,
                            "idremision": idremision,
                            "idsucursal": idsucursal,
                            "idformapago": idformapago,
                            "conceptos_json": conceptos_json,
                    },
                    dataType:"json",
                    success: function(respuesta) {
                        $("#tabla_conceptosnota_tbody").html(respuesta.html);
                        $("#idremision").val(respuesta.idremision);
                        sumarimportes();

                        if(respuesta.idremision==-1){
                            $("#btn-facturar").show();
                            $("#btn-guardar").show();
                            $("#btn-eliminar").hide();

                            $(".importes").attr('disabled',false);
                            $("#idsucursal").attr('disabled',false);
                            $("#idformapago").attr('disabled',false);

                            //CREANDO NUEVA NOTA
                            $.notify({
                                icon:"fas fa-plus-circle",
                                title: "",
                                message: "Usted está generando una nueva nota"
                            },{
                                type: "success",
                            });
                        }else{
                            $("#idremision").val(respuesta.idremision);
                            $("#idformapago").val(respuesta.idformapago);
                            if(respuesta.estatus=="F"){
                                //FACTURADA
                                $("#btn-facturar").hide();
                                $("#btn-guardar").hide();
                                $("#btn-eliminar").hide();

                                $(".importes").attr('disabled',true);
                                $("#idsucursal").attr('disabled',true);
                                $("#idformapago").attr('disabled',true);

                                //OBSERVANDO NOTA FACTURADA
                                $.notify({
                                    icon:"fas fa-eye",
                                    title: "",
                                    message: "Usted está observando una nota facturada"
                                },{
                                    type: "info",
                                });
                            }else{
                                //ACTIVA
                                $("#btn-facturar").show();
                                $("#btn-guardar").show();
                                $("#btn-eliminar").show();

                                $(".importes").attr('disabled',false);
                                $("#idsucursal").attr('disabled',false);
                                $("#idformapago").attr('disabled',false);

                                //EDITANDO NOTA NO FACTURADA
                                $.notify({
                                    icon:"fas fa-exclamation",
                                    title: "",
                                    message: "Usted está editando una nota no facturada"
                                },{
                                    type: "warning",
                                });
                            }
                        }
                    },
                    error: function(jqXHR, exception) {
                        if(jqXHR.status!=419){
                            // Swal.fire({
                            //     type: 'error',
                            //     title: 'There is an error!',
                            //     text: 'There is an error, please again later!'
                            // });
                        }else{
                            Swal.fire({
                                type: 'warning',
                                title: '¡Sesión expirada!',
                            });
                            setTimeout(
                                function(){
                                    window.location.href='{{ route('login')}}'
                                },
                                3000
                            );
                        }
                    }
                });

            }


        }

        /**
         * Permite sumar los importes de los conceptos y colocarlo como total
         * @return {[type]} [description]
         */
        function sumarimportes(){
            var suma = 0;
            $(".importes").each(function(){
                value = $(this).val();
                if(value != ""){
                    value = value.replace(',','');
                    suma = suma + parseFloat(value);
                }
            });

            suma = $.number(suma, 2, '.', ',');
            $("#tabla_conceptosnota_tfoot_sumaimporte").html(suma);
        }


        $(document).ready(function() {
            buscarconceptoseimportes();
        });
    </script>
@endsection

