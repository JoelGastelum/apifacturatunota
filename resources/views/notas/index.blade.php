@extends('plantillas.privada')
@section('content')
    <!-- BOOSTRAP DATEPICKER-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
    <script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                language: 'es',
                format: "dd-mm-yyyy",
            });
        });
    </script>

    <!--SELECT 2-->
    <link rel="stylesheet" href="{{ asset('privada/js/plugins/select2/css/select2.min.css') }}">
    <script src="{{ asset('privada/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>jQuery(function(){ Dashmix.helpers('select2'); });</script>


    <!-- ANEXANDO NAVEGACION -->
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-12">
            <div class="text-right">
                <?php if(isset($arraybtn['create'])):?>
                    <a href="{{ route('notas.create')}}" class="btn btn-sm btn-success">
                        <i class="fas fa-plus"></i> Crear
                    </a>
                <?php endif;?>
                <button type="button" class="btn btn-sm btn-info" onclick = "$('#filtrado_modal').modal('show');">
                    <i class="fas fa-filter"></i> Filtrar
                </button>
            </div>
        </div>
    </div>
    <br>

    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            @if(session('success'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('warning'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(session('danger'))
                <div class="row">
                    <div class="container">
                        <div class="alert alert-danger">
                            {{ session('danger') }}
                        </div>
                    </div>
                </div>
            @endif
            <div id="tabladatos" class="table2 table-responsive" style="font-size: 12px;">
            </div>
        </div>
    </div>

    <div class="modal fade" id="filtrado_modal" tabindex="-1" role="dialog" aria-labelledby="modal-block-fadein" aria-hidden="true">
        <div class="modal-dialog modal-dialog-fadein modal-lg" role="document">
            <div class="modal-content" id="capacitydetail_contenido">
                <div class="block block-rounded block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">
                            Filtrar listado
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('fecha_inicial') ? ' has-error' : '' }}">
                                    {{ Form::label('fecha_inicial', 'Fecha inicial') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::text(
                                                'fecha_inicial',
                                                '01-'.date('m-Y'),
                                                [
                                                    'class'=>'datepicker form-control',
                                                    'id'=>'fecha_inicial',
                                                    'name'=>'fecha_inicial',
                                                    'placeholder'=>'Ingrese fecha inicial',
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                                @if ($errors->has('fecha_inicial'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fecha_inicial') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('fecha_final') ? ' has-error' : '' }}" name="fecha_final">
                                    {{ Form::label('fecha_final', 'Fecha final') }}
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        {{
                                            Form::text(
                                                'fecha_final',
                                                date('d-m-Y'),
                                                [
                                                    'class'=>'datepicker form-control',
                                                    'id'=>'fecha_final',
                                                    'name'=>'fecha_final',
                                                    'placeholder'=>'Ingrese fecha final',
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                                @if ($errors->has('fecha_final'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fecha_final') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group required {{ $errors->has('estatus') ? ' has-error' : '' }}">
                                    {{ Form::label('estatus', 'Estatus') }}
                                    <div class="input-group">
                                        {{
                                            Form::select(
                                                'estatus',
                                                $estatus_notas,
                                                'T',
                                                [
                                                    'name'=>'estatus',
                                                    'class'=>'form-control js-select2',
                                                    'style'=>'width:100%;',
                                                ]
                                            )
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content block-content-full text-right bg-light">
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal" onclick="filtrar();">Filtrar</button>
                        <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->
    <script>
        function eliminar(id){
            Swal.fire({
                title: '¿Estas seguro que desea eliminar?',
                text: "Usted está por eliminar",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'No',
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Sí, estoy seguro!"
            }).then((result) => {
                if (result.value) {
                    $('#FormDelete'+id).submit();
                }
            });
        }

        var ObjTabla=new myDataTable('tabladatos',{
            height:450,
            columns:[
                {data:'id',visible:false},
                {data:'acciones',title:'Acciones',filter:false,visible:true},
                {data:'noticket',title:'Folio',filter:true,visible:true},
                {data:'nombrecliente',title:'Cliente',filter:true,visible:true},
                {data:'total',title:'Importe',filter:true,visible:true,style:"text-align:right;"},
                {data:'estatus',title:'Estatus',filter:true,visible:true}
            ],
            instanceVarName:'ObjTabla',
            responsive:true
        });

        /**
         * Funcion que permite filtrar el listado
         */
        function filtrar(){
            $.ajax({
                type: "POST",
                url: "{{ route('notas.loaddata') }}",
                dataType: 'json',
                beforeSend: function() {
                    Swal.fire({
                        title: 'Cargando, por favor espere',
                        allowOutsideClick: false
                    });
                    Swal.showLoading();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    'estatus':$("#estatus").val(),
                    'fecha_inicial':$("#fecha_inicial").val(),
                    'fecha_final':$("#fecha_final").val(),
                },
                success: function(respuesta) {
                    Swal.close();
                    ObjTabla.initializeTable(respuesta.data);
                },
                error: function(jqXHR, exception) {
                    Swal.close();
                    if(jqXHR.status!=419 && jqXHR.status!=401){
                        Swal.fire({
                            type: 'error',
                            title: '¡Ha ocurrido un error!',
                            text: '¡Ha ocurrido un error, por favor intente de nuevo!'
                        });
                    }else{
                        Swal.fire({
                            type: 'warning',
                            title: '¡Su sesión expiró!',
                        });
                        setTimeout(
                            function(){
                                window.location.href='{{ route('login')}}'
                            },
                            3000
                        );
                    }
                }
            });
        }

        $(document).ready(function() {
            filtrar();
        });
    </script>
@endsection

