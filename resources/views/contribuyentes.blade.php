@extends('plantillas.privada')
@section('content')
    <style>
        .movleft{
            transform: translateX(-100px);
            position:relative;
            background-color:#e6ebf4;
        }
        .movleft::after{
            position:absolute;
            z-index: -1;
            content: '\f303';
            font-family: 'Font Awesome 5 Free';
            top:0;
            height: 100%;
            /*width: 50%;*/
            right: -55px;
            font-weight: bold;
            background-color:#0665d0;
            padding-top: 25px;
            padding-left: 25px;
            padding-right: 25px;
            cursor:pointer;
            color:white;
        }

        .movright{
            transform: translateX(0px);
        }
    </style>

    <!--HAMMER JS-->
    <script src="https://hammerjs.github.io/dist/hammer.js"></script>
    <script src="{{ asset('plugins/jquery-hammer/jquery.hammer.js') }}"></script>
    <script src="{{ asset('plugins/jquery-hammer/PreventGhostClick.js') }}"></script>

   @if(session('success'))
        <div class="row">
            <div class="container">
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('warning'))
        <div class="row">
            <div class="container">
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            </div>
        </div>
    @endif
    @if(session('danger'))
        <div class="row">
            <div class="container">
                <div class="alert alert-danger">
                    {{ session('danger') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            @foreach($clientes as $cliente)
                <div id="{{$cliente->id}}" data-editar="{{route('clientes.edit',$cliente->id)}}" data-rfc="{{$cliente->rfc}}" data-hexa="{{$cliente->hexa}}" class="row row_clientes" style="border-bottom:solid 1px #e7eaf3;padding-top: 0.2rem;padding-right: 0.75rem;padding-bottom: 0.75rem;padding-left: 0.75rem;">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-10 col-sm-11" style="cursor:pointer;padding-top: 0.8rem;">
                                <span>
                                    <strong>{{$cliente->rfc}}</strong>
                                </span>
                            </div>
                            <div id="edit_{{$cliente->id}}" class="col-2 col-sm-1" style="cursor:pointer;" onclick="window.location='{{route('clientes.Viajes',array($cliente->rfc,-1))}}'">
                                <div class="text-right">
                                    <div class="item item-circle bg-body-dark" style="width: 3rem;height: 3rem;">
                                        <span class="fas fa-chevron-right" title="Facturas"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <span style="color:#929ca6;font-size: 0.8rem;">
                                    {{$cliente->nombre}}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <a class="btn-float-primary text-center" href="{{ route('clientes.create')}}">
	    <span class="fa fa-plus" style="margin-top:4px;line-height: 1.2;"></span>
    </a>

    <!-- Modal -->
    <div class="modal fade" id="modal-qr" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <img id="qr-image" src="" style="margin-bottom:15px" class="img-fluid" width="200px" height="200px" alt="QR image">
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <div class="text-center" id="codigoHex">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 offset-md-3">
                            <select name="idviaje" id="idviaje" class="form-control"></select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-11 offset-1 col-sm-9 offset-sm-3 col-lg-9 offset-lg-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend" onClick="compartir('WhatsApp')" >
                                        <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                            <i id="button" class="text-success fab fa-whatsapp" style="cursor:pointer;"></i>
                                        </span>
                                    </div>
                                    <div class="input-group-prepend" >
                                        <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                            <a id="botonCompartir" style="cursor:pointer;" href="whatsapp://send?text=aaa" data-action="share/whatsapp/share">
                                                <i class="fas fa-share-alt" style="font-size: 1.75em;"></i>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="input-group-prepend" onClick="compartir('Correo')"  >
                                        <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                            <a  style="cursor:pointer;">
                                                <i class="fa fa-envelope"></i>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="input-group-prepend" onClick="compartir('Sms')" >
                                        <span class="input-group-text" style="padding: 0.8rem 1.2rem;">
                                            <a  style="cursor:pointer;">
                                                <i class="fas fa-sms"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('numtel'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('numtel') }}</strong>
                                </span>
                            @endif
                            <input type="hidden" value="" id="urlQr">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-right">
                                <button type="button" class="btn btn-sm btn-light" onclick="cerrarmodalqr();" data-dismiss="modal" aria-label="Close">
                                    Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-compartir" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                  <h3 class="block-title" id="tipo"></h3>
                  <div class="block-options">
                      <button type="button" class="btn-block-option" onClick="cerrarmodalqr()" data-dismiss="modal" aria-label="Close">
                          <i class="fa fa-times-circle"></i>
                      </button>
                  </div>
                </div>
                <div class="block-content text-center">
                <input type="hidden" name="tipoEnvio" id="tipoEnvio">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i id='iconCompartir' class="fas fa-user-alt"></i>
                                        </span>
                                    </div>
                                    {{
                                        Form::text(
                                            'compartir',
                                            null,
                                            [
                                                'class'=>'form-control ',
                                                'id'=>'compartir',
                                                'placeholder'=>'',
                                                'required'=>true,
                                            ]
                                        )
                                    }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                <button type="button" onclick="completarEnvio()" class="btn btn-sm btn-success"> Enviar</button>
                            </div>
                        </div>
                    </div>
                    <br>
                  {{Form::close()}}
                </div>
              </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="buscar">

<!-- SWEETALERT2-->
<link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
    <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<!-- FIN DE SWEETALERT2-->
<!-- JQUERY NUMBER-->
    <script src="{{ asset('plugins/jquery-number/jquery.number.min.js') }}"></script>
<!-- FIN DE JQUERY NUMBER-->

<script>
    /*FUNCIONES PARA HAMMER.JS*/
    $(document).ready(function(){
        var nohacerclick=0;
        $(".row_clientes").each(function(){
            var self=this;
            $(this).hammer().bind("swipeleft", function(ev) {
                id=$(self).attr("id");
                $(".row_viajes").removeClass("movleft");
                // console.log(self);
                $(self).addClass("movleft");

                $("#edit_"+id).hide();
            });
            $(this).hammer().bind("swiperight", function(ev) {
                $(self).removeClass("movleft");
                $("#edit_"+id).show();
                nohacerclick=1;
            });
        });

        $(".row_clientes").click(function(e){
            if(nohacerclick==1){
                nohacerclick=0;
            }else{
                if(
                    $(this).hasClass("movleft")
                ){
                    if(
                        e.clientX > ($(this).width()+$(this).offset().left)
                    ){
                        var ruta=$(this).attr("data-editar");
                        window.location=ruta;
                    }else{
                    }
                }else{
                    // console.log('click normal');
                    var rfc=$(this).attr("data-rfc");
                    var hexa=$(this).attr("data-hexa");
                    abrirQRModal(rfc,hexa)
                }

            }
        });
    });

    //Funciones para compartir
        /**
         * Funcion que coloca el icono y la acción del formulario para compartir
         */
        function compartir(tipo){
            document.getElementById("compartir").value="";
            $('#tipo').html('Enviar por '+tipo);
            if(tipo=='Sms'){
                $('#compartir').prop('placeholder','Número teléfono');
                $('#tipoEnvio').val('Sms');
                $('#iconCompartir').prop('class','fas fa-sms');
            }
            if(tipo=='Correo'){
                $('#compartir').prop('placeholder','Correo');
                $('#tipoEnvio').val('Correo');
                $('#iconCompartir').prop('class','fa fa-envelope');

            }
            if(tipo=='WhatsApp'){
                $('#compartir').prop('placeholder','Número de teléfono');
                $('#tipoEnvio').val('WhatsApp');
                $('#iconCompartir').prop('class','text-success fab fa-whatsapp');
            }
            $('#modal-compartir').modal("toggle");
        }

        /**
         * Funcion que recibe el formulario de compartir y llama la funcion correspondiente
         */
        function completarEnvio(){
            let tipo=$('#tipoEnvio').val();
            if(tipo=='Sms'){
                enviarSms();
            }
            if(tipo=='Correo'){
                mandarACorreo();
            }
            if(tipo=='WhatsApp'){
                sendMsg();
            }
        }

        /**
         * Permite enviar sms
         */
        function enviarSms(){
            var message = document.getElementById("urlQr").value;

            var phone = document.getElementById("compartir").value;
            if (phone.length < 9 || phone.length < 2){
                Swal.fire({
                    type: 'warning',
                    title: '',
                    text: 'No se puede enviar a este numero'
                });
            }else{
                phone =	phone.replace(/ /gi, "")
                if (phone.length == 9 ){
                   phone =`34${phone}`
                }
                <?php  if($isAndroid==1):?>
                    window.location.href = `sms://+52${phone}?body=${message}`;
                <?php  else:?>
                    window.location.href = `sms://+52${phone}&body=${message}`;
                <?php  endif; ?>
                $("#modal-compartir").modal('toggle');
           }
        }

        /**
         * Permite mandar un correo para compartir el qr y su código
         */
        function mandarACorreo(){
            var mensaje = document.getElementById("urlQr").value;
            var correo = document.getElementById("compartir").value;
            var datos = new FormData();
                datos.append( '_token', "{{ csrf_token() }}");
                datos.append('mensaje',mensaje);
                datos.append('correo',correo);

            $.ajax({
                url: '{{url("contribuyentes/enviarqr")}}',
                type:'POST',
                data:datos,
                contentType:false,
                processData:false,
                success:function(respuesta){
                    if(respuesta.res){
                        Swal.fire({
                            type: 'success',
                            title: '',
                            text: 'Se envio su correo'
                        });
                        $("#modal-compartir").modal('toggle');
                    }else{
                        Swal.fire({
                            type: 'warning',
                            title: '',
                            text: 'Hubo un error'
                        });
                    }

                }
            });
        }

        /**
         * Enviar mensaje por whastapp
         */
        function sendMsg(){
            var message = document.getElementById("urlQr").value;
            var phone = document.getElementById("compartir").value;
            if (phone.length < 9 || phone.length < 2){
                Swal.fire({
                    type: 'warning',
                    title: '',
                    text: 'No se puede enviar a este numero'
                });
            }else{
               phone =	phone.replace(/ /gi, "")
               if (phone.length == 9 ){
                   phone =`34${phone}`
               }
               window.location.href = `https://api.whatsapp.com/send?phone=+52${phone}&text=${message}`;
               $("#modal-compartir").modal('toggle');
            }
        }

    /**
     * Permite generar y mostrar qr del cliente
     */
    function abrirQRModal(id,hexa){
        $('#buscar').val('1');
        $.ajax({
            type: "POST",
            url: "{{ route('contribuyentes.crearqr') }}",
            data: {
              "_token": "{{ csrf_token() }}",
              "iduser": "{{auth()->user()->id}}",
              "idcliente": id,
            },
            dataType:"json",
            async: false,
            success: function(respuesta) {
                if(respuesta.error==0){
                    document.getElementById("botonCompartir").href = "whatsapp://send?text="+respuesta.urlQr;

                    var src = respuesta.imagen;
                    var viajes=respuesta.viajes;
                    $('#codigoHex').html("Consulta con el código: "+hexa);
                    $("#qr-image").attr('src',src);
                    $('#modal-qr').modal({backdrop: 'static', keyboard: false});
                    select = document.getElementById("idviaje");

                    $('#idviaje').empty();

                        urlQr=document.getElementById('urlQr');
                        urlQr.value=respuesta.urlQr;


                    option = document.createElement("option");
                    option.value = 0;
                    option.text = 'Sin viaje';
                    select.appendChild(option);
                    for(i = 0; i <viajes.length; i++){

                        option = document.createElement("option");
                        option.value = viajes[i].id;
                        option.text = viajes[i].descripcion;
                        if(viajes[i].activo==1){
                        option.selected=true;
                       }
                        select.appendChild(option);
                    }
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: '',
                        text: 'No se consiguio QR, por favor intente de nuevo'
                    });
                }
                buscarrespuesta();
            },
            error: function() {
            }
        });
    }

    /**
     * Funcion que buscar si se realizado una factura con el qr que se muestra en el modal
     */
    function buscarrespuesta(){
        if($('#buscar').val()=='1'){
            $.ajax({
                type: "GET",
                url: "{{ url('empresa/listopdf/'.auth()->user()->id) }}",
                async: false,
                success: function(respuesta) {
                    if(!respuesta.res){
                        //NO LO HA CONSEGUIDO
                        setTimeout(function () {
                            buscarrespuesta();
                        }, 1000);
                    }else{
                        window.location=("{{url('contribuyentes/visualizarpdf')}}/"+respuesta.idcomprobante)
                    }
                },
                error: function() {
                }
            });
        }
    }

    /**
     * Permite cerrar modal y eliminar el qr creado
     * @return {[type]} [description]
     */
    function cerrarmodalqr(){
        $('#buscar').val('0');
        $.ajax({
           type: "POST",
           url: "{{ route('contribuyentes.eliminarqr') }}",
           data: {
            "_token": "{{ csrf_token() }}"
           },
           dataType:"json",
           async: false,
           success: function(respuesta) {
               // console.log(respuesta);
           },
           error: function() {
           }
       });
    }

    $('select').change(function() {
        $.ajax({
            type: "POST",
            url: "{{ route('clientes.activarviajeWeb')  }}",
            data:{"idviaje":this.value,
                "_token": "{{ csrf_token() }}",
                "idusuario": "{{auth()->user()->id}}",
            },
            dataType:"json",
            async: false,
            success: function(respuesta) {
              console.log(respuesta);
            },
            error: function() {
            }
        });
    });

    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
          navigator.serviceWorker.register('{{ route('welcome')  }}/sw2.js');
        });
    }
</script>

<style>
	.btn-float-primary{
		position: fixed;
		bottom: 20px;
		right: 20px;
		box-shadow: 0px 5px 5px -3px rgba(0,0,0,0.2), 0px 8px 10px 1px rgba(0,0,0,0);
		transform: translate3d(0,0,0);
		min-width: 0;
		border-radius: 50%;
		width: 56px;
		height: 56px;
		padding: 0;
		box-sizing: border-box;
		cursor: pointer;
		user-select: none;
		outline: none;
		border: none;
		display: inline-block;
		white-space: nowrap;
		text-decoration: none;
		vertical-align: baseline;
		font-size: 2rem;
		font-family: Roboto;
		font-weight: 500;
		background-color: #3b5998;
		color: #ffff;
	}
    .btn-float-primary:hover {
      color: #ffff;
    }
</style>
@endsection
