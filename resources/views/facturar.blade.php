@extends('plantillas.publica')
@section('content')
    <style>
        ul li:before {
            font-family: 'FontAwesome';
            content: '\f067';
        }
    </style>


    <!-- JS-->
        <script src="{{ asset('jquery/jquery-3.3.1.min.js') }}"></script>

    <!-- ======= Header ======= -->
      @include('elementos.public_header')
    <!-- End Header -->

    <!-- ======= factura ======= -->
    <section id="contact" class="contact">
      <br>
        @if(session('success'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif
        @if(session('danger'))
            <div class="row">
                <div class="container">
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                </div>
            </div>
        @endif
        <br>
      <br>
      <?php if($error==0):?>
        <div id="div_contenido" class="container" data-aos="fade-up">
          <div class="section-title" style="padding-bottom: 0px;">
            <h4><?php if(isset($objRemision->empresa->nombrecomercial)){ echo '<strong>'.$objRemision->empresa->nombrecomercial.'</strong>';}?> le ha generado una prefactura</h4>
            <h5>Fecha de consumo <strong>{{ date('d/m/Y',strtotime($objRemision->fecha)) }}</strong> de importe <strong>${{ number_format($objRemision->total,2,'.',',')}}</strong>.</h5>
          </div>
          <div class="row mt-3 justify-content-md-center" style="margin-top:0px;">
            <div class="col-lg-10 mt-3 mt-lg-0">
              <form id="registroform" action="#" method="post" role="form">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="idcliente" name="idcliente" value="-1">
                <input type="hidden" id="idempresa" name="idempresa" value="<?php if(isset($objRemision->empresa->id)){ echo $objRemision->empresa->id;}?>">
                <h5>Capture sus datos fiscales</h5>
                <div class="form-row">
                  <div class="col-md-4 form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-id-card"></i>
                            </span>
                        </div>
                        <input <?php if($disabled==true){echo "disabled";?> value="<?php if(isset($objRemision->cliente->rfc)){ echo $objRemision->cliente->rfc;}}?>" onchange="verificarrfc();" type="text" name="rfc" class="form-control" id="rfc" placeholder="RFC" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                    </div>
                  </div>
                  <div class="col-md-8 form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-user-alt"></i>
                            </span>
                        </div>
                        <input <?php if($disabled==true){echo "disabled";?> value="<?php if(isset($objRemision->cliente->nombre)){ echo $objRemision->cliente->nombre;}}?>" type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre o Razón Social" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-12 form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-envelope"></i>
                            </span>
                        </div>
                      <input <?php if($disabled==true){echo "disabled";?> value="<?php if(isset($objRemision->cliente->correo)){ echo $objRemision->cliente->correo;}}?>" type="email" name="correo" class="form-control" id="correo" placeholder="Ingrese correo electrónico" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" required/>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-md-6 form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-map-marker-alt"></i>
                            </span>
                        </div>
                        <textarea <?php if($disabled==true){echo "disabled";}?> class="form-control" name="calle" rows="4" id="calle" placeholder="Ingrese domicilio"><?php if(isset($objRemision->cliente->calle)){ echo $objRemision->cliente->calle;}?></textarea>
                    </div>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fas fa-globe-americas"></i>
                            </span>
                        </div>
                        <textarea <?php if($disabled==true){echo "disabled";}?> class="form-control" name="localidad" rows="4" id="localidad" placeholder="Ingrese localidad"><?php if(isset($objRemision->cliente->localidad)){ echo $objRemision->cliente->localidad;}?></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                    <div class="col-md-12 form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fas fa-user-secret"></i>
                                </span>
                            </div>
                            <select name="usodecfdi" id="usodecfdi" class="form-control">
                                  <?php foreach ( $objUsoComprobanteSat as $key => $value ):?>
                                      <option value="<?=$value;?>" <?php if($objRemision->idusocfdi==$key){echo "selected";}?>><?=$value;?></option>
                                  <?php endforeach;?>
                            </select>
                        </div>
                  </div>
                </div>
                <?php if(session('danger') || $typemsj=="danger") :?>
                   <div class="mb-3">
                      <div class="error-message" style="color: #fff;background: #ed3c0d;text-align: left;padding: 15px;font-weight: 600;">
                          <?php
                              if($typemsj=="danger"){
                                  echo $msj;
                              }else{
                                  echo session('danger');
                              }
                          ?>
                      </div>
                   </div>
                <?php else:?>
                    <div class="text-center">
                      <button type="button" onclick="guardarcliente();" id="btn-save" style="background: #E4AE30;border: 0;padding: 10px 30px;color: #fff;transition: 0.4s;border-radius: 4px;">Facturar</button>
                    </div>
                <?php endif;?>
              </form>
            </div>
          </div>
        </div>
      <?php endif;?>
      <?php if($error==-1):?>
        <?php if(session('danger') || $typemsj=="danger") :?>
          <div class="row mt-3 justify-content-md-center">
            <div class="col-lg-10 mt-3 mt-lg-0">
              <div class="mb-3">
                <div class="error-message" style="color: #fff;background: #ed3c0d;text-align: left;padding: 15px;font-weight: 600;">
                    <?php
                        if($typemsj=="danger"){
                            //NO CONSIGUE FACTURA
                            echo $msj;
                        }else{
                            echo session('danger');
                        }
                    ?>
                </div>
             </div>
            </div>
          </div>
        <?php endif;?>
      <?php endif;?>
      <div id="div_pdf" class="container" style="display:none;">
        <div class="section-title" style="padding-bottom: 0px;">
          <div class="error-message" style="color: #fff;background: #176010;text-align: left;padding: 15px;font-weight: 600;">
            Felicidades.. Factura generada correctamente.
          </div>
          <div id="herop">
            <a target="blank" id="download-xml" href="#" class="btn-get-started scrollto" style="background:#88E180;">Descargar Xml</a>
            <a target="blank" id="download-pdf" href="#" class="btn-get-started scrollto" style="background:#EC424A;">Descargar Pdf</a>
          </div>
          <hr>
          <h2>¿Aún no cuentas con mefactura.com?</h2>
          <div class="text-left">
            <div class="offset-md-2">
                <h3>Registrate y disfruta de sus beneficios:</h3>
                <ol>
                    <li>Solicita factura en cualquier establecimiento solo mostrando un código QR.</li>
                    <li>Administra en tu aplicación las facturas que recibas.</li>
                    <li>Genera dinero recomendando mefactura.com</li>
                </ol>
            </div>
          </div>
        </div>
        <div class="row mt-3 justify-content-md-center">
          <div class="col-lg-10 mt-3 mt-lg-0">
              <div class="mb-3">
                <div class="about-video">
                  <div class="col-lg-12 pt-3 pt-lg-0 content" data-aos="zoom-in" data-aos-delay="100">
                    <img src="{{ asset('publica/img/about-video.jpg') }}" class="img-fluid" alt="">
                    <a href="https://youtu.be/QtXPAg8Vrlc" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                  </div>
                </div>
              </div>
              <br>
              <div class="text-center">
                <h3>¿Qué esperas?. Regístrate es <a href="{{ route('consumidor') }}">gratis</a></h3>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End factura -->

    <!-- ======= Footer ======= -->
      @include('elementos.public_footer')
    <!-- End Footer -->

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>

    <!-- SWEETALERT2-->
        <link href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- FIN DE SWEETALERT2-->

    <script type="text/javascript">
        <?php if($error==0):?>
            /**
             * Permite verificar que campo email sea correcto
             */
            function validarEmail(valor) {
                re=/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
                if(!re.exec(valor)){
                    return 1;//ESTA MAL
                }else{
                    return 0;//ESTA BIEN
                }
            }

            /**
             * Funcion que permite verificar si correo ya existe
             */
            function verificarrfc(){
                var error=0;
                var rfc = $("#rfc").val();
                if(rfc=== "undefined" || rfc=== ""){
                    error=1;
                }

                if(error==0){
                    $.ajax({
                        type: "POST",
                        url: "{{ route('facturar.verificarrfc') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "rfc": rfc,
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            if(respuesta===null){
                                $("#idcliente").val(-1);
                            }else{
                                $("#idcliente").val(respuesta.id);
                                $("#nombre").val(respuesta.nombre);
                                $("#correo").val(respuesta.correo);
                                $("#calle").val(respuesta.calle);
                                $("#localidad").val(respuesta.localidad);
                                $("#correo").focus();
                            }
                        },
                        error: function() {
                        }
                    });
                }
            }

            /**
             * Funcion para crear el cliente (si no existe)
             */
            function guardarcliente(){
                $("#btn-save").attr('disabled',true);
                $('#btn-save').css("background-color","#E4AE3096");

                var msj='';
                var error=0;
                var usodecfdi = $("#usodecfdi").val();
                if(usodecfdi=== "undefined" || usodecfdi=== ""){
                    error=1;
                    msj = "Debe de seleccionar un valor para comprobante uso de CFDI";
                }
                var correo = $("#correo").val();
                if(correo=== "undefined" || correo=== ""){
                    error=1;
                    msj = "Correo electrónico debe contener valor";
                }else{
                    error=validarEmail(correo);
                    if(error==1){
                        msj = "Correo electrónico no válido";
                    }
                }
                var localidad = $("#localidad").val();
                // if(localidad=== "undefined" || localidad=== ""){
                //     error=1;
                //     msj = "Localidad debe contener valor";
                // }
                var calle = $("#calle").val();
                // if(calle=== "undefined" || calle=== ""){
                //     error=1;
                //     msj = "Domicilio debe contener valor";
                // }
                var nombre = $("#nombre").val();
                // if(nombre=== "undefined" || nombre=== ""){
                //     error=1;
                //     msj = "Nombre debe contener valor";
                // }
                var rfc = $("#rfc").val();
                if(rfc=== "undefined" || rfc=== ""){
                    error=1;
                    msj = "RFC debe contener valor";
                }
                var idcliente = $("#idcliente").val();
                var idempresa = $("#idempresa").val();
                if(idempresa=== "undefined" || idempresa=== ""){
                    error=1;
                    msj = "Empresa no encontrada, por favor pongase en contacto con fesoluciones";
                }

                if(error==1){
                    Swal.fire({
                        type: 'warning',
                        title: msj,
                        text: '',
                        allowOutsideClick: false
                    });
                    $("#btn-save").attr('disabled',false);
                    $('#btn-save').css("background-color","#E4AE30");
                }else{
                    //INSERTAR O MODIFICAR CLIENTE EN TABLA
                    $.ajax({
                        type: "POST",
                        url: "{{ route('facturar.guardarcliente') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "localidad":localidad,
                            "calle":calle,
                            "correo":correo,
                            "nombre":nombre,
                            "rfc":rfc,
                            "idempresa":idempresa,
                        },
                        dataType:"json",
                        success: function(respuesta) {
                            if(respuesta.error==0){
                                $("#idcliente").val(respuesta.idcliente);
                                facturacion();
                            }else{
                                Swal.fire({
                                    type: 'warning',
                                    title: 'No se pudo insertar el cliente',
                                    text: ''
                                });
                            }
                        },
                        error: function() {
                        }
                    });
                }
            }

            /**
             * Funcion para hacer la facturacion
             */
            function facturacion(){
                var msj='';
                var error=0;
                var usodecfdi = $("#usodecfdi").val();
                if(usodecfdi=== "undefined" || usodecfdi=== ""){
                    error=1;
                    msj = "Debe de seleccionar un valor para comprobante uso de CFDI";
                }
                var correo = $("#correo").val();
                if(correo=== "undefined" || correo=== ""){
                    error=1;
                    msj = "Correo electrónico debe contener valor";
                }else{
                    error=validarEmail(correo);
                    if(error==1){
                        msj = "Correo electrónico no válido";
                    }
                }
                var localidad = $("#localidad").val();
                // if(localidad=== "undefined" || localidad=== ""){
                //     error=1;
                //     msj = "Localidad debe contener valor";
                // }
                var calle = $("#calle").val();
                // if(calle=== "undefined" || calle=== ""){
                //     error=1;
                //     msj = "Domicilio debe contener valor";
                // }
                var nombre = $("#nombre").val();
                // if(nombre=== "undefined" || nombre=== ""){
                //     error=1;
                //     msj = "Nombre debe contener valor";
                // }
                var rfc = $("#rfc").val();
                if(rfc=== "undefined" || rfc=== ""){
                    error=1;
                    msj = "RFC debe contener valor";
                }
                var idcliente = $("#idcliente").val();
                var idempresa = $("#idempresa").val();
                if(idempresa=== "undefined" || idempresa=== ""){
                    error=1;
                    msj = "Empresa no encontrada, por favor pongase en contacto con el administrador de factura tu nota";
                }

                if(error==1){
                    Swal.fire({
                        type: 'warning',
                        title: msj,
                        text: '',
                        allowOutsideClick: false
                    });
                    $("#btn-save").attr('disabled',false);
                    $('#btn-save').css("background-color","#E4AE30");
                }else{
                    if(idcliente!=-1){
                        $.ajax({
                            type: "POST",
                            url: "{{ route('empresa.facturar') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "localidad":localidad,
                                "calle":calle,
                                "correo":correo,
                                "nombre":nombre,
                                "rfc":rfc,
                                "idcliente":idcliente,
                                "usodecfdi":usodecfdi,
                                "idempresa":idempresa,
                                "idremision":{{ $objRemision->id }},
                                "plataforma":'web',
                            },
                            dataType:"json",
                            beforeSend: function() {
                                Swal.fire({
                                    title: 'Facturando',
                                    allowOutsideClick: false
                                });
                                Swal.showLoading();
                            },
                            success: function(respuesta) {
                                Swal.close();
                                if(respuesta.res==true){
                                    $("#btn-save").attr('disabled',false);
                                    $('#btn-save').css("background-color","#E4AE30");
                                    Swal.fire({
                                        type: 'success',
                                        title: respuesta.msg,
                                        text: '',
                                        allowOutsideClick: false
                                    });

                                    //COLOCAR PDF EN OBJETO
                                      $("#div_contenido").hide();
                                      window.location = location.href;

                                      // //CARGAR OBJETO
                                      // //pdf
                                      // var url_base ="{{ route('empresa.facturar') }}";
                                      // url_base = url_base.replace('facturar', '');
                                      // url = url_base+'comprobante/'+respuesta.idcomprobante;
                                      // $("#download-pdf").attr('href',url);

                                      // //xml
                                      // var url_base ="{{ route('empresa.facturar') }}";
                                      // url_base = url_base.replace('facturar', '');
                                      // url = url_base+'descargarcomprobantexmlxid/'+respuesta.idcomprobante;
                                      // $("#download-xml").attr('href',url);
                                      // var url_base ="{{ route('empresa.facturar') }}";

                                      // $("#div_pdf").show();
                                }else{
                                    $("#btn-save").attr('disabled',false);
                                    $('#btn-save').css("background-color","#E4AE30");
                                    Swal.fire({
                                        type: 'error',
                                        title: respuesta.msg,
                                        text: '',
                                        allowOutsideClick: false
                                    });
                                }
                            },
                            error: function() {
                                Swal.close();
                            }
                        });
                    }
                }
            }

            $("#registroform").submit(function(){
                facturacion();
            });
        <?php endif;?>

        $(document).ready(function() {
            $("#menu-header").remove();
            $(".mobile-nav-toggle").remove();
          <?php if($disabled==true):?>
            $("#div_contenido").hide();
            <?php if($error==1):?>
              //pdf
              var url_base ="{{ route('empresa.facturar') }}";
              url_base = url_base.replace('facturar', '');
              url = url_base+'comprobante/'+{{ $objRemision->idcomprobante }};
              $("#download-pdf").attr('href',url);

              //xml
              var url_base ="{{ route('empresa.facturar') }}";
              url_base = url_base.replace('facturar', '');
              url = url_base+'descargarcomprobantexmlxid/'+{{ $objRemision->idcomprobante }};
              $("#download-xml").attr('href',url);

              $("#div_pdf").show();
            <?php endif;?>
          <?php else:?>

          <?php endif;?>
        });
    </script>
@endsection
