<?php

namespace App;

use App\Cliente;
use App\Bitacora;
use nusoap_client;
use DB;
use App\Empresa;
use ZipArchive;
use Illuminate\Database\Eloquent\Model;

class Comprobante extends Model
{
    public $timestamps=true;
    private $strmsg = "";
    protected $table = 'comprobantes';
    protected $fillable = [
        'idempresa',
        'tipo',
        'serie',
        'folio',
        'idcliente',
        'total',
		'fecha',
		'fechatimbre',
        'estatus',
        'uuid',
        'razoncancelacion',
        'metodopago',
        'numcuenta',
        'formapago',
        'condicionespago',
        'idsucursal',
        'idusocfdi',
        'tiporelacion',
        'cfdirelacionado',
        'version',
        'tipodocumento',
		'abonos',
		'cadenaoriginal',
		'sellodigitalsat',
		'sellodigitalemisor'
	];

	/**
	 * Permite retornar el mensaje de error
	 */
	function ErrorMsg()
	{
		return iconv('ISO-8859-1','UTF-8',$this->strmsg);
	}

    /**
     * Permite timbrar los comprobantes
     */
    function timbrar()
	{
		global $db;
		$valret=false;
		$this->strmsg='';

		$documento=array();
		//Generamos la Factura
		if($this->idempresa!=1){
			$documento['idexterno'] = $this->id;
		}

		$documento['serie'] = $this->serie;
		$documento['folio'] = $this->folio;
		//$documento['fecha'] = date('Y-m-d H:i:s');
		$documento['formapago'] = iconv('UTF-8','ISO-8859-1',$this->formapago);
		$documento['condicionesDePago'] = iconv('UTF-8','ISO-8859-1',$this->condicionespago);

        $documento['tipodocumento'] = $this->tipo;

        if($this->cfdirelacionados!=""){
            $this->tiporelacion ="01";
            $documento['CFDIRelacionados'] = [
                "TipoRelacion" =>  $this->tiporelacion,
                "UUIDS" => $this->cfdirelacionados
            ];
		}


		$documento['metodopago'] = iconv('UTF-8','ISO-8859-1',$this->metodopago);



        $objcte=Cliente::findOrFail($this->idcliente);


		//Agregamos los datos del Receptor
		$documento['receptor']['rfc'] = $objcte->rfc;
		$documento['receptor']['nombre'] = iconv('UTF-8','ISO-8859-1',$objcte->nombre);
		if($objcte->rfc!="XAXX010101000"){
			$documento['receptor']['calle'] = iconv('UTF-8', 'ISO-8859-1', $objcte->calle);
			$documento['receptor']['num_ext'] = iconv('UTF-8', 'ISO-8859-1', $objcte->noexterior);
			$documento['receptor']['num_int'] = iconv('UTF-8', 'ISO-8859-1', $objcte->nointerior);
			$documento['receptor']['colonia'] = iconv('UTF-8', 'ISO-8859-1', $objcte->colonia);
			$documento['receptor']['localidad'] = iconv('UTF-8', 'ISO-8859-1', $objcte->localidad);
			$documento['receptor']['municipio'] = iconv('UTF-8', 'ISO-8859-1', $objcte->municipio);
			$documento['receptor']['estado'] = iconv('UTF-8', 'ISO-8859-1', $objcte->estado);
			//$documento['receptor']['pais'] = iconv('UTF-8','ISO-8859-1',$objcte->pais);
			$documento['receptor']['codigopostal'] = $objcte->codigopostal;
		}

        $documento['receptor']['usodelcomprobante'] = $this->idusocfdi;


        $arrayIva = [];
        $arrayIeps = [];

		//Agregamos los conceptos
		$i = 0;
		$subt = 0;

		//print_r($this->conceptos);
		$importeCalculado = 0;
		$importehospedaje = 0;

		$conceptos=DB::select("Select * from detallecomprobantes where idcomprobante =".$this->id);
        foreach($conceptos as $concepto)
		{
			$concepto->iva=0;
			//$documento['conceptos'][$i]['codigo'] = $concepto->clave;
            $documento['conceptos'][$i]['clave'] = $concepto->codigogrupoprodsersat;
            $documento['conceptos'][$i]['cantidad'] = $concepto->cantidad;
            $documento['conceptos'][$i]['codigounidad'] = $concepto->codigounidadsat;
            $documento['conceptos'][$i]['unidad'] =$concepto->unidad;
			$documento['conceptos'][$i]['descripcion'] = iconv('UTF-8','ISO-8859-1',$concepto->descripcion);
			$documento['conceptos'][$i]['valorUnitario'] = $concepto->precio;
            $documento['conceptos'][$i]['importe'] = $concepto->importe;
			//$documento['conceptos'][$i]['descuento'] = 0;

			$importeCalculado += round((floatval($concepto->importe)+floatval($concepto->importeiva)),2);

            $iva = [];

        	//REGISTRANDO IVA DEL CONCEPTO
            if($concepto->tasaiva>-1){
                $iva['impuesto'] = '002';
                $iva['base'] = $concepto->importe;
                $iva['factor'] = 'Tasa';
                $iva['tasaocuota'] = $concepto->tasaiva/100;
                $iva['importe'] = $concepto->importeiva;

                $documento['conceptos'][$i]['impuestostrasladados'][] = $iva;

            	//ACUMULANDO DIFERENTES TASAS DE IVA EN arrayIva
                if(isset($arrayIva[$concepto->tasaiva])){
                    $arrayIva[$concepto->tasaiva]['importe']+=$iva['importe'];
                }else{
                    $arrayIva[$concepto->tasaiva]['tipofactor'] = 'Tasa';
                    $arrayIva[$concepto->tasaiva]['impuesto'] = $iva['impuesto'];
                    $arrayIva[$concepto->tasaiva]['tasaocuota'] = $iva['tasaocuota'];
                    $arrayIva[$concepto->tasaiva]['importe'] = $iva['importe'];
				}
            }

            //ACUMULANDO IMPORTE HOSPEDAJE
            if($concepto->importehospedaje>0){
            	$importehospedaje +=$concepto->importehospedaje;
            }

            /*$ieps = [];
            if($concepto->eips>0){
                $ieps['impuesto'] = '003';
                $ieps['base'] = $concepto->importe;
                $ieps['factor'] = 'Tasa';
                $ieps['tasaocuota'] = $concepto->tasaieps;
                $ieps['importe'] = $concepto->importeieps;

                $documento['conceptos'][$i]['impuestosretenidos'][] = $ieps;

                if(isset($arrayIeps[$concepto->tasaieps])){
                    $arrayIeps[$concepto->tasaieps]['importe']+=$ieps['importe'];

                }else{
                    $arrayIeps[$concepto->tasaieps]['tipofactor'] = 'Tasa';
                    $arrayIeps[$concepto->tasaieps]['impuesto'] = $ieps['impuesto'];
                    $arrayIeps[$concepto->tasaieps]['tasaocuota'] = $ieps['tasaocuota'];
                    $arrayIeps[$concepto->tasaieps]['importe'] = $ieps['importe'];
                }
            }*/

			$subt+=$concepto->importe;
			$i++;
        }

        if($importehospedaje>0){
        	$documento['impuestoslocales'][]=array(
        		'tipo'=>'traslados',
        		'descripcion'=>'ISH',
        		'tasa'=>0.03,
        		'importe'=>number_format($importehospedaje, 2 , '.',"")
        	);
        }

		$documento['subtotal'] = number_format($subt,2,".","");

        $traslados = ["totaltraslados" => 0, "impuestostrasladados" => []];
        foreach($arrayIva as $iva){
            $traslados['totaltraslados'] += $iva['importe'];
            $traslados['impuestostrasladados'][] = $iva;
        }

        // foreach($arrayIeps as $ieps){
        //     $traslados['totaltraslados'] += $ieps['importe'];
        //     $traslados['impuestostrasladados'][] = $ieps;
        // }

        $traslados['totaltraslados'] = number_format($traslados['totaltraslados'], 2 , '.',"");
        $documento['traslados'] = $traslados;
		//total
		$documento['total'] = $this->total;
		// $documento['total'] = $importeCalculado;
		//Moneda
		$documento['moneda'] =  'MXN';
		$documento['tipocambio']  =  1;
		//error_reporting(0);


		$nuSoapClient = new nusoap_client("http://integratucfdi.com/webservices/wscfdi.php?wsdl",true);
		$err = $nuSoapClient->getError();
		if ($err){
			// echo 'error soap';
			// exit();
			$this->strmsg=$err;
		}else
		{

			//Tomar la serie en base al tipo de comprobante
			if($this->tipo=='I'){
				$query = "SELECT idsucursalintegra , idserie FROM sucursales where id = $this->idsucursal";
			}else{
				$query = "SELECT idsucursalintegra, idserienc as idserie FROM sucursales where id = $this->idsucursal";
			}

			$res = DB::select($query);
			$rowsucursal = $res[0];

			$query = "SELECT * FROM empresas where id = $this->idempresa";
			$res = DB::select($query);

			$rowempresa= $res[0];

			$documento['regimenfiscal'] = $rowempresa->regimen;

			$query = "SELECT idcertificadointegra FROM series where id =".$rowsucursal->idserie;
			$res = DB::select($query);
			$rowserie = $res[0];

		// 	print_r(array ('id'=> $rowsucursal['idsucursalintegra'],
		// 																'rfcemisor' =>$rowempresa['rfc'],
		// 																'idcertificado'  => $rowserie['idcertificadointegra'],
		// 																'documento'  => $documento,
		// 																'usuario'  => $rowempresa['usremisor'],
		// 																'password'  => $rowempresa['pwdemisor']
		// ));
            //Invocamos el metodo para generar el comprobante
			// print_r($documento);
			// exit;
			//print_r("idsucursalintegra: ".$rowsucursal['idsucursalintegra']." - rfc: ".$rowempresa['rfc']." - idcertificadointegra: ".$rowserie['idcertificadointegra']." - usremisor: ".$rowempresa['usremisor']." - pwdemisor: ".$rowempresa['pwdemisor']);


            $respuesta = $nuSoapClient->call('GenerarComprobante',array (
            															'id'=> $rowsucursal->idsucursalintegra,
																		'rfcemisor' =>$rowempresa->rfc,
																		'idcertificado'  => $rowserie->idcertificadointegra,
																		'documento'  => $documento,
																		'usuario'  => $rowempresa->usremisor,
																		'password'  => $rowempresa->pwdemisor
                    						));

			// print_r($respuesta);
			// exit;
			$respuesta['mensaje'] = iconv('ISO-8859-1', 'UTF-8', $respuesta['mensaje']);
			// echo 'respuesta: '.$respuesta['mensaje'];
			// exit();
			Bitacora::setMsj(json_encode($documento), json_encode($respuesta));
			if ($nuSoapClient->fault)
				$this->strmsg='Falla al timbrar el comprobante en SFE';
			else
			{
				$err  =  $nuSoapClient->getError();
				if ($err){
					$this->strmsg='Error al timbrar el comprobante en SFE:'.$err;
                }
				else
				{
					if($respuesta['resultado'])
					{
						//Actualizar DB de folios.
						$empresa = new Empresa();
						$empresa=Empresa::findOrFail($this->idempresa);

						$empresa->foliosconsumidos += 1;
						$empresa->folioscomprados  -= 1;
						$empresa->save();

						//echo "SI TIMBRE";
						//Tomar la informacion de Retorno
						$dir ="comprobantes/emisores/".$rowempresa->rfc."/";
						//Leer el Archivo Zip
						$fileresult = $respuesta['archivos'];
						$strzipresponse = base64_decode($fileresult);
						$filename = "cfdi_{$this->idempresa}_{$this->id}";
						$zip  =  new ZipArchive;
						if(!is_dir($dir)){
							mkdir($dir);
						}
						file_put_contents($dir.$filename.'.zip', $strzipresponse);
						if ($zip->open($dir.$filename.'.zip')===TRUE)
						{
							$strxml = $zip->getFromName('xml.xml');
							file_put_contents($dir.$filename.'.xml', $strxml);
							$strpdf = $zip->getFromName('formato.pdf');
							file_put_contents($dir.$filename.'.pdf', $strpdf);
							$zip->close();
							//Actualizar el comprobante
							$this->cadenaoriginal=$respuesta['cadenaoriginal'];
							$this->sellodigitalemisor = $respuesta['sellodocumento'];
							$this->sellodigitalsat = $respuesta['sellotimbre'];
							$this->estatus='T';
							$this->uuid = $respuesta['uuid'];
							$this->fecha = $respuesta['fechadocumento'];
							$this->fechatimbre = $respuesta['fechatimbre'];
							if($this->save()){
								$valret=true;
							}
						}
						else{
							$this->strmsg = 'Error al descomprimir el archivo'.$dir.$filename.'.zip';
						}
					}
					else{
						//EVALUAR LOS MENSAJES RECIBIDOS DE INTEGRA
						$sql = "
							SELECT *
							FROM mensajestimbrados
							WHERE ? like concat('%',cadenadebusqueda,'%')
						";
						$row = DB::select($sql,[$respuesta['mensaje']]);
						if(count($row)>0){
							if(isset($row[0])){
								$this->strmsg = $row[0]->cadenalegible;
								$this->strmsg = str_replace('{{rfc}}',$documento['receptor']['rfc'],$this->strmsg);
							}else{
								$this->strmsg = $respuesta['mensaje'];
							}
						}else{
							$this->strmsg = $respuesta['mensaje'];
						}
					}
				}
			}
			//echo $respuesta['strmsg'];
		}
		return $valret;
	}

		function clearconceptos()
		{
			$this->conceptos=array();
		}
		function addconcepto($concepto)
		{
			array_push($this->conceptos,$concepto);
		}
}
