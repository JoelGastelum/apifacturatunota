<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    public $timestamps=false;
    protected $table = 'certificados';
}

