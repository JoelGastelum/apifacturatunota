<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnviarZip extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'Ha recibido tus facturas!';
    public $data;
    public $excel;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$excel)
    {
        //
        $this->data=$data;
        $this->excel=$excel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.Zip')
        ->attachData($this->data, 'MisFactura.zip', [
            'mime' => 'application/zip',
            ]
        );
    }
}
