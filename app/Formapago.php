<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formapago extends Model
{
    public $timestamps=false;
    protected $table = 'formaspago';
}

