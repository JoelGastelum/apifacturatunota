<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombreruta
 * @property int $idpadre
 * @property string $icono
 * @property string $nombre
 * @property string $orden
 * @property string $estatus
 * @property string $created_at
 * @property string $updated_at
 */
class Opcion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'opciones';

    /**
     * @var array
     */
    protected $fillable = ['id', 'nombreruta', 'idpadre', 'icono', 'nombre', 'estatus', 'created_at', 'updated_at'];
}
