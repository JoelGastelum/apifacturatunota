<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Afiliado extends Model
{
    public $timestamps=false;
    protected $table = 'afiliados';
}

