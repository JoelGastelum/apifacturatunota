<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $iduser
 * @property int $idopcion
 * @property string $estatus
 * @property string $created_at
 * @property string $updated_at
 */
class Permiso extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permisos';

    /**
     * @var array
     */
    protected $fillable = ['id', 'iduser', 'idopcion','estatus', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'iduser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function opcion()
    {
        return $this->belongsTo('App\Opcion', 'idopcion');
    }

    /**
     * Verifica si tiene permiso para acceder a esa ruta
     */
    public function VerificarPermiso($idtipousuario, $nombreruta)
    {
        // dd($idtipousuario,$nombreruta);
        $sql ='
            SELECT
                count(*) AS valido
            FROM permisos AS Permiso
            WHERE
                Permiso.idtipousuario='.$idtipousuario.'
                AND Permiso.idopcion IN (
                    SELECT
                        id
                    FROM opciones AS Opcion
                    WHERE Opcion.nombreruta="'.$nombreruta.'"
                    AND Opcion.estatus ="A"
                )
                AND Permiso.estatus ="A"
        ';
        $data = DB::select($sql);
        // dd($sql);
        if($data[0]->valido>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Permite crear el menu del usuario segun sus permisos y del modulo de donde se encuentre
     */
    public function menu($idpadre=0,$idtipousuario,$ruta,$accion)
    {
        $valret='';
        $sql ='
            SELECT
                Opcion.id,
                Opcion.controlador,
                Opcion.accion,
                Opcion.nombreruta,
                Opcion.idpadre,
                Opcion.icono,
                Opcion.nombre,
                Opcion.orden
            FROM opciones AS Opcion
            INNER JOIN permisos Permiso ON Permiso.idopcion=Opcion.id
            WHERE
                Permiso.idtipousuario='.$idtipousuario.'
                AND Permiso.estatus="A"
                AND Opcion.idpadre='.$idpadre.'
                AND Opcion.nombre IS NOT null
            ORDER BY Opcion.orden DESC
        ';
        $opciones = DB::select($sql);
        if(count($opciones)>0)
        {
            foreach($opciones as $opcion)
            {
                $hijos=$this->menu($opcion->id,$idtipousuario,$ruta,$accion);
                $open='';
                $active='';
                if($opcion->controlador!=''){
                    //VERIFICAMOS SI TIENE VARIOS CONTRALADORES
                    if(explode('@',$opcion->controlador)>0){
                        $array=explode('@',$opcion->controlador);
                        for($i=0;$i<count($array);$i++){
                            if(strpos($ruta, $array[$i]) !== false){
                                $open='open';
                                // if($opcion->accion == $accion){
                                    $active='active';
                                // }
                                break;
                            }
                        }
                    }else{
                        if(strpos($ruta, $opcion->controlador) !== false){
                            $open='open';
                            // if($opcion->accion == $accion){
                                $active='active';
                            // }
                        }
                    }
                }
                if($hijos!=''){
                    //TIENE HIJOS
                    $valret.='
                        <li class="nav-main-item '.$open.'">
                            <a class="nav-main-link nav-main-link-submenu '.$active.'" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="">
                                <i class="nav-main-link-icon '.$opcion->icono.'"></i>
                                <span class="nav-main-link-name">'.$opcion->nombre.'</span>
                            </a>
                            <ul class="nav-main-submenu">
                                '.$hijos.'
                            </ul>
                        </li>';
                }
                else{
                    $href=route($opcion->nombreruta);
                    if($idpadre==0){
                        $valret.='
                            <li class="nav-main-item">
                                <a class="nav-main-link  '.$active.'" href="'.$href.'">
                                    <i class="nav-main-link-icon '.$opcion->icono.'"></i>
                                    <span class="nav-main-link-name">'.$opcion->nombre.'</span>
                                </a>
                            </li>
                        ';
                    }
                    else{
                        $valret.='
                            <li class="nav-main-item">
                                <a class="nav-main-link  '.$active.'" href="'.$href.'">
                                    <i class="nav-main-link-icon '.$opcion->icono.'"></i>
                                    <span class="nav-main-link-name">'.$opcion->nombre.'</span>
                                </a>
                            </li>
                        ';
                    }
                }
            }
        }
        return $valret;
    }

    /**
     * Verifica los permisos que tiene el usuario referido al controlador
     */
    public function VerificarPermisosDelControlador($idtipousuario, $controlador)
    {
        $sql ='
            SELECT
                Permiso.idtipousuario,
                Permiso.idopcion,
                Permiso.estatus,
                Opcion.accion
            FROM permisos AS Permiso
            INNER JOIN opciones AS Opcion ON Opcion.id=Permiso.idopcion
            WHERE
                Permiso.idtipousuario='.$idtipousuario.'
                AND Opcion.controlador="'.$controlador.'"
                AND Opcion.idpadre <> 0
                AND Opcion.estatus ="A"
                AND Permiso.estatus ="A"
        ';
        $data = DB::select($sql);
        return $data;
    }

    /**
     * Permite eliminar todos los permisos del usuario
     */
    public function DeletePermisosPorLoteDeUsuario($iduser)
    {
        $sql ='
            DELETE FROM permisos
            WHERE iduser = '.$iduser.'
        ';
        $data = DB::select($sql);
        return true;
    }
}
