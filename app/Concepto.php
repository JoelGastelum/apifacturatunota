<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    public $timestamps=false;
    protected $table = 'conceptosxdefault';
    
    protected $fillable = [
        'codigo','concepto','codigounidadsat','unidad','codigogrupoprodsersat','idempresa'
    ];
}

