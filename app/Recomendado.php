<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recomendado extends Model
{
    //

    protected $table = 'recomendados';
    public $timestamps=false;
    protected $fillable = [
        'idusuario',
        'nombre',
        'direccion',
        'telefono',
        'rechaRec',
        'fechaComp',
        'fechaPag',
        'comision',
        'estatus',
        'personacontacto'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'idusuario', 'id');
    }
}
