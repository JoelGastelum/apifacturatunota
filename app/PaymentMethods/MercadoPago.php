<?php

namespace App\PaymentMethods;

use App\Orden;
use Illuminate\Http\Request;
use MercadoPago\Item;
use MercadoPago\MerchantOrder;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;

class MercadoPago
{
	public function __construct()
	{
		// SDK::setClientId(
			// config("payment-methods.mercadopago.client")
		// );
		// SDK::setClientSecret(
			// config("payment-methods.mercadopago.secret")
		// );
	}

	public function setupPaymentAndGetRedirectURL(Orden $orden): string
	{
		//BUSCANDO EL ACCESSTOKEN
		if(env('SANDBOX_MP')==true){
			//ES SANDBOX
			SDK::setAccessToken(env('MP_SANDBOX_ACCESS_TOKEN'));
		}else{
			//ES PRODUCCION
			SDK::setAccessToken(env('MP_ACCESS_TOKEN'));
		}

		// PRUEBA BASICA
		// $itemserv = new Item();
		// $itemserv->title ='producto';
		// $itemserv->quantity = 1;
		// $itemserv->unit_price = 1000;
		// $itemserv->currency_id = "MXN";
		// $preference = new Preference();
		// $preference->binary_mode=true;
		// $preference->back_urls = array(
		//     "success" => route('mercadopago.gracias'),
		// 	"pending" => route('mercadopago.pendiente'),
		// 	"failure" => route('mercadopago.fallido'),
		// );
		// $preference->auto_return = "approved";
		// $preference->installments=1;
		// $preference->external_reference= substr( md5(microtime()), 1, 19);
		// $preference->items = array($itemserv);
		// $preference->save();
		// dd($preference);




	 	# Create a preference object
	 	$preference = new Preference();

		# Create an item object
		$item = new Item();
		$item->id = $orden->id;
		$item->title = $orden->titulo;
		$item->quantity = 1;
		$item->currency_id = 'MXN';
		$item->unit_price = $orden->total;
		// $item->picture_url = $orden->featured_img;

		# Create a payer object
		$payer = new Payer();
		$payer->email = $orden->email;
		$payer->name = $orden->razonsocial;

		# Setting preference properties
		$preference->items = array($item);
		// $preference->payer = $payer;

		# Save External Reference
		$preference->external_reference = $orden->id;
		$preference->back_urls = [
			"success" => route('mercadopago.gracias'),
			"pending" => route('mercadopago.pendiente'),
			"failure" => route('mercadopago.fallido'),
		];

		$preference->auto_return = "all";
		// $preference->notification_url = route('ipn');
		// $preference->notification_url = route('welcome');

		//PREFERENCIAS NUEVAS
		$preference->binary_mode=true;
		$preference->auto_return = "approved";
        $preference->installments=1;
        $preference->external_reference= $orden->tracker;

		# Save and POST preference
		$preference->save();

		if(env('SANDBOX_MP')==true){
			return $preference->sandbox_init_point;
		}
	  	return $preference->init_point;
	}
}
