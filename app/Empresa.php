<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    public $timestamps=false;
    protected $table = 'empresas';
    
    protected $fillable = [
       'idempresa',
       'idafiliado',
       'razonsocial',
       'nombrecomercial',
       'regimen',
       'rfc',
       'email ',
       'estatus',
       'usremisor',
       'pwdemisor',
       'fechavigencia',
       'idusocfdidefault',
       'grupoprodserdefault',
       'unidadsatdefault',
       'facturaglobal',
       'idpublicogeneral',
       'fechaalta',
       'idempresaintegra',
       'pasowizard',
       'folioscomprados',
       'foliosconsumidos',
    ];
}
