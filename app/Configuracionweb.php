<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $parametros
 * @property string $tipo
 * @property string $created_at
 * @property string $updated_at
 */
class Configuracionweb extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'configuracionweb';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'parametros', 'tipo', 'created_at', 'updated_at'];

}
