<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordenesdetalle extends Model
{
    protected $table = 'ordenesdetalles';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orden()
    {
        return $this->belongsTo('App\Orden', 'idorden');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Servicio', 'idservicio');
    }
}

