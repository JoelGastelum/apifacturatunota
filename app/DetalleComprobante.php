<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleComprobante extends Model
{
    //
    public $timestamps=true;
    protected $table = 'detallecomprobantes';

    protected $fillable = [
        'idempresa',
        'idremision',
        'descripcion',
        'unidad',
        'cantidad',
        'precio',
        'tasaiva',
        'importeiva',
        'tasaieps',
        'importeieps',
        'importe',
        'total',
        'idcomprobante',
        'codigounidadsat',
        'codigogrupoprodsersat'
    ];
}
