<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //

    public $timestamps=false;
    protected $table = 'clientes';
    
    protected $fillable = [
        'idempresa', 'idusuario', 'rfc','nombre','calle','noexterior','nointerior','colonia','localidad','municipio','estado','pais','codigopostal','correo','estatus'
    ];
}
