<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remision extends Model
{
    protected $table="remisiones";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idempresa',
        'noticket' ,
        'idcliente',
        "fecha",
        "subtotal",
        "ieps",
        "iva",
        "total",
        "idformapago",
        "formapago",
        "fechavence",
        "estatus",
        "uuid",
        "serie",
        "folio",
        "razoncancelacion",
        "idsucursal",
        "numerocliente",
        "nombrecliente",
        "idcomprobante",
        "saldo",
        "idconcilacion"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Cliente', 'idcliente', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formaspago()
    {
        return $this->belongsTo('App\Formapago', 'idformapago');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comprobante()
    {
        return $this->belongsTo('App\Comprobante', 'idcomprobante', 'id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usocomprobantesat()
    {
        return $this->belongsTo('App\Usocomprobantesat', 'idusocfdi', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sucursales()
    {
        return $this->belongsTo('App\Sucursal', 'idsucursal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'idempresa', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function remisionesdetalle()
    {
        return $this->hasMany('App\Remisiondetalle', 'idremision');
    }
}
