<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    public $timestamps=false;
    protected $table = 'usuarios';

    protected $fillable = [
        'login', 'nombre', 'pwd','movil','estatus','idtipousuario','idempresa',
        'idsucursal','tokenlogin','clabeinterbancaria'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo('App\Empresa', 'idempresa');
    }
}
