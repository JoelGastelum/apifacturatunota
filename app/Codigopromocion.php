<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigopromocion extends Model
{
    protected $table = 'codigopromociones';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recomendado()
    {
        return $this->belongsTo('App\Recomendado', 'idrecomendado', 'id');
    }
}

