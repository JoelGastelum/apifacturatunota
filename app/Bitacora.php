<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    public $timestamps=false;
    protected $table = 'bitacora';
    
    protected $fillable = [
        'respuesta','fecha','peticion'
    ];


    static function setMsj($msjdocumento, $msjpack){
        $bitacora = new Bitacora();
        $bitacora->peticion = $msjdocumento;
        $bitacora->respuesta = $msjpack;
        $bitacora->fecha = date('Y-m-d');
        
        return $bitacora->save();
    }
}

