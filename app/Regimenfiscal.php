<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regimenfiscal extends Model
{
    protected $table = 'regimenfiscal';
    public $timestamps=false;
}
