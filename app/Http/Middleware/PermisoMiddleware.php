<?php

namespace App\Http\Middleware;

use Closure;
use App\Modulo;
use App\Permiso;

use Illuminate\Support\Facades\Route;

class PermisoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //ARMANDO EL MENU DEL USUARIO
        $ruta= Route::getCurrentRoute()->getActionName();
        $accion=explode('@', Route::getCurrentRoute()->getActionName())[1];
        $objpermiso = new Permiso;
        $menu = $objpermiso->menu(0,auth()->user()->idtipousuario,$ruta,$accion);
        session()->put('Menu',$menu);

        //VERIFICANDO LA PERMISOLOGIA DEL USUARIO
        $nombreruta= Route::currentRouteName(); // Nombre de la ruta;
        if($nombreruta=='home' || $nombreruta=='logout' || $nombreruta=='eliminartoken' || $nombreruta=='contribuyentes.visualizarpdf'){
            return $next($request);
        }else{
            $idtipousuario=auth()->user()->idtipousuario;

            if(
                $nombreruta=='users.privacidad'
                || $nombreruta=='users.sprivacidad'
                || $nombreruta=='usuarios.cambiarcontrasena'
                || $nombreruta=='usuarios.saveContra'
            )
            {
                $validez=true;
            }else{
                //BUSCAR SI EL PERFIL TIENE PERMISO PARA ESE NOMBRE RUTA
                $objPermiso = new Permiso;
                $validez=$objPermiso->VerificarPermiso($idtipousuario,$nombreruta);
            }


            if($validez==true){
                return $next($request);
            }else{
               return redirect('/home');
            }
        }
    }
}
