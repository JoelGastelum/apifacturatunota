<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\User;
use App\Sucursal;
use App\Tokenlogin;

class UsuariosController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';

        $sql_where .= ' AND usuarios.idempresa = "'.auth()->user()->idempresa.'" AND usuarios.idtipousuario = 3 AND usuarios.estatus = "A"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                usuarios.id,
                usuarios.login,
                usuarios.nombre
            FROM usuarios
            '.$sql_where.'
            ORDER BY
                usuarios.login ASC
        ';
        $coleccion = DB::select($sql);

        foreach ($coleccion as $data) {
            $acciones='';
                $acciones.='<a href="'.route('usuarios.edit',$data->id).'" class="btn btn-sm btn-warning" title="Editar">
                    <i class="fas fa-edit"></i>
                </a>';
                $acciones.='&nbsp';
                $acciones.='<form method="POST" action="'.route('usuarios.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <i class="far fa-trash-alt"></i>
                </button>';
                $acciones.='</form>';
            $data->acciones=$acciones;
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR UsuariosController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'UsuariosController');

        return view('usuarios.index', [
            'arraybtn' => $arraybtn,
            'tituloencabezado'=>'Usuarios'
        ]);
    }

    /**
     * Permite crear una serie
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objUser = new User;

        //sucursales
        $sucursales_a = Sucursal::where("idempresa", "=", auth()->user()->idempresa)->where("estatus", "=", 'A')->get();
        $sucursales=array();
        foreach ($sucursales_a as $sucursal) {
            $sucursales[$sucursal->id]=$sucursal->nombre;
        }

        return view('usuarios.edit', [
            'objUser' => $objUser,
            'sucursales' => $sucursales,
            'tituloencabezado'=>'Usuarios'
        ]);
    }

    /**
     * Permite editar una
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objUser = User::where("id", "=", $id)->where("estatus", "=", 'A')->first();
        if(!empty($objUser)){
            //sucursales
            $sucursales_a = Sucursal::where("idempresa", "=", auth()->user()->idempresa)->where("estatus", "=", 'A')->get();
            $sucursales=array();
            foreach ($sucursales_a as $sucursal) {
                $sucursales[$sucursal->id]=$sucursal->nombre;
            }

            return view('usuarios.edit', [
                'objUser' => $objUser,
                'sucursales' => $sucursales,
                'tituloencabezado'=>'Usuarios'
            ]);
        }else{
            return redirect()->route('usuarios.index')->with('danger','Error: Usuario no encontrado');
        }
    }

    /**
     * Permite actualizar los datos de una entidad del modelo en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        DB::beginTransaction();
        if($id==-1){
            $canti = User::where("login","=",$request->login)->where("estatus","=","A")->count();
            $objUser = new User;

            //COLOCANDO FOTO DE PERFIL
            $numero = rand(1,12);
            $objUser->foto_perfil ='img/compartidos/avatar'.$numero.'.jpg';
        }else{
            $canti = User::where("id","<>",$id)->where("login","=",$request->login)->where("estatus","=","A")->count();
            $objUser = User::where("id", "=", $id)->first();
        }

        if($canti==0){
            $objUser->idempresa = auth()->user()->idempresa;
            $objUser->idtipousuario = '3';
            $objUser->nombre = $request->nombre;
            $objUser->idsucursal = $request->idsucursal;
            $objUser->login = $request->login;
            $objUser->estatus = 'A';



            if($request->pwd!=null && $request->pwd!='' && !empty($request->pwd)){
                $objUser->pwd = $request->pwd;
                $objUser->password =  Hash::make($request->pwd);
            }

            if($objUser->save()){
                DB::commit();
                return redirect()->route('usuarios.index')->with('success','Se ha guardado los cambios con éxito');
            }else{
                DB::rollback();
                return redirect()->route('usuarios.index')->with('danger','No se pudo guardar los cambios');
            }
        }else{
            DB::rollback();
            return redirect()->route('usuarios.index')->with('danger','Usuario con login existente');
        }

    }

    /**
     * Permite eliminar una sucursal
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objUser = User::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();
            //ELIMINACIÓN FISICA
            if(!$objUser->delete()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }else{
            DB::beginTransaction();
            //ELIMINACIÓN LOGICA
            $objUser->estatus = 'D';

            if(!$objUser->save()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }
        return redirect()->route('usuarios.index')->with($typemsj,$msj);
    }

    /**
     * Permite buscar si el token guardardo en localstorage existe para mandar a iniciar sesion automaticamente
     */
    public function eliminartoken(Request $request){
        $data_envio['encontrado']=0;

        $token = Tokenlogin::where("token", "=", $request->remember_token)->first();
        if(!empty($token)){
            $token->delete();
            // $ObjUser = User::where("id", "=", $token->idusuario)->first();
            $data_envio['encontrado']=1;
        }
        echo json_encode($data_envio);
    }
}
