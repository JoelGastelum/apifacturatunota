<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\User;
use App\Sucursal;

class PrivacidadesController extends Controller
{
    public function __construct(){
    }

    /**
     * Permite mostrar la vista para actualizar los datos de configuración del usuario
     * @param type $id
     * @return type
     */
    public function privacidad(){
        $User = User::findOrFail(auth()->user()->id);
        return view('usuarios.privacidad', ['User' => $User,"rutaAnt"=>route("home"),"tituloencabezado"=>"Mi Perfil"]);
    }

    public function cambiarcontrasena(){

        $User = User::findOrFail(auth()->user()->id);

        return view('usuarios.cambiarcontrasena', ['User' => $User,"rutaAnt"=>route("users.privacidad"),"tituloencabezado"=>"Cambiar Contraseña"]);
    }
    /**
     * Permite actualizar los datos de configuración del usuario
     * @param Request $request
     * @return type
     */
    public function sprivacidad(Request $request){


        $id = $request->id;

        if($User=User::findOrFail($id)){

            $canti = User::where("id","<>",$id)->where("login","=",$request->login)->where("estatus","=","A")->count();
            if($canti==0){
                if($request->email!=null && $request->email!='' && !empty($request->email)){
                    $canti=User::where("id","<>",$id)->where("email","=",$request->email)->where("estatus","=","A")->count();
                }
                if($canti==0){
                   // $User->login = $request->login;
                    $User->email = $request->email;
                    $User->movil = $request->movil;

                    if($request->password!=null && $request->password!='' && !empty($request->password)){
                        $User->pwd =  $request->password;
                        $User->password =  Hash::make($request->password);
                    }

                    //VERIFICAMOS SI TRAJO ARCHIVO
                    if($request->hasFile('foto_perfil')){
                        if($User->foto_perfil!='img/compartidos/avatar10.jpg'){
                            //ELIMINAR FISICAMENTE LA FOTO
                            Storage::delete('public/'.$User->foto_perfil);
                        }
                        $sitioguardar='public/img';
                        $foto_perfil = $request->file('foto_perfil');
                        $nombreoriginal=$foto_perfil->getClientOriginalName();
                        $tipo=$this->typeFile($nombreoriginal);

                        $nombre_archivo = $foto_perfil->storeAs($sitioguardar,$id.'-'.uniqid().'.'.$tipo);
                        $nombre_archivo = str_replace("public/", "", $nombre_archivo);

                        $User->foto_perfil = $nombre_archivo;
                    }
                    $User->save();
                    return redirect('home')->with('success','Perfil actualizado con éxito.');
                }
                else{
                    return redirect('home')->with('danger','Email existente.');
                }
            }else{
                return redirect('home')->with('danger','Login existente.');
            }
        }else{
            return redirect('home')->with('danger','Acción invalida.');
        }
    }

    public function saveContra(Request $request){


        $id = $request->id;

            if($User=User::findOrFail($id)){
                if($request->passwordact ==$User->password || $request->passwordact ==$User->pwd   ){
                    if($request->password==$request->recontrasena){
                        $User->password =  Hash::make($request->password);
                        $User->pwd      =$request->password;
                    }else{
                        return redirect()->route('usuarios.cambiarcontrasena')->with('danger','Las contraseñas no coinciden.');
                    }

                    if($User->save()){
                        return redirect()->route('users.privacidad')->with('success','Perfil actualizado con éxito.');
                    }else{
                        return redirect()->route('usuarios.cambiarcontrasena')->with('danger','No se pudo actualizar.');
                    }
                }else{
                    return redirect()->route('usuarios.cambiarcontrasena')->with('danger','Contraseña actual no coincide.');
                }

            }
            else{
                return redirect()->route('usuarios.cambiarcontrasena')->with('danger','Acción invalida.');
            }

    }
}
