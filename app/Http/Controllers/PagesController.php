<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

use nusoap_client;

use App\Configuracionweb;
use App\Contactos;
use App\Afiliado;
use App\Empresa;
use App\Formapago;
use App\Orden;
use App\Ordenesdetalle;
use App\Servicio;
use App\Codigopromocion;
use App\Remision;
use App\Remisiondetalle;
use App\User;
use App\Concepto;
use \App\PaymentMethods\MercadoPago;

use App\Mail\Registrateestablecimiento;
use App\Mail\Informacionpago;
use App\Mail\Contacto;

//ENVIO DE EMAILS
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Permite enviar el mensaje de contacto
     *
     */
    public function enviarcontacto(Request $request){
        $data_envio['error']=0;
        $data_envio['mensaje']='';

        $objContacto = new Contactos;
        $objContacto->email=$request->email;
        $objContacto->name=$request->name;
        $objContacto->phone=$request->phone;
        $objContacto->subject=$request->subject;
        $objContacto->message=$request->message;

        if($objContacto->save()){
            $email=env('MAIL_USERNAME');
            // $email='arredondojose8@gmail.com';
            $data= array(
                'email'=>$request->email,
                'name'=>$request->name,
                'phone'=>$request->phone,
                'subject'=>$request->subject,
                'message'=>$request->message,
            );
            Mail::to($email)->send(new Contacto($data));
        }else{
            $data_envio['error']=1;
            $data_envio['mensaje']='No se pudo guardar en contacto';
        }
        echo json_encode($data_envio);
    }

    /**
     * Permite mostrar la pagina donde se muestra los planes de comprar para establecimientos
     *
     * @return \Illuminate\Http\Response
     */
    public function establecimiento($codigopromocion=null)
    {
        //construir html
        $html='';
        $suscripciones = Servicio::where("tipo", "=", '2')->orderby('id','ASC')->get();
        $paquetes = Servicio::where("tipo", "=", '1')->orderby('id','ASC')->get();
        $numero_class=1;
        foreach ($suscripciones as $subscripcion) {
            $classbox='box';
            if ($numero_class%2==0){
                //ES PAR
                $classbox='box featured';
            }

            $html .= '<div class="col-lg-4 col-md-6" data-aos="zoom-im" data-aos-delay="100">';
                $html .= '<div class="'.$classbox.'">';
                    $html .= '<h3>'.$subscripcion->producto.'</h3>';
                    $html .= '<h4 class="sindescuento"><sup>$</sup>'.number_format($subscripcion->precio,2,'.',',').'</h4>';
                    $html .= '<h4 class="condescuento" style="display:none;"><sup>$</sup>'.number_format($subscripcion->precio-(float)$subscripcion->descuento,2,'.',',').'</h4>';
                    $html .= '<ul class="text-left" style="padding-left:2rem;">';
                        $html .= '<li class="text-center" >Selecciona un paquete de folio</li>';
                        foreach ($paquetes as $paquete) {
                            $html .= '<div class="form-check">';
                                $html .= '<input onclick="actualizarprecio('.$subscripcion->id.','.$paquete->id.')" class="form-check-input pa_'.$subscripcion->id.'" type="radio" name="paquete_'.$subscripcion->id.'" id="paquete_'.$paquete->id.'" value="'.$paquete->id.'">';
                                $html .= '<label class="form-check-label" for="paquete_'.$paquete->id.'">';
                                    $html .= $paquete->producto.' (<sup>$</sup>'.number_format($paquete->precio,2,'.',',').')';
                                $html .= '</label>';
                            $html .= '</div>';
                        }
                    $html .= '</ul>';
                    $html .= '<div class="btn-wrap">';
                        $html .= '<a href="javascript:void(0);" onclick="buscarplanseleccionado('.$subscripcion->id.');" class="boton btn-buy">';
                            $html .= '<sup>$</sup><span id="total_'.$subscripcion->id.'">'.number_format($subscripcion->precio,2,'.',',').'</span>';
                            $html .= '<br>';
                            $html .= 'Comprar';
                        $html .= '</a>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</div>';

            $numero_class++;
        }

        return view('establecimiento',[
            'html'=>$html,
            'codigopromocion'=>$codigopromocion
        ]);
    }

        /**
         * Permite verificar si el codigo de promocion esta vigente y es valido
         * @return \Illuminate\Http\Response
         */
        public function verificarcodigopromocion(Request $request){
            $data_envio['respuesta']=0;
            $objCodigopromocion = Codigopromocion::where("codigo", "=", trim($request->codigo))
                                                  ->whereNull("iduser")
                                                  ->where("estatus", "=", 'H')
                                                  ->first();
            if(!empty($objCodigopromocion)){
                $fecha_hoy = date("Y-m-d");
                if($objCodigopromocion->fechavigencia!=null){
                    if($this->comparafecha($fecha_hoy,$objCodigopromocion->fechavigencia)<=0){
                        //ES MENOR O ESTA EN EL LIMITE DE LA FECHA (ES VALIDO EL CODIGO)
                        $data_envio['respuesta']=1;
                        $user_nombre='';
                        $user_login='';
                        if($objCodigopromocion->iddueno!=null){
                            $objUser = User::where("id", "=", $objCodigopromocion->iddueno)->first();
                            if(!empty($objUser)){
                                $user_nombre=$objUser->nombre;
                                $user_login=$objUser->login;
                            }
                        }
                        $data_envio['usuario']='Referido a través de: '.$user_nombre.' ('.$user_login.')';
                    }
                }else{
                    $data_envio['respuesta']=1;
                    $user_nombre='';
                    $user_login='';
                    if($objCodigopromocion->iddueno!=null){
                        $objUser = User::where("id", "=", $objCodigopromocion->iddueno)->first();
                        if(!empty($objUser)){
                            $user_nombre=$objUser->nombre;
                            $user_login=$objUser->login;
                        }
                    }
                    $data_envio['usuario']='Referido a través de: '.$user_nombre.' ('.$user_login.')';
                }
            }
            echo json_encode($data_envio);
        }

        /**
         * Permite buscar el precio del plan seleccionado antes de hace click en boton comprar
         * @return \Illuminate\Http\Response
         */
        public function buscarprecioplanseleccionado(Request $request){
            //construir html
            $data_envio['total']=0.00;
            $subscripcion = Servicio::where("id", "=", (int)$request->idserv)->first();
            if((int)$request->idpaquete>0){
                $paquete = Servicio::where("id", "=", (int)$request->idpaquete)->first();
            }

            //BUSCAR SI CODIGO PROMOCION ES VALIDO
                $codigopromocionaplica=0;
                if($request->codigopromocion!=null && $request->codigopromocion!=""){
                    $objCodigopromocion = Codigopromocion::where("codigo", "=", trim($request->codigopromocion))
                                                          ->whereNull("iduser")
                                                          ->where("estatus", "=", 'H')
                                                          ->first();
                    if(!empty($objCodigopromocion)){
                        $fecha_hoy = date("Y-m-d");
                        if($objCodigopromocion->fechavigencia!=null){
                            if($this->comparafecha($fecha_hoy,$objCodigopromocion->fechavigencia)<=0){
                                //ES MENOR O ESTA EN EL LIMITE DE LA FECHA (ES VALIDO EL CODIGO)
                                $codigopromocionaplica=1;
                            }
                        }else{
                            $codigopromocionaplica=1;
                        }
                    }
                }

            if($codigopromocionaplica==0){
                //SIN CODIGO PROMOCION
                $data_envio['total']=(float)$subscripcion->precio+(float)$paquete->precio;
            }else{
                //CON PROMOCION
                $data_envio['total']=((float)$subscripcion->precio-(float)$subscripcion->descuento)+(float)$paquete->precio;
            }
            $data_envio['total'] = number_format($data_envio['total'],2,'.',',');
            echo json_encode($data_envio);
        }

        /**
         * Permite mostrar el plan seleccionado en la misma vista y solicita los campos de rfc, razon social y email
         * @return \Illuminate\Http\Response
         */
        public function buscarplanseleccionado(Request $request){
            //construir html
            $data_envio['html']='';
            $html='';
            $subscripcion = Servicio::where("id", "=", (int)$request->idserv)->first();
            if((int)$request->idpaquete>0){
                $paquete = Servicio::where("id", "=", (int)$request->idpaquete)->first();
            }

            //BUSCAR SI CODIGO PROMOCION ES VALIDO
                $codigopromocionaplica=0;
                if($request->codigopromocion!=null && $request->codigopromocion!=""){
                    $objCodigopromocion = Codigopromocion::where("codigo", "=", trim($request->codigopromocion))
                                                          ->whereNull("iduser")
                                                          ->where("estatus", "=", 'H')
                                                          ->first();
                    if(!empty($objCodigopromocion)){
                        $fecha_hoy = date("Y-m-d");
                        if($objCodigopromocion->fechavigencia!=null){
                            if($this->comparafecha($fecha_hoy,$objCodigopromocion->fechavigencia)<=0){
                                //ES MENOR O ESTA EN EL LIMITE DE LA FECHA (ES VALIDO EL CODIGO)
                                $codigopromocionaplica=1;
                            }
                        }else{
                            $codigopromocionaplica=1;
                        }
                    }
                }

            $classbox='box featured';
            $html .= '<div class="col-lg-12" data-aos="zoom-im" data-aos-delay="100">';
                $html .= '<div class="'.$classbox.'">';
                    $html .= '<h3>'.$subscripcion->producto.'</h3>';
                    $precio = (float)$subscripcion->precio;
                    if($codigopromocionaplica==1){
                        $precio = (float)$subscripcion->precio-(float)$subscripcion->descuento;
                    }
                    $html .= '<ul>';
                        if((int)$request->idpaquete>0){
                            $html .= '<div class="form-check">';
                                $html .= '<label class="form-check-label">';
                                    $html .= $paquete->producto;
                                $html .= '</label>';
                            $html .= '</div>';
                            $precio += (float)$paquete->precio;
                        }
                    $html .= '</ul>';
                    $html .= '<h4><sup>$</sup>'.number_format($precio,2,'.',',').'</h4>';
                $html .= '</div>';
            $html .= '</div>';

            $data_envio['html']=$html;

            echo json_encode($data_envio);
        }

        /**
         * Permite buscar rfc en ordenes a ver si en algun momento se hizo y se mostrara en un modal
         *
         */
        public function revisarrfcenorden(Request $request){
            $data_envio['conseguido']=0;

            $canti = Empresa:: where("rfc", "=", $request->rfc)
                                    ->where("estatus", "=", "A")
                                    ->count();
            if($canti==0){
                $ObjOrden = Orden::where("rfc", "=", $request->rfc)
                                   ->whereIn("estatus", array('0','1'))
                                   ->first();

                if(!empty($ObjOrden)){
                    foreach ($ObjOrden->ordenesdetalle as $ordendetalle) {
                        if($ordendetalle->tipo==2){
                            $ObjOrdenesdetalleServicio=$ordendetalle;
                        }
                        if($ordendetalle->tipo==1){
                            $ObjOrdenesdetallePaquete=$ordendetalle;
                        }
                    }

                    $data_envio['conseguido']=1;
                    $data_envio['idorden']=$ObjOrden->id;
                    $data_envio['rfc']=$ObjOrden->rfc;
                    $data_envio['subscripcion']=$ObjOrdenesdetalleServicio->servicio->producto.' a un precio de $'.number_format($ObjOrdenesdetalleServicio->total,2,'.',',');
                    $data_envio['paquete']=$ObjOrdenesdetallePaquete->servicio->producto.' folios a un precio de $'.number_format($ObjOrdenesdetallePaquete->total,2,'.',',');
                    $data_envio['total']='$'.number_format($ObjOrden->total,2,'.',',');
                }
            }
            echo json_encode($data_envio);
        }

        /**
         * Se selecciono en el modal de ordenes anterior, la orden anterior y se enviara a la vista la orden para que se seleccione en la vista los datos visuales
         *
         */
        public function seleccionarordenanterior(Request $request){
            $data_envio['error']=1;

            $ObjOrden = Orden::where("id", "=", $request->idorden)->first();

            if(!empty($ObjOrden)){
                $data_envio['error']=0;
                foreach ($ObjOrden->ordenesdetalle as $ordendetalle) {
                    if($ordendetalle->tipo==2){
                        $ObjOrdenesdetalleServicio=$ordendetalle;
                    }
                    if($ordendetalle->tipo==1){
                        $ObjOrdenesdetallePaquete=$ordendetalle;
                    }
                }
                // dd($ObjOrdenesdetalleServicio->servicio,$ObjOrdenesdetallePaquete->servicio);

                $data_envio['idserv'] = $ObjOrdenesdetalleServicio->servicio->id;
                $data_envio['idpaquete'] = $ObjOrdenesdetallePaquete->servicio->id;

                $html='';
                $classbox='box featured';
                $html .= '<div class="col-lg-12" data-aos="zoom-im" data-aos-delay="100">';
                    $html .= '<div class="'.$classbox.'">';
                        $html .= '<h3>'.$ObjOrdenesdetalleServicio->servicio->producto.'</h3>';
                        $precio = (float)$ObjOrden->total;
                        $html .= '<ul>';
                            $html .= '<div class="form-check">';
                                $html .= '<label class="form-check-label">';
                                    $html .= $ObjOrdenesdetallePaquete->servicio->producto;
                                $html .= '</label>';
                            $html .= '</div>';
                        $html .= '</ul>';
                        $html .= '<h4><sup>$</sup>'.number_format($precio,2,'.',',').'</h4>';
                    $html .= '</div>';
                $html .= '</div>';

                $data_envio['html']=$html;
            }

            echo json_encode($data_envio);
        }

        /**
         * Permite comprar el plan seleccionado (1.16)
         * @return \Illuminate\Http\Response
         */
        public function compraplan(Request $request){
            $data_envio=array();

            //CONFIGURACION
            $metodopago=$request->metodopago;//MERCADO PAGO
            $ambito=env('AMBITO');//0 = SANDBOX
            $iva_incluido=1;
            $iva_porcentaje=16;


            DB::beginTransaction();

            //VERIFICAR SI POSEE CODIGO DE PROMOCION
                $idcodigopromocion=null;
                if($request->codigo!=null && $request->codigo!=""){
                    $objCodigopromocion = Codigopromocion::where("codigo", "=", trim($request->codigo))
                                                          ->where("estatus", "=", 'H')
                                                          ->first();
                    if(!empty($objCodigopromocion)){
                        $fecha_hoy = date("Y-m-d");
                        if($objCodigopromocion->fechavigencia!=null){
                            if($this->comparafecha($fecha_hoy,$objCodigopromocion->fechavigencia)<=0){
                                //ES MENOR O ESTA EN EL LIMITE DE LA FECHA (ES VALIDO EL CODIGO)
                                $idcodigopromocion=$objCodigopromocion->id;
                                if($objCodigopromocion->tipo==2){
                                    $objCodigopromocion->estatus='T';
                                    if(!$objCodigopromocion->save()){
                                        $error='No se pudo actualizar el codigo de promocion';
                                    }
                                }
                            }
                        }else{
                            $idcodigopromocion=$objCodigopromocion->id;
                            if($objCodigopromocion->tipo==2){
                                $objCodigopromocion->estatus='T';
                                if(!$objCodigopromocion->save()){
                                    $error='No se pudo actualizar el codigo de promocion';
                                }
                            }
                        }
                    }
                }

            if(!isset($error)){
                //CALCULAR EL PRECIO Y COSTO
                    $subscripcion = Servicio::where("id", "=", (int)$request->idserv)->first();
                    if(empty($subscripcion)){
                        $error='No se consigio la subscripcion';
                    }
                    if(!isset($error)){
                        $paquete = Servicio::where("id", "=", (int)$request->idpaquete)->first();
                        if(empty($paquete)){
                            $error='No se consigio el paquete';
                        }
                        if(!isset($error)){
                            $sum_folio = 0;
                            $sum_total = 0;
                            $sum_subtotal = 0;
                            $sum_iva_monto = 0;
                            //CREAR DETALLE ORDENES
                            $ObjOrdenesdetalleServicio = New Ordenesdetalle;
                            $ObjOrdenesdetalleServicio->idservicio = $subscripcion->id;
                            if($idcodigopromocion!=null){
                                //TIENE DESCUENTO
                                $ObjOrdenesdetalleServicio->total = (float)$subscripcion->precio - (float)$subscripcion->descuento;
                            }else{
                                $ObjOrdenesdetalleServicio->total = (float)$subscripcion->precio;
                            }
                            $ObjOrdenesdetalleServicio->subtotal = round((float)$ObjOrdenesdetalleServicio->total/(($iva_porcentaje/100)+1),2);
                            $ObjOrdenesdetalleServicio->iva_incluido = $iva_incluido;
                            $ObjOrdenesdetalleServicio->iva_porcentaje = $iva_porcentaje;
                            $ObjOrdenesdetalleServicio->iva_monto = round($ObjOrdenesdetalleServicio->total - $ObjOrdenesdetalleServicio->subtotal,2);
                            $ObjOrdenesdetalleServicio->tipo = 2;//SUBSCRIPCION
                            $duracion = $subscripcion->duracion;

                            $sum_folio += (int)$subscripcion->numfolios;
                            $sum_total += $ObjOrdenesdetalleServicio->total;
                            $sum_subtotal += $ObjOrdenesdetalleServicio->subtotal;
                            $sum_iva_monto += $ObjOrdenesdetalleServicio->iva_monto;

                            $ObjOrdenesdetallePaquete = New Ordenesdetalle;
                            $ObjOrdenesdetallePaquete->idservicio = $paquete->id;
                            $ObjOrdenesdetallePaquete->total = (float)$paquete->precio;
                            $ObjOrdenesdetallePaquete->subtotal = round((float)$ObjOrdenesdetallePaquete->total/(($iva_porcentaje/100)+1),2);
                            $ObjOrdenesdetallePaquete->iva_incluido = $iva_incluido;
                            $ObjOrdenesdetallePaquete->iva_porcentaje = $iva_porcentaje;
                            $ObjOrdenesdetallePaquete->iva_monto = round($ObjOrdenesdetallePaquete->total - $ObjOrdenesdetallePaquete->subtotal,2);
                            $ObjOrdenesdetallePaquete->tipo = 1;//PAQUETE DE FOLIO

                            $sum_folio += (int)$paquete->numfolios;
                            $sum_total += $ObjOrdenesdetallePaquete->total;
                            $sum_subtotal += $ObjOrdenesdetallePaquete->subtotal;
                            $sum_iva_monto += $ObjOrdenesdetallePaquete->iva_monto;
                        }
                    }

                if(!isset($error)){
                    if($request->idorden=='-1'){
                        $ObjOrden = new Orden;
                        $ObjOrden->idcodigopromocion = $idcodigopromocion;
                    }else{
                        $ObjOrden = Orden::where("id", "=", (int)$request->idorden)->first();
                        if(!empty($ObjOrden)){
                            $sql = 'DELETE FROM `ordenesdetalles` WHERE idorden = ?';
                            DB::delete($sql,[$ObjOrden->id]);
                        }
                    }

                    $ObjOrden->metodopago = $metodopago;
                    $ObjOrden->ambito = $ambito;
                    $ObjOrden->costometodopago = 0;
                    $ObjOrden->titulo = 'Subscripción de '.$subscripcion->producto.' con '.$paquete->producto.' folios.';
                    $ObjOrden->razonsocial = $request->razonsocial;
                    $ObjOrden->rfc = $request->rfc;
                    $ObjOrden->email = $request->email;
                    $ObjOrden->tracker = bin2hex(random_bytes(32));
                    $ObjOrden->total = $sum_total;
                    $ObjOrden->folio = $sum_folio;
                    $ObjOrden->duracion = $duracion;
                    $ObjOrden->iva_incluido = $iva_incluido;
                    $ObjOrden->iva_porcentaje = $iva_porcentaje;
                    $ObjOrden->iva_monto = $sum_iva_monto;
                    $ObjOrden->subtotal = $sum_subtotal;

                    if($ObjOrden->save()){
                        $ObjOrdenesdetalleServicio->idorden=$ObjOrden->id;
                        $ObjOrdenesdetallePaquete->idorden=$ObjOrden->id;
                        if($ObjOrdenesdetalleServicio->save() && $ObjOrdenesdetallePaquete->save()){
                            // dd($ObjOrden,$ObjOrdenesdetalleServicio,$ObjOrdenesdetallePaquete);
                            DB::commit();
                            $data_envio['idorden']=$ObjOrden->id;

                            //MERCADO PAGO
                            if($metodopago==1){
                                // dd($ObjOrden,$ObjOrdenesdetalleServicio,$ObjOrdenesdetallePaquete);
                                $method = new \App\PaymentMethods\MercadoPago;
                                $url= $method->setupPaymentAndGetRedirectURL($ObjOrden);
                                $data_envio['url']=$url;
                                // return redirect()->to($url);
                            }elseif($metodopago==2){
                                //OTRO METODO
                                //ENVIAR CORREO CON INFORMACIÓN DE COBRO
                                $cuentabanco="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "cuentabanco")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $cuentabanco = $ObjConfiguracionweb->parametros;
                                }
                                $banco="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "banco")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $banco = $ObjConfiguracionweb->parametros;
                                }
                                $numerodecontacto="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "numerodecontacto")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $numerodecontacto = $ObjConfiguracionweb->parametros;
                                }
                                $clabeinterbancaria="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "clabeinterbancaria")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $clabeinterbancaria = $ObjConfiguracionweb->parametros;
                                }
                                $numerotarjeta="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "numerotarjeta")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $numerotarjeta = $ObjConfiguracionweb->parametros;
                                }
                                $numerotarjeta="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "numerotarjeta")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $numerotarjeta = $ObjConfiguracionweb->parametros;
                                }
                                $personarfc="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "personarfc")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $personarfc = $ObjConfiguracionweb->parametros;
                                }
                                $personanombre="";
                                $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "personanombre")->first();
                                if(!empty($ObjConfiguracionweb)){
                                    $personanombre = $ObjConfiguracionweb->parametros;
                                }

                                //ARMANDO ARRAY PARA MOSTRAR
                                $data_envio['cuentabanco']=$cuentabanco;
                                $data_envio['banco']=$banco;
                                $data_envio['numerodecontacto']=$numerodecontacto;
                                $data_envio['clabeinterbancaria']=$clabeinterbancaria;
                                $data_envio['numerotarjeta']=$numerotarjeta;
                                $data_envio['personarfc']=$personarfc;
                                $data_envio['personanombre']=$personanombre;

                                $data_envio['rfc']=$ObjOrden->rfc;
                                $data_envio['subscripcion']=$subscripcion->producto.' a un precio de $'.number_format($ObjOrdenesdetalleServicio->total,2,'.',',');
                                $data_envio['paquete']=$paquete->producto.' folios a un precio de $'.number_format($ObjOrdenesdetallePaquete->total,2,'.',',');
                                $data_envio['total']='$'.number_format($ObjOrden->total,2,'.',',');

                                //ENVIAR CORREO
                                $data= array(
                                    'cuentabanco'=>$data_envio['cuentabanco'],
                                    'banco'=>$data_envio['banco'],
                                    'numerodecontacto'=>$data_envio['numerodecontacto'],
                                    'clabeinterbancaria'=>$data_envio['clabeinterbancaria'],
                                    'numerotarjeta'=>$data_envio['numerotarjeta'],
                                    'personarfc'=>$data_envio['personarfc'],
                                    'personanombre'=>$data_envio['personanombre'],
                                    'rfc'=>$data_envio['rfc'],
                                    'subscripcion'=>$data_envio['subscripcion'],
                                    'paquete'=>$data_envio['paquete'],
                                    'total'=>$data_envio['total'],
                                );
                                Mail::to($ObjOrden->email)->send(new Informacionpago($data));
                            }else{
                                $error= 'No existe metodo pago disponible';
                            }
                        }else{
                            DB::rollback();
                            $error='No se pudo crear el detalla de la orden';
                        }
                    }else{
                        DB::rollback();
                        $error='No se pudo crear la orden';
                    }
                }
            }

            $data_envio['error']=0;
            $data_envio['metodopago']=$metodopago;
            if(isset($error)){
                $data_envio['error']=$error;
            }

            echo json_encode($data_envio);
        }

        /**
         * Permite guardar una vez se retorne del mercado pago correctamente
         * @return [type] [description]
         */
        public function mercadopago_gracias(){
            // dd($_GET);
            $metodopago=1;//MERCADO PAGO
            $error = array();

            if(isset($_GET['collection_status'])){
                if($_GET['collection_status']=='approved'){
                    $tracker = $_GET['external_reference'];

                    $ObjOrden = Orden::where("tracker", "=", $tracker)->first();
                    if(!empty($ObjOrden)){
                        $arraymetodopago=$_GET;
                        if($ObjOrden->renovacion==0){
                            $respuesta = $this->pagarplan($ObjOrden,$metodopago,$arraymetodopago);
                            if($respuesta['error']==0){
                                return redirect()->route('usuarioconfirmado',[$respuesta['iduser'],$respuesta['b64_idremision']]);
                            }else{
                                $error = $respuesta['error'];
                            }
                        }else{
                            //RENOVACION DE PLAN
                            $respuesta = $this->renovarplan($ObjOrden,$metodopago,$arraymetodopago);
                            if($respuesta['error']==0){
                                return redirect()->route('home')->with('success','Plan renovado con éxito');
                            }else{
                                $error = $respuesta['error'];
                            }
                        }
                    }else{
                        //NO SE CONSIGUIO LA ORDEN
                        $error[]='No se consiguio la orden';
                    }
                }else{
                    return redirect()->route('mercadopago_fallido');
                }
            }else{
                return redirect()->route('mercadopago_fallido');
            }
            if(count($error)>0){
                dd($error);
            }
        }

        /**
         * Permite cancelar la orden pues no se pudo pagar por falta de dinero
         * @return [type] [description]
         */
        public function mercadopago_fallido(){
            $codigopromocion=null;
            $comprarplan_idorden=null;
            $razonsocial=null;
            $rfc=null;
            $email=null;

            if(isset($_GET['external_reference'])){
                $tracker = $_GET['external_reference'];
                $ObjOrden = Orden::where("tracker", "=", $tracker)->first();
                if(!empty($ObjOrden)){
                    $comprarplan_idorden=$ObjOrden->id;
                    $razonsocial=$ObjOrden->razonsocial;
                    $rfc=$ObjOrden->rfc;
                    $email=$ObjOrden->email;

                    // $ObjOrden->estatus='-1';
                    // $ObjOrden->fecharechazo=date("Y-m-d");
                    // $ObjOrden->save();

                    if($ObjOrden->idcodigopromocion !=null){
                        $objCodigopromocion = Codigopromocion::where("id", "=", $ObjOrden->idcodigopromocion)->first();
                        if(!empty($objCodigopromocion)){
                            $codigopromocion=$objCodigopromocion->codigo;
                            if($objCodigopromocion->tipo==2){
                                $objCodigopromocion->estatus='H';
                                if(!$objCodigopromocion->save()){
                                    $error[]='No se pudo actualizar el codigo de promocion';
                                }
                            }
                        }
                    }
                }
            }

            if(isset($ObjOrden)){
                if(!empty($ObjOrden)){
                    if((int)$ObjOrden->renovacion==0){
                        //MOSTRAR PLAN SELECCIONADO
                        //construir html
                        $html='';
                        $suscripciones = Servicio::where("tipo", "=", '2')->orderby('id','ASC')->get();
                        $paquetes = Servicio::where("tipo", "=", '1')->orderby('id','ASC')->get();
                        $numero_class=1;
                        foreach ($suscripciones as $subscripcion) {
                            $classbox='box';
                            if ($numero_class%2==0){
                                //ES PAR
                                $classbox='box featured';
                            }

                            $html .= '<div class="col-lg-4 col-md-6" data-aos="zoom-im" data-aos-delay="100">';
                                $html .= '<div class="'.$classbox.'">';
                                    $html .= '<h3>'.$subscripcion->producto.'</h3>';
                                    $html .= '<h4 class="sindescuento"><sup>$</sup>'.number_format($subscripcion->precio,2,'.',',').'</h4>';
                                    $html .= '<h4 class="condescuento" style="display:none;"><sup>$</sup>'.number_format($subscripcion->precio-(float)$subscripcion->descuento,2,'.',',').'</h4>';
                                    $html .= '<ul class="text-left" style="padding-left:2rem;">';
                                        $html .= '<li class="text-center" >Selecciona un paquete de folio</li>';
                                        foreach ($paquetes as $paquete) {
                                            $html .= '<div class="form-check">';
                                                $html .= '<input onclick="actualizarprecio('.$subscripcion->id.','.$paquete->id.')" class="form-check-input pa_'.$subscripcion->id.'" type="radio" name="paquete_'.$subscripcion->id.'" id="paquete_'.$paquete->id.'" value="'.$paquete->id.'">';
                                                $html .= '<label class="form-check-label" for="paquete_'.$paquete->id.'">';
                                                    $html .= $paquete->producto.' (<sup>$</sup>'.number_format($paquete->precio,2,'.',',').')';
                                                $html .= '</label>';
                                            $html .= '</div>';
                                        }
                                    $html .= '</ul>';
                                    $html .= '<div class="btn-wrap">';
                                        $html .= '<a href="javascript:void(0);" onclick="buscarplanseleccionado('.$subscripcion->id.');" class="boton btn-buy">';
                                            $html .= '<sup>$</sup><span id="total_'.$subscripcion->id.'">'.number_format($subscripcion->precio,2,'.',',').'</span>';
                                            $html .= '<br>';
                                            $html .= 'Comprar';
                                        $html .= '</a>';
                                    $html .= '</div>';
                                $html .= '</div>';
                            $html .= '</div>';

                            $numero_class++;
                        }

                        return view('establecimiento',[
                            'html'=>$html,
                            'codigopromocion'=>$codigopromocion,
                            'comprarplan_idorden'=>$comprarplan_idorden,
                            'razonsocial'=>$razonsocial,
                            'rfc'=>$rfc,
                            'email'=>$email,
                            'msjerror'=>'El pago fallo'
                        ]);
                    }else{
                        //MOSTRAR PLAN SELECCIONADO EN RENOVACION
                        $idserv=-1;
                        $idpaquete=-1;
                        foreach ($ObjOrden->ordenesdetalle as $ObjOrdenesdetalle) {
                            if($ObjOrdenesdetalle->tipo==2){
                                $idserv=$ObjOrdenesdetalle->idservicio;
                            }elseif ($ObjOrdenesdetalle->tipo==1){
                                $idpaquete=$ObjOrdenesdetalle->idservicio;
                            }
                        }
                        $suscripciones = Servicio::where("tipo", "=", '2')->orderby('id','ASC')->get();
                        $paquetes = Servicio::where("tipo", "=", '1')->orderby('id','ASC')->get();

                        return view('comprarplan.index',[
                            'suscripciones'=>$suscripciones,
                            'paquetes'=>$paquetes,
                            'idserv'=>$idserv,
                            'idpaquete'=>$idpaquete,
                            'comprarplan_idorden'=>$comprarplan_idorden,
                            'msjerror'=>'El pago fallo'
                        ]);
                    }
                }
            }
        }

        /**
         * Permite cancelar la orden pues no se pudo pagar porque no se animo
         * @return [type] [description]
         */
        public function mercadopago_pendiente(){
            $codigopromocion=null;
            $comprarplan_idorden=null;
            $razonsocial=null;
            $rfc=null;
            $email=null;

            if(isset($_GET['external_reference'])){
                $tracker = $_GET['external_reference'];
                $ObjOrden = Orden::where("tracker", "=", $tracker)->first();
                if(!empty($ObjOrden)){
                    $comprarplan_idorden=$ObjOrden->id;
                    $razonsocial=$ObjOrden->razonsocial;
                    $rfc=$ObjOrden->rfc;
                    $email=$ObjOrden->email;

                    // $ObjOrden->estatus='1';
                    // $ObjOrden->fecharechazo=date("Y-m-d");
                    // $ObjOrden->save();

                    if($ObjOrden->idcodigopromocion !=null){
                        $objCodigopromocion = Codigopromocion::where("id", "=", $ObjOrden->idcodigopromocion)->first();
                        if(!empty($objCodigopromocion)){
                            $codigopromocion=$objCodigopromocion->codigo;
                            if($objCodigopromocion->tipo==2){
                                $objCodigopromocion->estatus='H';
                                if(!$objCodigopromocion->save()){
                                    $error[]='No se pudo actualizar el codigo de promocion';
                                }
                            }
                        }
                    }
                }
            }

            if(isset($ObjOrden)){
                if(!empty($ObjOrden)){
                    if((int)$ObjOrden->renovacion==0){
                        //MOSTRAR PLAN SELECCIONADO
                        //construir html
                        $html='';
                        $suscripciones = Servicio::where("tipo", "=", '2')->orderby('id','ASC')->get();
                        $paquetes = Servicio::where("tipo", "=", '1')->orderby('id','ASC')->get();
                        $numero_class=1;
                        foreach ($suscripciones as $subscripcion) {
                            $classbox='box';
                            if ($numero_class%2==0){
                                //ES PAR
                                $classbox='box featured';
                            }

                            $html .= '<div class="col-lg-4 col-md-6" data-aos="zoom-im" data-aos-delay="100">';
                                $html .= '<div class="'.$classbox.'">';
                                    $html .= '<h3>'.$subscripcion->producto.'</h3>';
                                    $html .= '<h4 class="sindescuento"><sup>$</sup>'.number_format($subscripcion->precio,2,'.',',').'</h4>';
                                    $html .= '<h4 class="condescuento" style="display:none;"><sup>$</sup>'.number_format($subscripcion->precio-(float)$subscripcion->descuento,2,'.',',').'</h4>';
                                    $html .= '<ul class="text-left" style="padding-left:2rem;">';
                                        $html .= '<li class="text-center" >Selecciona un paquete de folio</li>';
                                        foreach ($paquetes as $paquete) {
                                            $html .= '<div class="form-check">';
                                                $html .= '<input onclick="actualizarprecio('.$subscripcion->id.','.$paquete->id.')" class="form-check-input pa_'.$subscripcion->id.'" type="radio" name="paquete_'.$subscripcion->id.'" id="paquete_'.$paquete->id.'" value="'.$paquete->id.'">';
                                                $html .= '<label class="form-check-label" for="paquete_'.$paquete->id.'">';
                                                    $html .= $paquete->producto.' (<sup>$</sup>'.number_format($paquete->precio,2,'.',',').')';
                                                $html .= '</label>';
                                            $html .= '</div>';
                                        }
                                    $html .= '</ul>';
                                    $html .= '<div class="btn-wrap">';
                                        $html .= '<a href="javascript:void(0);" onclick="buscarplanseleccionado('.$subscripcion->id.');" class="boton btn-buy">';
                                            $html .= '<sup>$</sup><span id="total_'.$subscripcion->id.'">'.number_format($subscripcion->precio,2,'.',',').'</span>';
                                            $html .= '<br>';
                                            $html .= 'Comprar';
                                        $html .= '</a>';
                                    $html .= '</div>';
                                $html .= '</div>';
                            $html .= '</div>';

                            $numero_class++;
                        }

                        return view('establecimiento',[
                            'html'=>$html,
                            'codigopromocion'=>$codigopromocion,
                            'comprarplan_idorden'=>$comprarplan_idorden,
                            'razonsocial'=>$razonsocial,
                            'rfc'=>$rfc,
                            'email'=>$email,
                            'msjerror'=>'El pago fue cancelado por el usuario'
                        ]);
                    }else{
                        //MOSTRAR PLAN SELECCIONADO EN RENOVACION
                        $idserv=-1;
                        $idpaquete=-1;
                        foreach ($ObjOrden->ordenesdetalle as $ObjOrdenesdetalle) {
                            if($ObjOrdenesdetalle->tipo==2){
                                $idserv=$ObjOrdenesdetalle->idservicio;
                            }elseif ($ObjOrdenesdetalle->tipo==1){
                                $idpaquete=$ObjOrdenesdetalle->idservicio;
                            }
                        }
                        $suscripciones = Servicio::where("tipo", "=", '2')->orderby('id','ASC')->get();
                        $paquetes = Servicio::where("tipo", "=", '1')->orderby('id','ASC')->get();

                        return view('comprarplan.index',[
                            'suscripciones'=>$suscripciones,
                            'paquetes'=>$paquetes,
                            'idserv'=>$idserv,
                            'idpaquete'=>$idpaquete,
                            'comprarplan_idorden'=>$comprarplan_idorden,
                            'msjerror'=>'El pago fue cancelado por el usuario'
                        ]);
                    }
                }
            }
        }
}
