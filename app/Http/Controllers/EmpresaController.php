<?php

namespace App\Http\Controllers;
use App\Usuario;
use App\Remision;
use App\Remisiondetalle;
use App\Concepto;
use App\Comprobante;
use App\DetalleComprobante;
use App\Cliente;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use App\Bitacora;
use App\Viaje;
use App\Serie;
use App\Empresa;
use App\Sucursal;

use App\Tokenlogin;


use DB;
use ZipArchive;
use Illuminate\Support\Facades\Storage;

//ENVIO DE EMAILS
use Illuminate\Support\Facades\Mail;

use Imagick;

use Illuminate\Http\Request;
use App\Mail\EnviarFactura;
use App\Mail\EnviarZip;


class EmpresaController extends Controller
{
    //
    public function Login(Request $request){

        /*POST REQUIRED
        {
        "login":"   ",
        "pwd";"     "
        }
        */

        $usuario =Usuario::where('login','=',$request->login)
        ->where('pwd','=',$request->pwd)
        ->where('idtipousuario','=','3')
        ->first();
        Bitacora::setMsj($request->login, $request->pwd);
        if(!empty($usuario)){
            //VERIFICAR SI NO SE HA VENCIDO LA MEMBRESIA A LA EMPRESA
            //SI SE LE VENCIO NO PERMITIR LOGUEARSE Y NOTIFICAR
                $fecha_hoy = date("Y-m-d");
                $fechavigencia = $usuario->empresa->fechavigencia;
                if($fechavigencia!=null){
                    if($this->comparafecha($fecha_hoy,$fechavigencia)==1){
                        return [
                            'msg' => 'Su empresa se le ha vencido la membresia, por favor pongase en contacto con su administrador',
                            'res' => false
                        ];
                    }
                }

            $token=bin2hex(random_bytes(32));
            $objTokenlogin = new Tokenlogin;
            $objTokenlogin->idusuario = $usuario->id;
            $objTokenlogin->token = $token;
            $objTokenlogin->save();
            //ACTUALIZANDO TOKEN
            $usuario->tokenlogin=$token;
            Bitacora::setMsj('VA A INICIAR SESION 22'.$request->login, $request->pwd);
        }else{
            Bitacora::setMsj($request->login, $request->pwd.' NO PUDO INICIAR SESION USUARIO INCORRECTO');
            return [
                'msg' => 'Nombre de usuario o contraseña incorrecta',
                'res' => false
            ];
        }

        return [
            'id' => $usuario->id,
            'res' => true,
            'tokenlogin'=>$token
        ];

    }

    public function infoEmpresa(Request $request){

        $usuario=Usuario::findOrFail($request->idusuario);
        if(true)
        {
            $msg="";
            $empresa=Empresa::where('id','=',$usuario->idempresa)->first();
            $muestraFormulario=$empresa->mostrarformuescanear;
            $sucursal=Sucursal::where('id','=',$usuario->idsucursal)->first();
            $serieactual=Serie::where('id','=',$sucursal->idserieremision)->first();
            $formasdepago= DB::select("select * from formaspago");
            $conceptosxdefault= DB::select("select * from conceptosxdefault where idempresa=$usuario->idempresa");
            $usodecfdi=DB::select("SELECT * FROM uso_comprobante_sat");
            $usodecfdiDefault=DB::select("SELECT us.* FROM uso_comprobante_sat us inner join empresas e on e.idusocfdidefault=us.id where e.id=$usuario->idempresa");
            if($empresa->folioscomprados<20){
                $msg="Quedan pocos folios!";
            }

            if(!$conceptosxdefault)
            return ["msg"=>$msg,"formasdepago"=>$formasdepago,"conceptosxdefault"=>$conceptosxdefault,"res"=>false,'usodecfdi'=>$usodecfdi,'usodecfdiDefault'=>$usodecfdiDefault,'serieActual'=>$serieactual,'muestraFormulario'=>$muestraFormulario];

            if(!$formasdepago)
            return ["msg"=>$msg,"formasdepago"=>$formasdepago,"conceptosxdefault"=>$conceptosxdefault,"res"=>false,'usodecfdi'=>$usodecfdi,'usodecfdiDefault'=>$usodecfdiDefault,'serieActual'=>$serieactual,'muestraFormulario'=>$muestraFormulario];

            return ["msg"=>$msg,"formasdepago"=>$formasdepago,"conceptosxdefault"=>$conceptosxdefault,"res"=>true,'usodecfdi'=>$usodecfdi,'usodecfdiDefault'=>$usodecfdiDefault,'serieActual'=>$serieactual,'muestraFormulario'=>$muestraFormulario];

        }
        return ['msg' => 'Atras, intruso','res'=>false];
    }


    /**
     * Permite crear la remision y su detalle
     */
    public function saveNota(Request $request){
        //GUARDAR EN BITACORA
        $request_array  = (array)$request->all();
        $request_array = json_encode($request_array);
        Bitacora::setMsj('saveNota',$request_array);
        // exit();

        $usuario=Usuario::findOrFail($request->idusuario);

        try
        {
            $totalRemision=0;

            $cliente=0;
            if($request->rfc){
                $arr=explode('/',$request->rfc);
                Bitacora::setMsj('saveNota2',json_encode($arr));
                /*
                [
                    "https:",
                    "",
                    "test.facturatunota.com",
                    "consultaqr",
                    "ac34fd9305fd693065ca0d21ffb10855e5155588be5a02b6827967ee4d428987",
                    "XAXX010101000"
                ]
                */
                /*
                [
                    "https:",
                    "",
                    "test.facturatunota.com",
                    "consultaqr",
                    "idcliente"
                ]
                */
                $idcliente=$arr[count($arr)-1];
                Bitacora::setMsj('saveNota3(idcliente)',$idcliente);
                $cliente=DB::select("
                        select *
                        from clientes
                        where md5(id)= ?
                    ",
                    array($idcliente)
                );
                if(count($cliente)==0){
                    return ["msg" => "Hubo un error al Leer RFC" , "res" => false,"idremision"=>0,'datosCliente'=>0,'importeTotal'=>$totalRemision];
                }
                $cliente=$cliente[0];
            }
            if($request->idcodigo){
                $idcliente=hexdec($request->idcodigo);
                $cliente=DB::select("select * from clientes where id='$idcliente'");
                if(count($cliente)==0){
                    return ["msg" => "Hubo un error al Leer RFC" , "res" => false,"idremision"=>0,'datosCliente'=>0,'importeTotal'=>$totalRemision];
                }
                $cliente=$cliente[0];
            }

            $conceptos=json_decode($request->conceptos);
            $idconceptos=json_decode($request->idconceptos);
            $remisionDuplicada=Remision::where("noticket",'=',$request->nota)->where('idsucursal','=',$usuario->idsucursal)->get();

            if(count($remisionDuplicada)>0){
                return ["msg" => "Esta nota ya esta registrada con un importe de ".$remisionDuplicada[0]->total , "res" => false,"idremision"=>$remisionDuplicada[0]->id,'datosCliente'=>$cliente,'importeTotal'=>$remisionDuplicada[0]->id];
            }

            $objSucursal = Sucursal::where("id", "=", $usuario->idsucursal)->first();
            if(!empty($objSucursal)){
                $objSerie = Serie::where("id", "=", $objSucursal->idserieremision)->first();
                if(!empty($objSerie)){
                    $serie=$objSerie->serie;
                    $folio_actual=(int)$objSerie->folio_actual;

                    //ACTUALIZAR SERIO Y FOLIO
                    $objSerie->folio_actual=(int)$objSerie->folio_actual+1;
                    $objSerie->save();
                }
            }
            $remision= new Remision;

            DB::beginTransaction();

            $remision->idempresa=$usuario->idempresa;
            $remision->noticket=$request->nota;
            $remision->fecha=date("Y-m-d");
            $remision->formapago=$request->formapago;
            $idformapago=DB::select("select * from formaspago where nombre='$request->formapago'");
            $remision->idformapago=$idformapago[0]->codigo;
            $remision->estatus="A";
            $remision->folio="0";
            $remision->subtotal="0";
            $remision->ieps=0;
            $remision->iva=0;
            $remision->metodopago="";
            $remision->numcuenta="";
            $remision->condicionespago="";
            $remision->uuid="";
            $remision->serie="";
            $remision->razoncancelacionadmin="";
            $remision->numerocliente="";
            $remision->idcomprobante="0";
            $remision->saldo="0";
            $remision->idconciliacion="0";
            $remision->idsucursal=$usuario->idsucursal;
            $remision->total= 0;

            if(isset($request->usodecfdi)){
                $usocfdi=DB::select("select * from uso_comprobante_sat where nombre='$request->usodecfdi'");
                if(isset($usocfdi[0]->idsat)){
                    $remision->idusocfdi=$usocfdi[0]->id;
                }
            }






            if(!$remision->save()){
                DB::rollback();
                return ["msg" => "Error al guardar Remision" , "res" => false,"idremision"=>0,'datosCliente'=>$cliente,'importeTotal'=>$totalRemision];
            }


            $subtotalRemision=0;
            $totalIva=0;
            $totalHospedaje=0;
            $totalRemision=0;
            for( $i=0 ; $i<count($conceptos) ; $i++){
                if($conceptos[$i]!=null && $conceptos[$i]!="0" && $conceptos[$i]!='null' ){
                    $concepto=Concepto::where('id','=',$idconceptos[$i])->first();
                    if(!$concepto){
                        return ["msg" => 'Error con al registrar concepto' , "res" =>false,"idremision"=>0,'datosCliente'=>$cliente,'importeTotal'=>$totalRemision];
                    }
                    $remisionDetalle = new Remisiondetalle;
                    $aux=1+$concepto->tasaiva+$concepto->tasahospedaje;
                    $precioconcepto = $conceptos[$i]/$aux;


                    $remisionDetalle->idconcepto              = $concepto->id;
                    $remisionDetalle->idempresa               = $usuario->idempresa;
                    $remisionDetalle->idremision              = $remision->id;
                    $remisionDetalle->codigo                  = $concepto->codigo;
                    $remisionDetalle->unidad                  = $concepto->unidad;
                    $remisionDetalle->cantidad                = 1;
                    $remisionDetalle->descripcion             = $concepto->concepto;
                    $remisionDetalle->importeiva              = round($precioconcepto*$concepto->tasaiva,2);
                    $remisionDetalle->tasaiva                 = $concepto->tasaiva*100;
                    $remisionDetalle->importehospedaje        = round($precioconcepto*$concepto->tasahospedaje,2);
                    $remisionDetalle->tasahospedaje           = $concepto->tasahospedaje*100;
                    $remisionDetalle->precio                  = round($precioconcepto,2);
                    $remisionDetalle->importe                 = round($precioconcepto,2);
                    $remisionDetalle->importeconimpuestos     = $remisionDetalle->importe+$remisionDetalle->importeiva+$remisionDetalle->importehospedaje;
                    $remisionDetalle->codigounidadsat         = $concepto->codigounidadsat;
                    $remisionDetalle->codigogrupoprodsersat   = $concepto->codigogrupoprodsersat;

                    //TOTALES
                    $subtotalRemision+=$remisionDetalle->importe;
                    $totalIva+=$remisionDetalle->importeiva;
                    $totalHospedaje+=$remisionDetalle->importehospedaje;
                    $totalRemision+=$remisionDetalle->importeconimpuestos;

                    if(!$remisionDetalle->save()){
                        DB::rollback();
                        return ["msg" => "Error al guardar la remision" , "res"=> false,'datosCliente'=>$cliente,'importeTotal'=>$totalRemision];
                    }
                }
            }

            $remision->subtotal= $subtotalRemision;
            $remision->iva=$totalIva;
            $remision->hospedaje=$totalHospedaje;
            $remision->total= $totalRemision;

            if(!$remision->update()){
                DB::rollback();
                return ["msg" => "Error al guardar" , "res" =>false,"idremision"=>0,'datosCliente'=>$cliente,'importeTotal'=>$totalRemision];
                Bitacora::setMsj('saveNota','Remision no creada');
            }else{
                Bitacora::setMsj('saveNota','Remision creada');
            }

            DB::commit();
            return ["msg" => "Guardado con exito" , "res"=>true,"idremision" => $remision->id,'datosCliente'=>$cliente,'importeTotal'=>$totalRemision] ;
        }catch (\Exception $e) {
            Bitacora::setMsj($e,"SaveNota");
            return ['msg'=>'Hubo un problema al leer qr','res'=>false];
        }
    }

    /**
     * Funcion que permite facturar una remisión
     * @see FacturarController@facturar
     * POST REQUIRED
     * {
     *     idremision
     *     rfc
     *     idusuario
     *     tokenLogin
     *     idusuariocliente
     *     usodecfdi
     *  }
     */
    public function facturar(Request $request)
    {
        // DB::beginTransaction();
        $plataforma='api';
        if(isset($request->plataforma)){
            if($request->plataforma!='web'){
                $usuario=Usuario::findOrFail($request->idusuario);
                Bitacora::setMsj($request->idusuariocliente,$request->idusuario);
            }elseif ($request->plataforma=='web') {
                $usuario = new Usuario;
                $usuario->idempresa = $request->idempresa;//DEBE SER LA EMPRESA QUE GENERA EL COMPROBANTE
                $plataforma='web';
            }
        }else{
            $usuario=Usuario::findOrFail($request->idusuario);
            Bitacora::setMsj($request->idusuariocliente,$request->idusuario);
        }

        $remision=Remision::findOrFail($request->idremision);

        if(true || $plataforma=='web')
        {
            if(!$remision)
            {
                return ['msg'=>'Nota de venta no encontrada', 'res' =>false];
            }
            else
            {
                if($plataforma=='api'){
                    $cliente=DB::select("select * from clientes where id=$request->idusuariocliente");
                    $idcliente= $cliente[0]->id;
                    $viajeActivo=Viaje::where('idcontribuyente','=',$cliente[0]->id)
                    ->where('activo','=','1')
                    ->where('estatus','=','1')->first();
                }elseif($plataforma=='web'){
                    $idcliente = $request->idcliente;
                }

                $serie='';
                $folio_actual=0;
                $objComprobante = new Comprobante;
                if($remision->idcomprobante>0){
                    //CARGANDO COMPROBANTE
                    $objComprobante = Comprobante::where("id", "=", $remision->idcomprobante)->first();
                    DB::delete("DELETE FROM detallecomprobantes WHERE idcomprobante = ?",[$objComprobante->id]);
                }else{
                    //ES NUEVO
                    //BUSCAR SERIO Y FOLIO DE LA SUCURSAL
                    $objSucursal = Sucursal::where("id", "=", $remision->idsucursal)->first();
                    if(!empty($objSucursal)){
                        $objSerie = Serie::where("id", "=", $objSucursal->idserie)->first();
                        if(!empty($objSerie)){
                            $serie=$objSerie->serie;
                            $folio_actual=(int)$objSerie->folio_actual;

                            //ACTUALIZAR SERIO Y FOLIO
                            $objSerie->folio_actual=(int)$objSerie->folio_actual+1;
                            $objSerie->save();
                        }
                    }
                }
                $folio_actual++;

                if($plataforma=='api'){
                    if($viajeActivo){
                        $objComprobante->idviaje=$viajeActivo->id;
                        Bitacora::setMsj($viajeActivo->id,$viajeActivo->descripcion);
                    }
                }
                $objComprobante->idempresa= $usuario->idempresa;
                $objComprobante->idsucursal = $remision->idsucursal;
                $objComprobante->tipo="I";
                $objComprobante->idcliente=$idcliente;
                $objComprobante->fecha=date('Y-m-d H:i:s');
                $objComprobante->total=$remision->total;
                $objComprobante->numcuenta="";
                $objComprobante->metodopago="PUE";
                $objComprobante->formapago="01";
                $objComprobante->serie=$serie;
                $objComprobante->folio=$folio_actual;

                $usocfdi=DB::select("select * from uso_comprobante_sat where nombre='$request->usodecfdi'");



                $objComprobante->idusocfdi=$usocfdi[0]->idsat;
                $objComprobante->cfdirelacionado=$remision->uuid;
                $objComprobante->tipodocumento='CF';
                $objComprobante->condicionespago="";
                $objComprobante->version=3;
                $objComprobante->abonos=0;
                if($objComprobante->save()){

                    $remisionDetalles=DB::select("Select * from remisionesdetalle where idremision=$remision->id");
                    foreach($remisionDetalles as $remisionDetalle){

                        $detalleComprobante= new Detallecomprobante;
                        $detalleComprobante->idcomprobante=$objComprobante->id;
                        $detalleComprobante->idremision=$remision->id;
                        $detalleComprobante->idempresa=$remision->idempresa;
                        $detalleComprobante->descripcion=$remisionDetalle->descripcion;
                        $detalleComprobante->unidad=$remisionDetalle->unidad;
                        $detalleComprobante->cantidad=$remisionDetalle->cantidad;
                        $detalleComprobante->precio=$remisionDetalle->precio;
                        $detalleComprobante->importe=$remisionDetalle->importe;
                        $detalleComprobante->tasaiva=$remisionDetalle->tasaiva;
                        $detalleComprobante->tasahospedaje=$remisionDetalle->tasahospedaje;
                        $detalleComprobante->importeiva=$remisionDetalle->importeiva;
                        $detalleComprobante->importehospedaje=$remisionDetalle->importehospedaje;
                        $detalleComprobante->tasaieps=$remisionDetalle->tasaieps;
                        $detalleComprobante->importeieps=$remisionDetalle->importeieps;
                        $detalleComprobante->total=$remisionDetalle->importeconimpuestos;
                        $detalleComprobante->codigounidadsat=$remisionDetalle->codigounidadsat;
                        $detalleComprobante->codigogrupoprodsersat=$remisionDetalle->codigogrupoprodsersat;
                        $detalleComprobante->save();
                    }



                    $remision->idcomprobante=$objComprobante->id;
                    $remision->idcliente=$idcliente;
                    if(!$remision->save()){

                        return ['msg' => 'Error al actualizar remision','res'=>false];
                    }
                    // dd($objComprobante,$remision);
                    // if(1==1){
                    if($objComprobante->timbrar()){
                        $remision->estatus='F';
                        if($remision->save()){

                            $cliente=DB::select("select * from clientes where id=$idcliente");
                            $idusuario= $cliente[0]->idusuario;
                            if(isset($cliente)){
                                $usuario=Usuario::where('id','=',$idusuario)->first();
                                $data= $this->descargarcomprobantepdfxid($objComprobante->id);
                                Mail::to($cliente[0]->correo)->send(new EnviarFactura($data));
                                if(!empty($usuario)){
                                    if($usuario->email!=null){
                                        Mail::to($usuario->email)->send(new EnviarFactura($data));
                                    }
                                }
                            }
                            return ['msg'=>'Nota de venta facturada','res'=>true,'idcomprobante'=>$objComprobante->id];
                            // if($plataforma=='web'){
                            // }else{
                            //     return ['msg'=>'Nota de venta facturada','res'=>true,'idcomprobante'=>$objComprobante->id];
                            // }
                        }else{
                            return ['msg'=>'Error al actualizar remision','res'=>false];
                        }
                    }else{
                        $msg=$objComprobante->ErrorMsg();
                        return ['msg' => $msg,'res'=>false];
                    }
                }else{
                    return ['msg' => 'Error al guardar comprobante','res'=>false];
                }
            }
        }
        return ['msg' => 'Atras, intruso','res'=>false];
    }

    function PdfListo($id){

        $now=date('Y-m-d H:i:s');
        $pasados = strtotime ( '-10 second' , strtotime ($now) ) ;
        $before=date ( 'Y-m-d H:i:s' , $pasados);

        try {
            $query="select comprobantes.id
            from comprobantes
            inner join clientes
            on clientes.id=comprobantes.idcliente
            where comprobantes.updated_at between '$before' and '$now'
            and comprobantes.estatus='T'
            and clientes.idusuario=$id
            and comprobantes.idempresa>0"
            ;
            $pdfListo=DB::select($query);

            $pdfListo[0]->id;

            return ["idcomprobante"=>$pdfListo[0]->id ,'msg'=>'Su pdf esta listo','res'=>true,'hora'=>$now];

        } catch (\Exception $e) {
            return ['msg'=>'Aun no esta listo','res'=>false,'hora'=>$now];
        }
    }

    function downloadPDF($id)
    {

       $comprobante=DB::select("SELECT comprobantes.* FROM `comprobantes` inner join clientes on clientes.id=comprobantes.idcliente WHERE clientes.idusuario=$id
       ORDER BY comprobantes.id DESC
       limit 1 ");

        $comprobante= $comprobante[0];

        $query  = DB::select("Select rfc from empresas where id = $comprobante->idempresa");
        $emp=$comprobante->idempresa;
        $rfcemisor  =  $query[0]->rfc;
        $filename  =  "comprobantes/emisores/$rfcemisor/cfdi_{$emp}_{$comprobante->id}";


        header("Expires: 0");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check = 0, pre-check = 0", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename = comprobantes/cfdi_{$emp}_{$comprobante->id}.pdf");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($filename . '.pdf'));


        return file_get_contents($filename . '.pdf');
    }

    function downloadPDFXId($id,$iduser)
    {


        $comprobante=Comprobante::where('id','=',$id)
        ->OrderBy('id','Desc')->first();

        if($comprobante->idempresa){
            $query  = DB::select("Select rfc from empresas where id = $comprobante->idempresa");
            $emp=$comprobante->idempresa;
            $rfcemisor  =  $query[0]->rfc;
            $filename  =  "comprobantes/emisores/$rfcemisor/cfdi_{$emp}_{$comprobante->id}";
            header("Expires: 0");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check = 0, pre-check = 0", false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename = comprobantes/cfdi_{$emp}_{$comprobante->id}.pdf");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($filename . '.pdf'));

            return file_get_contents($filename . '.pdf');
        }else{

            $filename  =  "comprobantes/emisores/$iduser/cfdi_{$iduser}_{$comprobante->id}";

            header("Expires: 0");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check = 0, pre-check = 0", false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename = comprobantes/cfdi_{$iduser}_{$comprobante->id}.pdf");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($filename . '.pdf'));

            return file_get_contents($filename . '.pdf');
        }



    }

    /**CREACION DE PDF Y MUESTRA EN PANTALLA*/
        /**
         * Permite visualizar comprobante en pdf por id de comprobante
         * @return
         * @author Joel Gastelum
         * @see APKS
         */
        function descargarcomprobantepdfxid($id,$rfc="",$importar=0)
        {
            require_once "facturaestandar.php";

            $comprobante=Comprobante::where('id','=',$id)->first();

            if(!$comprobante->idempresa){
                //COMPROBANTE EXTERNO
                $cliente=Cliente::findOrFail($comprobante->idcliente);
                $filename  =  "comprobantes/emisores/$cliente->idusuario/cfdi_{$cliente->idusuario}_{$comprobante->id}";
            }else{
                $cliente=Cliente::where('id','=',$comprobante->idcliente)->first();
                $remision=Remision::where("idcomprobante","=",$id)->first();
                $empresa=Empresa::where('id','=',$comprobante->idempresa)->first();

                $documento['total']=$comprobante->total;
                $documento['moneda']="$";

                //Datos Receptor

                $documento['receptor']=[
                    'rfc'=>$cliente->rfc,
                    'nombre'=>iconv('UTF-8','ISO-8859-1',$cliente->nombre),
                    'codigopostal'=>$cliente->codigopostal,
                    'pais'=>iconv('UTF-8','ISO-8859-1',"México"),
                    'estado'=>iconv('UTF-8','ISO-8859-1',$cliente->estado),
                    'municipio'=>iconv('UTF-8','ISO-8859-1',$cliente->municipio),
                    'localidad'=>iconv('UTF-8','ISO-8859-1',$cliente->localidad),
                    'calle'=>iconv('UTF-8','ISO-8859-1',$cliente->calle),
                    'noExterior'=>$cliente->noexterior,
                    'noInterior'=>$cliente->nointerior,
                    'colonia'=>iconv('UTF-8','ISO-8859-1',$cliente->colonia),
                    'codigoPostal'=>$cliente->codigopostal,
                    'usodelcomprobante'=>$comprobante->idusocfdi
                ];

                //$documento['receptor']['localidad']="Loc";


                //Datos de Logo

                $sitioguardarcomprobar='../storage/app/public/logos';

                if (!file_exists($sitioguardarcomprobar)) {
                    Storage::makeDirectory('public/logos');
                }

                $sitioguardar='../storage/app/public/logos/';

                $filename=null;
                if($empresa->logopdf!=null){
                    $filename='../storage/app/public/'.$empresa->logopdf;
                }

                $documento['logo']=$filename;
                $documento['xinicial'] ="5";
                $documento['yinicial']="5";
                $documento['largo']=$empresa->wlogopdf;
                $documento['alto']=$empresa->hlogopdf;
                //DATOS DE FACTURA
                $documento['tipodocumento']=$comprobante->tipo;
                $documento['id']=934;
                $documento['UUID']=$comprobante->uuid;
                $documento['serie']=$comprobante->serie;
                $documento['folio']=$comprobante->folio;
                $documento['fecha']=$comprobante->fecha;
                $documento["FechaTimbrado"]=$comprobante->fechatimbre;
                $documento['nocertificado']="";
                $documento['noCertificadoSAT']="";
                $documento['metodopago']=$comprobante->metodopago;
                $datos['NumCtaPago']="000";
                $documento['formapago']=$comprobante->formapago;
                $documento['condicionesDePago']=$comprobante->condicionespago;
                $documento['fechavence']=$remision->fechavence;
                $documento['LugarExpedicion']="";
                $documento['subtotal']=$remision->subtotal;
                $documento['tasadescuento']="0";
                $documento['descuento']="0";
                $documento['total']=$comprobante->total;
                $documento['observaciones']="";
                $documento['cbb']="";
                $documento['cadena_original']=$comprobante->cadenaoriginal;
                $documento['sello']=$comprobante->sellodigitalemisor;
                $documento['selloSAT']=$comprobante->sellodigitalsat;
                //CONCEPTOS

                $conceptos=DetalleComprobante::where('idcomprobante','=',$comprobante->id)->get();
                $documento['conceptos']=$conceptos;


                $iva=0;
                $hospedaje=0;
                foreach ($conceptos as $concepto){
                    $iva+=$concepto->importeiva;
                    $hospedaje=$concepto->importehospedaje;

                }

                //TRASLADOS
                $documento['traslados']['impuestostrasladados']=[['impuesto' => 'IVA' , 'importe' => $iva],['impuesto'=>'tasahospedaje','importe'=>$hospedaje]];
                $documento['retenciones']['impuestosretenidos']=[['impuesto' => 'IVA' , 'importe' => 0]];

                //COLORES DOC
                $arraycolor = explode("@", $empresa->colorfondopdf);
                $documento["colorfondo"]['r']=$arraycolor[0];
                $documento["colorfondo"]['g']=$arraycolor[1];
                $documento["colorfondo"]['b']=$arraycolor[2];
                $arraycolor = explode("@", $empresa->colorletrapdf);
                $documento["colorletra"]['r']=$arraycolor[0];
                $documento["colorletra"]['g']=$arraycolor[1];
                $documento["colorletra"]['b']=$arraycolor[2];


                //DATOS EMISOR
                $documento['Emisor']['nombre']=iconv('UTF-8','ISO-8859-1',$empresa->nombrecomercial);
                $documento['Emisor']['rfc']=$empresa->rfc;
                $documento['regimenfiscal']=$empresa->regimen;
                $datos['Emisor']['ExpedidoEn']=['pais'=>"",'estado'=>"",'municipio'=>"",'localidad'=>"", 'calle'=>"", 'noExterior'=>"",'noInterior'=>"",'colonia'=>"",'codigoPostal'=>""];
                $datos['Emisor']['Domicilio']=['pais'=>"",'estado'=>"",'municipio'=>"",'localidad'=>"", 'calle'=>"", 'noExterior'=>"",'noInterior'=>"",'colonia'=>"",'codigoPostal'=>""];


                //DATOS ARRAY
                $datos['regimen']=$empresa->regimen;
                $datos['ocultar_expedidoen']="";
                $datos['id']="";

                $filename = "comprobantes/emisores/17/a";

                $pdf=generaFormato($documento,$datos);

                return $pdf->Output('S','factura.pdf', true); //el pdf se muestra en el navegador
                exit();
            }
            if(isset($pdf)){
                $tipodispositivo=$this->dispositivo();
                if($tipodispositivo>0){
                    $navegador=$this->obtenerNavegadorWeb();
                    if($navegador['isandroid']==1){
                        //GUARDANDO PDF EN LUGAR DESECHABLE
                        $nombre=uniqid();
                        $archivo=$pdf->Output('S',$nombre.'.pdf', true); //el pdf se guarda

                        $sitioguardarcomprobar='../storage/app/public/pdfdesechables';
                        if (!file_exists($sitioguardarcomprobar)) {
                            Storage::makeDirectory('public/pdfdesechables');
                        }
                        $filename='public/pdfdesechables/'.$nombre.'.pdf';
                        Storage::put($filename, $archivo);

                        //ES ANDROID
                        $im = new Imagick();
                        $im->setResolution(198,153);
                        $imangedir='../storage/app/public/pdfdesechables/'.$nombre.'.pdf[0]';//..storage/app/public/pdfdesechables/6021f8bdeb4c1.pdf[0]
                        $im->readimage($imangedir);
                        $im->setImageFormat('jpg');

                        //eliminar
                        unlink('../storage/app/public/pdfdesechables/'.$nombre.'.pdf');

                        header('Content-Type: image/jpeg');
                        echo $im;
                        exit();
                    }
                }
                $pdf->Output('I','factura.pdf', true); //el pdf se muestra en el navegador
                exit();
            }else{
                if(file_exists($filename.'.pdf')){
                    $tipodispositivo=$this->dispositivo();
                    if($tipodispositivo>0){
                        $navegador=$this->obtenerNavegadorWeb();
                        if($navegador['isandroid']==1){
                            //ES ANDROID
                            $im = new Imagick();
                            $im->setResolution(198,153);
                            // $im->setResolution(2551.18,3295.28);
                            $im->readimage($filename.'.pdf[0]');
                            $im->setImageFormat('jpg');
                            header('Content-Type: image/jpeg');
                            echo $im;
                            exit();
                        }
                    }

                    header('Content-type: application/pdf');
                    header("Content-Disposition: inline; filename = {$filename}.pdf");
                    header('Content-Transfer-Encoding: binary');
                    header('Content-Length: ' . filesize($filename . '.pdf'));
                    header('Accept-Ranges: bytes');
                    readfile($filename . '.pdf');
                }else{
                    //NO SE CONSIGUIO ARCHIVO
                    if(isset(auth()->user()->id)){
                        return view('error', [
                            'rutaAnt' => route('clientes.misfacturas',[$rfc]),
                            'tituloencabezado' => "Mostrar comprobante",
                            'msj' => "Oops.. Documento no encontrado",
                        ]);
                    }
                }
            }
        }

        /**
         * Permite visualizar comprobante en pdf por id de comprobante
         * @return
         * @author Joel Gastelum
         * @see misfacturas.blade.php
         */
        function descargarcomprobantepdfxidweb($id,$rfc="",$importar=0)
        {
            require_once "facturaestandar.php";

            $comprobante=Comprobante::where('id','=',$id)->first();

            if(!$comprobante->idempresa){
                //COMPROBANTE EXTERNO
                $cliente=Cliente::findOrFail($comprobante->idcliente);
                $filename  =  "comprobantes/emisores/$cliente->idusuario/cfdi_{$cliente->idusuario}_{$comprobante->id}";
            }else{
                $cliente=Cliente::where('id','=',$comprobante->idcliente)->first();
                $remision=Remision::where("idcomprobante","=",$id)->first();
                $empresa=Empresa::where('id','=',$comprobante->idempresa)->first();

                $documento['total']=$comprobante->total;
                $documento['moneda']="$";

                //Datos Receptor

                $documento['receptor']=[
                    'rfc'=>$cliente->rfc,
                    'nombre'=>iconv('UTF-8','ISO-8859-1',$cliente->nombre),
                    'codigopostal'=>$cliente->codigopostal,
                    'pais'=>iconv('UTF-8','ISO-8859-1',"México"),
                    'estado'=>iconv('UTF-8','ISO-8859-1',$cliente->estado),
                    'municipio'=>iconv('UTF-8','ISO-8859-1',$cliente->municipio),
                    'localidad'=>iconv('UTF-8','ISO-8859-1',$cliente->localidad),
                    'calle'=>iconv('UTF-8','ISO-8859-1',$cliente->calle),
                    'noExterior'=>$cliente->noexterior,
                    'noInterior'=>$cliente->nointerior,
                    'colonia'=>iconv('UTF-8','ISO-8859-1',$cliente->colonia),
                    'codigoPostal'=>$cliente->codigopostal,
                    'usodelcomprobante'=>$comprobante->idusocfdi
                ];

                //$documento['receptor']['localidad']="Loc";


                //Datos de Logo

                $sitioguardarcomprobar='../storage/app/public/logos';

                if (!file_exists($sitioguardarcomprobar)) {
                    Storage::makeDirectory('public/logos');
                }

                $sitioguardar='../storage/app/public/logos/';

                $filename=null;
                if($empresa->logopdf!=null){
                    $filename='../storage/app/public/'.$empresa->logopdf;
                }

                $documento['logo']=$filename;
                $documento['xinicial'] ="5";
                $documento['yinicial']="5";
                $documento['largo']=$empresa->wlogopdf;
                $documento['alto']=$empresa->hlogopdf;
                //DATOS DE FACTURA
                $documento['tipodocumento']=$comprobante->tipo;
                $documento['id']=934;
                $documento['UUID']=$comprobante->uuid;
                $documento['serie']=$comprobante->serie;
                $documento['folio']=$comprobante->folio;
                $documento['fecha']=$comprobante->fecha;
                $documento["FechaTimbrado"]=$comprobante->fechatimbre;
                $documento['nocertificado']="";
                $documento['noCertificadoSAT']="";
                $documento['metodopago']=$comprobante->metodopago;
                $datos['NumCtaPago']="000";
                $documento['formapago']=$comprobante->formapago;
                $documento['condicionesDePago']=$comprobante->condicionespago;
                $documento['fechavence']=$remision->fechavence;
                $documento['LugarExpedicion']="";
                $documento['subtotal']=$remision->subtotal;
                $documento['tasadescuento']="0";
                $documento['descuento']="0";
                $documento['total']=$comprobante->total;
                $documento['observaciones']="";
                $documento['cbb']="";
                $documento['cadena_original']=$comprobante->cadenaoriginal;
                $documento['sello']=$comprobante->sellodigitalemisor;
                $documento['selloSAT']=$comprobante->sellodigitalsat;
                //CONCEPTOS

                $conceptos=DetalleComprobante::where('idcomprobante','=',$comprobante->id)->get();
                $documento['conceptos']=$conceptos;


                $iva=0;
                $hospedaje=0;
                foreach ($conceptos as $concepto){
                    $iva+=$concepto->importeiva;
                    $hospedaje=$concepto->importehospedaje;

                }

                //TRASLADOS
                $documento['traslados']['impuestostrasladados']=[['impuesto' => 'IVA' , 'importe' => $iva],['impuesto'=>'tasahospedaje','importe'=>$hospedaje]];
                $documento['retenciones']['impuestosretenidos']=[['impuesto' => 'IVA' , 'importe' => 0]];

                //COLORES DOC
                $arraycolor = explode("@", $empresa->colorfondopdf);
                $documento["colorfondo"]['r']=$arraycolor[0];
                $documento["colorfondo"]['g']=$arraycolor[1];
                $documento["colorfondo"]['b']=$arraycolor[2];
                $arraycolor = explode("@", $empresa->colorletrapdf);
                $documento["colorletra"]['r']=$arraycolor[0];
                $documento["colorletra"]['g']=$arraycolor[1];
                $documento["colorletra"]['b']=$arraycolor[2];


                //DATOS EMISOR
                $documento['Emisor']['nombre']=iconv('UTF-8','ISO-8859-1',$empresa->nombrecomercial);
                $documento['Emisor']['rfc']=$empresa->rfc;
                $documento['regimenfiscal']=$empresa->regimen;
                $datos['Emisor']['ExpedidoEn']=['pais'=>"",'estado'=>"",'municipio'=>"",'localidad'=>"", 'calle'=>"", 'noExterior'=>"",'noInterior'=>"",'colonia'=>"",'codigoPostal'=>""];
                $datos['Emisor']['Domicilio']=['pais'=>"",'estado'=>"",'municipio'=>"",'localidad'=>"", 'calle'=>"", 'noExterior'=>"",'noInterior'=>"",'colonia'=>"",'codigoPostal'=>""];


                //DATOS ARRAY
                $datos['regimen']=$empresa->regimen;
                $datos['ocultar_expedidoen']="";
                $datos['id']="";

                $filename = "comprobantes/emisores/17/a";

                // dd($documento);
                $pdf=generaFormato($documento,$datos);
            }
            if(isset($pdf)){
                $tipodispositivo=$this->dispositivo();
                if($tipodispositivo>0){
                    $navegador=$this->obtenerNavegadorWeb();
                    if($navegador['isandroid']==1){
                        //GUARDANDO PDF EN LUGAR DESECHABLE
                        $nombre=uniqid();
                        $archivo=$pdf->Output('S',$nombre.'.pdf', true); //el pdf se guarda

                        $sitioguardarcomprobar='../storage/app/public/pdfdesechables';
                        if (!file_exists($sitioguardarcomprobar)) {
                            Storage::makeDirectory('public/pdfdesechables');
                        }
                        $filename='public/pdfdesechables/'.$nombre.'.pdf';
                        Storage::put($filename, $archivo);

                        //ES ANDROID
                        $im = new Imagick();
                        $im->setResolution(198,153);
                        $imangedir='../storage/app/public/pdfdesechables/'.$nombre.'.pdf[0]';//..storage/app/public/pdfdesechables/6021f8bdeb4c1.pdf[0]
                        $im->readimage($imangedir);
                        $im->setImageFormat('jpg');

                        //eliminar
                        unlink('../storage/app/public/pdfdesechables/'.$nombre.'.pdf');

                        header('Content-Type: image/jpeg');
                        echo $im;
                        exit();
                    }
                }
                $pdf->Output('I','factura.pdf', true); //el pdf se muestra en el navegador
                exit();
            }else{
                if(file_exists($filename.'.pdf')){
                    $tipodispositivo=$this->dispositivo();
                    if($tipodispositivo>0){
                        $navegador=$this->obtenerNavegadorWeb();
                        if($navegador['isandroid']==1){
                            //ES ANDROID
                            $im = new Imagick();
                            $im->setResolution(198,153);
                            // $im->setResolution(2551.18,3295.28);
                            $im->readimage($filename.'.pdf[0]');
                            $im->setImageFormat('jpg');
                            header('Content-Type: image/jpeg');
                            echo $im;
                            exit();
                        }
                    }

                    header('Content-type: application/pdf');
                    header("Content-Disposition: inline; filename = {$filename}.pdf");
                    header('Content-Transfer-Encoding: binary');
                    header('Content-Length: ' . filesize($filename . '.pdf'));
                    header('Accept-Ranges: bytes');
                    readfile($filename . '.pdf');
                }else{
                    //NO SE CONSIGUIO ARCHIVO
                    if(isset(auth()->user()->id)){
                        return view('error', [
                            'rutaAnt' => route('clientes.misfacturas',[$rfc]),
                            'tituloencabezado' => "Mostrar comprobante",
                            'msj' => "Oops.. Documento no encontrado",
                        ]);
                    }
                }
            }
        }

        /**
         * Permite visualizar comprobante en pdf por id de comprobante desde la parte publica
         * @return
         * @author Arredondo, José
         * @see facturar.blade.php
         */
        function descargarcomprobantepdfxidafuera($id)
        {
            require_once "facturaestandar.php";

            $comprobante=Comprobante::where('id','=',$id)->first();

            if(!$comprobante->idempresa){
                //COMPROBANTE EXTERNO
                $cliente=Cliente::findOrFail($comprobante->idcliente);
                $filename  =  "comprobantes/emisores/$cliente->idusuario/cfdi_{$cliente->idusuario}_{$comprobante->id}";
            }else{
                $cliente=Cliente::where('id','=',$comprobante->idcliente)->first();
                $remision=Remision::where("idcomprobante","=",$id)->first();
                $empresa=Empresa::where('id','=',$comprobante->idempresa)->first();

                $documento['total']=$comprobante->total;
                $documento['moneda']="$";

                //Datos Receptor

                $documento['receptor']=[
                    'rfc'=>$cliente->rfc,
                    'nombre'=>iconv('UTF-8','ISO-8859-1',$cliente->nombre),
                    'codigopostal'=>$cliente->codigopostal,
                    'pais'=>iconv('UTF-8','ISO-8859-1',"México"),
                    'estado'=>iconv('UTF-8','ISO-8859-1',$cliente->estado),
                    'municipio'=>iconv('UTF-8','ISO-8859-1',$cliente->municipio),
                    'localidad'=>iconv('UTF-8','ISO-8859-1',$cliente->localidad),
                    'calle'=>iconv('UTF-8','ISO-8859-1',$cliente->calle),
                    'noExterior'=>$cliente->noexterior,
                    'noInterior'=>$cliente->nointerior,
                    'colonia'=>iconv('UTF-8','ISO-8859-1',$cliente->colonia),
                    'codigoPostal'=>$cliente->codigopostal,
                    'usodelcomprobante'=>$comprobante->idusocfdi
                ];

                //$documento['receptor']['localidad']="Loc";


                //Datos de Logo

                $sitioguardarcomprobar='../storage/app/public/logos';

                if (!file_exists($sitioguardarcomprobar)) {
                    Storage::makeDirectory('public/logos');
                }

                $sitioguardar='../storage/app/public/logos/';

                $filename=null;
                if($empresa->logopdf!=null){
                    $filename='../storage/app/public/'.$empresa->logopdf;
                }

                $documento['logo']=$filename;
                $documento['xinicial'] ="5";
                $documento['yinicial']="5";
                $documento['largo']=$empresa->wlogopdf;
                $documento['alto']=$empresa->hlogopdf;
                //DATOS DE FACTURA
                $documento['tipodocumento']=$comprobante->tipo;
                $documento['id']=934;
                $documento['UUID']=$comprobante->uuid;
                $documento['serie']=$comprobante->serie;
                $documento['folio']=$comprobante->folio;
                $documento['fecha']=$comprobante->fecha;
                $documento["FechaTimbrado"]=$comprobante->fechatimbre;
                $documento['nocertificado']="";
                $documento['noCertificadoSAT']="";
                $documento['metodopago']=$comprobante->metodopago;
                $datos['NumCtaPago']="000";
                $documento['formapago']=$comprobante->formapago;
                $documento['condicionesDePago']=$comprobante->condicionespago;
                $documento['fechavence']=$remision->fechavence;
                $documento['LugarExpedicion']="";
                $documento['subtotal']=$comprobante->subtotal;
                $documento['tasadescuento']="0";
                $documento['descuento']="0";
                $documento['total']=$comprobante->total;
                $documento['observaciones']="";
                $documento['cbb']="";
                $documento['cadena_original']=$comprobante->cadenaoriginal;
                $documento['sello']=$comprobante->sellodigitalemisor;
                $documento['selloSAT']=$comprobante->sellodigitalsat;
                //CONCEPTOS

                $conceptos=DetalleComprobante::where('idcomprobante','=',$comprobante->id)->get();
                $documento['conceptos']=$conceptos;


                $iva=0;
                $hospedaje=0;
                foreach ($conceptos as $concepto){
                    $iva+=$concepto->importeiva;
                    $hospedaje=$concepto->importehospedaje;

                }

                //TRASLADOS
                $documento['traslados']['impuestostrasladados']=[['impuesto' => 'IVA' , 'importe' => $iva],['impuesto'=>'tasahospedaje','importe'=>$hospedaje]];
                $documento['retenciones']['impuestosretenidos']=[['impuesto' => 'IVA' , 'importe' => 0]];

                //COLORES DOC
                $arraycolor = explode("@", $empresa->colorfondopdf);
                $documento["colorfondo"]['r']=$arraycolor[0];
                $documento["colorfondo"]['g']=$arraycolor[1];
                $documento["colorfondo"]['b']=$arraycolor[2];
                $arraycolor = explode("@", $empresa->colorletrapdf);
                $documento["colorletra"]['r']=$arraycolor[0];
                $documento["colorletra"]['g']=$arraycolor[1];
                $documento["colorletra"]['b']=$arraycolor[2];


                //DATOS EMISOR
                $documento['Emisor']['nombre']=iconv('UTF-8','ISO-8859-1',$empresa->nombrecomercial);
                $documento['Emisor']['rfc']=$empresa->rfc;
                $documento['regimenfiscal']=$empresa->regimen;
                $datos['Emisor']['ExpedidoEn']=['pais'=>"",'estado'=>"",'municipio'=>"",'localidad'=>"", 'calle'=>"", 'noExterior'=>"",'noInterior'=>"",'colonia'=>"",'codigoPostal'=>""];
                $datos['Emisor']['Domicilio']=['pais'=>"",'estado'=>"",'municipio'=>"",'localidad'=>"", 'calle'=>"", 'noExterior'=>"",'noInterior'=>"",'colonia'=>"",'codigoPostal'=>""];


                //DATOS ARRAY
                $datos['regimen']=$empresa->regimen;
                $datos['ocultar_expedidoen']="";
                $datos['id']="";

                $filename = "comprobantes/emisores/17/a";



                $pdf=generaFormato($documento,$datos);


                return $pdf->output('D','factura.pdf', true);
            }

            if(file_exists($filename.'.pdf')){
                // $tipodispositivo=$this->dispositivo();
                // if($tipodispositivo>0){
                //     $navegador=$this->obtenerNavegadorWeb();
                //     if($navegador['isandroid']==1){
                //         //ES ANDROID
                //         $im = new Imagick();
                //         $im->setResolution(198,153);
                //         // $im->setResolution(2551.18,3295.28);
                //         $im->readimage($filename.'.pdf[0]');
                //         $im->setImageFormat('jpg');
                //         header('Content-Type: image/jpeg');
                //         echo $im;
                //         exit();
                //     }
                // }

                header('Content-type: application/pdf');
                header("Content-Disposition: inline; filename = {$filename}.pdf");
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: ' . filesize($filename . '.pdf'));
                header('Accept-Ranges: bytes');
                readfile($filename . '.pdf');
            }else{
                //NO SE CONSIGUIO ARCHIVO
                if(isset(auth()->user()->id)){
                    return view('error', [
                        'rutaAnt' => route('clientes.misfacturas',[$rfc]),
                        'tituloencabezado' => "Mostrar comprobante",
                        'msj' => "Oops.. Documento no encontrado",
                    ]);
                }
            }
            //
        }


    /**
     * Permite visualizar comproante en xml por id de comprobante
     * @author Arredondo, José
     * @see facturar.blade.php
     */
    function descargarcomprobantexmlxid($id)
    {
        $comprobante=Comprobante::where('id','=',$id)->first();

        if($comprobante->idempresa){
            $query  = DB::select("Select rfc from empresas where id = $comprobante->idempresa");
            $emp=$comprobante->idempresa;
            $rfcemisor  =  $query[0]->rfc;
            $filename  =  "comprobantes/emisores/$rfcemisor/cfdi_{$emp}_{$comprobante->id}";
        }else{
            $cliente=Cliente::findOrFail($comprobante->idcliente);
            $filename  =  "comprobantes/emisores/$cliente->idusuario/cfdi_{$cliente->idusuario}_{$comprobante->id}";
        }

        header("Expires: 0");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check = 0, pre-check = 0", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename = ".$filename.".xml");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize($filename . '.xml'));

        return file_get_contents($filename . '.xml');
    }


    function toString($array){
        $cadena="";
        $i=0;
        foreach ($array as $ar){
            if($i<count($array)-1)
            $cadena.=$ar.',';
            else
            $cadena.=$ar;
            $i++;
        }

        return $cadena;
    }



    function EnviarFacturas(Request $request)
    {


        $correo=$request->correo;
        $idfacturas=json_decode($request->idfacturas);


        $token = bin2hex(random_bytes(10));
        $zip = new ZipArchive();
        $sitioguardarcomprobar="../storage/app/public/zips/Zip{$request->idusuario}.zip";

        if (!file_exists($sitioguardarcomprobar)) {
            Storage::makeDirectory( 'public/zips');


        }
        $sitioguardar='../storage/app/public/zips/';

        if($zip->open($sitioguardarcomprobar,ZipArchive::CREATE)===true){


            for($k=0;$k<count($idfacturas); $k++){
                $comprobante=Comprobante::where('id','=',$idfacturas[$k])
                ->OrderBy('id','Desc')->first();


            if($comprobante->idempresa){
                $emisor=Empresa::where('id','=',$comprobante->idempresa)->first();

                $data= $this->descargarcomprobantepdfxid($idfacturas[$k],"",$k);

                $filename  =  "comprobantes/emisores/cfdi_{$request->idusuario}_{$comprobante->id}";
                file_put_contents( $filename.".pdf",$data);

                $zip->addFile($filename.'.pdf');





              //  unlink($sitioguardar."Factura{$comprobante->id}.pdf");


            }else{


                $filename  =  "comprobantes/emisores/$request->idusuario/cfdi_{$request->idusuario}_{$comprobante->id}";
                if(file_exists($filename)){
                    $zip->addFile($filename.'.pdf');
                    $zip->addFile($filename.'.jpg');

                }
            }
        }

        $location=route('cliente.verexcel',$this->toString($idfacturas));

            $zip->close();
        }

        $Infozip=file_get_contents($sitioguardarcomprobar);


        Mail::to($request->correo)->send(new EnviarZip($Infozip,$location));

        for($k=0;$k<count($idfacturas); $k++){
            $comprobante=Comprobante::where('id','=',$idfacturas[$k])
            ->OrderBy('id','Desc')->first();

            $filename  =  "comprobantes/emisores/cfdi_{$request->idusuario}_{$comprobante->id}";
            if(file_exists($filename))
            unlink($filename.'.pdf');
        }
        unlink($sitioguardarcomprobar);

        //file_get_contents();

        return ["res"=>true , "msg"=>"Correo enviado"];
    }



    function MandarCorreo(Request $request)
    {

        // if($request->idcliente){
        //     $comprobante=Cliente::where('idcliente','=',$request->idcliente)
        //     $comprobante=Comprobante::where('idcliente','=',$request->idcliente)
        //     ->OrderBy('id','Desc')->first();
        // }
      /*  if($request->idcomprobante){
            $comprobante=Comprobante::where('id','=',$request->idcomprobante)
            ->OrderBy('id','Desc')->first();
        }
        if($comprobante->idempresa){

            $query  = DB::select("Select rfc from empresas where id = $comprobante->idempresa");
            $emp=$comprobante->idempresa;
            $rfcemisor  =  $query[0]->rfc;
            $filename  =  "comprobantes/emisores/$rfcemisor/cfdi_{$emp}_{$comprobante->id}";
        }else{
            $cliente=Cliente::findOrFail($comprobante->idcliente);
            $filename  =  "comprobantes/emisores/$cliente->idusuario/cfdi_{$cliente->idusuario}_{$comprobante->id}";
        }*/

      $data= $this->descargarcomprobantepdfxid($request->idcomprobante);

        Mail::to($request->correo)->send(new EnviarFactura($data));

        //file_get_contents();
     /*       $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->Driver = "smtp";
            $mail->Mailer = "smtp";
            $mail->IsHTML(true);
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'smtp.gmail.com';
            $mail->Port = 587;
            $mail->Username = 'gestioncalidadfe@gmail.com';
            $mail->Password = 'dpdbibewdqpbblbg';
            $mail->SMTPAuth = true;
            $mail->body="Facturado";
            $mail->Body    = 'Su factura fue hecha!';
            $mail->AltBody    ='';
            $mail->Subject = 'Exito!';
            $mail->addAttachment($filename . '.pdf');
            //$mail->SMTPDebug = 2;
            $mail->From = 'gestioncalidadfe@gmail.com';
            $mail->FromName = 'Factura tu nota';
            $mail->addAddress($request->correo);     // Add a recipient
            $mail->Subject = 'Su factura esta hecha!!';
            $mail->From = 'gestioncalidadfe@gmail.com';
            $mail->AddReplyTo($request->correo);
            $mail->FromName = 'Mefactura';

            $mail->Send();*/

        return ["res"=>true , "msg"=>"Correo enviado"];
    }

}
