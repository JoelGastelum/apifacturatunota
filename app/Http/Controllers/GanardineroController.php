<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cliente;
use App\Viaje;
use App\Comprobante;
use App\Codigopromocion;
use App\Genero;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

class GanardineroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Permite mostrar una vista mostrar un pagina con el vídeo
     * de como ganar dinero donde titulo, y colocar un parrafo que diga,
     * con mefactura/com es muy facil ganar dinero, solo sigue
     * el siguiente video y aprenderas como. video
     * y un boton diga si quiero ganar dinero con mefactura.com
     * y reenviar a pagina de terminos y condiciones
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ganardinero.index', [
            'tituloencabezado' => "Gana dinero con nosotros",
            'rutaAnt'=>route('home')
        ]);
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function terminoscondiciones()
    {
        $objUser = User::where("id", "=", auth()->user()->id)->first();
        $generos = Genero::where("idusercreated", "=", auth()->user()->id)
                          ->orWhereNull('idusercreated')
                          ->pluck('descripcion','id');
        return view('ganardinero.terminoscondiciones', [
            'objUser' => $objUser,
            'generos' => $generos,
            'tituloencabezado' => "Terminos y condiciones",
            'rutaAnt'=>route('ganardinero.index')
        ]);
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function sterminoscondiciones(Request $request)
    {
        $error=0;
        DB::beginTransaction();
        $objUser = User::where("id", "=", auth()->user()->id)->first();
        $objUser->nombre=$request->nombre;
        $objUser->clabeinterbancaria=$request->clabeinterbancaria;
        $objUser->movil=$request->movil;
        $objUser->fnacimiento=$request->fnacimiento;
        $objUser->idtipousuario=5;

        $idgenero = $request->idgenero;
        if($request->idgenero==env('IDGENEROPERSONALIZADO')){
            //ES PERSONALIZADO VAMOS A CREAR EL GENERO
            $ObjGenero = new Genero;
            $ObjGenero->descripcion = $request->descripcion;
            $ObjGenero->referirsecomo = $request->referirsecomo;
            $ObjGenero->idusercreated = auth()->user()->id;
            if(!$ObjGenero->save()){
                $error=1;
            }else{
                $idgenero = $ObjGenero->id;
            }
        }
        $objUser->idgenero=$idgenero;

        if($error==0){
            if($objUser->save()){
                //CODIGO DE PROMOCIONES PASAR
                $objCodigopromocion = new Codigopromocion;
                $objCodigopromocion->idrecomendado = $objUser->id;
                $objCodigopromocion->iddueno = $objUser->id;
                $objCodigopromocion->codigo = 'V'.substr($objUser->codigopromocion, 1);
                $objCodigopromocion->tipo = 1;

                if($objCodigopromocion->save()){
                    DB::commit();
                    return redirect()->route('contribuyentes')->with('success','Bienvenido a mefactura.com ahora podras recomendar establecimiento así ganar dinero.');
                }else{
                    DB::rollback();
                    return redirect()->route('contribuyentes')->with('danger','Intente de nuevo');
                }
            }else{
                DB::rollback();
                return redirect()->route('contribuyentes')->with('danger','Intente de nuevo');
            }
        }else{
            DB::rollback();
            return redirect()->route('contribuyentes')->with('danger','Intente de nuevo');
        }
    }
}
