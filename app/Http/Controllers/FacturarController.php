<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Cliente;
use App\Remision;
use App\Usocomprobantesat;

class FacturarController extends Controller
{
    public function __construct(){
    }

    /**
     * Permite facturar
     */
    public function facturar($idremision_code){
        // $nota=base64_encode(18);
        // dd(base64_decode($idremision_code),$nota);

        $error=0;
        $msj='';
        $typemsj='';
        $disabled="";
        $idremision=base64_decode($idremision_code);
        $objRemision = Remision::where("id", "=", $idremision)
                               ->first();

        if(!empty($objRemision)){
            if($objRemision->estatus=='A'){
            }elseif($objRemision->estatus=='F'){
                $typemsj='danger';
                $disabled=true;
                $msj = 'Nota '.$objRemision->noticket.' facturada anteriormente.';
                $error=1;//FACTURADA
            }
        }else{
            $typemsj='danger';
            $disabled=true;
            $msj='Nota no encontrada.';
            $error=-1;//NO ENCONTADA
        }

        //BUSCAR uso_comprobante_sat
        $objUsoComprobanteSat = Usocomprobantesat::orderby('nombre','ASC')->pluck('nombre','nombre');
        // dd($idremision,$objRemision,$objUsoComprobanteSat);

        return view('facturar', [
            'objRemision' => $objRemision,
            'objUsoComprobanteSat' => $objUsoComprobanteSat,
            'disabled' => $disabled,
            'typemsj' => $typemsj,
            'error' => $error,
            'msj' => $msj,
        ]);
    }

    /**
     * Permite verificar si existe anteriormente el rfc
     */
    public function verificarrfc(Request $request){
        $objCliente = Cliente::where("rfc", "=", $request->rfc)
                             ->whereNull('idusuario')
                             ->first();

        echo json_encode($objCliente);
    }

    /**
     * Permite verificar si existe anteriormente el rfc
     */
    public function guardarcliente(Request $request){
        $error=0;
        $idcliente=-1;

        $objCliente = Cliente::where("rfc", "=", $request->rfc)
                             ->whereNull('idusuario')
                             ->first();
        if(empty($objCliente)){
            $objCliente = new CLiente;
        }

        $objCliente->rfc=$request->rfc;
        if(!empty($request->idempresa) && $request->idempresa!='' && $request->idempresa!=null){
            $objCliente->idempresa=$request->idempresa;
        }else{
            $objCliente->idempresa=0;
        }
        $objCliente->nombre=$request->nombre;
        $objCliente->correo=$request->correo;
        $objCliente->calle=$request->calle;
        $objCliente->localidad=$request->localidad;
        $objCliente->idusuario=null;
        $objCliente->estatus= 'A' ;

        if($objCliente->save()){
            $idcliente=$objCliente->id;
        }else{
            $error=1;
        }

        $data_envio=array();
        $data_envio['error']=$error;
        $data_envio['idcliente']=$idcliente;

        echo json_encode($data_envio);
    }
}
