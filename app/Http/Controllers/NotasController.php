<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Remision;

class NotasController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1
         ';
        if($request->fecha_inicial!='' && $request->fecha_inicial!=null && !empty($request->fecha_inicial)){
            if($request->fecha_final!='' && $request->fecha_final!=null && !empty($request->fecha_final)){
                $fecha_inicial = date('Y-m-d',strtotime($request->fecha_inicial));
                $fecha_final = date('Y-m-d',strtotime($request->fecha_final));

                $sql_where .= ' AND remisiones.fecha >= "'.$fecha_inicial.'"';
                $sql_where .= ' AND remisiones.fecha <= "'.$fecha_final.'"';
            }
        }

        if($request->estatus!='T'){
            $sql_where .= ' AND remisiones.estatus = "'.$request->estatus.'"';
        }else{
            $sql_where .= ' AND remisiones.estatus <> "D"';
        }

        $sql_where .= ' AND remisiones.idempresa = "'.auth()->user()->idempresa.'"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                remisiones.id,
                remisiones.noticket,
                ifnull(clientes.nombre,"") as nombrecliente,
                DATE_FORMAT(remisiones.fechavence,"'.$formatdatemysql.'") as fechavence,
                FORMAT(remisiones.subtotal,2) as subtotal,
                FORMAT(remisiones.ieps,2) as ieps,
                FORMAT(remisiones.iva,2) as iva,
                FORMAT(remisiones.total,2) as total,
                remisiones.estatus
            FROM remisiones
            LEFT JOIN clientes ON clientes.id = remisiones.idcliente
            '.$sql_where.'
            ORDER BY
                remisiones.id ASC
        ';
        $coleccion = DB::select($sql);


        $estatus_notas=$this->variablesglobales("estatus_notas");

        foreach ($coleccion as $data) {

            $acciones='';
                $acciones.='<a href="'.route('notas.visualizar',$data->id).'" class="btn btn-sm btn-info" title="Visualizar">
                    <i class="fas fa-eye"></i>
                </a>';
                if($data->estatus=='A'){
                    $acciones.='&nbsp';
                    $acciones.='<form method="POST" action="'.route('notas.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                    $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                    $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        <i class="far fa-trash-alt"></i>
                    </button>';
                    $acciones.='</form>';
                }
            $data->acciones=$acciones;

            if(isset($estatus_notas[$data->estatus])){
                $data->estatus = $estatus_notas[$data->estatus];
            }
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR CobranzaController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'NotasController');

        $estatus_notas=$this->variablesglobales("estatus_notas");
        $estatus_notas['T']='Todos';

        return view('notas.index', [
            'arraybtn' => $arraybtn,
            'estatus_notas' => $estatus_notas,
            'tituloencabezado'=>'Notas'
        ]);
    }

    /**
     * Permite visualizar la nota
     *
     * @return \Illuminate\Http\Response
     */
    public function visualizar($id)
    {
        $objRemision = Remision::where("id", "=", $id)->first();
        if(!empty($objRemision)){
            return view('notas.visualizar', [
                'objRemision' => $objRemision,
                'tituloencabezado'=>'Notas'
            ]);
        }else{
            return redirect()->route('notas.index')->with('danger','Error: nota no encontrada');
        }
    }

        /**
         * Funcion que permite buscar las cuentas de banco para la moneda seleccionada
         * @param  Request $request [description]
         * @return json
         */
        public function buscarconceptosdenotacreada(Request $request){
            $html='';
            $error=0;

            $remision = Remision::where("id", "=", $request->idremision)->first();
            if(empty($remision)){
                $error='No se consiguio nota';
            }else{
                foreach ($remision->remisionesdetalle as $data) {
                    $nombreconcepto='Concepto no encontrado';
                    if(isset($data->concepto->codigo) || isset($data->concepto->concepto)){
                        $nombreconcepto='';
                        if(isset($data->concepto->codigo)){
                            $nombreconcepto.=$data->concepto->codigo.' ';
                        }
                        if(isset($data->concepto->concepto)){
                            $nombreconcepto.=$data->concepto->concepto;
                        }
                    }
                    if($nombreconcepto=='Concepto no encontrado'){
                        $nombreconcepto=$data->descripcion;
                    }

                    $html .= '<tr>';
                        $html .= '<td>';
                            $html .= $nombreconcepto;
                        $html .= '</td>';
                        $html .= '<td style="text-align:right;">';
                            $html .= number_format($data->importe,2,'.',',');
                        $html .= '</td>';
                    $html .= '</tr>';
                }
            }

            $data_envio['html']=$html;
            $data_envio['error']=$error;
            echo json_encode($data_envio);
        }

    /**
     * Permite eliminar un gastp
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objRemision = Remision::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();

            if(!isset($error)){
                //ELIMINACIÓN FISICA
                if(!$Remision->delete()){
                    DB::rollback();
                    $typemsj='danger';
                    $msj='No se ha podido eliminar';
                }else{
                    DB::commit();
                }
            }else{
                DB::rollback();
            }
        }else{
            DB::beginTransaction();
            if(!isset($error)){
                //ELIMINACIÓN LOGICA
                $objRemision->estatus = 'D';

                if(!$objRemision->save()){
                    DB::rollback();
                    $typemsj='danger';
                    $msj='No se ha podido eliminar';
                }else{
                    DB::commit();
                }
            }else{
                DB::rollback();
            }
        }
        return redirect()->route('notas.index')->with($typemsj,$msj);
    }
}
