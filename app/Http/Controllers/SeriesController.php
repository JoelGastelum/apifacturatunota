<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Serie;
use App\Certificado;

class SeriesController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';

        $sql_where .= ' AND series.idempresa = "'.auth()->user()->idempresa.'" AND series.estatus = "A"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                series.id,
                series.idempresa,
                series.serie,
                series.folio_actual,
                certificados.idintegra
            FROM series
            LEFT JOIN certificados ON series.idcertificadointegra=certificados.id
            '.$sql_where.'
            ORDER BY
                series.serie ASC,
                series.folio_actual ASC
        ';
        $coleccion = DB::select($sql);

        foreach ($coleccion as $data) {
            $acciones='';
                $acciones.='<a href="'.route('series.edit',$data->id).'" class="btn btn-sm btn-warning" title="Editar">
                    <i class="fas fa-edit"></i>
                </a>';
                $acciones.='&nbsp';
                $acciones.='<form method="POST" action="'.route('series.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <i class="far fa-trash-alt"></i>
                </button>';
                $acciones.='</form>';
            $data->acciones=$acciones;
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR SeriesController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'SeriesController');

        return view('series.index', [
            'arraybtn' => $arraybtn,
            'tituloencabezado'=>'Series'
        ]);
    }

    /**
     * Permite crear una serie
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objSerie = new Serie;

        //certificados
        $certicados = Certificado::where("idempresa", "=", auth()->user()->idempresa)->where("estatus", "=", 'A')->pluck('serie','id');

        return view('series.edit', [
            'objSerie' => $objSerie,
            'certicados' => $certicados,
            'tituloencabezado'=>'Series'
        ]);
    }

    /**
     * Permite editar una
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objSerie = Serie::where("id", "=", $id)->where("estatus", "=", 'A')->first();
        if(!empty($objSerie)){
            //certificados
            $certicados = Certificado::where("idempresa", "=", auth()->user()->idempresa)->where("estatus", "=", 'A')->pluck('serie','id');

            return view('series.edit', [
                'objSerie' => $objSerie,
                'certicados' => $certicados,
                'tituloencabezado'=>'Series'
            ]);
        }else{
            return redirect()->route('series.index')->with('danger','Error: Serie no encontrada');
        }
    }

    /**
     * Permite actualizar los datos de una entidad del modelo en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        DB::beginTransaction();
        if($id==-1){
            $objSerie = new Serie;
        }else{
            $objSerie = Serie::where("id", "=", $id)->first();
        }

        $objSerie->idempresa = auth()->user()->idempresa;
        $objSerie->serie = $request->serie;
        $objSerie->folio_actual = $request->folio_actual;
        $objSerie->idcertificadointegra = $request->idcertificadointegra;

        if($objSerie->save()){
            DB::commit();
            return redirect()->route('series.index')->with('success','Se ha guardado los cambios con éxito');
        }else{
            DB::rollback();
            return redirect()->route('series.index')->with('danger','No se pudo guardar los cambios');
        }
    }

    /**
     * Permite eliminar un gastp
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objSerie = Serie::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();
            //ELIMINACIÓN FISICA
            if(!$objSerie->delete()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }else{
            DB::beginTransaction();
            //ELIMINACIÓN LOGICA
            $objSerie->estatus = 'D';

            if(!$objSerie->save()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }
        return redirect()->route('series.index')->with($typemsj,$msj);
    }
}
