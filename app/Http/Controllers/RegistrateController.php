<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

//ENVIO DE EMAILS
use Illuminate\Support\Facades\Mail;

//MODELOS
use App\User;
use App\Empresa;

use App\Mail\Codigoconfirmacion;
use App\Mail\Registrate;

/**
 * Controlar para el registro de usuarios tipo cliente o consumidor
 */
class RegistrateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function consumidor($codigopromocion=null)
    {
        return view('consumidor',[
            'codigopromocion'=>$codigopromocion
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gana($codigopromocion=null)
    {
        return view('gana',[
            'codigopromocion'=>$codigopromocion
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function espereconfirmacion($iduser)
    {
        $objUser = User:: where("id", "=", $iduser)->first();
        if(!empty($objUser)){
            if($objUser->estatus=="S"){
                return view('espereconfirmacion',[
                    'objUser'=>$objUser
                ]);
            }
        }

        return redirect()->route('welcome');
    }

    /**
     * Permite mostrar una vista diciendo que ya su usuario esta confirmado y su creedenciales en su correo
     *
     * @return \Illuminate\Http\Response
     */
    public function usuarioconfirmado($iduser,$idremisionencode=null)
    {
        $objUser = User:: where("id", "=", $iduser)->first();
        if(!empty($objUser)){
            if($objUser->estatus=="A"){
                return view('usuarioconfirmado',[
                    'objUser'=>$objUser,
                    'idremisionencode'=>$idremisionencode
                ]);
            }
        }

        return redirect()->route('welcome');
    }

        /**
         * Funcion que permite verificar si el correo existe previamente
         *
        */
        public function verificarusuarioocorreo(Request $request){
            $dataenvio['error']=0;
            $dataenvio['title']='';
            $dataenvio['msj']='';

            if($request->tipo=='email'){
                if($this->is_valid_email($request->email)){
                    $idtipousuario=explode(',', $request->idtipousuario);

                    $objUser = User:: where("email", "=", $request->email)
                                    ->whereIn('idtipousuario', $idtipousuario)
                                        // ->where("estatus", "=", "A")
                                        ->first();

                    if(!empty($objUser)){
                        $dataenvio['error']=1;
                        $dataenvio['title']='Correo existente';
                        if($objUser->estatus=="S"){
                            //FALTA POR CONFIRMAR
                            $ruta = route('codigoconfirmacion',[$objUser->codigoconfirmacion]);
                            //ENVIAR CORREO PARA CONFIRMACION DE
                                $data= array(
                                    'login'=>$objUser->login,
                                    'nombre'=>$objUser->nombre,
                                    'link'=>$ruta,
                                );
                                Mail::to($objUser->email)->send(new Codigoconfirmacion($data));
                            $dataenvio['msj']='Falta por confirmar cuenta, se acaba de enviar correo de confirmación.';
                        }else{
                            $dataenvio['msj']='';
                        }
                    }
                }else{
                    $dataenvio['error']=1;
                    $dataenvio['msj']='No es correo válido';
                }
            }elseif($request->tipo=='login'){
                $objUser = User:: where("login", "=", $request->login)
                                    // ->where("estatus", "=", "A")
                                    ->first();

                if(!empty($objUser)){
                    $dataenvio['error']=1;
                    $dataenvio['title']='Login existente';
                    if($objUser->estatus=="S"){
                        //FALTA POR CONFIRMAR
                        $ruta = route('codigoconfirmacion',[$objUser->codigoconfirmacion]);
                        //ENVIAR CORREO PARA CONFIRMACION DE
                            $data= array(
                                'login'=>$objUser->login,
                                'nombre'=>$objUser->nombre,
                                'link'=>$ruta,
                            );
                            Mail::to($objUser->email)->send(new Codigoconfirmacion($data));
                        $dataenvio['msj']='Falta por confirmar cuenta, se acaba de enviar correo de confirmación.';
                    }else{
                        $dataenvio['msj']='';
                    }
                }
            }elseif($request->tipo=='codigopromocion'){
                $objUser = User:: where("codigopromocion", "=", $request->codigopromocion)
                                    // ->where("estatus", "=", "A")
                                    ->first();

                if(empty($objUser)){
                    $dataenvio['error']=1;
                    $dataenvio['msj']='Código no válido';
                }else{
                    $dataenvio['usuario']='Referido a través de: '.$objUser->nombre.' ('.$objUser->login.')';
                }
            }elseif($request->tipo=='rfc'){
                $canti = Empresa:: where("rfc", "=", $request->rfc)
                                    ->where("estatus", "=", "A")
                                    ->count();

                if($canti>0){
                    $dataenvio['error']=1;
                    $dataenvio['msj']='Rfc proporcionado ya se encuentra registrado, por favor hacer renovación desde el panel de administración';
                }
            }

            echo json_encode($dataenvio);
        }

        /**
         * Funcion que permite verificar si el correo existe previamente
         *
        */
        public function reenviarcorreoconfirmacion(Request $request){
            $dataenvio['error']=0;
            $dataenvio['msj']='Correo reenviado';
            $objUser = User:: where("id", "=", $request->iduser)->first();

            if(!empty($objUser)){
                if($objUser->estatus=="S"){
                    $ruta = route('codigoconfirmacion',[$objUser->codigoconfirmacion]);

                    //ENVIAR CORREO PARA CONFIRMACION DE
                        $data= array(
                            'login'=>$objUser->login,
                            'nombre'=>$objUser->nombre,
                            'link'=>$ruta,
                        );
                        Mail::to($objUser->email)->send(new Codigoconfirmacion($data));
                }else{
                    $dataenvio['error']=1;
                    $dataenvio['msj']='Usuario ya confirmo correo';
                }
            }else{
                $dataenvio['error']=1;
                $dataenvio['msj']='Usuario no encontrado';
            }
            echo json_encode($dataenvio);
        }

        /**
         * Funcion que confirmar codigo enviado por correo a traves de un link
         *
        */
        public function codigoconfirmacion($codigoconfirmacion){
            $objUser = User:: where("codigoconfirmacion", "=", $codigoconfirmacion)
                                    ->where("estatus", "=", "S")
                                    ->first();

            if(!empty($objUser)){
                $objUser->estatus="A";

                if($objUser->save()){
                    //ENVIAR CORREO PARA CULMINACION DE REGISTRO
                    $ruta = route('login');
                    $data= array(
                        'login'=>$objUser->login,
                        'nombre'=>$objUser->nombre,
                        'pwd'=>$objUser->pwd,
                        'link'=>$ruta
                    );
                    Mail::to($objUser->email)->send(new Registrate($data));
                    // if(!isset(auth()->user()->id)){
                    //     Auth::loginUsingId([$objUser->id]);
                    // }
                    return redirect()->route('usuarioconfirmado',[$objUser->id]);
                }else{
                    echo 'No se pudo guardar';
                    exit();
                }
            }else{
                return redirect()->route('welcome');
            }
        }

    /**
    * FUNCIÓN PARA REGISTRAR LOS USUARIOS DEL SISTEMA
    *
    */
    public function sconsumidor(Request $request)
    {
        $canti = User:: where("email", "=", $request->email)
                      // ->where("estatus", "=", "A")
                      ->whereIn('idtipousuario', [4, 5])
                      ->count();

        if($canti>0){
            return redirect()->route('consumidor',[$request->codigopromocion])->with('danger','Correo electrónico existente.');
        }else{
            $canti = User:: where("login", "=", $request->login)
                          // ->where("estatus", "=", "A")
                          ->whereIn('idtipousuario', [4, 5])
                          ->count();
            if($canti>0){
                return redirect()->route('consumidor',[$request->codigopromocion])->with('danger','Login existente.');
            }else{
                DB::beginTransaction();
                $objUser = new User;

                $objUser->login = $request->login;
                $objUser->email = $request->email;
                $objUser->estatus = 'S';
                $objUser->idtipousuario = '4';//CONSUMIDOR
                $objUser->idempresa = 0;
                $objUser->idsucursal = 0;

                //COLOCANDO FOTO DE PERFIL
                $numero = rand(1,12);
                $objUser->foto_perfil ='img/compartidos/avatar'.$numero.'.jpg';

                if($request->pwd!=null && $request->pwd!='' && !empty($request->pwd)){
                    $objUser->pwd = $request->pwd;
                    $objUser->password =  Hash::make($request->pwd);
                }

                if($request->codigopromocion!=null){
                    $usuario = User:: where("codigopromocion", "=", $request->codigopromocion)
                                            ->where("estatus", "=", "A")
                                            ->first();
                    if(!empty($usuario)){
                        $objUser->idusuariorecomendon = $usuario->id;
                    }
                }

                if($objUser->save()){
                    $codigoconfirmacion = $this->generacodigoconfirmacion($objUser->id);
                    $codigopromocion = 'U'.$this->generacodigo($objUser->id);

                    $objUser->codigoconfirmacion=$codigoconfirmacion;
                    $objUser->codigopromocion=$codigopromocion;

                    if($objUser->save()){
                        $ruta = route('codigoconfirmacion',[$codigoconfirmacion]);

                        //ENVIAR CORREO PARA CONFIRMACION DE
                            $data= array(
                                'login'=>$objUser->login,
                                'nombre'=>$objUser->nombre,
                                'link'=>$ruta,
                            );
                            Mail::to($objUser->email)->send(new Codigoconfirmacion($data));

                        DB::commit();
                        // Auth::loginUsingId([$objUser->id]);
                        return redirect()->route('espereconfirmacion',[$objUser->id]);
                    }
                }else{
                    DB::rollback();
                    return redirect()->route('consumidor',[$request->codigopromocion])->with('danger','No se pudo regustrar');
                }
            }
        }
    }
}
