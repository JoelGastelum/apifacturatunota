<?php
error_reporting(0);
/**
 * Permite leer el xml y guardar el comprobante
 */
use App\Cliente;
use App\Comprobante;
use App\Detallecomprobante;


function leerxmlyguardarcdfi($xml,$rfc_parametro){

    //Tomar la informacion del comprobante
    $respuesta='';
    $Comprobante        = $xml->getElementsByTagName("Comprobante")->item(0);
    // $Impuestos           = $Comprobante->getAttributeNode("Version")->value;
    $version = $Comprobante->getAttributeNode("Version")->value;
    if($version==''){
        $version            = $Comprobante->getAttributeNode('version')->value;
    }
    if ($version!="3.0"&&$version!="3.2"&&$version!="3.3"){
        $respuesta='La version del comprobante es incorrecta';
    }else{
        if($version=="3.3"){
            $serie              = $Comprobante->getAttributeNode('Serie')->value;
            $folio              = $Comprobante->getAttributeNode('Folio')->value;
            $fecha              = $Comprobante->getAttributeNode('Fecha')->value;
            $tipocomprobante    = $Comprobante->getAttributeNode('TipoDeComprobante')->value;
            $moneda             = $Comprobante->getAttributeNode('Moneda')->value;
            $tipocambio         = $Comprobante->getAttributeNode('TipoCambio')->value;
            $subtotal           = $Comprobante->getAttributeNode('SubTotal')->value;
            $descuento          = $Comprobante->getAttributeNode('Descuento')->value;
            $total              = $Comprobante->getAttributeNode('Total')->value;

            //$numero_certificado   = $Comprobante->getAttributeNode('NoCertificado')->value;
            //$certificado      = $Comprobante->getAttributeNode('Certificado')->value;
            //$sello                = $Comprobante->getAttributeNode('Sello')->value;

            $Emisor             = $Comprobante->getElementsByTagName("Emisor")->item(0);
            $rfcemisor          = $Emisor->getAttributeNode('Rfc')->value;
            $nombreemisor       = $Emisor->getAttributeNode('Nombre')->value;

            $Receptor           = $Comprobante->getElementsByTagName("Receptor")->item(0);
            $rfcreceptor        = $Receptor->getAttributeNode('Rfc')->value;
            $nombrereceptor     = $Receptor->getAttributeNode('Nombre')->value;
        }else{
            $serie              = $Comprobante->getAttributeNode('serie')->value;
            $folio              = $Comprobante->getAttributeNode('folio')->value;
            $fecha              = $Comprobante->getAttributeNode('fecha')->value;
            $tipocomprobante    = $Comprobante->getAttributeNode('tipoDeComprobante')->value;
            if($tipocomprobante=='ingreso'){
                $tipocomprobante='I';
            }elseif($tipocomprobante=='egreso'){
                $tipocomprobante='E';
            }elseif($tipocomprobante=='pago'){
                $tipocomprobante='P';
            }elseif($tipocomprobante=='traslado'){
                $tipocomprobante='T';
            }elseif($tipocomprobante=='nomina'){
                $tipocomprobante='n';
            }

            $moneda             = $Comprobante->getAttributeNode('Moneda')->value;
            $tipocambio         = $Comprobante->getAttributeNode('TipoCambio')->value;
            $subtotal           = $Comprobante->getAttributeNode('subTotal')->value;
            $descuento          = $Comprobante->getAttributeNode('descuento')->value;
            $total              = $Comprobante->getAttributeNode('total')->value;

            //$numero_certificado   = $Comprobante->getAttributeNode('noCertificado')->value;
            //$certificado      = $Comprobante->getAttributeNode('certificado')->value;
            //$sello                = $Comprobante->getAttributeNode('sello')->value;
            $Emisor             = $Comprobante->getElementsByTagName("Emisor")->item(0);
            $rfcemisor          = $Emisor->getAttributeNode('rfc')->value;
            $nombreemisor       = $Emisor->getAttributeNode('nombre')->value;

            $Receptor           = $Comprobante->getElementsByTagName("Receptor")->item(0);
            $rfcreceptor        = $Receptor->getAttributeNode('rfc')->value;
            $nombrereceptor     = $Receptor->getAttributeNode('nombre')->value;
        }

        $rfcreceptor=strtoupper($rfcreceptor);
        $rfcemisor=strtoupper($rfcemisor);

      
        $id_tercero = 0;
       
        if($rfc_parametro==$rfcemisor){


           return false;
            //LA PERSONA QUE CONSUME EL SERVICIO ES EL MISMO QUE LO EMITE
            //BUSCAR SI EL RECEPTOR SE ENCUENTRA EN EL SISTEMA SINO EXISTE CREARLE UN REGISTRO EN users Y EN CONTRIBUYENTES
           /* $tipo=1;
            $clienteoproveedor=$nombrereceptor;

            //BUSCAMOS SI EXISTE EL USUARIO RECEPTOR
                $canti=1;
                $objUsuario = new User;
                if($objUsuario->islaravel()==true){
                    $objUsuario = User::where("rfc", "=", $rfcreceptor)->first();
                    if(empty($objUsuario)){
                        $canti =0;
                    }
                }else{
                    if(!$objUsuario->load('rfc=?',[$rfcreceptor])){
                        $canti=0;
                    }
                }

                if($canti==0){
                    $objUsuario = new User;
                    $objUsuario->rfc = $rfcreceptor;
                    $objUsuario->user = $nombrereceptor;
                    $objUsuario->passvisual = $rfcreceptor;
                    $objUsuario->nombre = $nombrereceptor;
                    $objUsuario->tipousuario = 1;
                    $objUsuario->foto_perfil = 'img/avatar10.jpg';
                    $objUsuario->estatus = 'A';

                    if(!$objUsuario->Save()){
                        // echo $objUsuario->ErrorMsg();
                        // exit();
                    }
                }

            //BUSCAR SI EXISTE EL CONTRIBUYENTE RECEPTOR
                $canti=1;
                $objCliente = new Cliente;
                if($objCliente->islaravel()==true){
                    $objCliente = Cliente::where("rfc", "=", $rfcreceptor)->first();
                    if(empty($objCliente)){
                        $canti =0;
                    }
                }else{
                    if(!$objCliente->load('rfc=?',[$rfcreceptor])){
                        $canti=0;
                    }
                }

                if($canti==0){
                    $objCliente = new Cliente;
                    $objCliente->iddistribuidor = 0;
                    $objCliente->rfc = $rfcreceptor;
                    $objCliente->razonsocial = $nombrereceptor;
                    $objCliente->tipocuenta = 0;
                    $objCliente->nombrecomercial = $nombrereceptor;
                    $objCliente->estatus = 'A';
                }
                $objCliente->idusuario = $objUsuario->id;
                if(!$objCliente->Save()){
                    // echo $objCliente->ErrorMsg();
                    // exit();
                }
                $idclienterecibido=$objCliente->id;

            //BUSCAR CONTRIBUYENTE ES LA PERSONA QUE EMITE EL XML
                $objClienteConsumidor = new Cliente;
                if($objClienteConsumidor->islaravel()==true){
                    $objClienteConsumidor = Cliente::where("rfc", "=", $rfcemisor)->first();
                }else{
                    $objClienteConsumidor->load('rfc=?',[$rfcemisor]);
                }
                $idclienteemitido = $objClienteConsumidor->id;*/
        }elseif($rfc_parametro==$rfcreceptor){
         
            return true;

        }
            //LA PERSONA QUE CONSUME EL SERVICIO ES EL RECEPTOR
            //BUSCAR SI EL EMISOR SE ENCUENTRA EN EL SISTEMA SINO EXISTE CREARLE UN REGISTRO EN users Y EN CONTRIBUYENTES
            /*$tipo=2;
            $clienteoproveedor=$nombreemisor;


            //BUSCAR SI EXISTE EL CONTRIBUYENTE EMISOR
                $canti=1;
                $objCliente = new Cliente;
                $objCliente = Cliente::where("rfc", "=", $rfcemisor)
                ->where('idusuario','=',auth()->user()->id)->first();
            
                $objCliente->idusuario = $objUsuario->id;
               
                $idclienteemitido=$objCliente->id;

            //BUSCAR CONTRIBUYENTE ES LA PERSONA QUE RECIBE EL XML
                $objClienteConsumidor = new Cliente;
                if($objClienteConsumidor->islaravel()==true){
                    $objClienteConsumidor = Cliente::where("rfc", "=", $rfcreceptor)->first();
                }else{
                    $objClienteConsumidor->load('rfc=?',[$rfcreceptor]);
                }
                $idclienterecibido = $objClienteConsumidor->id;
        }else{
            $respuesta='El comprobantes no pertenece al cliente '.$rfc_parametro;
        }

        //SE GUARDARON LOS CONTRIBUYENTES QUE NO EXISTEN TANTO SI ES RECEPTOR O EMISOR
        if($respuesta=='')
        {
            if($version=="3.3"){
                //Obtener informacion del timbre
                    $complemento = $Comprobante->getElementsByTagName("Complemento")->item(0);
                    $timbre_fiscal_digital = $complemento->getElementsByTagName("TimbreFiscalDigital")->item(0);
                    $uuid = $timbre_fiscal_digital->getAttributeNode('UUID')->value;
                    $fechatimbrado = $timbre_fiscal_digital->getAttributeNode('FechaTimbrado')->value;

                //Partidas del documento conceptos
                    if($Comprobante->getElementsByTagName("Conceptos")->item(0) ){
                        $Conceptos           = $Comprobante->getElementsByTagName("Conceptos")->item(0);
                        $Concepto            = $Conceptos->getElementsByTagName("Concepto");
                        $conceptos           = array();
                        foreach($Concepto as $concepto )
                        {
                            //Agregamos la partida al documento
                            $conceptos[]=array(   'cantidad'             => $concepto->getAttributeNode('Cantidad')->value,
                                                  'descripcion'          => $concepto->getAttributeNode('Descripcion')->value,
                                                  'valor_unitario'       => $concepto->getAttributeNode('ValorUnitario')->value,
                                                  'importe'              => $concepto->getAttributeNode('Importe')->value,
                                                  'clave_prod_serv'      => $concepto->getAttributeNode('ClaveProdServ')->value
                                                );
                        }
                    }

                //Obtener Impuestos
                    $iva=0;
                    $ieps = 0;
                    $retiva = 0;
                    $retisr = 0;
                    $Impuestos = $Comprobante->getElementsByTagName("Impuestos")->item(0);
                    if(!is_null($Impuestos))
                    {
                        $Traslados = $Impuestos->getElementsByTagName("Traslados")->item(0);
                        if(!is_null($Traslados))
                        {
                            $traslados = $Traslados->getElementsByTagName('Traslado');
                            foreach ($traslados as $traslado)
                            {
                                $impuesto=$traslado->getAttributeNode('Impuesto')->value;
                                if($impuesto=='002'){
                                    $iva+=$traslado->getAttributeNode('Importe')->value;
                                }

                                if($impuesto=='003'){
                                    $ieps+=$traslado->getAttributeNode('Importe')->value;
                                }
                            }
                        }

                        $Retenciones = $Impuestos->getElementsByTagName("Retenciones")->item(0);
                        if(!is_null($Retenciones))
                        {
                            $retenciones = $Retenciones->getElementsByTagName('Retencion');
                            foreach ($retenciones as $retencion)
                            {
                                $impuesto=$retencion->getAttributeNode('Impuesto')->value;
                                if($impuesto=='002'){
                                    $retiva+=$retencion->getAttributeNode('Importe')->value;
                                }

                                if($impuesto=='001'){
                                    $retisr+=$retencion->getAttributeNode('Importe')->value;
                                }
                            }
                        }
                    }
            }else{
                //Obtener informacion del timbre
                    $complemento = $Comprobante->getElementsByTagName("Complemento")->item(0);
                    $timbre_fiscal_digital = $complemento->getElementsByTagName("TimbreFiscalDigital")->item(0);
                    $uuid = $timbre_fiscal_digital->getAttributeNode('UUID')->value;
                    $fechatimbrado = $timbre_fiscal_digital->getAttributeNode('FechaTimbrado')->value;

                //Partidas del documento conceptos
                    if($Comprobante->getElementsByTagName("Conceptos")->item(0) ){
                        $Conceptos           = $Comprobante->getElementsByTagName("Conceptos")->item(0);
                        $Concepto            = $Conceptos->getElementsByTagName("Concepto");
                        $conceptos           = array();
                        foreach($Concepto as $concepto )
                        {
                            //Agregamos la partida al documento
                            $conceptos[]=array(   'cantidad'             => $concepto->getAttributeNode('cantidad')->value,
                                                  'descripcion'          => $concepto->getAttributeNode('descripcion')->value,
                                                  'valor_unitario'       => $concepto->getAttributeNode('valor_unitario')->value,
                                                  'importe'              => $concepto->getAttributeNode('importe')->value,
                                                  'clave_prod_serv'      => $concepto->getAttributeNode('clave_prod_serv')->value
                                                );
                        }
                    }

                //Obtener Impuestos
                    $iva=0;
                    $ieps = 0;
                    $retiva = 0;
                    $retisr = 0;
                    $Impuestos = $Comprobante->getElementsByTagName("Impuestos")->item(0);
                    if(!is_null($Impuestos))
                    {
                        $Traslados = $Impuestos->getElementsByTagName("Traslados")->item(0);
                        if(!is_null($Traslados))
                        {
                            $traslados = $Traslados->getElementsByTagName('Traslado');
                            foreach ($traslados as $traslado)
                            {
                                $impuesto=$traslado->getAttributeNode('impuesto')->value;
                                if($impuesto=='IVA'){
                                    $iva+=$traslado->getAttributeNode('importe')->value;
                                }

                                if($impuesto=='IEPS'){
                                    $ieps+=$traslado->getAttributeNode('importe')->value;
                                }
                            }
                        }

                        $Retenciones = $Impuestos->getElementsByTagName("Retenciones")->item(0);
                        if(!is_null($Retenciones))
                        {
                            $retenciones = $Retenciones->getElementsByTagName('Retencion');
                            foreach ($retenciones as $retencion)
                            {
                                $impuesto=$retencion->getAttributeNode('impuesto')->value;
                                if($impuesto=='IVA'){
                                    $retiva+=$retencion->getAttributeNode('importe')->value;
                                }

                                if($impuesto=='ISR'){
                                    $retisr+=$retencion->getAttributeNode('importe')->value;
                                }
                            }
                        }
                    }
            }

            //CREANDO DIRECTOR PARA ARCHIVOS
                $archivo = 'public/empresa/'.$objClienteConsumidor->id.'-'.$objClienteConsumidor->rfc.'/'.$uuid.'.xml';
                if($descuento==''){
                	$descuento=0;
                }

            //CREAR COMPROBANTE
                $canti=1;
                $objComprobante = new Comprobante;
                if($objComprobante->islaravel()==true){
                    $objComprobante = Comprobante::where("uuid", "=", $uuid)->first();
                    if(empty($objComprobante)){
                        $canti =0;
                    }
                }else{
                    if(!$objComprobante->load('uuid=?',[$uuid])){
                        $canti=0;
                    }
                }
                if($canti==0){
                    $objComprobante = new Comprobante;
                }

                $objComprobante->idempresa=numfmt_get_locale;
                $objComprobante->tipo='I';
                $objComprobante->clienteoproveedor=$clienteoproveedor;
                $objComprobante->idclienteemitido=$idclienteemitido;
                $objComprobante->rfcemisor=$rfcemisor;
                $objComprobante->uuid=$uuid;
                $objComprobante->serie=$serie;
                $objComprobante->folio=$folio;
                $objComprobante->fecha=$fecha;
                $objComprobante->fechatimbrado=$fechatimbrado;
                $objComprobante->tipocomprobante=$tipocomprobante;
                $objComprobante->tipo=$tipo;
                $objComprobante->subtotal=$subtotal;
                $objComprobante->descuento=$descuento;
                $objComprobante->retiva=$retiva;
                $objComprobante->retisr=$retisr;
                $objComprobante->ieps=$ieps;
                $objComprobante->iva=$iva;
                $objComprobante->total=$total;
                $objComprobante->moneda=$moneda;
                $objComprobante->tipocambio=$tipocambio;
                $objComprobante->estatus='4';
                $objComprobante->saldo = $total;
                $objComprobante->archivo = $archivo;

                if(!$objComprobante->save()){
                    $msj='';
                    if($objComprobante->islaravel()==false){
                        $msj = $objComprobante->ErrorMsg();
                    }
                    $respuesta='No se pudo guardar el comprobante uuid = '.$uuid.' porque: '.$msj;
                }else{
                    //GUARDAR LOS DETALLES DEL COMPROBANTES (conceptos)
                    if(count($conceptos)>0){
                        foreach($conceptos as $concepto )
                        {
                            $objDetallecomprobante = new Detallecomprobante;
                            $objDetallecomprobante->idcomprobante=$objComprobante->id;
                            $objDetallecomprobante->cantidad=$concepto['cantidad'];
                            $objDetallecomprobante->descripcion=$concepto['descripcion'];
                            $objDetallecomprobante->valor_unitario=$concepto['valor_unitario'];
                            $objDetallecomprobante->importe=$concepto['importe'];
                            $objDetallecomprobante->clave_prod_serv=$concepto['clave_prod_serv'];

                            if(!$objDetallecomprobante->save()){
                                $msj='';
                                if($objDetallecomprobante->islaravel()==false){
                                    $msj = $objDetallecomprobante->ErrorMsg();
                                }
                                $respuesta='No se pudo guardar los detalles del comprobante uuid = '.$uuid.' porque: '.$msj;
                            }
                        }
                    }

                    if($respuesta=='')
                    {
                        $respuesta='OK';

                        //Guardar el xml en disco
                        if($objComprobante->islaravel()==false){
                            //VERIFICAR SI EXISTE CARPETA
                            $sitioguardarcomprobar='../../public/storage/empresa/'.$objClienteConsumidor->id.'-'.$objClienteConsumidor->rfc.'/';
                            $directorioexistente=1;
                            if (!file_exists($sitioguardarcomprobar)) {
                                mkdir('../../public/storage/empresa/'.$objClienteConsumidor->id.'-'.$objClienteConsumidor->rfc.'/',0777);
                            }

                            $sitioguardar='../../public/storage/empresa/'.$objClienteConsumidor->id.'-'.$objClienteConsumidor->rfc.'/';
                            $xml->Save($sitioguardar.$uuid.'.xml');
                        }else{
                            //ES LARAVEL RETORNAR un array con data y el uuid
                            $respuesta=array('respuesta'=>$respuesta,'uuid'=>$uuid);
                        }
                    }
                }
        }*/
        return $respuesta;

    }
}
?>
