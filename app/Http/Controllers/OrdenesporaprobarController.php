<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS

use nusoap_client;

use App\Configuracionweb;
use App\Afiliado;
use App\Empresa;
use App\Formapago;
use App\Orden;
use App\Ordenesdetalle;
use App\Servicio;
use App\Codigopromocion;
use App\Remision;
use App\Remisiondetalle;
use App\User;
use App\Concepto;
use App\Mail\Registrateestablecimiento;
use App\Mail\Informacionpago;

//ENVIO DE EMAILS
use Illuminate\Support\Facades\Mail;

class OrdenesporaprobarController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1
         ';
        if($request->fecha_inicial!='' && $request->fecha_inicial!=null && !empty($request->fecha_inicial)){
            if($request->fecha_final!='' && $request->fecha_final!=null && !empty($request->fecha_final)){
                $fecha_inicial = date('Y-m-d',strtotime($request->fecha_inicial));
                $fecha_final = date('Y-m-d',strtotime($request->fecha_final));

                $sql_where .= ' AND CAST(ordenes.updated_at AS date) >= "'.$fecha_inicial.'"';
                $sql_where .= ' AND CAST(ordenes.updated_at AS date) <= "'.$fecha_final.'"';
            }
        }

        if($request->estatus=='T'){
            $sql_where .= ' AND ordenes.estatus IN ("0","1")';
        }else{
            $sql_where .= ' AND ordenes.estatus = "'.$request->estatus.'"';
        }

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                ordenes.id,
                ordenes.rfc,
                ordenes.metodopago,
                ordenes.razonsocial,
                DATE_FORMAT(CAST(ordenes.updated_at AS DATE),"'.$formatdatemysql.'") as updated_at,
                FORMAT(ordenes.total,2) as total,
                ordenes.estatus
            FROM ordenes
            '.$sql_where.'
            ORDER BY
                CAST(ordenes.updated_at AS DATE) DESC
        ';
        $coleccion = DB::select($sql);


        $estatus_ordenespago=$this->variablesglobales("estatus_ordenespago");
        $metodopago=$this->variablesglobales("metodopago");

        foreach ($coleccion as $data) {
            $acciones='';
                $acciones.='<a href="'.route('ordenesporaprobar.mostrar',$data->id).'" class="btn btn-sm btn-info" title="Visualizar">
                    <i class="fas fa-eye"></i>
                </a>';
                $acciones.='&nbsp';
                $acciones.='<a href="'.route('ordenesporaprobar.aprobar',$data->id).'" class="btn btn-sm btn-success" title="Aprobar">
                    <i class="far fa-check-circle"></i>
                </a>';
            $data->acciones=$acciones;

            if(isset($estatus_ordenespago[$data->estatus])){
                $data->estatus = $estatus_ordenespago[$data->estatus];
            }
            if(isset($metodopago[$data->metodopago])){
                $data->metodopago = $metodopago[$data->metodopago];
            }
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR CobranzaController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'NotasController');

        $estatus_ordenespago=$this->variablesglobales("estatus_ordenespago");
        unset($estatus_ordenespago['-1']);
        unset($estatus_ordenespago['2']);
        $estatus_ordenespago['T']='Todos';

        return view('ordenesporaprobar.index', [
            'arraybtn' => $arraybtn,
            'estatus_ordenespago' => $estatus_ordenespago,
            'tituloencabezado'=>'Ordenes por aprobar'
        ]);
    }

    /**
     * Permite visualizar la nota
     *
     * @return \Illuminate\Http\Response
     */
    public function mostrar($id)
    {
        $objOrden = Orden::where("id", "=", $id)->first();
        if(!empty($objOrden)){
            $metodopago=$this->variablesglobales("metodopago");
            return view('ordenesporaprobar.mostrar', [
                'objOrden' => $objOrden,
                'metodopago' => $metodopago,
                'tituloencabezado'=>'Ordenes por aprobar'
            ]);
        }else{
            return redirect()->route('ordenesporaprobar.index')->with('danger','Error: Orden no encontrada');
        }
    }

    /**
     * Permite visualizar la nota
     *
     * @return \Illuminate\Http\Response
     */
    public function aprobar($id)
    {
        $objOrden = Orden::where("id", "=", $id)->first();
        if(!empty($objOrden)){
            $metodopago=$this->variablesglobales("metodopago");
            return view('ordenesporaprobar.aprobar', [
                'objOrden' => $objOrden,
                'metodopago' => $metodopago,
                'tituloencabezado'=>'Ordenes por aprobar'
            ]);
        }else{
            return redirect()->route('ordenesporaprobar.index')->with('danger','Error: Orden no encontrada');
        }
    }

    /**
     * Funcion que permite guardar una aprobación
     */
    public function saprobar(Request $request){
        $metodopago=2;
        $arraymetodopago=array();
        $error = array();
        $ObjOrden = Orden::where("id", "=", $request->id)->first();
        if(!empty($ObjOrden)){
            $arraymetodopago['tracker']=$request->tracker;
            $arraymetodopago['fechapagado']=$request->fechapagado;
            $arraymetodopago['payment_type']=null;
            if($ObjOrden->renovacion==0){
                $respuesta = $this->pagarplan($ObjOrden,$metodopago,$arraymetodopago);
            }else{
                $respuesta = $this->renovarplan($ObjOrden,$metodopago,$arraymetodopago);
            }
            if($respuesta['error']==0){
                return redirect()->route('ordenesporaprobar.index')->with('success','Orden aprobada con éxito.');
            }else{
                $error = $respuesta['error'];
            }
        }else{
            //NO SE CONSIGUIO LA ORDEN
            $error[]='No se consiguio la orden';
        }

        if(count($error)>0){
            dd($error);
        }
    }
}
