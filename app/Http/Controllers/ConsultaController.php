<?php
namespace App\Http\Controllers;
use App\Usuario;
use App\Cliente;
use App\Tokenlogin;

use DB;

use Illuminate\Http\Request;

class ConsultaController extends Controller
{
    /**
     * Permite consultar un cliente a traves de un codigo
     */
    public function consultacodigo(){
        return view('consultacodigo', [
        ]);
    }

    public function sconsultacodigo(Request $request){
        // dd($request);
        $idcliente=$this->desgeneracodigo($request->codigocliente);
        $idcliente=md5($idcliente);
        return redirect()->route('consultaqr',[$idcliente]);
    }

    /**
     * Permite consultar a traves de un tokenlogin y rfc de cliente
     */
    public function consultaqr($idcliente){
        $msj='';
        $error=0;

        //BUSCAR POR ID CLIENTE
        $cliente=DB::select("
                select *
                from clientes
                where md5(id)= ?
            ",
            array($idcliente)
        );
        if(count($cliente)==0){
            $msj='No se consigue el contribuyente';
            $error=1;
        }else{
            $cliente=$cliente[0];
            $cliente = (array)$cliente;
        }

        return view('consultaqr', [
            'msj' => $msj,
            'error' => $error,
            'cliente'=>$cliente
        ]);
    }

    /**
     * Permite leer un qr y redireccionar del qr
     */
    public function leerqr(){
        return view('leerqr', [
        ]);
    }
}
