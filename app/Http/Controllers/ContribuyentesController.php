<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cliente;
use App\Viaje;
use App\Codigopromocion;
use App\Comprobante;
use App\Recomendado;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Illuminate\Support\Facades\Mail;
use App\Mail\EnviarQr;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

class ContribuyentesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Permite buscar los viajes del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function buscarviajes(Request $request)
    {
        //request->idviajes (el viaje que esta marcado actualmente)

        $data_envio['error']=0;
        $data_envio['html']='';

        $query=Viaje::query();
            $query->where("idusuario", "=", $request->iduser);
            $query->where("idcontribuyente", "=", $request->idcontribuyente);
            $query->where("eliminado", "=", 0);
            if($request->configfiltroviajes!=-1 && $request->configfiltroviajes!=0){
                if($request->configfiltroviajes==1){
                    //ABIERTO
                    $query->where("estatus", "=", 1);
                }elseif($request->configfiltroviajes==2){
                    //CERRADOS
                    $query->where("estatus", "=", 0);
                }
            }
            $query->orderBy("viajes.id");
        $viajes = $query->get();


        //CAMBIAR CONFIGURACION DE FILTRADO DE VIAJE PARA EL CLIENTE
            if($request->configfiltroviajes!=-1){
                $objCliente = Cliente::where("id", "=", $request->idcontribuyente)->first();
                if(!empty($objCliente)){
                    $objCliente->configfiltroviajes=$request->configfiltroviajes;
                    $objCliente->save();
                }
            }

        $html='';
        $html .='<tr>';
            $html .='<td style="width:85%;cursor:pointer;" onclick="seleccionarviaje(-1,\'Todos\')">';
                if($request->idviajes==-1){
                    $html .= '<strong>';
                }
                $html .= 'Todos';
                if($request->idviajes==-1){
                    $html .= '</strong>';
                }
            $html .='</td>';
            $html .='<td style="text-align:center;width:15%">';
            $html .='</td>';
        $html .='</tr>';
        $html .='<tr>';
            $html .='<td style="width:85%;cursor:pointer;" onclick="seleccionarviaje(0,\'Sin viaje\')">';
                if($request->idviajes==0){
                    $html .= '<strong>';
                }
                $html .= 'Sin viaje';
                if($request->idviajes==0){
                    $html .= '</strong>';
                }
            $html .='</td>';
            $html .='<td style="text-align:center;width:15%">';
            $html .='</td>';
        $html .='</tr>';

        if(count($viajes)>0){
            foreach ($viajes as $viaje) {
                $html .='<tr>';
                    $html .='<td  style="width:85%;cursor:pointer;color:#2d4373;" onclick="seleccionarviaje('.$viaje->id.',\''.$viaje->descripcion.'\');">';
                        if($request->idviajes==$viaje->id){
                            $html .= '<strong>';
                        }
                        $html .= $viaje->descripcion;
                        if($viaje->estatus==0){
                            $html .= ' <i class="fas fa-lock" style="font-size: 1em;"></i>';
                        }
                        if($viaje->estatus==1){
                            $html .= ' <i class="fa fa-unlock" style="font-size: 1em;"></i>';
                        }
                        if($request->idviajes==-1){
                            $html .= '</strong>';
                        }
                    $html .='</td>';
                    $html .='<td style="text-align:center;width:15%;color:#e82727;cursor:pointer;" onclick="editarviaje('.$viaje->id.')">';
                        $html .='<span class="far fa-edit text-primary"></span>';
                    $html .='</td>';
                $html .='</tr>';
            }
        }
        $html .='<tr>';
            $html .='<td colspan="2" style="text-align:center;width:100%;cursor:pointer;" onclick="editarviaje(0);">';
                $html .='<button class="btn btn-success"><i class="far fa-calendar-plus"></i> Agregar</button>';
            $html .='</td>';
        $html .='</tr>';

        $data_envio['error']=0;
        $data_envio['html']=$html;

        echo json_encode($data_envio);
    }

    /**
     * Permite editar o crear nuevo viaje, la idea es buscar los datos para mostrar en la vista
     *
     * @return \Illuminate\Http\Response
     */
    public function editarviaje(Request $request)
    {
        $nuevo=1;
        $canti=0;
        if($request->idviaje>0){
            $nuevo=0;
            $viaje = Viaje::where("id", "=", $request->idviaje)->first();
            //BUSCAR SI POSEE COMPROBANTES A ESE VIAJE
            $canti = Comprobante::where("idviaje", "=", $viaje->id)->count();
        }else{
            $viaje = new Viaje;
            $viaje->id=-1;
            $viaje->descripcion='';
            $viaje->presupuesto=0.00;
            $viaje->activo=0;
            $viaje->estatus=1;
            $viaje->eliminado=0;
            $viaje->idcontribuyente=$request->idcontribuyente;
            $viaje->idusuario=auth()->user()->id;
        }

        $data_envio['nuevo']=$nuevo;
        $data_envio['id']=$viaje->id;
        $data_envio['descripcion']=$viaje->descripcion;
        $data_envio['presupuesto']=number_format($viaje->presupuesto,'2','.',',');
        $data_envio['estatus']=$viaje->estatus;
        $data_envio['canti']=$canti;

        echo json_encode($data_envio);
    }

    /**
     * Permite guardar o cambiar valores a un viaje del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function seditarviaje(Request $request)
    {
        $data_envio['error']=0;
        $data_envio['html']="";
        $html="";
        $esNuevo=true;
        $data_envio['viaje']=null;

        if($request->idviaje>0){
            $esNuevo=false;
            $viaje = Viaje::where("id", "=", $request->idviaje)->first();
        }else{
            $viaje = new Viaje;
            $viaje->estatus=1;
            $viaje->eliminado=0;
        }
        $viaje->descripcion=$request->descripcion;
        $viaje->presupuesto=$this->converExcelMoneyADecimal($request->presupuesto);
        $viaje->idcontribuyente=$request->idcontribuyente;
        $viaje->idusuario=auth()->user()->id;

        if(!$viaje->save()){
            $data_envio['error']=1;
        }
        $data_envio['viaje']=$viaje;

        if($esNuevo){
            $location=route('clientes.misfacturas',array($request->rfcDefault,$viaje->id));
                $html .= "<div id='tr$viaje->id' class='abiertos'> ";
                $html .='   <div class="row" style="border-bottom:solid 1px #e7eaf3;padding-top: 0.2rem;padding-right: 0.75rem;padding-bottom: 0.75rem;padding-left: 0.75rem;">';
                $html .='     <div class="col-12">     ';
                $html .='       <div class="row">';
                $html .='           <div class="col-8 col-sm-10" style="cursor:pointer;padding-top: 0.8rem;">';
                               $html .= '<i style="margin-right:10px" id="viajeCerrado'.$viaje->id.'" class="fa fa-unlock"></i> ';
                $html .='               <span>';
                $html .='                   <strong>'.$viaje->descripcion.'</strong>';
                $html .='               </span>';
                $html .='           </div>';
                $html .='           <div class="col-2 col-sm-1" style="cursor:pointer;" onclick="editarviaje('.$viaje->id.')">';
                $html .='               <div class="text-right">';
                $html .='                   <div class="item item-circle bg-body-dark" style="width: 3rem;height: 3rem;">';
                $html .='                       <span class="far fa-edit text-primary" title="Editar"></span>';
                $html .='                   </div>';
                $html .='               </div>';
                $html .='           </div>';
                $html .="           <div class='col-2 col-sm-1' style='cursor:pointer;' onclick=\"window.location='$location'\" > ";
                $html .='               <div class="text-right">';
                $html .='                   <div class="item item-circle bg-body-dark" style="width: 3rem;height: 3rem;">';
                $html .='                       <span class="fas fa-chevron-right" title="Facturas"></span>';
                $html .='                   </div>';
                $html .='               </div>';
                $html .='           </div>';
                $html .='       </div>';
                $html .='       <div class="row">';
                $html .='           <div class="col-12">';
                $html .='               <span style="color:#929ca6;font-size: 0.8rem;">';
                $html .='                   '.number_format($viaje->presupuesto,2).' $ ';
                $html .='               </span>';
                $html .='           </div>';
                $html .='       </div>';
                $html .='     </div>';
                $html .='   </div>';
                $html .=' </div>';
        }

        $data_envio['html']=$html;



        echo json_encode($data_envio);
    }

    function enviarQr(Request $request)
	{

        // if($request->idcliente){
        //     $comprobante=Cliente::where('idcliente','=',$request->idcliente)
        //     $comprobante=Comprobante::where('idcliente','=',$request->idcliente)
        //     ->OrderBy('id','Desc')->first();
        // }

        //file_get_contents();




        Mail::to($request->correo)->send(new EnviarQr($request->mensaje));


        return ["res"=>true , "msg"=>"Correo enviado"];
    }

    /**
     * Permite cerrar un viaje del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function cerrarviaje(Request $request)
    {


        $data_envio['error']=0;
        $data_envio['html']='';



        $viaje = Viaje::where("id", "=", $request->idviaje)
                      ->first();

        if(!empty($viaje)){
            $viaje->estatus=0;
            if(!$viaje->save()){
                $data_envio['error']=1;
            }
        }

        echo json_encode($data_envio);
    }

    /**
     * Permite eliminar un viaje del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function eliminarviaje(Request $request)
    {
        $data_envio['error']=0;
        $data_envio['html']='';

        $viaje = Viaje::where("id", "=", $request->idviaje)
                      ->first();

        if(!empty($viaje)){
            $viaje->eliminado=1;
            if(!$viaje->save()){
                $data_envio['error']=1;
            }
        }

        echo json_encode($data_envio);
    }

    /**
     * Permite crear qr de un cliente por el token del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function crearqr(Request $request)
    {
        $data_envio['error']=1;
        $data_envio['imagen']='';

        $user = User::where("id", "=", $request->iduser)->first();
        if(!empty($user)){
            if($request->idcliente){
                //idcliente NO ES idcliente = RFC
                //BUSCAR EL CLIENTE
                $cliente =Cliente::where('rfc','=',$request->idcliente)
                             ->where('idusuario','=',$request->iduser)
                             ->first();
                if(!empty($cliente)){
                    // dd($cliente->id,md5($cliente->id));
                    // $codigo = md5($cliente->id);
                    $ruta = route("consultaqr",array(md5($cliente->id)));
                    $code=$ruta;

                    //GUARDAR fisicamente
                    $sitioguardarcomprobar='../storage/app/public/qr_volatil';
                    if (!file_exists($sitioguardarcomprobar)) {
                        Storage::makeDirectory('public/qr_volatil');
                    }
                    $sitioguardar='../storage/app/public/qr_volatil/';

                    $filename=auth()->user()->id.'-'.$cliente->id;
                    include(app_path('Librerias'). '/phpqrcode/qrlib.php');
                    $image = imagecreatefrompng($sitioguardar.$filename.".png");
                    imagejpeg($image, $sitioguardar.$filename.".jpg", 100);
                    unlink($sitioguardar.$filename.".png");

                    $viajes=Viaje::where("idusuario",'=',auth()->user()->id)
                    ->where('idcontribuyente','=',$cliente->id)
                    ->where('estatus','=',1)
                    ->where('eliminado','=',0)
                    ->orderBy('activo','DESC')
                    ->orderBy('descripcion','ASC')
                    ->get();
                    $data_envio['error']=0;
                    $data_envio['imagen']=asset('storage/qr_volatil/'.$filename.".jpg");
                    $data_envio['viajes']=$viajes;
                    $data_envio['urlQr']="Consulta mis datos en $code";
                    // $data_envio['codigo']=$codigo;
                }
            }else{
                //GENERANDO QR PARA RECOMENDACION DE ESTABLECIMIENTO

                //BUSCAR CODIGO
                $codigo=Codigopromocion::where('idrecomendado','=',$request->idrecomendado)->first();
                if(!empty($codigo)){
                    $hexa = $codigo->codigo;

                    $ruta = route("establecimiento",[$hexa]);
                    // $ruta='https://mefactura.com/establecimiento';
                    // dd($ruta);
                    $code=$ruta;
                    // $hexa=str_pad(dechex($request->idrecomendado), 5, "0", STR_PAD_LEFT);

                    //GUARDAR fisicamente
                    $sitioguardarcomprobar='../storage/app/public/qr_volatil';
                    if (!file_exists($sitioguardarcomprobar)) {
                        Storage::makeDirectory('public/qr_volatil');
                    }
                    $sitioguardar='../storage/app/public/qr_volatil/';

                    $filename=auth()->user()->id.'-rec-'.$request->idrecomendado;
                    include(app_path('Librerias'). '/phpqrcode/qrlib.php');
                    $image = imagecreatefrompng($sitioguardar.$filename.".png");
                    imagejpeg($image, $sitioguardar.$filename.".jpg", 100);
                    unlink($sitioguardar.$filename.".png");

                    $data_envio['error']=0;
                    $data_envio['imagen']=asset('storage/qr_volatil/'.$filename.".jpg");
                    $data_envio['urlQr']="Te invito a conocer mefactura. com registrarte en el siguiente link:$code";
                    $data_envio['url']=$code;
                }
            }
        }

        echo json_encode($data_envio);
    }

    /**
     * Permite eliminar qr
     * @param  Request $request [description]
     * @return [type]           [description]
     * @author Joel Gastelum
     */
    public function eliminarqr(Request $request)
    {
        $msg="No se encontro archivo qr";


        if($request->borraRecomendados){
            $recomendados=Recomendado::where('idusuario','=',auth()->user()->id)->get();
            foreach($recomendados as $recomendado){
                $filename="../storage/app/public/qr_volatil/".auth()->user()->id.'-rec-'.$recomendado->id;
                if (file_exists($filename.'.jpg')) {
                    unlink($filename.'.jpg');
                    $msg="Se elimino archivo qr";
                }
            }

        }else{
            $clientes=Cliente::where('idusuario','=',auth()->user()->id)->get();

            foreach($clientes as $cliente){
                $filename="../storage/app/public/qr_volatil/".auth()->user()->id.'-'.$cliente->rfc;
                if (file_exists($filename.'.jpg')) {
                    unlink($filename.'.jpg');
                    $msg="Se elimino archivo qr";
                }
            }

        }

        return [
            "msg"=>$msg
        ];
    }

    /**
     *
     * Permite mostrar vista donde se muestre un pdf
    */
    public function visualizarpdf($idcomprobante,$rfc=null,$idviaje=null){
        $isandroid=0;
        $tipodispositivo=$this->dispositivo();
        if($tipodispositivo>0){
            $navegador=$this->obtenerNavegadorWeb();
            $isandroid=$navegador['isandroid'];
        }

        $rutaAnt= route('contribuyentes');
        if($idviaje!=null && $rfc!=null){
            $rutaAnt= route('clientes.misfacturas',[$rfc,$idviaje]);
        }

        return view('visualizarpdf', [
            'idcomprobante' => $idcomprobante,
            'rfc' => $rfc,
            'isandroid' => $isandroid,
            'rutaAnt' => $rutaAnt,
            'tituloencabezado' => "Mostrar comprobante",
        ]);
    }
}
