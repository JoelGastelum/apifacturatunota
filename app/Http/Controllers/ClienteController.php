<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Cliente;
use App\Viaje;
use DB;
use DateTime;
use App\Comprobante;
use ZipArchive;
use App\Bitacora;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use App\Http\Controllers\fpdf;
use App\Tokenlogin;

class ClienteController extends Controller
{
    //METODO PARA LOGIN
    public function Login(Request $request){
        /*POST REQUIRED
        {
            "login":"   ",
            "pwd";"     "
        }
        */
        Bitacora::setMsj($request->login, $request->pwd);
        $usuario =Usuario::where('login','=',$request->login)
        ->where('pwd','=',$request->pwd)
        ->whereIn('idtipousuario',array(4,5))
        ->first();
        if($usuario){
            $token=bin2hex(random_bytes(32));
            $objTokenlogin = new Tokenlogin;
            $objTokenlogin->idusuario = $usuario->id;
            $objTokenlogin->token = $token;
            $objTokenlogin->save();
            //ACTUALIZANDO TOKEN
            $usuario->tokenlogin=$token;
        }
        return $usuario;
    }

    function toString($array){
        $cadena="";

        foreach ($array as $ar){
            $cadena.=$ar.',';
        }

        return $cadena;
    }

    public function update(Request $request, $id)
    {
        if($id==-1){
            $objCliente = new Cliente;
        }else{
            $objCliente = Cliente::where("id", "=", $id)->first();
        }
        $objCliente->idusuario=auth()->user()->id;
        $objCliente->idempresa=0;
        $objCliente->nombre= $request->nombre;
        $objCliente->rfc = $request->rfc;
        $objCliente->correo = $request->correo;
        $objCliente->calle = $request->calle;
        $objCliente->localidad = $request->localidad;
        $objCliente->estatus='A';

        if($objCliente->save()){
            DB::commit();
            return redirect()->route('conceptos.index')->with('success','Se ha guardado los cambios con éxito');
        }else{
            DB::rollback();
            return redirect()->route('conceptos.index')->with('danger','No se pudo guardar los cambios');
        }
    }

    /**
     * Crear contribuyente
     * @see  contribuyentes.blade
     */
    public function create()
    {
        $objCliente = new Cliente;

        return view('clientes.edit', [
            'objCliente' => $objCliente,
            'rutaAnt' => route('contribuyentes'),
            'tituloencabezado' => "Nuevo contribuyente",
            'accionencabezado' => array(
                // array(
                //     'icono'=>'far fa-fw fa-save',
                //     'funcion'=>'guardar();',
                // )
            ),
        ]);
    }

    /**
     * Editar contribuyente
     * @see  contribuyentes.blade
     */
    public function edit($id)
    {
        $objCliente = Cliente::where("id", "=", $id)->first();
        if(!empty($objCliente)){
            return view('clientes.edit', [
                'objCliente' => $objCliente,
                'rutaAnt' => route('contribuyentes'),
                'tituloencabezado' => "Editar contribuyente",
                'accionencabezado' => array(
                    // array(
                    //     'icono'=>'far fa-fw fa-save',
                    //     'funcion'=>'guardar();',
                    // )
                ),
            ]);
        }else{
            return redirect()->route('home')->with('danger','Error: No se consiguio contribuyente');
        }
    }

    /**
     * Permite eliminar cliente
     * @param Request $request [description]
     */
    public function DeleteCliente(Request $request){
        $cliente=Cliente::findOrFail($request->idcliente);
        if($cliente->idusuario==$request->idusuario || $cliente->idusuario==auth()->user()->id)
        {
            $cliente->estatus="D";
            if($cliente->save()){
                if($request->plataforma){
                    return redirect()->route('conceptos.index');
                }
                return ['msg'=>"Usuario eliminado","res"=>true];
            }else{
                return ['msg'=>"Error al eliminar","res"=>false];
            }
        }else{
            return ['msg'=>"No tiene permisos de eliminar este usuario","res"=>false];
        }
    }

    function descargarExcel($ids){
        $excel="";
        $arr=explode(',',$ids);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment; filename=facturas.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $excel.="<table border=1> ";
        $excel.="<tr> ";
        $excel.="<th>Folio</th> ";
        $excel.="<th>Fecha</th> ";
        $excel.="<th>Emisor</th> ";
        $excel.="<th>Total</th> ";
        $excel.="</tr> ";

        foreach($arr as $ar){
            if($ar!=''){
            $comprobante=Comprobante::where('id','=',$ar)
            ->OrderBy('id','Desc')->first();

            $query  = DB::select("Select rfc from empresas where id = $comprobante->idempresa");
            $emp=$comprobante->idempresa;
            $rfcemisor  =  $query[0]->rfc;
            $excel.= "<tr> ";
            $excel.= "<td>Sin Folio</td> ";
            $excel.= "<td>".$comprobante->fechatimbre."</td> ";
            $excel.= "<td>".$rfcemisor."</td> ";
            $excel.= "<td>".number_format($comprobante->total,2)."</td> ";
            $excel.= "</tr> ";
            }
        }
        $excel.= "</table>";



        return $excel;
    }

    function verexcel($fechas,$emisores,$totales,$ids){

        $excel='<a href="https://test.facturatunota.com/cliente/verexcel/'.$this->toString($ids).'" ><Button
        style="
        margin:10px 0 10px 0;color:#ffffff;font-weight:bold;display:inline-block;padding:6px 12px;font-size:16px;text-align:center;cursor:pointer;background-image:none;border:1px solid transparent;border-radius:4px;outline: 0;
        background-color:#04B431;">Descargar Excel
        </Button></a>';

       /* header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=archivo.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $excel.= "<table border=1> ";
        $excel.= "<tr> ";
        $excel.= "<th>Folio</th> ";
        $excel.= "<th>Fecha</th> ";
        $excel.= "<th>Emisor</th> ";
        $excel.= "<th>Total</th> ";
        $excel.= "</tr> ";
        for($i=0; $i<count($fechas);$i++){
            $excel.= "<tr> ";
            $excel.= "<td>Sin Folio</td> ";
            $excel.= "<td>".$fechas[$i]."</td> ";
            $excel.= "<td>".$emisores[$i]."</td> ";
            $excel.= "<td>".$totales[$i]."</td> ";
            $excel.= "</tr> ";
        }

        $excel.= "</table>";*/
        return $excel;
    }

    function sendwhats(){
        return view('usuarios.sendwhats');
    }

    public function saveExterno(Request $request){

        require_once 'leerxmlyguardarcdfi.php';
        $request->fecha= str_replace("/","-",$request->fecha);
        DB::beginTransaction();


        $fecha_actual = new DateTime($request->fecha);
        $cadena_fecha_actual = $fecha_actual->format("Y-m-d");
        $fecha=$cadena_fecha_actual;

        $contribuyente=Cliente::where('rfc','=',$request->idcliente)
        ->where('idusuario','=',$request->idusuario)->first();

        $objComprobante = new Comprobante;

        $objComprobante->tipo="I";
        $objComprobante->idcliente=$contribuyente->id;
        $objComprobante->fecha=date('Y-m-d H:i:s');
        $objComprobante->total=$this->converExcelMoneyADecimal($request->total);
        $objComprobante->numcuenta="";
        $objComprobante->metodopago="PUE";
        $objComprobante->formapago="01";
        $objComprobante->tipodocumento='CF';
        $objComprobante->condicionespago="";
        $objComprobante->version=3;
        $objComprobante->abonos=0;
        $objComprobante->estatus='T';
        $objComprobante->fecha = $fecha;
        $objComprobante->fechatimbre = $fecha;
        if($request->plataforma){
            if($request->idviaje)
            $objComprobante->idviaje=$request->idviaje;

        }



        if($objComprobante->save()){
            if($request->foto){

                $dir ="comprobantes/emisores/$request->idusuario/";
                if(!is_dir($dir)){
                    mkdir($dir);
                }
                $filename = "cfdi_{$request->idusuario}_{$objComprobante->id}";
                $filename2 = "cfdi_{$request->idusuario}_{$objComprobante->id}";
                if($request->plataforma){
                    if($request->hasFile('foto')){
                       $file=$request->file('foto');
                       $filename2.='.'.$file->getClientOriginalExtension();
                       $strzipresponse = file_get_contents($request->file('foto'));
                       file_put_contents($dir.$filename2, $strzipresponse);
                       if($file->getClientOriginalExtension()!='xml'){
                        $pdf = new FPDF();
                        $pdf->AddPage();
                        $pdf->Image($dir.$filename2,20,40,170,170);
                        $pdf->Output($dir.$filename.'.pdf', 'F');
                       }else{
                        $xml = new \DOMdocument("1.0","UTF-8");
                        if (!$xml->loadXML($strzipresponse)){
                            DB::rollBack();
                            return redirect()->route('clientes.misfacturas',$request->idcliente)->with("danger","No se pudo cargar xml");
                        }else{
                        if(!leerxmlyguardarcdfi($xml,$request->idcliente)){
                            DB::rollBack();
                            return redirect()->route('clientes.misfacturas',$request->idcliente)->with("danger","Este XML no pertenece al usuario");
                        }
                        }

                       }

                    }
                }else{
                    $strzipresponse = base64_decode($request->foto);
                    file_put_contents($dir.$filename.'.jpg', $strzipresponse);

                    $pdf = new FPDF();
                    $pdf->AddPage();
                    $pdf->Image($dir.$filename.'.jpg',20,40,170,170);
                    $pdf->Output($dir.$filename.'.pdf', 'F');

                }
            }

            if($request->plataforma){
                 DB::commit();
                return redirect()->route('clientes.misfacturas',array($request->idcliente,$request->idviaje))->with("success","Se guardo su factura");
            }
            return ['res'=>true ,'msg'=>'Se guardo con exito'];
        }

        return ['res'=>false ,'msg'=>$objComprobante->errorMsg()];
    }

    /**
     * Permite mostrar las facturas de ese contribuyente
     * @param  [type] $rfc [description]
     * @return [type]      [description]
     * @see misfacturas.blade
     */
    public function verfacturas($rfc,$idviaje=""){
        $gastado='0.00';
        $faltante='0.00';
        $sql_where='';
        $nombreviaje="Todos";
        $cerrado=0;
        $ids=[];
        if($idviaje!=""){
            if($idviaje!=-1 && $idviaje!=0){
                $viaje=Viaje::where('id','=',$idviaje)->first();
                $nombreviaje=$viaje->descripcion;
                $sql_where="and comprobantes.idviaje=".$idviaje;

                //VERIFICAR SI VIAJE ESTA ABIERTO
                $cerrado=Viaje::where('id','=',$idviaje)->where('estatus','=',0)->count();
            }else if($idviaje==-1){
                $cerrado=1;
                $nombreviaje="Todos";
            }else{
                $nombreviaje="Sin Viaje";
                $sql_where="and comprobantes.idviaje=".$idviaje;
            }
        }

        $cliente=Cliente::where("idusuario",'=',auth()->user()->id)
                        ->where('rfc','=',$rfc)
                        ->first();

        $query="
            select
                (
                    case
                        when comprobantes.idempresa>0
                        then empresas.nombrecomercial
                        ELSE 'Externo'
                    end
                ) as 'nombrecomercial',
                comprobantes.*,
                clientes.rfc,
                clientes.nombre
            from comprobantes
            inner join clientes on clientes.id = comprobantes.idcliente
            left join empresas on empresas.id=comprobantes.idempresa
            where
                clientes.rfc='$rfc'
                $sql_where
                and clientes.idusuario=".auth()->user()->id."
                and comprobantes.estatus='T'
            order by comprobantes.fecha DESC
        ";
        $misfacturas=DB::select($query);
        foreach($misfacturas as $mifactura){
            array_push($ids,$mifactura->id);
            $mifactura->fecha = date('d/m/Y',strtotime($mifactura->fecha));
            $gastado+=$mifactura->total;
        }

        $ids=json_encode($ids);

        //Bitacora::setMsj($query, $query);
        return view("clientes.misfacturas",[
            'ids'=>$ids,
            'rutaAnt' => route('clientes.Viajes',array($rfc)),
            'rfcDefault'=>$rfc,
            'misfacturas'=>$misfacturas,
            "gastado"=>number_format($gastado,2),
            "tituloencabezado"=> $rfc,
            "idcontribuyente"=> $cliente->id,
            "configfiltroviajes"=> $cliente->configfiltroviajes,
            "nombreViaje"=>$nombreviaje,
            "idviaje"=>$idviaje,
            "cerrado"=>$cerrado,
        ]);
    }
        /**
         * Buscas las facturas segun filtrado en la sección de ver facturas de ese cliente
         * @see misfacturas.blade
         */
        public function buscarfacturas(Request $request){
            $data_envio['html']='';
            $data_envio['ids']='';
            $ids=[];
            $sql_where="";

            if($request->rfc!="" && $request->rfc!="null"){
                $sql_where .=" and clientes.rfc='$request->rfc'";
            }
            if($request->idviaje!="-1" && $request->idviaje!=null){
                $sql_where .="and comprobantes.idviaje=".$request->idviaje;
            }

            $request->fecha= str_replace("/","-",$request->fecha);
            $request->nuevafecha=str_replace("/","-",$request->nuevafecha);

            $fecha_actual = new DateTime($request->fecha);
            $cadena_fecha_actual = $fecha_actual->format("Y-m-d");

            $nuevafecha_actual = new DateTime($request->nuevafecha);
            $cadena_nuevafecha_actual = $nuevafecha_actual->format("Y-m-d");

            $fecha=$cadena_fecha_actual;
            $nuevafecha=$cadena_nuevafecha_actual;

            $query="
                select
                    (
                        case
                            when comprobantes.idempresa > 0 then empresas.nombrecomercial
                            ELSE 'Externo'
                        end
                    ) as 'nombrecomercial',
                    comprobantes.*,
                    clientes.rfc,
                    clientes.nombre
                from comprobantes
                inner join clientes on clientes.id = comprobantes.idcliente
                left join empresas on empresas.id=comprobantes.idempresa
                where
                    comprobantes.fechatimbre between '$nuevafecha' and '$fecha'
                    and clientes.idusuario=$request->idcliente
                    and comprobantes.estatus='T'
                    ".$sql_where."
                order by
                    comprobantes.fecha DESC
            ";
            $misfacturas=DB::select($query);

            $html = '';
            foreach($misfacturas as $mifactura){
                array_push($ids,$mifactura->id);

                $html .='<div class="row" onclick="verFactura('.$mifactura->id.')" style="cursor:pointer;border-bottom:solid 1px #e7eaf3;padding: 0.75rem;">';
                    $html .='<div class="col-12">';
                        $html .='<div class="row">';
                            $html .='<div class="col-6">';
                                $html .='<div>';
                                    $html .='<strong>'.date('d-m-Y',strtotime($mifactura->fecha)).'</strong>';
                                $html .='</div>';
                            $html .='</div>';
                            $html .='<div class="col-6">';
                                $html .='<div class="text-right">';
                                    $html .='<strong>$'.number_format($mifactura->total,2,'.',',').'</strong>';
                                $html .='</div>';
                            $html .='</div>';
                        $html .='</div>';
                        $html .='<div class="row">';
                            $html .='<div class="col-12">';
                                $html .=$mifactura->nombrecomercial;
                            $html .='</div>';
                        $html .='</div>';
                    $html .='</div>';
                $html .='</div>';
            }
            $data_envio['html']=$html;
            $data_envio['ids']=json_encode($ids);


            echo json_encode($data_envio);
        }

        /**
         * Permite generar factura del rfc
         * @see misfacturas.blade
         */
        public function facturaexterna($rfc,$idviaje=""){

            $cliente=Cliente::where("rfc",'=',$rfc)
            ->where('idusuario','=',auth()->user()->id)->first();



            $viajes=Viaje::where('idcontribuyente','=',$cliente->id)
            ->where('estatus','=',1)
            ->orderBy('activo','desc')
            ->pluck('descripcion','id');


            return view('clientes.facturaexterna', [
                'rfcDefault' => $rfc,
                "tituloencabezado"=> 'Nuevo comprobante',
                'rutaAnt' => route('clientes.misfacturas',[$rfc,$idviaje]),
                'idviaje'=>$idviaje,
                'viajes'=>$viajes,
                'idcontribuyente'=>$cliente->id
            ]);
        }

    public function misfacturas(Request $request){
        $sqlrfc="";
        $sqlidviaje="";
        $Viaje = new Viaje;
        $Viaje->presupuesto="0";
        $gastado='0.00';
        $faltante='0.00';
        $CorreoUS="";
        if($request->rfc!="Contribuyentes"){
            $sqlrfc="and clientes.rfc='$request->rfc'";
            $CorreoUS=Cliente::
            where('rfc','=',$request->rfc)
            ->where('idusuario','=',$request->idcliente)->first();
            $CorreoUS=$CorreoUS->correo;
        }
        if($request->idviaje!=""  && $request->idviaje!=null && $request->idviaje!='undefined'){
            $Viaje=Viaje::where('descripcion','=',$request->idviaje)
            ->where('idusuario','=',$request->idcliente)->first();
            $sqlidviaje="and comprobantes.idviaje=$Viaje->id";
        }


            $request->fecha= str_replace("/","-",$request->fecha);
            $request->nuevafecha=str_replace("/","-",$request->nuevafecha);

            $fecha_actual = new DateTime($request->fecha);
            $cadena_fecha_actual = $fecha_actual->format("Y-m-d");

            $nuevafecha_actual = new DateTime($request->nuevafecha);
            $cadena_nuevafecha_actual = $nuevafecha_actual->format("Y-m-d");

            $fecha=$cadena_fecha_actual;
            $nuevafecha=$cadena_nuevafecha_actual;

            $query="select  (case
            when comprobantes.idempresa>0
             then empresas.nombrecomercial
             ELSE 'Externo'
            end) as 'nombrecomercial',comprobantes.*,clientes.rfc,clientes.nombre
            from comprobantes inner join clientes on clientes.id = comprobantes.idcliente
            left join empresas on empresas.id=comprobantes.idempresa
            where comprobantes.fechatimbre between '$nuevafecha' and '$fecha'
            and clientes.idusuario=$request->idcliente
            and comprobantes.estatus='T'
            $sqlrfc
            $sqlidviaje
            order by comprobantes.updated_at DESC
            ";



         //Bitacora::setMsj($query, $query);
        $misfacturas=DB::select($query);
        foreach($misfacturas as $mifactura){
            $gastado+=$mifactura->total;
            $mifactura->total=number_format($mifactura->total,2);
            $nuevafecha_actual = new DateTime($mifactura->fechatimbre);
            $cadena_nuevafecha_actual = $nuevafecha_actual->format("d-m-Y");
            $mifactura->fechatimbre=$cadena_nuevafecha_actual ;
        }
        $faltante=$Viaje->presupuesto-$gastado;


        return [
            "misfacturas"=>$misfacturas,
            "faltante"=> number_format($faltante,2),
            "gastado"=>number_format($gastado,2),
            "presupuesto"=>number_format($Viaje->presupuesto,2),
            "correo"=>$CorreoUS
        ];
    }

    public function LoginWithToken(Request $request){
        /*POST REQUIRED
        {
            "tokenLogin":"   ",
        }
        */

        $token=Tokenlogin::where('token','=',$request->tokenLogin)
        ->first();

        $usuario=Usuario::where('id','=',$token->idusuario)->first();

        return $usuario;
    }

    //METODO PARA VER CONTRIBUYENTES DE UN USUARIO
    public function clientes(Request $request){

        $user=Usuario::findOrFail($request->idusuario);

        $viajes=Viaje::where("idusuario",'=',$request->idusuario)->get();

            /*POST REQUIRED
            {
                "idusuario":"   "
            }
            */

            $clientes=Cliente::
            where('idusuario','=',$request->idusuario)
            ->where('estatus','=','A')->get();
            foreach($clientes as $cliente){
                $hexa=dechex($cliente->id);
                $transform= str_pad($hexa, 5, "0", STR_PAD_LEFT);

                $cliente->hexa=$transform;
            }


            return ['clientes'=>$clientes,'viajes'=>$viajes];

        return ['msg' => 'Atras, intruso','res'=>false];
    }

    public function saveCliente(Request $request){

        $user=Usuario::findOrFail($request->idusuario);

            /*POST REQUIRED
            {
                "rfc":" ",
                "nombre":" ",
                "correo":"",
                "calle": " ",
                "idusuario": " ",
                "localidad": " ",
            }
            */
            $clientes=Cliente::where('rfc','=',$request->rfc)
            ->where('idusuario','=',$request->idusuario)->where('estatus','=','A')->get();

            if(!count($clientes)>0)
            {
                $cliente=new Cliente ;
                $cliente->rfc=$request->rfc;
                $cliente->idempresa=$user->idempresa;
                $cliente->nombre=$request->nombre;
                $cliente->correo=$request->correo;
                $cliente->calle=$request->calle;
                $cliente->localidad=$request->localidad;
                $cliente->idusuario=$request->idusuario;
                $cliente->estatus= 'A' ;

                if(!$cliente->save()){
                    return  ['msg'=>'Error al guardar',"res"=> false];
                }
                return  ['msg'=>'Guardado',"res"=> true];
            }

            return ['msg'=>'Registros duplicados',"res"=> false];
        return ['msg' => 'Atras, intruso','res'=>false];
    }

    public function EditCliente(Request $request){

        $user=Usuario::findOrFail($request->idusuario);

            /*POST REQUIRED
            {
                "rfc":" ",
                "nombre":" ",
                "correo":"",
                "calle": " ",
                "idusuario": " ",
                "localidad": " ",
                "idcliente",
            }
            */
            $cliente=Cliente::findOrFail($request->idcliente);



            if($cliente->idusuario==$request->idusuario)
            {
                    $cliente->rfc=$request->rfc;
                    $cliente->idempresa=$user->idempresa;
                    $cliente->nombre=$request->nombre;
                    $cliente->correo=$request->correo;
                    $cliente->calle=$request->calle;
                    $cliente->localidad=$request->localidad;
                    $cliente->idusuario=$request->idusuario;
                    $cliente->estatus= 'A' ;

                    if(!$cliente->save()){
                        return  ['msg'=>'Error al guardar',"res"=> false];
                    }
                    return  ['msg'=>'Guardado',"res"=> true];
            }else{
                return ['msg'=>'No puede editar este usuario',"res"=> false];
            }

            return ['msg'=>'No puede editar este usuario',"res"=> false];
    }



    public function newViaje(Request $request){
        if($request->descripcion){
            $viaje= new Viaje;
            $viaje->descripcion=$request->descripcion;
            $viaje->idusuario=$request->idusuario;
            $viaje->presupuesto=$request->presupuesto;
            $cliente=Cliente::where('idusuario','=',$request->idusuario)
            ->where('rfc','=',$request->rfcContribuyente)->first();

            $viaje->idcontribuyente=$cliente->id;

            if(!$viaje->save()){
                if($request->plataforma){
                    return redirect()->route('conceptos.misfacturas')->with("danger","Error al guardar");
                }
                return ["msg"=>"Hubo un error al registrar su viaje","res"=>false];
            }
        if($request->plataforma){
            return redirect()->route('clientes.misfacturas',$request->rfcContribuyente)->with("success","Se guardo su viaje");
        }

            return ["msg"=>"Se guardo su viaje","res"=>true];
        }
        return ["msg"=>"Teclee una descripción valida","res"=>false];

    }

    public function mis(Request $request){

      $sqlEstatus="";
      if($request->estatus!="Todos"){
        if($request->estatus=="Cerrado"){
            $sqlEstatus="AND v.estatus=0";
        }
        if($request->estatus=="Abierto"){
            $sqlEstatus="AND v.estatus=1";
        }
      }
        $query="SELECT v.descripcion,v.id,v.presupuesto,v.activo,
        (case
                    when v.estatus=0
                     then 'Cerrado'
                     ELSE 'Abierto'
                    end) as 'estatus',
                    round(SUM(c.total),2) as comprobado,
                    round((v.presupuesto- SUM(c.total)),2) as faltante
        FROM `viajes` v
        LEFT join comprobantes c on c.idviaje=v.id WHERE idusuario=$request->idusuario
        $sqlEstatus
        GROUP by v.id
        ";

        $viajes=DB::select($query);


        return ["viajes"=>$viajes,"res"=>true];

    }

    /**
     * Permite mostrar información para la vista de mis viajes
     * @see misviajes.blade.php
     */
    public function Viajes($rfc,$idviaje=""){
        $contribuyente=Cliente::where("rfc",'=',$rfc)
                              ->where("idusuario",'=',auth()->user()->id)
                              ->first();

        $query="
            SELECT
                v.descripcion,
                v.id,
                v.presupuesto,
                v.activo,
                (case
                    when v.estatus=0 then 'Cerrado'
                    ELSE 'Abierto'
                end) as 'estatus',
                round(SUM(c.total),2) as comprobado,
                round((v.presupuesto- SUM(c.total)),2) as faltante,
                ifnull(count(c.id),0) AS canticomprobante
            FROM `viajes` v
            LEFT join comprobantes c on c.idviaje=v.id
            WHERE
                v.idusuario=".auth()->user()->id."
                and v.idcontribuyente=$contribuyente->id
                and v.eliminado=0
                and c.estatus='T'
            GROUP by
                v.descripcion,
                v.id,
                v.presupuesto,
                v.activo,
                v.estatus
            order by
                v.activo DESC
        ";
        $viajes=DB::select($query);
        // dd($contribuyente->id);


        //CONSTRUYENDO html


        //BUSCANDO EL TOTAL DE COMPROBANTES DE TODOS LOS VIAJES
        $query="
            SELECT
                ifnull(count(c.id),0) AS canticomprobante
            FROM `viajes` v
            LEFT join comprobantes c on c.idviaje=v.id
            WHERE
                /*v.idusuario=".auth()->user()->id."
                and*/ v.idcontribuyente=$contribuyente->id
                and v.eliminado=0
                and c.estatus='T'
        ";
        $canticomprobante_sql=DB::select($query);
        $canticomprobante = 0;
        if(isset($canticomprobante_sql[0]->canticomprobante)){
            if($canticomprobante_sql[0]->canticomprobante!=null){
                $canticomprobante = $canticomprobante_sql[0]->canticomprobante;
            }
        }

        //BUSCANDO EL TOTAL DE COMPROBANTES DE SIN VIAJE LOS VIAJES
        $query="
            select
                ifnull(count(comprobantes.id),0) AS canticomprobante
            from comprobantes
            where
                comprobantes.idcliente=".$contribuyente->id."
                and comprobantes.estatus='T'
                and comprobantes.idviaje=0
        ";
        // dd($query);
        $canticomprobante_sql=DB::select($query);
        $canticomprobante_sinviaje = 0;
        if(isset($canticomprobante_sql[0]->canticomprobante)){
            if($canticomprobante_sql[0]->canticomprobante!=null){
                $canticomprobante_sinviaje = $canticomprobante_sql[0]->canticomprobante;
            }
        }

        $canticomprobante += $canticomprobante_sinviaje;



        $html="";
        $location=route('clientes.misfacturas',array($rfc,-1));
        $html .='   <div class="row" onclick="window.location=\''.$location.'\'" style="cursor:pointer;border-bottom:solid 1px #e7eaf3;padding-top: 0.2rem;padding-right: 0.75rem;padding-bottom: 0.75rem;padding-left: 0.75rem;">';
        $html .='     <div class="col-12">     ';
        $html .='       <div class="row">';
        $html .='           <div class="col-12" style="padding-top: 0.8rem;">';
        $html .='               <span>';
        $html .='                   <strong>Todos</strong>';
        $html .='               </span>';
        $html .='               <span class="badge badge-success badge-pill">'.$canticomprobante.'</span>';
        $html .='           </div>';
        $html .='       </div>';
        $html .='       <div class="row">';
        $html .='           <div class="col-12">';
        $html .='               <span style="color:#929ca6;font-size: 0.8rem;">';
        $html .='                    ';
        $html .='               </span>';
        $html .='           </div>';
        $html .='       </div>';
        $html .='     </div>';
        $html .='   </div>';




        $location=route('clientes.misfacturas',array($rfc,0));
        $html .='   <div class="row" onclick="window.location=\''.$location.'\'" style="cursor:pointer;border-bottom:solid 1px #e7eaf3;padding-top: 0.2rem;padding-right: 0.75rem;padding-bottom: 0.75rem;padding-left: 0.75rem;">';
        $html .='     <div class="col-12">     ';
        $html .='       <div class="row">';
        $html .='           <div class="col-12" style="padding-top: 0.8rem;">';
        $html .='               <span>';
        $html .='                   <strong>Sin Viaje</strong>';
        $html .='               </span>';
        $html .='               <span class="badge badge-success badge-pill">'.$canticomprobante_sinviaje.'</span>';
        $html .='           </div>';
        $html .='       </div>';
        $html .='       <div class="row">';
        $html .='           <div class="col-12">';
        $html .='               <span style="color:#929ca6;font-size: 0.8rem;">';
        $html .='                    ';
        $html .='               </span>';
        $html .='           </div>';
        $html .='       </div>';
        $html .='     </div>';
        $html .='   </div>';



        //route('clientes.misfacturas',array($rfc,$viaje->id))
        if(count($viajes)>0){
            foreach ($viajes as $viaje) {
                $location=route('clientes.misfacturas',array($rfc,$viaje->id));
                if($viaje->estatus=='Abierto'){
                    $html .= "<div id='tr$viaje->id' class='abiertos'> ";
                }
                if($viaje->estatus=='Cerrado'){
                    $html .= "<div id='tr$viaje->id'  class='cerrados'>";
                }
                $html .='   <div class="row row_viajes" data-location="'.$location.'" data-idviaje="'.$viaje->id.'" style="cursor:pointer;border-bottom:solid 1px #e7eaf3;padding-top: 0.2rem;padding-right: 0.75rem;padding-bottom: 0.75rem;padding-left: 0.75rem;">';
                $html .='     <div class="col-12">     ';
                $html .='       <div class="row">';
                $html .='           <div class="col-12" style="padding-top: 0.8rem;">';
                if($viaje->estatus=='Abierto'){
                    $html .= '<i style="margin-right:10px" id="viajeCerrado'.$viaje->id.'" class="fa fa-unlock"></i> ';
                }
                if($viaje->estatus=='Cerrado'){
                    $html .= ' <span style="margin-right:10px" id="viajeCerrado'.$viaje->id.'" class="fa fa-lock"></span>';
                }

                $html .='               <span>';
                $html .='                   <strong>$'.number_format($viaje->presupuesto,2).'</strong>';
                $html .='               </span>';
                $html .='               <span class="badge badge-success badge-pill">'.$viaje->canticomprobante.'</span>';
                $html .='           </div>';
                $html .='       </div>';
                $html .='       <div class="row">';
                $html .='           <div class="col-12">';
                $html .='               <span style="color:#929ca6;font-size: 1rem;">';
                $html .='                   '.$viaje->descripcion;
                $html .='               </span>';
                $html .='           </div>';
                $html .='       </div>';
                $html .='     </div>';
                $html .='   </div>';
                $html .=' </div>';
            }
        }

           /* $html .=' <div class="row" style="border-bottom:solid 1px #e7eaf3;padding-top: 0.2rem;padding-right: 0.75rem;padding-bottom: 0.75rem;padding-left: 0.75rem;">';
            $html .='   <div class="col-12">     ';
            $html .='     <div class="row">';
            $html .='         <div class="col-8 col-sm-10" style="cursor:pointer;padding-top: 0.8rem;">';
            $html .='             <span>';
            $html .='                 <strong>{{$cliente->rfc}}</strong>';
            $html .='             </span>';
            $html .='         </div>';
            $html .='         <div class="col-2 col-sm-1" style="cursor:pointer;" onclick="editarviaje('.$viaje->id.')">';
            $html .='             <div class="text-right">';
            $html .='                 <div class="item item-circle bg-body-dark" style="width: 3rem;height: 3rem;">';
            $html .='                     <span class="far fa-edit text-primary" title="Editar"></span>';
            $html .='                 </div>';
            $html .='             </div>';
            $html .='         </div>';
            $html .="         <div class='col-2 col-sm-1' style='cursor:pointer;' onclick=\"window.location='$location'\" > ";
            $html .='             <div class="text-right">';
            $html .='                 <div class="item item-circle bg-body-dark" style="width: 3rem;height: 3rem;">';
            $html .='                     <span class="fas fa-chevron-right" title="Facturas"></span>';
            $html .='                 </div>';
            $html .='             </div>';
            $html .='         </div>';
            $html .='     </div>';
            $html .='     <div class="row">';
            $html .='         <div class="col-12">';
            $html .='             <span style="color:#929ca6;font-size: 0.8rem;">';
            $html .='                 {{$cliente->nombre}}';
            $html .='             </span>';
            $html .='         </div>';
            $html .='     </div>';
            $html .='   </div>';
            $html .=' </div>'; */

        return view("clientes.viajes",['listaviajes'=>$html,
            'rfcDefault'=>$rfc,
            'idcontribuyente'=>$contribuyente->id,
            'tituloencabezado'=>'Mis Viajes',
            'rutaAnt'=>route('home')
        ]);
      }

    public function ActivarViaje(Request $request){

        $viajeActivo=Viaje::where('idusuario','=',$request->idusuario)
        ->where('activo','=',1)->first();

        if($viajeActivo){
            $viajeActivo=Viaje::findOrFail($viajeActivo->id);
            $viajeActivo->activo=0;

            $viajeActivo->save();

        }


        $ActivarViaje=Viaje::where('idusuario','=',$request->idusuario)
        ->where('id','=',$request->idviaje)->first();

        $ActivarViaje=Viaje::findOrFail($ActivarViaje->id);
        $ActivarViaje->activo=1;

        $ActivarViaje->save();

        return ["msg"=>"Se realizo su activacion","res"=>true];

    }

    public function cambiarEstatus(Request $request){


        $ActivarViaje=Viaje::where('idusuario','=',$request->idusuario)
        ->where('id','=',$request->idviaje)->first();


        $ActivarViaje=Viaje::findOrFail($ActivarViaje->id);

        if($ActivarViaje->estatus==0){
            $ActivarViaje->estatus=1;
        }else{
            $ActivarViaje->estatus=0;
        }


        $ActivarViaje->save();

        return ["msg"=>"Se realizo su activacion","res"=>true];

    }

    public function buscarTicket(Request $request){

        /*POST REQUIRED
        {
            "rfc":"",
            "idempresa":"",
            "notaicket:"",
            "total":""
        }
        */
        $nota =DB::select("Select ifnull(idcomprobante,0) as idcomprobante, estatus
        from remisiones
        where idempresa=$request->idempresa
        and noticket=$request->nota
        and total   =$request->total"); // and idfactura  =  0";

        if(!count($nota)>0){
            return ['msg'=>"No existe","res"=>false];
        }else{
            $rfc = "";
            if($nota[0]->estatus=='F'){
                $query =DB::select("SELECT cli.rfc, idcliente FROM comprobantes c
                inner join clientes cli on cli.id = c.idcliente
                where c.id = $nota->idcomprobante");

                if(!empty($query)){
                    if($query->rfc==$request->rfc){
                        return ["msg"=>"Nota facturada","res"=>false];
                    }
                }
            }
            if($nota[0]->estatus=='C'){
                return ["msg"=>"Nota cancelada","res"=>false];
            }
        }
        return ["msg"=>"Nota ","res"=>true,"data"=>$nota[0]->idcomprobante];

        $query =DB::select("Select id,estatus,total from remisiones where noticket = $request->nota and idempresa  =  $request->idempresa");
    }
}
