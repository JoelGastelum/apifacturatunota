<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Pago;

class PagosController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';
        $sql_where .= ' AND pagos.fecha >= "'.$request->fecha_inicial.'" AND pagos.fecha <= "'.$request->fecha_final.'"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                usuarios.nombre,
                DATE_FORMAT(pagos.fecha,"'.$formatdatemysql.'") as fecha,
                FORMAT(pagos.importe,2) AS importe,
                ifnull(pagos.cuenta,"") AS cuenta,
                ifnull(pagos.referencia,"") AS referencia
            FROM pagos
            LEFT JOIN usuarios ON usuarios.id=pagos.iduser
            '.$sql_where.'
            ORDER BY
                pagos.fecha ASC,
                usuarios.nombre ASC
        ';
        $coleccion = DB::select($sql);

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pagos.index');
    }
}
