<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Afiliado;
use App\Certificado;
use SoapClient;

use nusoap_client;

class CertificadosController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';

        $sql_where .= ' AND certificados.idempresa = "'.auth()->user()->idempresa.'" AND certificados.estatus = "A"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                certificados.id,
                certificados.idempresa,
                certificados.idintegra,
                certificados.serie,
                DATE_FORMAT(certificados.fechainicio,"%d-%m-%Y") AS fechainicio,
                DATE_FORMAT(certificados.fechafin,"%d-%m-%Y") AS fechafin
            FROM certificados
            '.$sql_where.'
            ORDER BY
                certificados.idintegra ASC,
                certificados.serie ASC
        ';
        $coleccion = DB::select($sql);

        foreach ($coleccion as $data) {
            $acciones='';
                // $acciones.='<form method="POST" action="'.route('certificados.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                // $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                // $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                //     <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                //     <i class="far fa-trash-alt"></i>
                // </button>';
                // $acciones.='</form>';
            $data->acciones=$acciones;
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR SeriesController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'SeriesController');

        return view('certificados.index', [
            'arraybtn' => $arraybtn,
            'tituloencabezado'=>'Certificados'
        ]);
    }

    /**
     * Permite crear una serie
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objCertificado = new Certificado;

        return view('certificados.edit', [
            'objCertificado' => $objCertificado,
            'tituloencabezado'=>'Certificados'
        ]);
    }

    /**
     * Permite editar una
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objCertificado = Certificado::where("id", "=", $id)->where("estatus", "=", 'A')->first();
        if(!empty($objCertificado)){
            return view('certificados.edit', [
                'objCertificado' => $objCertificado,
                'tituloencabezado'=>'Certificados'
            ]);
        }else{
            return redirect()->route('certificados.index')->with('danger','Error: Certificado no encontrado');
        }
    }

    /**
     * Permite actualizar los datos de una entidad del modelo en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        DB::beginTransaction();
        $error=0;
        $typemsj='success';
        $msj = '¡El certificado se ha registrado con éxito!';
        if($id==-1){
            $empresa = auth()->user()->empresa;
            if(!empty($empresa) && $empresa!=null){
                $sitioguardarcomprobar='../storage/app/public/certificados/'.$empresa->id.'-'.$empresa->rfc;
                $directorioexistente=1;
                if (!file_exists($sitioguardarcomprobar)) {
                    Storage::makeDirectory('public/certificados/'.$empresa->id.'-'.$empresa->rfc);
                }

                if ($directorioexistente==1) {
                    $sitioguardar='public/certificados/'.$empresa->id.'-'.$empresa->rfc;
                }

                $certificado=new Certificado();
                $certificado->idempresa = $empresa->id;

                $name = date("Ymd_His").'__';
                if($request->hasFile('archivocertificado')){
                    $file = $request ->file('archivocertificado');
                    $nombrecer = $name.$file->getClientOriginalName();
                    $tipoarchivo = $this->typeFile($nombrecer);
                    $archivocer = $file->storeAs($sitioguardar,$nombrecer);
                }
                if($request->hasFile('archivokey')){
                    $file = $request ->file('archivokey');
                    $nombrekey = $name.$file->getClientOriginalName();
                    $tipoarchivo = $this->typeFile($nombrekey);
                    $archivokey = $file->storeAs($sitioguardar,$nombrekey);
                }
                $certificado->password= $request->password;

                //configurando el espacio fisico en disco
                $baseurlfisico=realpath(getcwd());
                $direccion_cer = $archivocer;
                $direccion_key = $archivokey;
                $baseurlfisico = str_replace("public", "storage/app/public", $baseurlfisico);
                $direccion_cer = str_replace("public", "", $direccion_cer);
                $direccion_key = str_replace("public", "", $direccion_key);
                $direccion_cer=$baseurlfisico.$direccion_cer;
                $direccion_key=$baseurlfisico.$direccion_key;

                $cer =  file_get_contents($direccion_cer);
                $key =  file_get_contents($direccion_key);

                $arrayCertificado = [
                    "id" => 0,
                    "idemisor" => $empresa->idempresaintegra,
                    "nombrecer" =>  $nombrecer,
                    "nombrekey" => $nombrekey,
                    "filecer" => base64_encode($cer),
                    "filekey" => base64_encode($key),
                    "passwordkey" => $request->password
                ];

                $SoapClient = new nusoap_client("http://integratucfdi.com/webservices/wsafiliados.php?wsdl", true);

                $err = $SoapClient->getError();
                if ($err){
                    $dataenvio['error']=1;
                    $dataenvio['msj']=$err;
                }else{
                    $ObjAfiliado = Afiliado::where("id", "=", (int)env('IDAFILIADO'))->first();
                    if(!empty($ObjAfiliado)){
                        // dd('idafiliado'. $ObjAfiliado->idafiliado,
                        //     'rfc'. $ObjAfiliado->rfc,
                        //     'usuario'. $ObjAfiliado->usuario,
                        //     'password'. $ObjAfiliado->password,
                        //     'certificado', $arrayCertificado
                        // );
                        $respuesta = $SoapClient->call("registracertificado",array(
                            'idafiliado' => $ObjAfiliado->idafiliado,
                            'rfc' => $ObjAfiliado->rfc,
                            'usuario' => $ObjAfiliado->usuario,
                            'password' => $ObjAfiliado->password,
                            'certificado' => $arrayCertificado
                            )
                        );

                        if($respuesta['resultado']){
                            $certificado->serie = $respuesta["serie"];
                            $certificado->idintegra = $respuesta["id"];
                            $certificado->fechainicio = $respuesta["fechainicio"];
                            $certificado->fechafin = $respuesta["fechafin"];
                            $certificado->archivocer = $direccion_cer;
                            $certificado->archivokey = $direccion_key;

                            if($certificado->save()){
                                DB::commit();
                            }else{
                                DB::rollback();
                                $typemsj='danger';
                                $error=1;
                                $msj='No se pudo guardar el certificado';
                            }
                        }else{
                            $typemsj='danger';
                            $error=1;
                            $msj=$respuesta['mensaje'];
                        }
                    }else{
                        $typemsj='danger';
                        $error=1;
                        $msj='Afiliado no encontrado';
                    }
                }

            }
        }


        if($error==0){
            DB::commit();
            return redirect()->route('certificados.index')->with($typemsj,$msj);
        }else{
            unlink($direccion_cer);
            unlink($direccion_key);
            DB::rollback();
            return redirect()->route('certificados.index')->with($typemsj,$msj);
        }
    }

    /**
     * Permite eliminar un gastp
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objCertificado = Certificado::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();
            //ELIMINACIÓN FISICA
            if(!$objCertificado->delete()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }else{
            DB::beginTransaction();
            //ELIMINACIÓN LOGICA
            $objCertificado->estatus = 'D';

            if(!$objCertificado->save()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }
        return redirect()->route('certificados.index')->with($typemsj,$msj);
    }
}
