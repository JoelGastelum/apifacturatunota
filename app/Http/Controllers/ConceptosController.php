<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Concepto;
use App\Unidadessat;
use App\Grupoproductoserviciosat;

class ConceptosController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';

        $sql_where .= ' AND conceptosxdefault.idempresa = "'.auth()->user()->idempresa.'" AND conceptosxdefault.estatus = "A"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                conceptosxdefault.id,
                conceptosxdefault.codigo,
                conceptosxdefault.concepto,
                conceptosxdefault.codigounidadsat,
                conceptosxdefault.unidad,
                conceptosxdefault.codigogrupoprodsersat,
                conceptosxdefault.idempresa
            FROM conceptosxdefault
            '.$sql_where.'
            ORDER BY
                conceptosxdefault.codigo ASC
        ';
        $coleccion = DB::select($sql);

        foreach ($coleccion as $data) {

            $acciones='';
                $acciones.='<a href="'.route('conceptos.edit',$data->id).'" class="btn btn-sm btn-warning" title="Editar">
                    <i class="fas fa-edit"></i>
                </a>';
                $acciones.='&nbsp';
                $acciones.='<form method="POST" action="'.route('conceptos.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <i class="far fa-trash-alt"></i>
                </button>';
                $acciones.='</form>';
            $data->acciones=$acciones;
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function buscarunidadessat(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        if($request->tipobusqueda==1){
            $coleccion=array();
            $sql_where = 'WHERE 1=1 ';
            if($request->texto!='' && $request->texto!=null){
                $sql_where .= ' AND (unidadessat.clave like "%'.$request->texto.'%"';
                $sql_where .= ' OR unidadessat.nombre like "%'.$request->texto.'%")';

                $sql = '
                    SELECT
                        unidadessat.id,
                        unidadessat.clave,
                        unidadessat.nombre
                    FROM unidadessat
                    '.$sql_where.'
                    ORDER BY
                        unidadessat.clave ASC
                ';
                $coleccion = DB::select($sql);
            }

            $html='';
            foreach ($coleccion as $data) {
                $html .= '<tr style="cursor:pointer;" onclick="asignaunidadsat(\''.$data->clave.'\')">';
                    $html .= '<td class="text-center">';
                        $html .=$data->clave;
                    $html .= '</td>';
                    $html .= '<td class="text-left">';
                        $html .=$data->nombre;
                    $html .= '</td>';
                $html .= '</tr>';
            }

            $data_envio['html']=$html;
        }elseif($request->tipobusqueda==2){
            $data_envio['error']=1;
            $data_envio['msj']='No se consiguio clave';

            $objUnidadessat = Unidadessat::where("clave", "=", trim($request->codigounidadsat))->first();
            if(!empty($objUnidadessat)){
                $data_envio['error']=0;
                $data_envio['msj']='';
            }
        }
        echo json_encode($data_envio);
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function buscarcodigoproductosat(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        if($request->tipobusqueda==1){
            $coleccion=array();
            $sql_where = 'WHERE 1=1 ';
            if($request->texto!='' && $request->texto!=null){
                $sql_where .= ' AND (grupoproductoserviciosat.clave like "%'.$request->texto.'%"';
                $sql_where .= ' OR grupoproductoserviciosat.nombre like "%'.$request->texto.'%")';

                $sql = '
                    SELECT
                        grupoproductoserviciosat.id,
                        grupoproductoserviciosat.clave,
                        grupoproductoserviciosat.nombre
                    FROM grupoproductoserviciosat
                    '.$sql_where.'
                    ORDER BY
                        grupoproductoserviciosat.clave ASC
                ';
                $coleccion = DB::select($sql);
            }

            $html='';
            foreach ($coleccion as $data) {
                $html .= '<tr style="cursor:pointer;" onclick="asignacodigoproductosat(\''.$data->clave.'\')">';
                    $html .= '<td class="text-center">';
                        $html .=$data->clave;
                    $html .= '</td>';
                    $html .= '<td class="text-left">';
                        $html .=$data->nombre;
                    $html .= '</td>';
                $html .= '</tr>';
            }

            $data_envio['html']=$html;
        }elseif($request->tipobusqueda==2){
            $data_envio['error']=1;
            $data_envio['msj']='No se consiguio clave';

            $objGrupoproductoserviciosat = Grupoproductoserviciosat::where("clave", "=", trim($request->codigogrupoprodsersat))->first();
            if(!empty($objGrupoproductoserviciosat)){
                $data_envio['error']=0;
                $data_envio['msj']='';
            }
        }
        echo json_encode($data_envio);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR ConceptosController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'ConceptosController');

        return view('conceptos.index', [
            'arraybtn' => $arraybtn,
            'tituloencabezado'=>'Conceptos'
        ]);
    }

    /**
     * Permite crear un concepto
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objConcepto = new Concepto;

        return view('conceptos.edit', [
            'objConcepto' => $objConcepto,
            'tituloencabezado'=>'Conceptos'
        ]);
    }

    /**
     * Permite editar
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objConcepto = Concepto::where("id", "=", $id)->where("estatus", "=", 'A')->first();
        if(!empty($objConcepto)){
            return view('conceptos.edit', [
                'objConcepto' => $objConcepto,
                'tituloencabezado'=>'Conceptos'
            ]);
        }else{
            return redirect()->route('conceptos.index')->with('danger','Error: concepto no encontrado');
        }
    }

    /**
     * Permite actualizar los datos de una entidad del modelo en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        DB::beginTransaction();
        if($id==-1){
            $objConcepto = new Concepto;
        }else{
            $objConcepto = Concepto::where("id", "=", $id)->first();
        }

        $objConcepto->idempresa = auth()->user()->idempresa;
        $objConcepto->codigo = $request->codigo;
        $objConcepto->unidad = $request->unidad;
        $objConcepto->codigounidadsat = $request->codigounidadsat;
        $objConcepto->codigogrupoprodsersat = $request->codigogrupoprodsersat;
        $objConcepto->concepto = $request->concepto;
        $objConcepto->tasaiva = $request->tasaiva;
        $objConcepto->tasahospedaje = $request->tasahospedaje;

        if($objConcepto->save()){
            DB::commit();
            return redirect()->route('conceptos.index')->with('success','Se ha guardado los cambios con éxito');
        }else{
            DB::rollback();
            return redirect()->route('conceptos.index')->with('danger','No se pudo guardar los cambios');
        }
    }

    /**
     * Permite eliminar un gastp
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objConcepto = Concepto::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();
            //ELIMINACIÓN FISICA
            if(!$objConcepto->delete()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }else{
            DB::beginTransaction();
            //ELIMINACIÓN LOGICA
            $objConcepto->estatus = 'D';

            if(!$objConcepto->save()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }
        return redirect()->route('conceptos.index')->with($typemsj,$msj);
    }
}
