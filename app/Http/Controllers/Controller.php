<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use DateTime;
use DateInterval;
use App\Permiso;
use App\Configuracionweb;
use App\Orden;
use App\Afiliado;
use App\Empresa;
use App\Formapago;
use App\Concepto;
use App\Codigopromocion;
use App\Recomendado;
use App\Remision;
use App\Remisiondetalle;
use App\Usuario;
use App\User;
use App\Servicio;

use nusoap_client;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ENVIO DE EMAILS
use Illuminate\Support\Facades\Mail;
use App\Mail\Registrateestablecimiento;
use App\Mail\Renovacionestablecimiento;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Permite retornar las variables globales que se usan en todo el sistema
     * @param type $iduser
     * @param type $controlador
     * @return type $arraybtn
     */
    public function variablesglobales($nombre_variable){
        $data=null;
        if($nombre_variable=='estatus_notas'){
            $data= array(
                'A' => 'Activa',
                'C' => 'Cancelada',
                'F' => 'Facturada'
            );
        }
        if($nombre_variable=='estatus_facturas'){
            $data= array(
                'A' => 'Activo',
                'C' => 'Cancelado',
                'T' => 'Timbrado',
                'P' => 'Solicitud cancelar',
                'S' => 'Por autorizar'
            );
        }
        if($nombre_variable=='tipo_facturas'){
            $data= array(
                'T' => 'Todas',
                'I' => 'Facturada',
                'E' => 'Nota de crédito',
                'P' => 'Comprobante de pago'
            );
        }
        if($nombre_variable=='estatus_ordenespago'){
            $data= array(
                '-1' => 'Intento de pago realizado',
                '0'  => 'En proceso',
                '1'  => 'Espera del metodo de pago',
                '2'  => 'Pagado'
            );
        }
        if($nombre_variable=='estatus_recomendados'){
            $data= array(
                'R'  => 'Recomendado',
                'C'  => 'Comprado por establecimiento',
                'P'  => 'Pago de comisión',
                'V'  => 'Vencido'
            );
        }
        if($nombre_variable=='metodopago'){
            $data= array(
                '1'  => 'Mercado pago',
                '2'  => 'Otro método',
            );
        }

        return $data;
    }

    /**
     * Permite verificar si puede ejecutar acciones del controlador y mostrar los botones de acciones en el index del controlador
     * @param type $iduser
     * @param type $controlador
     * @return type $arraybtn
     */
    public function btnfunciones($idtipousuario,$controlador){
        //BUSCAR PERMISOS LIGADOS AL CONTROLADOR DEL USUARIO
        $objPermiso = new Permiso;
        $datas = $objPermiso->VerificarPermisosDelControlador($idtipousuario,$controlador);
        $arraybtn = array();
        if(count($datas)>0){
            foreach ($datas as $data) {
                //TODOS LOS CONTROLADORES
	                if($data->accion=='create'){
	                    $arraybtn['create']=true;
	                }
	                if($data->accion=='editar'){
                        $arraybtn['editar']=true;
	                }
	                if($data->accion=='eliminar'){
	                    $arraybtn['eliminar']=true;
	                }

                if($controlador=='UsersController')
                {
                    if($data->accion=='cambiarpassword'){
                        $arraybtn['cambiarpassword']=true;
                    }
                    if($data->accion=='permisos'){
                        $arraybtn['permisos']=true;
                    }
                    if($data->accion=='permizonas'){
                        $arraybtn['permizonas']=true;
                    }
                }
            }
        }
        return $arraybtn;
    }

    /**
     * Permite actualizar la configuración de la sessión actual
     * @param type $tipo_configuracion
     * @return type
     */
    public function ActualizarSesionConfiguracion($tipo_configuracion=1)
    {
        //BUSCANDO LAS CONFIGURACIONES DEL SITIO WEB
            $ConfiguracionWebs = Configuracionweb::where('tipo', '=', $tipo_configuracion)->get();
            foreach ($ConfiguracionWebs as $ConfiguracionWeb) {
                if($ConfiguracionWeb->tipo==$tipo_configuracion){
                    $nombre = $ConfiguracionWeb->nombre;
                    $parametros = $ConfiguracionWeb->parametros;

                    //session()->forget('Configuracion.Web.'.$nombre);
                    session()->put('Configuracion.Web.'.$nombre, $parametros);
                }
            }
        return true;
    }

    /**
     * Permite crear un slug
     * @param type $text
     * @return type $text
     */
    public function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        //lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return '';
        }

        return $text;
    }

     /**
     * Permite formatear un campo para convertir en decimal
     * @param  string $valor $paramname description
     * @return decimal $valor
     */
    public function formatearADecimal($valor){
        $valor = str_replace("$", "", (string)$valor);
        $valor = str_replace(",", "", (string)$valor);
        // $valor = str_replace(".", ",", (string)$valor);

        return $valor;
    }

    /**
     * Permite convertir el formato money de un excel a un decimal para el campo en la tabla
     * @param  [money] $numero
     * @return [decimal] $numero
     */
    public function converExcelMoneyADecimal($numero){
        $numero = str_replace("$", "", $numero);
        $numero = str_replace(",", "", $numero);
        $numero = (float)trim($numero);

        return $numero;
    }

    /**
     * Obtener el formato del archivo
     * @param  text $file
     */
    public function typeFile($file){
        $array = explode(".", $file);
        return end( $array );
    }

    /**
     * Permite obtener el nombre del dia en ingles segun fecha
     * @param type $diasemana
     */
    public function diasemanaingles($diasemana){
        if($diasemana==1){
            //LUNES
            return 'Monday';
        }
        if($diasemana==2){
            //Martes
            return 'Tuesday';
        }
        if($diasemana==3){
            //Miercoles
            return 'Wednesday';
        }
        if($diasemana==4){
            //Jueves
            return 'Thursday';
        }
        if($diasemana==5){
            //Viernes
            return 'Friday';
        }
        if($diasemana==6){
            //Sabado
            return 'Saturday';
        }
        if($diasemana==7){
            //DOMINGO
            return 'Sunday';
        }
        return $data;
    }

    /**
     * Retorna la fecha en formato ingles de base de datos
     * @param type $fecha
     */
    public function formatoinglesfecha($fecha){
        //COLOCANDO FECHA EXCELS A INGLES
        if (strpos(strtolower($fecha), 'ene') !== false){
            $fecha  = str_replace('ene', 'jan', $fecha);
        }
        if (strpos(strtolower($fecha), 'ago') !== false){
            $fecha  = str_replace('ago', 'aug', $fecha);
        }
        if (strpos(strtolower($fecha), 'dic') !== false){
            $fecha  = str_replace('dic', 'dec', $fecha);
        }

        $fecha = date('Y-m-d', strtotime($fecha));
        return $fecha;
    }

    /**
     * Permite comparar fecha
     * @param type $fecha1
     * @param type $fecha2
     * @return -1 fecha1<fecha2, 0 fecha1=fech2, 1 fecha1>fecha2
     */
    public function comparafecha($fecha1,$fecha2){
        $fecha1 = strtotime(date($fecha1));
        $fecha2 = strtotime($fecha2);

        if($fecha1 > $fecha2){
            return 1;
        }elseif($fecha1 < $fecha2){
            return -1;
        }elseif($fecha1 == $fecha2){
            return 0;
        }
    }

    /**
     * Permite dar los dias de diferentes entre fechas
     * @param type $fecha1
     * @param type $fecha2
     * @return int
     */
    public function diaDiferenciaFechas($fecha1,$fecha2){
        $fecha1=date_create($fecha1);
        $fecha2=date_create($fecha2);
        $interval = date_diff($fecha1, $fecha2);
        return (int)($interval->format('%R%a'));
    }

    /**
     * Permite obtener la fecha inicial y final de un numero de la  semana de un año
     * @param type int nsemana, text anio, numero dia de semana
     * @return array
     */
    public function obtenerFechasDeUnNumeroSemana($nsemana,$anio,$ndiapagoinstitucion=0){
        // Se crea objeto DateTime del 1/enero del año ingresado
        $fecha = DateTime::createFromFormat('Y-m-d', $anio . '-1-2');

        $numerosemana=false;
        while($numerosemana==false){
            $w = $fecha->format('W');
            if($w==$nsemana){
                $numerosemana=true;
            }else{
                $fecha->add(DateInterval::createfromdatestring('+1 week'));
            }
        }
        $w = $fecha->format('W'); // Número de la semana

        // Se debe obtener el primer y el último día
            // Si $fecha no es el primer día de la semana, se restan días
            if ($fecha->format('N') > 1) {
                $format = '-' . ($fecha->format('N') - 1) . ' day';
                $fecha->add(DateInterval::createfromdatestring($format));
            }
            // Ahora $fecha es el primer día de esa semana

            // Se clona la fecha en $fecha2 y se le agrega 6 días
            $fecha2 = clone($fecha);
            $fecha2->add(DateInterval::createfromdatestring('+6 day'));


        $data['fecha_inicial']=$fecha->format('Y-m-d');//SIEMPRE LUNES
        $data['fecha_final']=$fecha2->format('Y-m-d');//SIEMPRE DOMINGO

        //BUSCAR LA FECHA DE COBRO DEPENDIENDO DE LA INSTITUCION
        if($ndiapagoinstitucion>0){
            $fecha_operacion=$data['fecha_inicial'];
            for ($i=1; $i<$ndiapagoinstitucion ; $i++) {
                $fecha_operacion=$this->fechaSumaORestaDias($fecha_operacion,'+',1);
            }
            $data['fecha_pago']=$fecha_operacion;
        }


        // Devuelve un array con ambas fechas
        return $data;
    }

    /**
     * Permite obtener el numero de la semana de una fecha en el año
     * @param type $fecha
     * @return int
     */
    public function obtenerNumeroSemanaDeFecha($fecha){
        $dia=date('d',strtotime($fecha));
        $mes=date('m',strtotime($fecha));
        $anio=date('Y',strtotime($fecha));

        return date('W',  mktime(0,0,0,$mes,$dia,$anio));
    }

    /**
     * Permite obtener la fecha inicial y final de una quincena
     * @param type int nquincena, text anio
     * @return array
     */
    public function obtenerFechasDeUnNumeroQuincena($nquincena,$anio){
        //OBTENIENDO EL MES
            $mes_decimal=$nquincena/2;
            $mes=round($mes_decimal);
            $mes=str_pad($mes, 2, "0", STR_PAD_LEFT);
            if(is_int($mes_decimal)){
                //SEGUNDA QUINCENA DEL MES
                $fecha_inicial=$anio.'-'.$mes.'-16';
                //BUSCAMOS ULTIMO DIA DEL MES
                    $fecha_inicial_mes = date('Y-m',strtotime($fecha_inicial));
                    $aux = date('Y-m-d', strtotime($fecha_inicial_mes." + 1 month"));
                    $ultimo_dia = date('Y-m-d', strtotime($aux." - 1 day"));
                $fecha_final=$ultimo_dia;
            }else{
                //PRIMERA QUINCENA DEL MES
                $fecha_inicial=$anio.'-'.$mes.'-01';
                $fecha_final=$anio.'-'.$mes.'-15';
            }
        $data['fecha_inicial']=$fecha_inicial;
        $data['fecha_final']=$fecha_final;
        return $data;
    }

    /**
     * Permite obtener el numero de la quincena de una fecha en el año
     * @param type $fecha (d-m-Y)
     * @return int
     */
    public function obtenerNumeroQuincenaDeFecha($fecha){
        $mes = date("m", strtotime($fecha));
        if($mes=='01'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 1.0;
            }else{
                return 2.0;
            }
        }elseif($mes=='02'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 3.0;
            }else{
                return 4.0;
            }
        }elseif($mes=='03'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 5.0;
            }else{
                return 6.0;
            }
        }elseif($mes=='04'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 7.0;
            }else{
                return 8.0;
            }
        }elseif($mes=='05'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 9.0;
            }else{
                return 10.0;
            }
        }elseif($mes=='06'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 11.0;
            }else{
                return 12.0;
            }
        }elseif($mes=='07'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 13.0;
            }else{
                return 14.0;
            }
        }elseif($mes=='08'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 15.0;
            }else{
                return 16.0;
            }
        }elseif($mes=='09'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 17.0;
            }else{
                return 18.0;
            }
        }elseif($mes=='10'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 19.0;
            }else{
                return 20.0;
            }
        }elseif($mes=='11'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 21.0;
            }else{
                return 22.0;
            }
        }elseif($mes=='12'){
            $dia = date("d", strtotime($fecha));
            if($dia<=15){
                return 23.0;
            }else{
                return 24.0;
            }
        }
    }

    /**
     * Permite obtener la fecha inicial y final de una mensualidad
     * @param type int nquincena, text anio
     * @return array
     */
    public function obtenerFechasDeUnNumeroMensualidad($nmensualidad,$anio){
        //OBTENIENDO EL MES
        if((float)$nmensualidad<10){
            $nmensualidad = '0'.$nmensualidad;
        }
        $fecha_inicial= $anio.'-'.$nmensualidad.'-01';
        //BUSCAMOS ULTIMO DIA DEL MES
            $fecha_inicial_mes = date('Y-m-d',strtotime($fecha_inicial));
            $aux = date('Y-m-d', strtotime($fecha_inicial_mes." + 1 month"));
            $ultimo_dia = date('Y-m-d', strtotime($aux." - 1 day"));
            $fecha_final=$ultimo_dia;


        $data['fecha_inicial']=$fecha_inicial;
        $data['fecha_final']=$fecha_final;
        return $data;
    }

    /**
     * Permite obtener el numero de la mensualidad de una fecha en el año
     * @param type $fecha (d-m-Y)
     * @return int
     */
    public function obtenerNumeroMensualDeFecha($fecha){
        $mes = date("m", strtotime($fecha));
        return (float)$mes;
    }

    /**
     * Permite obtener el numero fecha de la siguiente quincena de una fecha
     * @param type $fecha (Y-m-d)
     * @return $fecha (Y-m-d)
     */
    public function obtenerQuincenaSiguienteDeUnaFecha($fecha){
        $dia =  date('d',strtotime($fecha));
        if($dia<15){
            $retorno = '15-'.date('m-Y',strtotime($fecha));
        }else{
            //BUSCAR EL ULTIMO DIA DEL MES
            $mes = date('Y-m',strtotime($fecha));
            $mes_solo = date('m',strtotime($fecha));
            $aux = date('Y-m-d', strtotime($mes." + 1 month"));
            $ultimo_dia = date('Y-m-d', strtotime($aux." - 1 day"));
            if($mes_solo!='02'){
                if($dia>=30 || ($this->comparafecha($fecha,$ultimo_dia)==0)){
                    //VERIFICAMOS QUE CORRESPONDE AL ULTIMO DIA DEL MES PORQUE DEBE SER LA SIGUIENTE QUINCENA DEL MES SIGUIENTE
                    $fecha = date("Y-m-d",strtotime($fecha."+ 4 day"));
                    $retorno = '15-'.date('m-Y',strtotime($fecha));
                }else{
                    //VERIFICAMOS QUE SEA EL MISMO AÑO
                    if(date('Y', strtotime($aux))==date('Y', strtotime($fecha))){
                        //SIGUE SIENDO EL MISMO AÑO
                        if(date('d',strtotime($ultimo_dia))>30){
                            $retorno = '30-'.date ("m-Y",strtotime($ultimo_dia));
                        }else{
                            $retorno = date ("d-m-Y",strtotime($ultimo_dia));
                        }
                    }else{
                        //VERIFICAMOS SI DIA ES MENOR QUE 30 PARA QUE SEA DEL MISMO AÑO
                        if($dia<30){
                            //MISMO AÑO DICIEMBRE 30
                            $retorno = '30-'.date ("m-Y",strtotime($fecha));
                        }else{
                            //OTRO AÑO
                            $retorno = '15-'.date ("m-Y",strtotime($aux));
                        }
                    }
                }
            }else{
                if($fecha!=$ultimo_dia){
                    $retorno = date("d-m-Y",strtotime($ultimo_dia));
                }else{
                    $fecha = date("Y-m-d",strtotime($fecha."+ 4 day"));
                    $retorno = '15-'.date('m-Y',strtotime($fecha));
                }
            }
        }

        return date("Y-m-d",strtotime($retorno));
    }

    /**
     * Permite obtener el numero de mensualidad fecha de la siguiente quincena de una fecha
     * @param type $fecha (Y-m-d)
     * @return $fecha (Y-m-d)
     */
    public function obtenerMensualidadSiguienteDeUnaFecha($fecha){
        $fecha_inicial = date('Y-m', strtotime($fecha." + 2 month"));
        //BUSCAMOS ULTIMO DIA DEL MES
            $aux = date('Y-m',strtotime($fecha_inicial));
            $ultimo_dia = date('Y-m-d', strtotime($aux." - 1 day"));
            $fecha_final=$ultimo_dia;
            // dd($fecha,$fecha_inicial,$fecha_final);


        $retorno = $fecha_final;
        // dd($retorno);

        return $retorno;
    }

    /**
     * Retorna la suma o la resta de dias a una fecha
     * @return html
     * @author Arredondo, José <arredondojose8@gmail.com>
    */
    public function fechaSumaORestaDias($fecha_operacion,$operacion,$nro_dias) {
        if($nro_dias!=0){
            $nuevafecha = strtotime ($operacion.$nro_dias.' day' , strtotime ( $fecha_operacion ));
        }

        $nuevafecha = date ('Y-m-d' , $nuevafecha );
        return $nuevafecha;
    }

    /**
     * Permite generar codigo aleatorios
     */
    public function generacodigoconfirmacion($id){
        $data = uniqid($id);
        return $data;
    }

    /**
     * Permite generar codigo aleatorios
     */
    public function generacodigo($id){
        // $data = random_bytes($longitud);
        $data = dechex($id);
        $data = str_pad($data, 5, "0", STR_PAD_LEFT);
        $data = strtoupper($data);
        // dd($id,$data);

        return $data;
    }
    /**
     * Permite generar codigo aleatorios
     */
    public function desgeneracodigo($id){
        // $data = random_bytes($longitud);
        $data = hexdec($id);
        $data = str_pad($data, 5, "0", STR_PAD_LEFT);
        $data = (int)($data);
        // dd($id,$data);

        return $data;
    }

    /**
     *
     * Valida un email usando expresiones regulares.
     *  Devuelve true si es correcto o false en caso contrario
     *
     * @param    string  $str la dirección a validar
     * @return   boolean
     *
     */
    function is_valid_email($str)
    {
      $matches = null;
      return (1 === preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/', $str, $matches));
    }

    /**
     *
     * Funcion que permite devolver si se esta usando un movil, o pc
     * @return 0 = pc, 1 = movil, 2 = tablet
    */
    function dispositivo(){
        $tablet_browser = 0;
        $mobile_browser = 0;
        $body_class = "desktop";

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
            $body_class = "tablet";
        }
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
            $body_class = "mobile";
        }
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
        }
        if ($tablet_browser > 0) {
            // Si es tablet has lo que necesites
            return 2;
        }
        else if ($mobile_browser > 0) {
            // Si es dispositivo mobil has lo que necesites
            return 1;
        }
        else {
            // Si es ordenador de escritorio has lo que necesites
            return 0;
        }
    }

    /**
     *
     * Funcion que devuelve el Navegador Actual
     */
    function obtenerNavegadorWeb()
    {
        $agente = $_SERVER['HTTP_USER_AGENT'];
        $navegador = 'Unknown';
        $platforma = 'Unknown';
        $version= "";

        #Obtenemos la Plataforma
        if (preg_match('/linux/i', $agente)) {
            $platforma = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $agente)) {
            $platforma = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $agente)) {
            $platforma = 'windows';
        }

        #Obtener el UserAgente
        if(preg_match('/MSIE/i',$agente) && !preg_match('/Opera/i',$agente))
        {
            $navegador = 'Internet Explorer';
            $navegador_corto = "MSIE";
        }
        elseif(preg_match('/Firefox/i',$agente))
        {
            $navegador = 'Mozilla Firefox';
            $navegador_corto = "Firefox";
        }
        elseif(preg_match('/Chrome/i',$agente))
        {
            $navegador = 'Google Chrome';
            $navegador_corto = "Chrome";
        }
        elseif(preg_match('/Safari/i',$agente))
        {
            $navegador = 'Apple Safari';
            $navegador_corto = "Safari";
        }
        elseif(preg_match('/Opera/i',$agente))
        {
            $navegador = 'Opera';
            $navegador_corto = "Opera";
        }
        elseif(preg_match('/Netscape/i',$agente))
        {
            $navegador = 'Netscape';
            $navegador_corto = "Netscape";
        }

        #Obtenemos la Version
        // $known = array('Version', $navegador_corto, 'other');
        // $pattern = '#(?' . join('|', $known) . ')[/ ]+(?[0-9.|a-zA-Z.]*)#';
        // if (!preg_match_all($pattern, $agente, $matches)) {
        //     #No se obtiene la version simplemente continua
        // }

        // $i = count($matches['browser']);
        // if ($i != 1) {
        //     if (strripos($agente,"Version") < strripos($agente,$navegador_corto)){
        //         $version= $matches['version'][0];
        //     } else {
        //         $version= $matches['version'][1];
        //     }
        // } else {
        //     $version= $matches['version'][0];
        // }

        // /*Verificamos si tenemos Version*/
        // if ($version==null || $version=="") {
        //     $version="?";
        // } /*Resultado final del Navegador Web que Utilizamos*/

        //VERIFICAR QUE ES ANDROID
        $pos = strpos($agente, 'Android');
        if ($pos === false) {
            $android=0;
        } else {
            $android=1;
        }


        return array(
            'agente' => $agente,
            'isandroid' => $android,
            'nombre'      => $navegador,
            'version'   => $version,
            'platforma'  => $platforma
        );
    }

    /**
     *
     * Permite pagar plan, creando todos los registros pertinente de un usuario tipo = 2
     */
    function pagarplan($ObjOrden,$metodopago,$arraymetodopago){
        $error=array();
        DB::beginTransaction();
        //COMISION A PAGAR
            $comisionestablecimiento=(float)env('COMISIONESTABLECIMIENDO');
            if($comisionestablecimiento==0){
                $comisionestablecimiento=200;
            }
            $comisiongenerico=(float)env('COMISIONCODIGOGENERICO');
            if($comisiongenerico==0){
                $comisiongenerico=200;
            }
            $comisionpadre=(float)env('COMISIONPADRE');
            if($comisionpadre==0){
                $comisionpadre=100;
            }

        $data_envio['error']=0;

        //CREAR EMPRESA
        //CREAR USUARIO
        //CREAR REMISION
        //CREAR DETALLEREMISION
        //CONFIRMAR ORDEN
        //ASIGNAR CODIGO PROMOCION
        //ENVIAR CORREO

        //CREAR EMPRESA
        if($ObjOrden->ambito==0){
            //SANDBOX
            $pruebasF = 1;
            $pruebasN = 1;
        }else{
            //PRODUCCION
            $pruebasF = 0;
            $pruebasN = 0;
        }
        $SoapClient = new nusoap_client("http://integratucfdi.com/webservices/wsafiliados.php?wsdl", true);
        $err = $SoapClient->getError();
        if ($err){
            $error[]=$err;
            $data_envio['error']=$error;
        }else{
            //BUSCAMOS EL AFILIADO DE NOSOTROS
            $ObjAfiliado = Afiliado::where("id", "=", (int)env('IDAFILIADO'))->first();
            if(!empty($ObjAfiliado)){
                $ArrayEmpresa=[
                    'id'=>0,
                    'rfc'=>$ObjOrden->rfc,
                    'razonsocial'=>$ObjOrden->razonsocial,
                    'regimen'=>" ",
                    'pruebasF' => $pruebasF,
                    'pruebasN' => $pruebasN
                ];
                // dd($ObjAfiliado,$ArrayEmpresa);

                //CONSUMIMOS SERVICIO DE INTEGRA
                $respuesta = $SoapClient->call("registraemisor",array(
                        'idafiliado' => $ObjAfiliado->idafiliado,
                        'rfc' => $ObjAfiliado->rfc,
                        'usuario' => $ObjAfiliado->usuario,
                        'password' => $ObjAfiliado->password,
                        'emisor' => $ArrayEmpresa
                    )
                );
                if($respuesta['resultado'] > 0){
                    //MANIPULACION DE FECHA
                    $fechahoy_espanol=date("d-m-Y");
                    $fechahoy=date("Y-m-d");
                    $fecha = date('Y-m-j');
                    $nuevafecha = strtotime ( '+ '.(int)$ObjOrden->duracion.' month' , strtotime ( $fecha ) ) ;
                    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );


                    //CREANDO EMPRESA
                    $empresa= new Empresa;
                    $empresa->idafiliado=$ObjAfiliado->id;
                    $empresa->idempresa=(int)env('IDEMPRESA');
                    $empresa->email=$ObjOrden->email;
                    $empresa->idempresaintegra=$respuesta['id'];
                    $empresa->razonsocial=$ObjOrden->razonsocial;
                    $empresa->rfc=$ObjOrden->rfc;
                    $empresa->nombrecomercial=$ObjOrden->razonsocial;
                    $empresa->estatus='A';
                    $empresa->usremisor=$respuesta['usuario'];
                    $empresa->pwdemisor=$respuesta['password'];
                    $empresa->fechaalta=$fechahoy;
                    $empresa->fechavigencia=$nuevafecha;
                    $empresa->idusocfdidefault=null;
                    $empresa->grupoprodserdefault='a';
                    $empresa->unidadsatdefault='a';
                    $empresa->facturaglobal=0;
                    $empresa->idpublicogeneral=0;
                    $empresa->foliosconsumidos=0;
                    $empresa->folioscomprados=$ObjOrden->folio;
                    $empresa->pasowizard=0;

                    // dd($ObjAfiliado,$ArrayEmpresa,$empresa);
                    if($empresa->save()){
                        //ASIGNAR LOS FOLIOS A LA EMPRESA
                        $SoapClient = new nusoap_client("http://integratucfdi.com/webservices/wsafiliados.php?wsdl", true);
                        $err = $SoapClient->getError();
                        if($err){
                            $error[]=$err;
                        }else{
                            $respuesta = $SoapClient->call("asignarfolios",array(
                                    'idafiliado' => $ObjAfiliado->idafiliado,
                                    'rfc' => $ObjAfiliado->rfc,
                                    'usuario' => $ObjAfiliado->usuario,
                                    'password' => $ObjAfiliado->password,
                                    'idempresa'=> $empresa->idempresaintegra,
                                    'cantidad'=> $ObjOrden->folio
                                )
                            );
                            if($respuesta['resultado'] > 0){
                                // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$respuesta);
                                //CREAR USUARIO
                                $pwd = substr( md5(microtime()), 1, 8);
                                $ObjUser = new User;
                                $ObjUser->login = $ObjOrden->rfc;
                                $ObjUser->nombre = $ObjOrden->razonsocial;
                                $ObjUser->password = Hash::make($pwd);
                                $ObjUser->email = $ObjOrden->email;
                                $ObjUser->pwd = $pwd;
                                $ObjUser->estatus='A';
                                $ObjUser->idtipousuario=2;
                                $ObjUser->idempresa=$empresa->id;
                                $ObjUser->idsucursal=0;

                                //COLOCANDO FOTO DE PERFIL
                                $numero = rand(1,12);
                                $ObjUser->foto_perfil ='img/compartidos/avatar'.$numero.'.jpg';

                                if($ObjUser->save()){
                                    // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser);
                                    $notaTicket='Fe'.substr( md5(microtime()), 1, 8);

                                    //CREAR REMISION
                                    $remision= new Remision;
                                    $remision->idempresa = (int)env('IDEMPRESA');
                                    $remision->noticket = $notaTicket;
                                    $remision->numerocliente = '';
                                    $remision->nombrecliente = '';
                                    $remision->idsucursal =(int)env('IDSUCURSAL');
                                    $remision->fecha = $fechahoy;
                                    $remision->subtotal = $ObjOrden->subtotal;
                                    $remision->ieps = 0;
                                    $remision->iva = $ObjOrden->iva_monto;
                                    $remision->total = $ObjOrden->total;

                                    //BUSCANDO NOMBRE DE LA FORMA DE PAGO=
                                        $nombreformapago='';
                                        $idformapago  = (int)env('IDFORMAPAGODEFAULT');
                                        $ObjFormapago = Formapago::where("slug", "=", $arraymetodopago['payment_type'])->first();
                                        if(!empty($ObjFormapago)){
                                            $nombreformapago  = $ObjFormapago->nombre;
                                            $idformapago = $ObjFormapago->id;
                                        }else{
                                            $ObjFormapago = Formapago::where("id", "=", (int)env('IDFORMAPAGODEFAULT'))->first();
                                            if(!empty($ObjFormapago)){
                                                $nombreformapago  = $ObjFormapago->nombre;
                                                $idformapago = $ObjFormapago->id;
                                            }
                                        }

                                    $remision->idformapago  = $idformapago;
                                    $remision->formapago = $nombreformapago;
                                    $remision->metodopago = "";
                                    $remision->numcuenta = "";
                                    $remision->condicionespago = "";
                                    $remision->fechavence = $fechahoy;
                                    $remision->estatus = "A";
                                    $remision->uuid = "";
                                    $remision->serie = "";
                                    $remision->folio = 0;
                                    $remision->razoncancelacion = "";
                                    $remision->razoncancelacionadmin = "";
                                    $remision->saldo = 0;
                                    $remision->idconciliacion = 0;

                                    if($remision->save()){
                                        // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision);
                                        //CREAR DETALLEREMISION
                                        foreach ($ObjOrden->ordenesdetalle as $ObjOrdenesdetalle) {
                                            $detallef= new Remisiondetalle;
                                            $detallef->idempresa = $remision->idempresa;
                                            $detallef->idremision = $remision->id;

                                            //BUSCAR EL CONCEPTO DEL SERVICO DEL DETALLE DE LA ORDEN
                                                if($ObjOrdenesdetalle->servicio->tipo==2){
                                                    //SUBSCRIPCION
                                                    $idconcepto=(int)env('IDCONCEPTOSUBSCRIPCION');
                                                }else{
                                                    //PAQUETE DE FOLIO
                                                    $idconcepto=(int)env('IDCONCEPTOPAQUETE');
                                                }
                                                if(isset($ObjOrdenesdetalle->servicio->concepto->id)){
                                                    $idconcepto=$ObjOrdenesdetalle->servicio->concepto->id;
                                                }
                                                $ObjConcepto = Concepto::where("id", "=",$idconcepto)->first();

                                            $detallef->idconcepto = $ObjConcepto->id;
                                            $detallef->codigo =   $ObjConcepto->codigo;
                                            $detallef->descripcion = $ObjConcepto->concepto;
                                            $detallef->unidad =  $ObjConcepto->unidad;

                                            $detallef->codigounidadsat =  $ObjConcepto->codigounidadsat;
                                            $detallef->codigogrupoprodsersat =  $ObjConcepto->codigogrupoprodsersat;
                                            $detallef->cantidad = 1;
                                            $detallef->precio = $ObjOrdenesdetalle->subtotal;
                                            $detallef->importe = $ObjOrdenesdetalle->subtotal;
                                            $detallef->importeconimpuestos =$ObjOrdenesdetalle->total;
                                            $detallef->tasaiva = $ObjOrdenesdetalle->iva_porcentaje;
                                            $detallef->importeiva =  $ObjOrdenesdetalle->iva_monto;
                                            $detallef->series = "";
                                            $detallef->tasaieps = 0;
                                            $detallef->importeieps = 0;
                                            if(!$detallef->save()){
                                                $error[] = 'No se pudo guardar el detalle de la remision';
                                            }
                                            // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision,$ObjOrdenesdetalle,$detallef);
                                        }

                                        if(count($error)==0){
                                            //CONFIRMAR ORDEN
                                            $ObjOrden->iduser=$ObjUser->id;
                                            $ObjOrden->fechapagado=$fechahoy;
                                            $ObjOrden->estatus=2;

                                            if($metodopago==1){
                                                //MERCADO PAGO
                                                $ObjOrden->collection_id = $arraymetodopago['collection_id'];
                                                $ObjOrden->collection_status = $arraymetodopago['collection_status'];
                                                $ObjOrden->payment_id = $arraymetodopago['payment_id'];
                                                $ObjOrden->status = $arraymetodopago['status'];
                                                $ObjOrden->payment_type = $arraymetodopago['payment_type'];
                                                $ObjOrden->merchant_order_id = $arraymetodopago['merchant_order_id'];
                                                $ObjOrden->preference_id = $arraymetodopago['preference_id'];
                                                $ObjOrden->site_id = $arraymetodopago['site_id'];
                                                $ObjOrden->processing_mode = $arraymetodopago['processing_mode'];
                                                $ObjOrden->merchant_account_id = $arraymetodopago['merchant_account_id'];
                                            }elseif($metodopago==2){
                                                $ObjOrden->tracker = $arraymetodopago['tracker'];
                                                $ObjOrden->fechapagado = $arraymetodopago['fechapagado'];
                                            }

                                            if($ObjOrden->save()){
                                                // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision,$ObjOrden);
                                                if($ObjOrden->idcodigopromocion!=null){
                                                    //ASIGNAR CODIGO PROMOCION
                                                    $objCodigopromocion = Codigopromocion::where("id", "=", $ObjOrden->idcodigopromocion)->first();
                                                    if(!empty($objCodigopromocion)){
                                                        if($objCodigopromocion->tipo==2){
                                                            //CODIGO DE ESTABLECIMIENTO
                                                            $objCodigopromocion->iduser=$ObjUser->id;
                                                            $objCodigopromocion->estatus='A';

                                                            if($objCodigopromocion->save()){
                                                                //CAMBIAR ESTATUS DE RECOMENDADO DE ESTABLECIMIENDO
                                                                $objRecomendado = Recomendado::where("id", "=", $objCodigopromocion->idrecomendado)->first();
                                                                if(!empty($objRecomendado)){
                                                                    //COLOCAR COMISIÓN EN RECOMENDADO
                                                                    $objRecomendado->comision = $comisionestablecimiento;
                                                                    //COLOCAR idempresacomprador
                                                                    $objRecomendado->idempresacomprador = $empresa->id;
                                                                    //COLOCAR fechaComp
                                                                    $objRecomendado->fechaComp = $fechahoy;
                                                                    $objRecomendado->estatus = 'C';

                                                                    if($objRecomendado->save()){
                                                                        //VERIFICAR SI TIENE USUARIO RECOMENDON
                                                                        $objDueno = Usuario::where("id", "=", $objCodigopromocion->iddueno)->first();
                                                                        if(!empty($objDueno)){
                                                                            //VERIFICAR SI DUENO TIENE PADRE
                                                                            if($objDueno->idusuariorecomendon!=null){
                                                                                $objRecomendadoPadre = new Recomendado;
                                                                                $objRecomendadoPadre->idusuario = $objDueno->idusuariorecomendon;
                                                                                $objRecomendadoPadre->idrecomendado = $objRecomendado->id;
                                                                                $objRecomendadoPadre->idempresacomprador = $empresa->id;
                                                                                $objRecomendadoPadre->idpago = null;
                                                                                $objRecomendadoPadre->nombre = null;
                                                                                $objRecomendadoPadre->direccion = null;
                                                                                $objRecomendadoPadre->telefono = null;
                                                                                $objRecomendadoPadre->fechaRec = null;
                                                                                $objRecomendadoPadre->fechaComp = $fechahoy;
                                                                                $objRecomendadoPadre->comision = $comisionpadre;
                                                                                $objRecomendadoPadre->estatus = 'C';

                                                                                if(!$objRecomendadoPadre->save()){
                                                                                    $error[]='No se pudo crear registro de recomendado padre';
                                                                                    $data_envio['error']=$error;
                                                                                }
                                                                            }
                                                                        }
                                                                    }else{
                                                                        $error[]='No se pudo actualizar registro del recomendado del establecimiento';
                                                                        $data_envio['error']=$error;
                                                                    }
                                                                }
                                                            }else{
                                                                $error[]='No se pudo actualizar el codigo de promoción de tipo establecimiento';
                                                                $data_envio['error']=$error;
                                                            }
                                                        }elseif($objCodigopromocion->tipo==1){
                                                            //CODIGO GENERICO

                                                            //CREAR REGISTRO DE RECOMENDADO PARA PAGAR AL CODIGO GENERICO
                                                            $objRecomendado = new Recomendado;
                                                            $objRecomendado->idusuario = $objCodigopromocion->iddueno;
                                                            $objRecomendado->idrecomendado = null;
                                                            $objRecomendado->idempresacomprador = $empresa->id;
                                                            $objRecomendado->idpago = null;
                                                            $objRecomendado->nombre = null;
                                                            $objRecomendado->direccion = null;
                                                            $objRecomendado->telefono = null;
                                                            $objRecomendado->fechaRec = null;
                                                            $objRecomendado->fechaComp = $fechahoy;
                                                            $objRecomendado->comision = $comisiongenerico;
                                                            $objRecomendado->estatus = 'C';
                                                            if($objRecomendado->save()){
                                                                //VERIFICAR SI TIENE USUARIO RECOMENDON
                                                                $objDueno = Usuario::where("id", "=", $objCodigopromocion->iddueno)->first();
                                                                if(!empty($objDueno)){
                                                                    //VERIFICAR SI DUENO TIENE PADRE
                                                                    if($objDueno->idusuariorecomendon!=null){
                                                                        $objRecomendadoPadre = new Recomendado;
                                                                        $objRecomendadoPadre->idusuario = $objDueno->idusuariorecomendon;
                                                                        $objRecomendadoPadre->idrecomendado = $objRecomendado->id;
                                                                        $objRecomendadoPadre->idempresacomprador = $empresa->id;
                                                                        $objRecomendadoPadre->idpago = null;
                                                                        $objRecomendadoPadre->nombre = null;
                                                                        $objRecomendadoPadre->direccion = null;
                                                                        $objRecomendadoPadre->telefono = null;
                                                                        $objRecomendadoPadre->fechaRec = null;
                                                                        $objRecomendadoPadre->fechaComp = $fechahoy;
                                                                        $objRecomendadoPadre->comision = $comisionpadre;
                                                                        $objRecomendadoPadre->estatus = 'C';

                                                                        if(!$objRecomendadoPadre->save()){
                                                                            $error[]='No se pudo crear registro de recomendado padre';
                                                                            $data_envio['error']=$error;
                                                                        }
                                                                    }
                                                                }
                                                            }else{
                                                                $error[]='No se pudo actualizar registro del recomendado del generico';
                                                                $data_envio['error']=$error;
                                                            }
                                                        }
                                                    }
                                                }
                                                // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision,$ObjOrden,$objCodigopromocion);

                                                if(count($error)==0){
                                                    DB::commit();
                                                    //ENVIAR CORREO
                                                    $urllogin= route('login');
                                                    $urlfacturar= route('facturar',[base64_encode($remision->id)]);
                                                    $data= array(
                                                        'login'=>$ObjUser->login,
                                                        'nombre'=>$ObjUser->nombre,
                                                        'pwd'=>$ObjUser->pwd,
                                                        'urllogin'=>$urllogin,
                                                        'urlfacturar'=>$urlfacturar,
                                                    );
                                                    // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision,$ObjOrden,$objCodigopromocion,$data);
                                                    Mail::to($ObjUser->email)->send(new Registrateestablecimiento($data));

                                                    if($metodopago==1){
                                                        //MERCADO PAGO
                                                        $data_envio['error']=0;
                                                        $data_envio['iduser']=$ObjUser->id;
                                                        $data_envio['b64_idremision']=base64_encode($remision->id);
                                                    }elseif($metodopago==2){
                                                        $data_envio['error']=0;
                                                    }
                                                }else{
                                                    DB::rollback();
                                                }
                                            }else{
                                                DB::rollback();
                                                $error[]='No se pudo actualizar la orden';
                                                $data_envio['error']=$error;
                                            }
                                        }
                                    }else{
                                        DB::rollback();
                                        $error[]='No se pudo guardar la remisión';
                                        $data_envio['error']=$error;
                                    }
                                }else{
                                    $error[]='No se pudo guardar el usuario';
                                    $data_envio['error']=$error;
                                }
                            }else{
                                $error[]='No se pudo guardar conectar con integra (asignarfolios)';
                                $data_envio['error']=$error;
                            }
                        }
                    }else{
                        $error[]='No se pudo guardar la empresa';
                        $data_envio['error']=$error;
                    }
                }else{
                    $error[]='No se pudo guardar conectar con integra (registraremisor)';
                    $data_envio['error']=$error;
                }
            }else{
                $error[]='No se consiguio el afiliado de Fe soluciones';
                $data_envio['error']=$error;
            }
        }
        return $data_envio;
    }

    /**
     *
     * Permite pagar plan, creando todos los registros pertinente de un usuario tipo = 2
     */
    function renovarplan($ObjOrden,$metodopago,$arraymetodopago){
        $error=array();
        DB::beginTransaction();
        //COMISION A PAGAR
            $comisionestablecimiento=(float)env('COMISIONESTABLECIMIENDO');
            if($comisionestablecimiento==0){
                $comisionestablecimiento=200;
            }
            $comisiongenerico=(float)env('COMISIONCODIGOGENERICO');
            if($comisiongenerico==0){
                $comisiongenerico=200;
            }
            $comisionpadre=(float)env('COMISIONPADRE');
            if($comisionpadre==0){
                $comisionpadre=100;
            }

        $data_envio['error']=0;

        //CREAR REMISION
        //CREAR DETALLEREMISION
        //CONFIRMAR ORDEN
        //ASIGNAR CODIGO PROMOCION
        //ENVIAR CORREO

        if($ObjOrden->ambito==0){
            //SANDBOX
            $pruebasF = 1;
            $pruebasN = 1;
        }else{
            //PRODUCCION
            $pruebasF = 0;
            $pruebasN = 0;
        }


        //CREAR REMISION
        $notaTicket='Fe'.substr( md5(microtime()), 1, 8);
        $fechahoy=date("Y-m-d");

        $remision= new Remision;
        $remision->idempresa = (int)env('IDEMPRESA');
        $remision->noticket = $notaTicket;
        $remision->numerocliente = '';
        $remision->nombrecliente = '';
        $remision->idsucursal =(int)env('IDSUCURSAL');
        $remision->fecha = $fechahoy;
        $remision->subtotal = $ObjOrden->subtotal;
        $remision->ieps = 0;
        $remision->iva = $ObjOrden->iva_monto;
        $remision->total = $ObjOrden->total;

        //BUSCANDO NOMBRE DE LA FORMA DE PAGO=
            $nombreformapago='';
            $idformapago  = (int)env('IDFORMAPAGODEFAULT');
            $ObjFormapago = Formapago::where("slug", "=", $arraymetodopago['payment_type'])->first();
            if(!empty($ObjFormapago)){
                $nombreformapago  = $ObjFormapago->nombre;
                $idformapago = $ObjFormapago->id;
            }else{
                $ObjFormapago = Formapago::where("id", "=", (int)env('IDFORMAPAGODEFAULT'))->first();
                if(!empty($ObjFormapago)){
                    $nombreformapago  = $ObjFormapago->nombre;
                    $idformapago = $ObjFormapago->id;
                }
            }

        $remision->idformapago  = $idformapago;
        $remision->formapago = $nombreformapago;
        $remision->metodopago = "";
        $remision->numcuenta = "";
        $remision->condicionespago = "";
        $remision->fechavence = $fechahoy;
        $remision->estatus = "A";
        $remision->uuid = "";
        $remision->serie = "";
        $remision->folio = 0;
        $remision->razoncancelacion = "";
        $remision->razoncancelacionadmin = "";
        $remision->saldo = 0;
        $remision->idconciliacion = 0;

        if($remision->save()){
            // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision);
            //CREAR DETALLEREMISION
            foreach ($ObjOrden->ordenesdetalle as $ObjOrdenesdetalle) {
                $detallef= new Remisiondetalle;
                $detallef->idempresa = $remision->idempresa;
                $detallef->idremision = $remision->id;

                //BUSCAR EL CONCEPTO DEL SERVICO DEL DETALLE DE LA ORDEN
                    if($ObjOrdenesdetalle->tipo==2){
                        //SUBSCRIPCION
                        $idconcepto=(int)env('IDCONCEPTOSUBSCRIPCION');
                        //AUMENTAR UN TIMPO LA FECHA DE VENCIMIENTO DEL USUARIO
                        $ObjUser = User::where("id", "=",$ObjOrden->iduser)->first();
                        if(!empty($ObjUser)){
                            $ObjEmpresa = Empresa::where("id", "=",$ObjUser->idempresa)->first();
                            if(!empty($ObjEmpresa)){
                                $ObjServicio = Servicio::where("id", "=",$ObjOrdenesdetalle->idservicio)->first();
                                if(!empty($ObjServicio)){
                                    $duracion = $ObjServicio->duracion;
                                    $fecha = $ObjEmpresa->fechavigencia;
                                    $nuevafecha = strtotime ( '+ '.(int)$duracion.' month' , strtotime ( $fecha ) ) ;
                                    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                                    $ObjEmpresa->fechavigencia = $nuevafecha;
                                    if(!$ObjEmpresa->save()){
                                        $error[]='No pudo actualizar la fecha de vigencia de la empresa';
                                    }
                                }else{
                                    $error[]='No se consiguio el servicio de subscripcion para actualizar fecha';
                                }
                            }else{
                                $error[]='No se consiguio la empresa';
                            }
                        }else{
                            $error[]='No se consiguio el usuario';
                        }
                    }else{
                        //PAQUETE DE FOLIO
                        $idconcepto=(int)env('IDCONCEPTOPAQUETE');
                        //AUMENTAR UN TIMPO LA FECHA DE VENCIMIENTO DEL USUARIO
                        $ObjUser = User::where("id", "=",$ObjOrden->iduser)->first();
                        if(!empty($ObjUser)){
                            $ObjEmpresa = Empresa::where("id", "=",$ObjUser->idempresa)->first();
                            if(!empty($ObjEmpresa)){
                                $ObjServicio = Servicio::where("id", "=",$ObjOrdenesdetalle->idservicio)->first();
                                if(!empty($ObjServicio)){
                                    $numfolios = (int)$ObjServicio->numfolios;
                                    $folioscomprados = (int)$ObjEmpresa->folioscomprados;
                                    $folioscomprados = $folioscomprados+$numfolios;
                                    $ObjEmpresa->folioscomprados = $folioscomprados;
                                    if(!$ObjEmpresa->save()){
                                        $error[]='No pudo actualizar los folios de la empresa';
                                    }
                                }else{
                                    $error[]='No se consiguio el servicio de subscripcion para actualizar fecha';
                                }
                            }else{
                                $error[]='No se consiguio la empresa';
                            }
                        }else{
                            $error[]='No se consiguio el usuario';
                        }
                    }
                    if(isset($ObjOrdenesdetalle->servicio->concepto->id)){
                        $idconcepto=$ObjOrdenesdetalle->servicio->concepto->id;
                    }
                    $ObjConcepto = Concepto::where("id", "=",$idconcepto)->first();

                $detallef->idconcepto = $ObjConcepto->id;
                $detallef->codigo =   $ObjConcepto->codigo;
                $detallef->descripcion = $ObjConcepto->concepto;
                $detallef->unidad =  $ObjConcepto->unidad;

                $detallef->codigounidadsat =  $ObjConcepto->codigounidadsat;
                $detallef->codigogrupoprodsersat =  $ObjConcepto->codigogrupoprodsersat;
                $detallef->cantidad = 1;
                $detallef->precio = $ObjOrdenesdetalle->subtotal;
                $detallef->importe = $ObjOrdenesdetalle->subtotal;
                $detallef->importeconimpuestos =$ObjOrdenesdetalle->total;
                $detallef->tasaiva = $ObjOrdenesdetalle->iva_porcentaje;
                $detallef->importeiva =  $ObjOrdenesdetalle->iva_monto;
                $detallef->series = "";
                $detallef->tasaieps = 0;
                $detallef->importeieps = 0;
                if(!$detallef->save()){
                    $error[] = 'No se pudo guardar el detalle de la remision';
                }
                // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision,$ObjOrdenesdetalle,$detallef);
            }

            if(count($error)==0){
                //CONFIRMAR ORDEN
                $ObjOrden->fechapagado=$fechahoy;
                $ObjOrden->estatus=2;

                if($metodopago==1){
                    //MERCADO PAGO
                    $ObjOrden->collection_id = $arraymetodopago['collection_id'];
                    $ObjOrden->collection_status = $arraymetodopago['collection_status'];
                    $ObjOrden->payment_id = $arraymetodopago['payment_id'];
                    $ObjOrden->status = $arraymetodopago['status'];
                    $ObjOrden->payment_type = $arraymetodopago['payment_type'];
                    $ObjOrden->merchant_order_id = $arraymetodopago['merchant_order_id'];
                    $ObjOrden->preference_id = $arraymetodopago['preference_id'];
                    $ObjOrden->site_id = $arraymetodopago['site_id'];
                    $ObjOrden->processing_mode = $arraymetodopago['processing_mode'];
                    $ObjOrden->merchant_account_id = $arraymetodopago['merchant_account_id'];
                }elseif($metodopago==2){
                    $ObjOrden->tracker = $arraymetodopago['tracker'];
                    $ObjOrden->fechapagado = $arraymetodopago['fechapagado'];
                }

                if($ObjOrden->save()){
                    if(count($error)==0){
                        DB::commit();
                        //ENVIAR CORREO
                        $urllogin= route('login');
                        $urlfacturar= route('facturar',[base64_encode($remision->id)]);
                        $data= array(
                            'login'=>$ObjUser->login,
                            'nombre'=>$ObjUser->nombre,
                            'urlfacturar'=>$urlfacturar,
                        );
                        // dd($ObjAfiliado,$ArrayEmpresa,$empresa,$ObjUser,$remision,$ObjOrden,$objCodigopromocion,$data);
                        Mail::to($ObjUser->email)->send(new Renovacionestablecimiento($data));

                        if($metodopago==1){
                            //MERCADO PAGO
                            $data_envio['error']=0;
                            $data_envio['iduser']=$ObjUser->id;
                            $data_envio['b64_idremision']=base64_encode($remision->id);
                        }elseif($metodopago==2){
                            $data_envio['error']=0;
                        }
                    }else{
                        DB::rollback();
                    }
                }else{
                    DB::rollback();
                    $error[]='No se pudo actualizar la orden';
                    $data_envio['error']=$error;
                }
            }else{
                DB::rollback();
                $data_envio['error']=$error;
            }
        }else{
            DB::rollback();
            $error[]='No se pudo guardar la remisión';
            $data_envio['error']=$error;
        }
        return $data_envio;
    }
}
