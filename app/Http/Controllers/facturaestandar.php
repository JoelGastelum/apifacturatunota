<?php
/***
Documento de formato de impresion estandar para todas las empresas emisoras
***/

use App\Http\Controllers\fpdf;


class PDFX extends FPDF {

	var $widths;
	var $aligns;

	function num2letras($num, $fem = false, $dec = false)
	{
		//if (strlen($num) > 14) die("El número introducido es demasiado grande");
		$matuni[2]  = "dos";   $matuni[3]  = "tres";   $matuni[4]  = "cuatro";   $matuni[5]  = "cinco";
		$matuni[6]  = "seis";   $matuni[7]  = "siete";   $matuni[8]  = "ocho";   $matuni[9]  = "nueve";
		$matuni[10] = "diez";   $matuni[11] = "once";   $matuni[12] = "doce";   $matuni[13] = "trece";
		$matuni[14] = "catorce";   $matuni[15] = "quince";   $matuni[16] = "dieciseis";   $matuni[17] = "diecisiete";
		$matuni[18] = "dieciocho";   $matuni[19] = "diecinueve";   $matuni[20] = "veinte";
		$matunisub[2] = "dos";   $matunisub[3] = "tres";   $matunisub[4] = "cuatro";   $matunisub[5] = "quin";
		$matunisub[6] = "seis";   $matunisub[7] = "sete";   $matunisub[8] = "ocho";   $matunisub[9] = "nove";
		$matdec[2] = "veint";   $matdec[3] = "treinta";   $matdec[4] = "cuarenta";   $matdec[5] = "cincuenta";
		$matdec[6] = "sesenta";   $matdec[7] = "setenta";   $matdec[8] = "ochenta";   $matdec[9] = "noventa";
		$matsub[3]  = 'mill';   $matsub[5]  = 'bill';     $matsub[7]  = 'mill';   $matsub[9]  = 'trill';
		$matsub[11] = 'mill';   $matsub[13] = 'bill';   $matsub[15] = 'mill';   $matmil[4]  = 'millones';
		$matmil[6]  = 'billones';   $matmil[7]  = 'de billones';     $matmil[8]  = 'millones de billones';
		$matmil[10] = 'trillones';   $matmil[11] = 'de trillones';   $matmil[12] = 'millones de trillones';
		$matmil[13] = 'de trillones';   $matmil[14] = 'billones de trillones';   $matmil[15] = 'de billones de trillones';
		$matmil[16] = 'millones de billones de trillones';
		$num = trim((string)@$num);
		if ($num[0] == '-') {
			$neg = 'menos ';
			$num = substr($num, 1);
		}else
			$neg = '';
		while ($num[0] == '0') $num = substr($num, 1);
		if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num;
		$zeros = true;
		$punt = false;
		$ent = '';
		$fra = '';
		for ($c = 0; $c < strlen($num); $c++) {
			$n = $num[$c];
			if (! (strpos(".,'''", $n) === false)) {
				if ($punt) break;
				else{
					$punt = true;
					continue;
				}
			}elseif (! (strpos('0123456789', $n) === false)) {
				if ($punt) {
					if ($n != '0') $zeros = false;
					$fra .= $n;
				}else
					$ent .= $n;
			}else
				break;
		}

		$ent = '     ' . $ent;

		if ($dec and $fra and ! $zeros) {
			$fin = ' coma';
			for ($n = 0; $n < strlen($fra); $n++) {
				if (($s = $fra[$n]) == '0')
					$fin .= ' cero';
				elseif ($s == '1')
					$fin .= $fem ? ' una' : ' un';
				else
					$fin .= ' ' . $matuni[$s];
			}
		}else
			$fin = '';
		if ((int)$ent === 0) return 'Cero ' . $fin;
		$tex = '';
		$sub = 0;
		$mils = 0;
		$neutro = false;

		while ( ($num = substr($ent, -3)) != '   ') {

			$ent = substr($ent, 0, -3);
			if (++$sub < 3 and $fem) {
				$matuni[1] = 'un';
				$subcent = 'os';
			}else{
				$matuni[1] = $neutro ? 'un' : 'uno';
				$subcent = 'os';
			}
			$t = '';
			$n2 = substr($num, 1);
			if ($n2 == '00') {
			}elseif ($n2 < 21)
			$t = ' ' . $matuni[(int)$n2];
			elseif ($n2 < 30) {
				$n3 = $num[2];
				if ($n3 != 0) $t = 'i' . $matuni[$n3];
				$n2 = $num[1];
				$t = ' ' . $matdec[$n2] . $t;
			}else{
				$n3 = $num[2];
				if ($n3 != 0) $t = ' y ' . $matuni[$n3];
				$n2 = $num[1];
				$t = ' ' . $matdec[$n2] . $t;
			}

			$n = $num[0];
			if ($n == 1) {
				if($n2=="00")
					$t = ' cien' . $t;
				else
					$t = ' ciento' . $t;
				//$t = ' ciento' . $t;
			}elseif ($n == 5){
				$t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
			}elseif ($n != 0){
				$t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
			}

			if ($sub == 1) {
			}elseif (! isset($matsub[$sub])) {
				if ($num == 1) {
					$t = ' mil';
				}elseif ($num > 1){
					$t .= ' mil';
				}
			}elseif ($num == 1) {
				$t .= ' ' . $matsub[$sub] . 'on';
			}elseif ($num > 1){
				$t .= ' ' . $matsub[$sub] . 'ones';
			}
			if ($num == '000') $mils ++;
			elseif ($mils != 0) {
				if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub];
				$mils = 0;
			}
			$neutro = true;
			$tex = $t . $tex;
		}
		$tex = $neg . substr($tex, 1) . $fin;
		return ucfirst($tex);
	}


	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=4*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'J';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			//$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,4,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
}


function generaFormato($documento,$datos){

	//Generar el PDF
	//CARGANDO LOGO
	if ($documento["colorfondo"]['r']==0 && $documento["colorfondo"]['b']==0 && $documento["colorfondo"]['g']==0) {
		//POR DEFAULT
		$r=238;
		$g=238;
		$b=238;
		$rl=0;
		$gl=0;
		$bl=0;
	}else{
		$r=$documento["colorfondo"]['r'];
		$g=$documento["colorfondo"]['g'];
		$b=$documento["colorfondo"]['b'];
		$rl=$documento["colorletra"]['r'];
		$gl=$documento["colorletra"]['g'];
		$bl=$documento["colorletra"]['b'];
	}


	$decimalescantidad=2;
	$bancocte='';
	$diascreditocte=0;

	$pdf = new PDFX();
	$pdf->AddPage();
	$pdf->AliasNbPages();
	$pdf->SetTopMargin(10);
	$pdf->SetFillColor($r,$g,$b);
	$pdf->SetDrawColor(0,0,0);
	$pdf->SetFont('courier','',8);
	$tot=number_format($documento['total'],2,".","");
	$IMPORTELETRAS="(SON:".strtoupper($pdf->num2letras(intval($tot))).$documento['moneda'].')';
	$moneda=$documento['moneda'];
	$domicilioemisor=$datos['Emisor']['Domicilio']['calle'];
	if($datos['Emisor']['Domicilio']['noExterior']!="")
		$domicilioemisor.=" {$datos['Emisor']['Domicilio']['noExterior']}";
	if($datos['Emisor']['Domicilio']['noInterior']!="")
		$domicilioemisor.=" INT. {$datos['Emisor']['Domicilio']['noInterior']}";
	if($datos['Emisor']['Domicilio']['colonia']!="")
		$domicilioemisor.=" {$datos['Emisor']['Domicilio']['colonia']}";
	$cpemisor=" C.P. {$datos['Emisor']['Domicilio']['codigoPostal']}";

	$ciudademisor=$datos['Emisor']['Domicilio']['localidad'];
	if($datos['Emisor']['Domicilio']['municipio']!="" && $datos['Emisor']['Domicilio']['localidad']!=$datos['Emisor']['Domicilio']['municipio'])
		$ciudademisor.=", {$datos['Emisor']['Domicilio']['municipio']}";
	if($datos['Emisor']['Domicilio']['estado']!="")
		$ciudademisor.=", {$datos['Emisor']['Domicilio']['estado']}";
	if($datos['Emisor']['Domicilio']['pais']!="")
		$ciudademisor.=", {$datos['Emisor']['Domicilio']['pais']}";

	$domicilioreceptor="";
	$domicilioreceptor=$documento['receptor']['calle'];
	if(isset($documento['receptor']['num_ext'])!="")
		$domicilioreceptor.=" {$documento['receptor']['num_ext']}";
	if(isset($documento['receptor']['num_int'])!="")
		$domicilioreceptor.=" int. {$documento['receptor']['num_int']}";
	if(isset($documento['receptor']['colonia'])!="")
		$domicilioreceptor.=" {$documento['receptor']['colonia']}";
	$domicilioreceptor.=" C.P. {$documento['receptor']['codigopostal']}";
	if(!isset($documento['receptor']['pais'])||$documento['receptor']['pais']=='')
		$documento['receptor']['pais']='MEXICO';
	$ciudadreceptor=$documento['receptor']['localidad'].', '.$documento['receptor']['municipio'].', '.$documento['receptor']['estado'].', '.$documento['receptor']['pais'];

	//Tomar los regimenes
	$regimen="";
	/*foreach($datos['regimen'] as $lregimen){
		if($regimen!='')
			$regimen.="\n";
		$regimen.=$lregimen;
	}*/

	if (isset($documento['logo'])) {
		if ($documento['logo']!=null) {
			$pdf->image($documento['logo'],$documento['xinicial'],$documento['yinicial'],$documento['largo'],$documento['alto']);
		}
	}

	$pdf->Cell(110,4,'',0,0,'L',0);
	$pdf->Ln(); //Siguiente Linea
	$y=$pdf->GetY();
	$pdf->Ln(); //Siguiente Linea
	$y2=$pdf->GetY();


	//Pintar la parte Derecha
	$pdf->SetY($y);
	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(110,4,'',0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	if($documento['tipodocumento']=="I")
		if($documento['id']==934)
			$pdf->Cell(87,6,'FACTURA ELECTRONICA',1,0,'C',true);
		else
			$pdf->Cell(87,6,'FACTURA',1,0,'C',true);
	else
		$pdf->Cell(82,6,'NOTA DE CREDITO',1,0,'C',true);

	$pdf->SetTextColor(0,0,0);
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,"",0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	$pdf->Cell(82,4,$documento['UUID'],0,0,'C');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,"",0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	$pdf->Cell(32,4,'SERIE-FOLIO',0,0,'L');
	$pdf->Cell(50,4,':'.$documento['serie'].$documento['folio'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,'',0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	$pdf->Cell(32,4,'FECHA EMISION',0,0,'L');
	$pdf->Cell(50,4,':'.date('d-m-Y H:i:s',strtotime($documento['fecha'])),0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,'',0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	$pdf->Cell(32,4,'FECHA TIMBRE',0,0,'L');
	$pdf->Cell(50,4,':'.date('d-m-Y H:i:s',strtotime($documento["FechaTimbrado"])),0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,'',0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	$pdf->Cell(32,4,'CERTIFICADO EMISOR',0,0,'L');
	$pdf->Cell(50,4,':'.$documento['nocertificado'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,'',0,0,'L',0);//Solo se deja para que la siguiente celda se posicione adelante
	$pdf->Cell(32,4,'CERTIFICADO SAT',0,0,'L');
	$pdf->Cell(50,4,':'.$documento['noCertificadoSAT'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(110,4,'',0,0,'L'); //Se deja para que salte a la siguiente posicion de la celda
	$pdf->Cell(32,4,'METODO DE PAGO',0,0,'L');
	$pdf->SetFont('courier','',6);
	$pdf->Cell(50,4,':'.$documento['metodopago'],0,0,'L');
	$pdf->SetFont('courier','',8);
	$pdf->Ln(); //Siguiente Linea



	if($datos['NumCtaPago']!=""){
		$pdf->Cell(110,4,'',0,0,'L'); //Se deja para que salte a la siguiente posicion de la celda
		$pdf->Cell(32,4,'NUMERO DE CUENTA',0,0,'L');
		$pdf->Cell(50,4,':'.$datos['NumCtaPago'],0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	$pdf->Cell(110,4,'',0,0,'L'); //Se deja para que salte a la siguiente posicion de la celda
	$pdf->Cell(32,4,'FORMA DE PAGO',0,0,'L');
	$pdf->Cell(50,4,':'.$documento['formapago'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	if($documento['condicionesDePago']!=''&&$datos['id']!=2915)
	{
		$pdf->Cell(110,4,'',0,0,'L'); //Se deja para que salte a la siguiente posicion de la celda
		$pdf->Cell(32,4,'CONDICIONES DE PAGO',0,0,'L');
		$pdf->Cell(40,4,':'.$documento['condicionesDePago'],0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	if($datos['id']==182)
	{
		$pdf->Cell(110,4,'',0,0,'L'); //Se deja para que salte a la siguiente posicion de la celda
		$pdf->Cell(72,4,'EFECTOS FISCALES AL PAGO',0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	if($documento['fechavence']!='')
	{
		$pdf->Cell(110,4,'',0,0,'L'); //Se deja para que salte a la siguiente posicion de la celda
		$pdf->Cell(32,4,'FECHA VENCIMIENTO',0,0,'L');
		$pdf->Cell(40,4,':'.$documento['fechavence'],0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	$yderecha=$pdf->GetY();
	//Ahora pintar la parte Izquierda
	$pdf->SetY($yderecha);

	$pdf->MultiCell(110,3,$documento['Emisor']['nombre'],0,'L');
	$pdf->Cell(110,4,$documento['Emisor']['rfc'],0,0,'L',0);
	$pdf->Ln(); //Siguiente Linea

	/*if($datos['ocultar_domicilio']!=1)
	{
		$pdf->MultiCell(110,3,$domicilioemisor.$cpemisor,0,'L');
		$pdf->Cell(110,4,$ciudademisor,0,0,'L',0);
	}
	$pdf->Ln(); //Siguiente Linea
	if($mostrartel==1)
	{
		$pdf->Cell(110,4,$telefono,0,0,'L',0);
		$pdf->Ln(); //Siguiente Linea
	}
	if($mostraremail==1)
	{
		$pdf->Cell(110,4,$email,0,0,'L',0);
		$pdf->Ln(); //Siguiente Linea
	}*/
	$pdf->MultiCell(110,3,'Regimen Fiscal:'.$documento['regimenfiscal'],0,'L');
	$pdf->MultiCell(200,3,'Uso Cfdi:'.$documento['receptor']['usodelcomprobante'],0,'L');
	if($pdf->GetY()<$yderecha)
		$pdf->SetY($yderecha);
	$pdf->Cell(192,4,'Lugar Expedicion:'.$documento['LugarExpedicion'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	if($datos['ocultar_expedidoen']!=1)
	{
		if(isset($datos['Emisor']['ExpedidoEn']))
		{
			$domicilio_expedicion = "Expedido en:{$datos['Emisor']['ExpedidoEn']['calle']} {$datos['Emisor']['ExpedidoEn']['noExterior']} {$datos['Emisor']['ExpedidoEn']['noInterior']} {$datos['Emisor']['ExpedidoEn']['colonia']} C.P. {$datos['Emisor']['ExpedidoEn']['codigoPostal']}, {$datos['Emisor']['ExpedidoEn']['localidad']}, {$datos['Emisor']['ExpedidoEn']['municipio']}, {$datos['Emisor']['ExpedidoEn']['estado']}, {$datos['Emisor']['ExpedidoEn']['pais']} ";
			$pdf->MultiCell(192,3,$domicilio_expedicion,0,"L");
		}
	}

	/*
	if($rowemisor['dato1']!='')
	{
		$pdf->Cell(192,4,$rowemisor['dato1'],0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	if($rowemisor['dato2']!='')
	{
		$pdf->Cell(192,4,$rowemisor['dato2'],0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	if($rowemisor['dato3']!='')
	{
		$pdf->Cell(192,4,$rowemisor['dato3'],0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}*/

	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(197,6,'DATOS DEL RECEPTOR',1,0,'C',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(192,4,$documento['receptor']['nombre'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	if($documento['receptor']['rfc']=='XEXX010101000')
		$pdf->Cell(192,4,$documento['receptor']['rfc'].'  Num Reg. Tributario:'.$documento['receptor']['registrotributario'],0,0,'L');
	else
		$pdf->Cell(192,4,$documento['receptor']['rfc'],0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	$pdf->MultiCell(192,3,$domicilioreceptor,0,'L');
	//$pdf->Cell(192,4,$domicilioreceptor,0,0,'L');
	//$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(192,4,$ciudadreceptor,0,0,'L');
	$pdf->Ln(); //Siguiente Linea
	if($bancocte!='')
	{
		$pdf->Cell(192,4,'Banco:'.$bancocte,0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}
	if($diascreditocte>0)
	{
		$pdf->Cell(192,4,'Dias de Credito:'.$diascreditocte,0,0,'L');
		$pdf->Ln(); //Siguiente Linea
	}

	$sizeofdetalle=8;
	//determinar el color del fondo
/*	$query="Select * from columnasxformato  where idemisor={$datos['id']} order by orden";
	$res=mysql_db_query($BaseDeDatos,$query);
	if(mysql_num_rows($res)>0)
	{
		$acampos=array(	'CODIGO'=>'codigo',
						'UNIDAD'=>'unidad',
						'CONCEPTO'=>'descripcion',
						'CANTIDAD'=>'cantidad',
						'PRECIO'=>'valorUnitario',
						'IMPORTE'=>'importe',
						'TASAIVA'=>'tasaiva',
						'IMPORTEIVA'=>'importeiva',
						'TASAIEPS'=>'tasaieps',
						'IMPORTEIEPS'=>'importeieps');
		$atitulos=array('CODIGO'=>'Codigo',
						'UNIDAD'=>'Unidad',
						'CONCEPTO'=>'Concepto',
						'CANTIDAD'=>'Cantidad',
						'PRECIO'=>'Precio',
						'IMPORTE'=>'Importe',
						'TASAIVA'=>'% IVA',
						'IMPORTEIVA'=>'IVA',
						'TASAIEPS'=>'% IEPS',
						'IMPORTEIEPS'=>'IEPS');
		$aalineacion=array(	'CODIGO'=>'L',
							'UNIDAD'=>'L',
							'CONCEPTO'=>'L',
							'CANTIDAD'=>'R',
							'PRECIO'=>'R',
							'IMPORTE'=>'R',
							'TASAIVA'=>'R',
							'IMPORTEIVA'=>'R',
							'TASAIEPS'=>'R',
							'IMPORTEIEPS'=>'R');
		$atipos=array(	'CODIGO'=>'C',
						'UNIDAD'=>'C',
						'CONCEPTO'=>'C',
						'CANTIDAD'=>'N',
						'PRECIO'=>'N',
						'IMPORTE'=>'N',
						'TASAIVA'=>'N',
						'IMPORTEIVA'=>'N',
						'TASAIEPS'=>'N',
						'IMPORTEIEPS'=>'N');
		$adecimales=array(	'CODIGO'=>0,
							'UNIDAD'=>0,
							'CONCEPTO'=>0,
							'CANTIDAD'=>$decimalescantidad,
							'PRECIO'=>$datos['Emisor']['numero_decimales'],
							'IMPORTE'=>$datos['Emisor']['numero_decimales'],
							'TASAIVA'=>$datos['Emisor']['numero_decimales'],
							'IMPORTEIVA'=>$datos['Emisor']['numero_decimales'],
							'TASAIEPS'=>$datos['Emisor']['numero_decimales'],
							'IMPORTEIEPS'=>$datos['Emisor']['numero_decimales']);
		$awidths=array();
		$aaligns=array();
		$acolumnas=array();
		while($row=mysql_fetch_array($res))
		{
			$acolumnas[]=array(
				'titulo'=>$atitulos[$row['columna']],
				'campo'=>$acampos[$row['columna']],
				'tipo'=>$atipos[$row['columna']],
				'decimales'=>$adecimales[$row['columna']],
				'ancho'=>$row['ancho']
			);
			$awidths[]=$row['ancho'];
			$aaligns[]=$aalineacion[$row['columna']];
		}
	}
	else
	{*/
		$awidths=array(17,20,80,20,30,30);
		$aaligns=array("L","L","L","R","R","R");
		$acolumnas=array(
			array('titulo'=>'UNIDAD',	'campo'=>'unidad',			'tipo'=>'C',	'decimales'=>0,										'ancho'=>15),
			array('titulo'=>'CPSSAT',	'campo'=>'clave',			'tipo'=>'C',	'decimales'=>0,										'ancho'=>20),
			array('titulo'=>'CONCEPTO',	'campo'=>'descripcion',		'tipo'=>'C',	'decimales'=>0,										'ancho'=>82),
			array('titulo'=>'CANTIDAD',	'campo'=>'cantidad',		'tipo'=>'N',	'decimales'=>2,					'ancho'=>20),
			array('titulo'=>'PRECIO',	'campo'=>'valorUnitario',	'tipo'=>'N',	'decimales'=>2,	'ancho'=>30),
			array('titulo'=>'IMPORTE',	'campo'=>'importe',			'tipo'=>'N',	'decimales'=>2,	'ancho'=>30)
		);
	//}
	# Cabecero
	$pdf->SetWidths($awidths);
	$pdf->SetAligns($aaligns);
	$pdf->SetFont('courier','',$sizeofdetalle);
	$pdf->SetTextColor($rl,$gl,$bl);
	foreach($acolumnas as $columna)
		$pdf->Cell($columna['ancho'],6,$columna['titulo'],1,0,'C',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Ln(); //Siguiente Linea
	for($i=0;$i<count($documento['conceptos']);$i++){
		$arrow=array();
		foreach($acolumnas as $columna)
		{
			if($columna['tipo']=='N')
				$arrow[]=number_format($documento['conceptos'][$i][$columna['campo']],$columna['decimales']);
			else
				$arrow[]=$documento['conceptos'][$i][$columna['campo']];
		}
		$pdf->Row($arrow);
	}

	$importeiva=0;
	$importeiesp=0;
	$importeisr=0;
	$importeivaret=0;
	$importeiespret=0;
	foreach ($documento['traslados']['impuestostrasladados'] as $traslado) {
		if($traslado['impuesto']=='IVA'){
			$importeiva+=$traslado['importe'];
		}else{
			$importeiesp+=$traslado['importe'];
		}
	}
	foreach ($documento['retenciones']['impuestosretenidos'] as $retenido) {
		if($retenido['impuesto']=='IVA'){
			$importeivaret+=$retenido['importe'];
		}elseif($retenido['impuesto']=='IEPS'){
				$importeiespret+=$retenido['importe'];
		}else{
			$importeisr+=$retenido['importe'];
		}
	}


	$pdf->Ln(); //Siguiente Linea
	$pdf->SetFont('courier','',8);
	$pdf->Cell(130,6,'',"T",0,'L');
	$y=$pdf->GetY()+2;   //Se pone Y para regresar a poner el valor con letra
	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(32,6,'SUBTOTAL ',1,0,'R',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(30,6,number_format($documento['subtotal'],2),"T",0,'R');
	$pdf->Ln(); //Siguiente Linea
	$pdf->Cell(130,6,'',0,0,'L');
	$pdf->SetTextColor($rl,$gl,$bl);
	if($documento['tasadescuento']>0)
		$pdf->Cell(32,6,'DESCUENTO '.$documento['tasadescuento'].'%',1,0,'R',true);
	else
		$pdf->Cell(32,6,'DESCUENTO ',1,0,'R',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(30,6,number_format($documento['descuento'],2),0,0,'R');
	$pdf->Ln(); //Siguiente Linea
	if($importeiesp>0)
	{
		$pdf->Cell(130,6,'',0,0,'L');
		$pdf->SetTextColor($rl,$gl,$bl);
		$pdf->Cell(32,6,'Hospedaje ',1,0,'R',true);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(30,6,number_format($importeiesp,2),0,0,'R');
		$pdf->Ln(); //Siguiente Linea
	}
	$pdf->Cell(130,6,'',0,0,'L');
	$pdf->SetTextColor($rl,$gl,$bl);
	$iva="";
	$pdf->Cell(32,6,'I.V.A.',1,0,'R',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(30,6,number_format($importeiva,2),0,0,'R');
	$pdf->Ln(); //Siguiente Linea
	if($importeivaret>0)
	{
		$pdf->Cell(130,6,'',0,0,'L');
		$pdf->SetTextColor($rl,$gl,$bl);
		$pdf->Cell(32,6,'I.V.A. RETENIDO',1,0,'R',true);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(30,6,number_format($importeivaret,2),0,0,'R');
		$pdf->Ln(); //Siguiente Linea
	}
	if($importeisr>0)
	{
		$pdf->Cell(130,6,'',0,0,'L');
		$pdf->SetTextColor($rl,$gl,$bl);
		$pdf->Cell(32,6,'I.S.R. RETENIDO',1,0,'R',true);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(30,6,number_format($importeisr,2),0,0,'R');
		$pdf->Ln(); //Siguiente Linea
	}
	/*if(isset($documento['impuestoslocales'])){
		$totaux=$datos['total'];
		$hayretenciones=false;
		$haytraslados=false;
		$tott=0;
		foreach($documento['impuestoslocales'] as $impuesto){
			if($impuesto['tipo']=="retensiones"){
				$totaux+=$impuesto['importe'];
				$hayretenciones=true;
			}
			else{
				$haytraslados=true;
				$tott+=$impuesto['importe'];
			}
		}
		if($haytraslados){
			$pdf->Cell(130,6,'',0,0,'L');
			$pdf->SetTextColor($rl,$gl,$bl);
			$pdf->Cell(32,6,'ISH ',1,0,'R',true);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(30,6,number_format($tott,2),0,0,'R');
			$pdf->Ln(); //Siguiente Linea
			$pdf->Cell(130,6,'',0,0,'L');
			$pdf->SetTextColor($rl,$gl,$bl);
			$pdf->Cell(32,6,'TOTAL ',1,0,'R',true);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(30,6,number_format($totaux,2),0,0,'R');
			$pdf->Ln(); //Siguiente Linea
		}
		else{
			$pdf->Cell(130,6,'',0,0,'L');
			$pdf->SetTextColor($rl,$gl,$bl);
			$pdf->Cell(32,6,'TOTAL ',1,0,'R',true);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(30,6,number_format($totaux,2),0,0,'R');
			$pdf->Ln(); //Siguiente Linea
			$pdf->Cell(130,6,'',0,0,'L');
			$pdf->SetTextColor($rl,$gl,$bl);
			$pdf->Cell(62,6,'DEDUCCIONES ',1,0,'C',true);
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln(); //Siguiente Linea
			foreach($documento['impuestoslocales'] as $impuesto)
				if($impuesto['tipo']=="retensiones"){
					$pdf->Cell(130,6,'',0,0,'L');
					$pdf->SetTextColor($rl,$gl,$bl);
					$pdf->Cell(32,6,$impuesto['descripcion'],1,0,'R',true);
					$pdf->SetTextColor(0,0,0);
					$pdf->Cell(30,6,number_format($impuesto['importe'],2),0,0,'R');
					$pdf->Ln(); //Siguiente Linea
				}
			$pdf->Cell(130,6,'',0,0,'L');
			$pdf->SetTextColor($rl,$gl,$bl);
			$pdf->Cell(32,6,'NETO A RECIBIR ',1,0,'R',true);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(30,6,number_format($datos['total'],2),0,0,'R');
		}
	}
	else*/

	$pdf->Cell(130,6,'',0,0,'L');
	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(32,6,'TOTAL ',1,0,'R',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(30,6,number_format($documento['total'],2),0,0,'R');

	$pdf->Ln(); //Siguiente Linea
	$pdf->Ln(); //Siguiente Linea
	$y2=$pdf->GetY();
	$pdf->SetY($y);
	$pdf->MultiCell(130,3,$IMPORTELETRAS,0,"L");
	$pdf->Ln();
	if(isset($documento['observaciones']))
		if($documento['observaciones']!='')
			$pdf->MultiCell(130,3,"Observaciones:".$documento['observaciones'],0,"L");

		$y3=$pdf->GetY();
		if($y3>$y2)
			$y2=$y3;

	$pdf->SetY($y2);
	$pdf->Cell(32,6,'',0,0,'C');
	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(160,6,'CADENA ORIGINAL',1,0,'C',true);
	$pdf->SetTextColor(0,0,0);
	$y2=$pdf->GetY();
	$pdf->Ln(); //Siguiente Linea
	$y=$pdf->GetY();
	$pdf->SetY($y2);
//	$pdf->Image($documento['cbb'],$pdf->GetX(),$pdf->GetY(),30);
	$pdf->SetY($y);
	$pdf->SetFont('courier','',6);
	$pdf->Cell(32,3,'',0,0,"C");
	$pdf->MultiCell(160,3,$documento['cadena_original'],0,"L");
	$pdf->Ln(); //Siguiente Linea
	$pdf->SetFont('courier','',9);
	$pdf->Cell(32,6,'',0,0,'C');
	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(160,6,'SELLO DIGITAL EMISOR',1,0,'C',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Ln(); //Siguiente Linea
	$pdf->SetFont('courier','',6);
	$pdf->Cell(32,6,'',0,0,'C');
	$pdf->MultiCell(160,3,$documento['sello'],0,"L");
	$pdf->Ln(); //Siguiente Linea
	$pdf->SetFont('courier','',9);
	$pdf->Cell(32,6,'',0,0,'C');
	$pdf->SetTextColor($rl,$gl,$bl);
	$pdf->Cell(160,6,'SELLO DIGITAL SAT',1,0,'C',true);
	$pdf->SetTextColor(0,0,0);
	$pdf->Ln(); //Siguiente Linea
	$pdf->SetFont('courier','',6);
	$pdf->Cell(32,6,'',0,0,'C');
	$pdf->MultiCell(160,3,$documento['selloSAT'],0,"L");
	$pdf->Ln(); //Siguiente Linea
	$pdf->SetFont('courier','',8);
	$pdf->Cell(192,6,utf8_decode('Este documento es una representacion impresa de un CFDI'),"T",0,'C');
	$pdf->Ln(); //Siguiente Linea (Se deja para regular la caida del ch)
	$pdf->Ln();//Siguiente Linea

	return 	$pdf;
}
