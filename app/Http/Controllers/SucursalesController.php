<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Sucursal;
use App\Serie;
use App\Certificado;

class SucursalesController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';

        $sql_where .= ' AND sucursales.idempresa = "'.auth()->user()->idempresa.'" AND sucursales.estatus = "A"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                sucursales.id,
                sucursales.nombre
            FROM sucursales
            '.$sql_where.'
            ORDER BY
                sucursales.nombre ASC
        ';
        $coleccion = DB::select($sql);

        foreach ($coleccion as $data) {
            $acciones='';
                $acciones.='<a href="'.route('sucursales.edit',$data->id).'" class="btn btn-sm btn-warning" title="Editar">
                    <i class="fas fa-edit"></i>
                </a>';
                $acciones.='&nbsp';
                $acciones.='<form method="POST" action="'.route('sucursales.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <i class="far fa-trash-alt"></i>
                </button>';
                $acciones.='</form>';
            $data->acciones=$acciones;
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR SucursalesController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'SucursalesController');

        return view('sucursales.index', [
            'arraybtn' => $arraybtn,
            'tituloencabezado'=>'Sucursales'
        ]);
    }

    /**
     * Permite crear una serie
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objSucursal = new Sucursal;

        //series
        $series = Serie::where("idempresa", "=", auth()->user()->idempresa)->where("estatus", "=", 'A')->pluck('serie','id');

        return view('sucursales.edit', [
            'objSucursal' => $objSucursal,
            'series' => $series,
            'tituloencabezado'=>'Sucursales'
        ]);
    }

    /**
     * Permite editar una
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objSucursal = Sucursal::where("id", "=", $id)->where("estatus", "=", 'A')->first();
        if(!empty($objSucursal)){
            //series
            $series = Serie::where("idempresa", "=", auth()->user()->idempresa)->where("estatus", "=", 'A')->pluck('serie','id');

            return view('sucursales.edit', [
                'objSucursal' => $objSucursal,
                'series' => $series,
                'tituloencabezado'=>'Sucursales'
            ]);
        }else{
            return redirect()->route('sucursales.index')->with('danger','Error: Sucursal no encontrada');
        }
    }

    /**
     * Permite actualizar los datos de una entidad del modelo en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        DB::beginTransaction();
        if($id==-1){
            $objSucursal = new Sucursal;
        }else{
            $objSucursal = Sucursal::where("id", "=", $id)->first();
        }

        $objSucursal->idempresa = auth()->user()->idempresa;
        $objSucursal->nombre = $request->nombre;
        $objSucursal->domicilio = $request->domicilio;
        $objSucursal->codigopostal = $request->codigopostal;
        $objSucursal->idserie = $request->idserie;
        $objSucursal->idserienc = $request->idserienc;
        $objSucursal->idseriepago = $request->idseriepago;
        $objSucursal->idserieremision = $request->idserieremision;

        if($objSucursal->save()){
            DB::commit();
            return redirect()->route('sucursales.index')->with('success','Se ha guardado los cambios con éxito');
        }else{
            DB::rollback();
            return redirect()->route('sucursales.index')->with('danger','No se pudo guardar los cambios');
        }
    }

    /**
     * Permite eliminar una sucursal
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objSucursal = Sucursal::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();
            //ELIMINACIÓN FISICA
            if(!$objSucursal->delete()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }else{
            DB::beginTransaction();
            //ELIMINACIÓN LOGICA
            $objSucursal->estatus = 'D';

            if(!$objSucursal->save()){
                DB::rollback();
                $typemsj='danger';
                $msj='No se ha podido eliminar';
            }else{
                DB::commit();
            }
        }
        return redirect()->route('sucursales.index')->with($typemsj,$msj);
    }
}
