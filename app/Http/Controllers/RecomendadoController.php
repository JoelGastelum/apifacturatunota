<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

use App\Recomendado;
use App\Usuario;
use App\Pago;
use App\Codigopromocion;
use App\User;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

class RecomendadoController extends Controller
{
    //RECOMENDANDO AMIGOS
        /**
         * Permite mostrar una lista de amigos que se han registrado con mi codigo
         *
         * @return \Illuminate\Http\Response
         */
        public function amigos()
        {
            return view('recomendados.amigos', [
                'tituloencabezado' => "Recomendar amigos",
                'rutaAnt'=>route('home')
            ]);
        }

        /**
         *
         * Permite mostrar el listado de usuario afiliados a la persona que inicio secion
         * @return \Illuminate\Http\Response
         */
        public function loaddata(Request $request){
            $data_envio['html']='';

            $request->fechainicio= str_replace("/","-",$request->fechainicio);
            $request->fechafin=str_replace("/","-",$request->fechafin);

            $fecha_actual = new DateTime($request->fechainicio);
            $cadena_fecha_actual = $fecha_actual->format("Y-m-d");


            $nuevafecha_actual = new DateTime($request->fechafin);
            $cadena_nuevafecha_actual = $nuevafecha_actual->format("Y-m-d");


            $fecha=$cadena_fecha_actual;
            $nuevafecha=$cadena_nuevafecha_actual;

            $sql="
                SELECT
                    id,
                    nombre,
                    login,
                    foto_perfil,
                    DATE_FORMAT(created_at,\"%d-%m-%Y\") as created_at,
                    ifnull(recomendadosconpagos.canti,0) AS cantiventas
                FROM usuarios
                LEFT JOIN (
                    SELECT
                        count(*) AS canti,
                        idusuario
                    FROM recomendados
                    WHERE
                        estatus IN ('C','P')
                    GROUP BY idusuario
                ) recomendadosconpagos ON recomendadosconpagos.idusuario= usuarios.id
                where
                    idusuariorecomendon=$request->idusuario
                    /*AND created_at between '$fecha' and '$nuevafecha'*/
                ORDER BY created_at DESC
            ";
            $recomendados=DB::select($sql);

            $html = '';
            if(count($recomendados)>0){
                foreach($recomendados as $recomendado){
                    $html .='<div class="row" style="border-bottom:solid 1px #e7eaf3;padding: 0.75rem;">';
                        $html .='<div class="col-12">';
                            $html .='<div class="row">';
                                $html .='<div class="col-6 align-self-center">';
                                    $html .='<img class="img-avatar img-fluid img-avatar-thumb" src="'.asset('storage/'.$recomendado->foto_perfil).'" alt="">';
                                    $html .='<br>';
                                    $html .='Cantidad de ventas <strong>'.$recomendado->cantiventas.'</strong>';
                                $html .='</div>';
                                $html .='<div class="col-6 align-self-center">';
                                    $html .='<div class="text-right">';
                                        $html .='<strong>'.$recomendado->login.'</strong>';
                                        $html .='<br>';
                                        $html .=$recomendado->nombre;
                                        $html .='<br>';
                                        $html .='<span style="font-size:12px;color:#4c5359d6;">Miembro desde '.$recomendado->created_at.'</span>';
                                    $html .='</div>';
                                $html .='</div>';
                            $html .='</div>';
                        $html .='</div>';
                    $html .='</div>';
                }
            }else{
                $html .='<div class="row" style="border-bottom:solid 1px #e7eaf3;padding: 0.75rem;">';
                    $html .='Sin amigos... Comparte tu código y haz amigo rápidamente';
                $html .='</div>';
            }
            $data_envio['html']=$html;

            echo json_encode($data_envio);
        }

        /**
         * Permite mostrar mis link publico para recomendar
         *
         * @return \Illuminate\Http\Response
         */
        public function mislinks()
        {
            $android=0;
            $navegador=$this->obtenerNavegadorWeb();
            if($navegador['isandroid']==1){
            $android=1;
            }
            $error=1;
            $imagen='';

            //CREANDO QR
            $user = User::where("id", "=", auth()->user()->id)->first();
            if(!empty($user)){
                $ruta= route('consumidor',[$user->codigopromocion]);
                // $ruta='https://mefactura.com/consumidor';
                // dd($ruta);

                $code=$ruta;

                //GUARDAR fisicamente
                $sitioguardarcomprobar='../storage/app/public/qr_recomendacion';
                if (!file_exists($sitioguardarcomprobar)) {
                    Storage::makeDirectory('public/qr_recomendacion');
                }
                $sitioguardar='../storage/app/public/qr_recomendacion/';

                $filename=$user->id.'-'.$user->codigopromocion;

                //VERIFICAR SI YA EXISTE EL ARCHIVO PARA NO CREARLO NUEVAMENTE
                if(!file_exists($sitioguardar.$filename.'.jpg')){
                    include(app_path('Librerias'). '/phpqrcode/qrlib.php');
                    $image = imagecreatefrompng($sitioguardar.$filename.".png");
                    imagejpeg($image, $sitioguardar.$filename.".jpg", 100);
                    unlink($sitioguardar.$filename.".png");
                }
                $imagen=asset('storage/qr_recomendacion/'.$filename.".jpg");

                return view('recomendados.mislinks', [
                    'tituloencabezado' => "Código de recomendado",
                    'rutaAnt'=>route('recomendados.amigos'),
                    'codigopromocion'=>$user->codigopromocion,
                    'ruta'=>$ruta,
                    'imagen'=>$imagen,
                    'isAndroid'=>$android
                ]);
            }else{
                return redirect()->route('contribuyentes')->with('danger','No se pudo crear QR');
            }
        }

        /**
         * Permite mostrar el codigo generico del vendedor
         *
         * @return \Illuminate\Http\Response
         */
        public function codigogenerico()
        {
            $android=0;
            $navegador=$this->obtenerNavegadorWeb();
            if($navegador['isandroid']==1){
            $android=1;
            }
            $error=1;
            $imagen='';

            //CREANDO QR
            $user = User::where("id", "=", auth()->user()->id)->first();
            $codigopromocion=Codigopromocion::where('idrecomendado','=',$user->id)
            ->where('tipo','=',1)->first();
            if(!empty($user)){
                $ruta= route('establecimiento',[$codigopromocion->codigo]);
                // $ruta='https://mefactura.com/consumidor';
                // dd($ruta);

                $code=$ruta;

                //GUARDAR fisicamente
                $sitioguardarcomprobar='../storage/app/public/qr_recomendacion';
                if (!file_exists($sitioguardarcomprobar)) {
                    Storage::makeDirectory('public/qr_recomendacion');
                }
                $sitioguardar='../storage/app/public/qr_recomendacion/';

                $filename=$user->id.'-'.$codigopromocion->codigo;

                //VERIFICAR SI YA EXISTE EL ARCHIVO PARA NO CREARLO NUEVAMENTE
                if(!file_exists($sitioguardar.$filename.'.jpg')){
                    include(app_path('Librerias'). '/phpqrcode/qrlib.php');
                    $image = imagecreatefrompng($sitioguardar.$filename.".png");
                    imagejpeg($image, $sitioguardar.$filename.".jpg", 100);
                    unlink($sitioguardar.$filename.".png");
                }
                $imagen=asset('storage/qr_recomendacion/'.$filename.".jpg");

                return view('recomendados.codigogenerico', [
                    'tituloencabezado' => "Código generico",
                    'rutaAnt'=>route('recomendados.amigos'),
                    'codigopromocion'=>$codigopromocion->codigo,
                    'ruta'=>$ruta,
                    'imagen'=>$imagen,
                    'isAndroid'=>$android
                ]);
            }else{
                return redirect()->route('contribuyentes')->with('danger','No se pudo crear QR');
            }
        }

        /**
         * Permite mostrar los banner publicitario
         *
         * @return \Illuminate\Http\Response
         */
        public function misbannergenerico()
        {
            $Codigopromocion = Codigopromocion::where("iddueno", "=", auth()->user()->id)->where("tipo", "=", 1)->first();
            if(!empty($Codigopromocion)){
                $ruta= route('establecimiento',[$Codigopromocion->codigo]);
                return view('recomendados.misbannergenerico', [
                    'tituloencabezado' => "Mis banners",
                    'rutaAnt'=>route('recomendados.codigogenerico'),
                    'ruta'=>$ruta
                ]);
            }
        }


        /**
         * Permite mostrar los banner publicitario
         *
         * @return \Illuminate\Http\Response
         */
        public function misbanner()
        {
            $user = User::where("id", "=", auth()->user()->id)->first();
            $ruta= route('consumidor',[$user->codigopromocion]);
            return view('recomendados.misbanner', [
                'tituloencabezado' => "Mis banners",
                'rutaAnt'=>route('recomendados.mislinks'),
                'ruta'=>$ruta
            ]);
        }

    //RECOMENDANDO ESTABLECIMIENTOS
        public function verfacturas(){
            $android=0;
            $navegador=$this->obtenerNavegadorWeb();
            if($navegador['isandroid']==1){
                $android=1;
            }

            $estatus_recomendados=$this->variablesglobales("estatus_recomendados");
            $estatus_recomendados['T']='Todos';

            $sqlEstatus=" AND recomendados.estatus != 'V'";
            $fecha=date("Y-m-")."01";
            $nuevafecha=date("Y-m-d");

            $sql="
                SELECT
                    recomendados.id,
                    if(
                        recomendados.idempresacomprador IS NULL,
                        recomendados.nombre,
                        empresas.nombrecomercial
                    ) AS nombre,
                    if( recomendados.fechaRec IS NULL,
                        DATE_FORMAT(recomendados.fechaComp,\"%d-%m-%Y\"),
                        DATE_FORMAT(recomendados.fechaRec,\"%d-%m-%Y\")
                    ) AS fecha,
                    recomendados.comision,
                    codigopromociones.codigo AS hexa,
                    recomendados.idrecomendado,
                    recomendados.estatus,
                    recomendados.direccion,
                    recomendados.telefono,
                    recomendados.personacontacto,
                    codigopromociones.fechavigencia AS fechaCorte,
                    if( recomendados.idrecomendado IS NOT NULL,
                        CONCAT('Comisión generada gracias a venta de ',userspadre.nombre),
                        -1
                    ) AS nombre_padre,
                    recomendados.estatus as estado
                FROM recomendados
                LEFT JOIN codigopromociones ON codigopromociones.idrecomendado = recomendados.id
                LEFT JOIN usuarios ON recomendados.idusuario = usuarios.id
                LEFT JOIN empresas ON empresas.id = recomendados.idempresacomprador
                LEFT JOIN recomendados recomendadospadre ON recomendadospadre.id = recomendados.idrecomendado
                LEFT JOIN usuarios userspadre ON recomendadospadre.idusuario = userspadre.id
                where
                    (
                        recomendados.fechaRec IS NOT NULL
                        AND recomendados.idusuario = ".auth()->user()->id."
                        ".$sqlEstatus."
                        AND recomendados.fechaRec between '".$fecha."' and '".$nuevafecha."'
                    )OR
                    (
                        recomendados.fechaRec IS NULL
                        AND recomendados.idusuario = ".auth()->user()->id."
                        ".$sqlEstatus."
                        AND recomendados.fechaComp between '".$fecha."' and '".$nuevafecha."'
                    )
                order by recomendados.id desc
            ";
            $recomendados=DB::select($sql);

            return view('recomendados.index', [
                'tituloencabezado' => "Mis recomendados",
                'estatus_recomendados'=>$estatus_recomendados,
                'recomendados'=>$recomendados,
                'isAndroid'=>$android
            ]);

        }

        public function buscarRecomendados(Request $request){
            $sqlEstatus=" AND recomendados.estatus != 'V'";
            if($request->estatus!="Todos" && $request->estatus!="T"){
                if($request->estatus=="Recomendado" || $request->estatus=="R"){
                    $sqlEstatus=" AND recomendados.estatus='R'";
                }
                if($request->estatus=="Comprado por establecimiento" || $request->estatus=="C"){
                    $sqlEstatus=" AND recomendados.estatus='C'";
                }
                if($request->estatus=="Pago de comisión" || $request->estatus=="P"){
                    $sqlEstatus=" AND recomendados.estatus='P'";
                }
                if($request->estatus=="Vencido" || $request->estatus=="V"){
                    $sqlEstatus=" AND recomendados.estatus='V'";
                }
            }
            $request->fechainicio= str_replace("/","-",$request->fechainicio);
            $request->fechafin=str_replace("/","-",$request->fechafin);

            $fecha_actual = new DateTime($request->fechainicio);
            $cadena_fecha_actual = $fecha_actual->format("Y-m-d");


            $nuevafecha_actual = new DateTime($request->fechafin);
            $cadena_nuevafecha_actual = $nuevafecha_actual->format("Y-m-d");

            $fecha=$cadena_fecha_actual;
            $nuevafecha=$cadena_nuevafecha_actual;

            $sql="
                SELECT
                    recomendados.id,
                    if(
                        recomendados.idempresacomprador IS NULL,
                        recomendados.nombre,
                        empresas.nombrecomercial
                    ) AS nombre,
                    if( recomendados.fechaRec IS NULL,
                        DATE_FORMAT(recomendados.fechaComp,\"%d-%m-%Y\"),
                        DATE_FORMAT(recomendados.fechaRec,\"%d-%m-%Y\")
                    ) AS fecha,
                    recomendados.comision,
                    codigopromociones.codigo AS hexa,
                    recomendados.idrecomendado,
                    recomendados.estatus,
                    recomendados.direccion,
                    recomendados.telefono,
                    recomendados.personacontacto,
                    codigopromociones.fechavigencia AS fechaCorte,
                    if( recomendados.idrecomendado IS NOT NULL,
                        CONCAT('Comisión generada gracias a venta de ',userspadre.nombre),
                        -1
                    ) AS nombre_padre,
                    recomendados.estatus as estado
                FROM recomendados
                LEFT JOIN codigopromociones ON codigopromociones.idrecomendado = recomendados.id
                LEFT JOIN usuarios ON recomendados.idusuario = usuarios.id
                LEFT JOIN empresas ON empresas.id = recomendados.idempresacomprador
                LEFT JOIN recomendados recomendadospadre ON recomendadospadre.id = recomendados.idrecomendado
                LEFT JOIN usuarios userspadre ON recomendadospadre.idusuario = userspadre.id
                where
                    (
                        recomendados.fechaRec IS NOT NULL
                        AND recomendados.idusuario = ".$request->idusuario."
                        ".$sqlEstatus."
                        AND recomendados.fechaRec between '".$fecha."' and '".$nuevafecha."'
                    )OR
                    (
                        recomendados.fechaRec IS NULL
                        AND recomendados.idusuario = ".$request->idusuario."
                        ".$sqlEstatus."
                        AND recomendados.fechaComp between '".$fecha."' and '".$nuevafecha."'
                    )
                order by recomendados.id desc
            ";
            $recomendados=DB::select($sql);

            $html='';
            foreach($recomendados as $recomendado){
                $onclick='javascript:void(0)';
                if($recomendado->estatus=='R'){
                    $onclick="verDatos(".$recomendado->id.",'".$recomendado->nombre."','".$recomendado->direccion."','".$recomendado->telefono."','".$recomendado->personacontacto."','".$recomendado->hexa."','".$recomendado->fechaCorte."','".$recomendado->estado."','".$recomendado->comision."')";
                }

                $nombre = $recomendado->nombre;
                if($recomendado->idrecomendado != null){
                    $nombre = $recomendado->nombre_padre;
                }

                $html .='<div class="card" style="cursor:pointer;" onclick="'.$onclick.'" >';
                    $html .='<div class="row" style="border-bottom:solid 1px #e7eaf3;padding: 0.75rem;">';
                        $html .='<div class="col-6">';
                            $html .= $nombre;
                        $html .='</div>';
                        $html .='<div class="col-6">';
                            $html .='<div class="text-right">';
                                $html .='<strong>'.$recomendado->fecha.'</strong>';
                            $html .='</div>';
                        $html .='</div>';
                        $html .='<div class="col-12" style="color:#929ca6;">';
                            $html .='<b>'.$recomendado->comision.'</b>';
                        $html .='</div>';
                    $html .='</div>';
                $html .='</div>';
            }
            $data_envio['html']=$html;
            echo json_encode($data_envio);
        }

        /**
         * Permite guardar un establecimiento recomendando
         */
        public function store(Request $request){
            $recomendado=new Recomendado;
            if($request->idrecomendado){
                $recomendado=Recomendado::findOrFail($request->idrecomendado);
            }else{
                $recomendado->fechaRec=date('Y-m-d');
            }
            if($request->personacontacto!=null){
                $recomendado->personacontacto=$request->personacontacto;

            }
            $recomendado->idusuario=$request->idusuario;
            if($request->nombre!=null)
            $recomendado->nombre=$request->nombre;

            if($request->direccion!=null)
            $recomendado->direccion=$request->direccion;

            $recomendado->telefono=$request->telefono;


            if($recomendado->save()){
                //CREAR CODIGO DE PROMOCION TIPO = 2
                $objCodigopromocion = new Codigopromocion;
                $objCodigopromocion->idrecomendado = $recomendado->id;
                $objCodigopromocion->iddueno = auth()->user()->id;
                $objCodigopromocion->codigo = 'P'.$this->generacodigo($recomendado->id);
                $objCodigopromocion->tipo = 2;

                $duracion=env("DURACIONCODESTABLECI");
                if($duracion==null){
                    $duracion=30;
                }
                $fecha = date('Y-m-j');
                $nuevafecha = strtotime ( '+ '.(int)$duracion.' day' , strtotime ( $fecha ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                $objCodigopromocion->fechavigencia = $nuevafecha;

                if($objCodigopromocion->save()){
                    if($request->plataforma){
                        return redirect()->route('recomendados.index')->with("success","Se guardo su recomendacion");
                    }

                    // $hexa=dechex($recomendado->id);
                    // $transform= str_pad($hexa, 5, "0", STR_PAD_LEFT);

                    $transform = $objCodigopromocion->codigo;
                    return ['msg'=>'Guardado!' , 'res'=>true , 'idrecomendado'=>$transform];
                }else{
                    if($request->plataforma){
                        return redirect()->route('recomendados.index')->with("danger","No se guardo su recomendacion");
                    }
                    return ['msg'=>'Eror al guardar!' , 'res'=>false];
                }
            }else{
                if($request->plataforma){
                    return redirect()->route('recomendados.index')->with("danger","No se guardo su recomendacion");
                }
                return ['msg'=>'Eror al guardar' , 'res'=>false];
            }
        }

    //PAGO DE COMISIONES DE RECOMENDADOS
        public function loaddata2(Request $request){
            set_time_limit(2400);
            ini_set('memory_limit', '2048M');
            ini_set('max_execution_time', 6000);

            $formatdatemysql="%d-%m-%Y";
            $sql = '
                SELECT
                    r.id as idrecomendado,
                    u.id idusuario,
                    IFNULL(e.nombrecomercial,"") as nombrecomercial,
                    IFNULL(e.rfc,"") as rfc,
                    DATE_FORMAT(r.fechaComp,"%d-%m-%Y") as fechaComp,
                    (
                        CASE
                            when u.nombre!="" THEN u.nombre
                            else
                                "Sin registro como vendedor"
                        end
                    ) as nombre,
                    r.comision
                FROM recomendados r
                left join empresas e on r.idempresacomprador=e.id
                left join usuarios u on u.id=r.idusuario
                where
                    r.estatus = "C" and
                    u.estatus = "A"
                ORDER by fechaComp DESC
            ';
            $coleccion = DB::select($sql);

            $estatus_ordenespago=$this->variablesglobales("estatus_ordenespago");
            $metodopago=$this->variablesglobales("metodopago");

            foreach ($coleccion as $data) {
                $acciones='';
                $acciones.='<div class="form-check">';
                if($data->nombre=="Sin registro como vendedor"){
                    $acciones.='    <input class="checkbox'.$data->idusuario.'" name="checkbox[]" onclick="seleccionar('.$data->idusuario.','.$data->idrecomendado.')"  type="checkbox" value="'.$data->idusuario.'-'.$data->idrecomendado.'" disabled />';
                }else{
                    $acciones.='    <input class="checkbox'.$data->idusuario.'" name="checkbox[]" onclick="seleccionar('.$data->idusuario.','.$data->idrecomendado.')"  type="checkbox" value="'.$data->idusuario.'-'.$data->idrecomendado.'" />';
                }
                $acciones.='    <label class="form-check-label" for="check'.$data->idrecomendado.'">';
                $acciones.='        ';
                $acciones.='    </label>';
                $acciones.='</div>';

                $data->acciones=$acciones;
            }

            $json_data = array(
                "data"  => $coleccion
            );

            echo json_encode($json_data);
        }

        public function comisionesadmin(){
            return view('recomendados.comisiones');
        }

        public function realizarpagos(Request $request){
            $recomendados=$request->checkbox;


            if(!empty($recomendados)){

                $aux=explode('-',$recomendados[0]);
                $iduser=$aux[0];
                $user=User::where('id','=',$iduser)->first();
                $arrayId=[];

                $ids='';
                $total=0;
                foreach ($recomendados as $recomendado){
                    $aux=explode('-',$recomendado);
                    $idrecomendado=$aux[1];
                    array_push($arrayId,$idrecomendado);

                    if($recomendado==$recomendados[count($recomendados)-1]){
                        $ids.=$idrecomendado.'';
                    }else{
                        $ids.=$idrecomendado.',';
                    }
                }


               $sqlpagos = '
               SELECT r.id as idrecomendado,u.id idusuario,e.nombrecomercial as nombrecomercial,e.rfc, DATE_FORMAT(r.fechaComp,"%d-%m-%Y") as fechaComp
               ,(CASE
                when u.nombre!=""
                THEN
                u.nombre
                 else
                  "Sin registro como vendedor"
                end) as nombre,
               r.comision
               FROM recomendados r
               left join empresas e on r.idempresacomprador=e.id
               left join usuarios u on u.id=r.idusuario
               where r.estatus="C"
               and r.id in ('.$ids.')
               and u.id='.$iduser.'
               ORDER by fechaComp DESC';


                $pagos=DB::select($sqlpagos);

                foreach ($pagos as $pago){
                    $total+=$pago->comision;
                }


               return view('recomendados.pagos',['ids'=>json_encode($arrayId),'total'=>$total,'user'=>$user]);
            }
        }

        public function savePago(Request $request){
            $idrecomendados=json_decode($request->ids);


            $pago = new Pago;
            $pago->iduser=$request->iduser;
            $pago->importe=$request->importe;
            $pago->fecha=$request->fecha;
            $pago->referencia=$request->referencia;
            $pago->importe=$request->total;
            $pago->cuenta=$request->cuenta;

            if($pago->save()){
                foreach ($idrecomendados as $idrecomendado){
                    $recomendado=Recomendado::findOrFail($idrecomendado);
                    $recomendado->estatus='P';
                    $recomendado->idpago=$pago->id;
                    $recomendado->fechaPag=$request->fecha;
                    $recomendado->save();
                }
                return redirect()->route('recomendados.comisiones');
            }

            else{
                return redirect()->route('recomendados.comisiones')->with('danger','No se guardar su pago');
            }
        }
}
