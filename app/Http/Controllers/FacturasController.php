<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Comprobante;
use App\Remision;
use App\Empresa;

class FacturasController extends Controller
{
    public function __construct(){
    }

    /**
     * Funcion que permite cargar la información de la tabla y se envia al index via ajax
     * @param  Request $request [description]
     * @return json
     */
    public function loaddata(Request $request){
        set_time_limit(2400);
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 6000);

        $sql_where = 'WHERE 1=1';
        if($request->fecha_inicial!='' && $request->fecha_inicial!=null && !empty($request->fecha_inicial)){
            if($request->fecha_final!='' && $request->fecha_final!=null && !empty($request->fecha_final)){
                $fecha_inicial = date('Y-m-d',strtotime($request->fecha_inicial));
                $fecha_final = date('Y-m-d',strtotime($request->fecha_final));

                $sql_where .= ' AND facturas.fecha >= "'.$fecha_inicial.'"';
                $sql_where .= ' AND facturas.fecha <= "'.$fecha_final.'"';
            }
        }

        if($request->estatus_facturas!='T'){
            $sql_where .= ' AND facturas.estatus = "'.$request->estatus_facturas.'"';
        }else{
            $sql_where .= ' AND facturas.estatus <> "D"';
        }

        if($request->idusuario>0){
            $sql_where .= ' AND clientes.idusuario = "'.$request->idusuario.'"';
        }

        $sql_where .= ' AND facturas.idempresa = "'.auth()->user()->idempresa.'"';

        $formatdatemysql="%d-%m-%Y";
        $sql = '
            SELECT
                facturas.id,
                IFNULL(remisiones.noticket,"") AS noticket,
                ifnull(clientes.nombre,"") as nombrecliente,
                DATE_FORMAT(facturas.fecha,"'.$formatdatemysql.'") as fecha,
                IFNULL(FORMAT(remisiones.subtotal,2),"") as subtotal,
                IFNULL(FORMAT(remisiones.ieps,2),"") as ieps,
                IFNULL(FORMAT(remisiones.iva,2),"") as iva,
                IFNULL(FORMAT(remisiones.total,2),"") as total,
                facturas.estatus
            FROM comprobantes facturas
            LEFT JOIN clientes ON clientes.id = facturas.idcliente
            LEFT JOIN remisiones ON facturas.id = remisiones.idcomprobante
            '.$sql_where.'
            ORDER BY
                facturas.id ASC
        ';
        // echo $sql;
        // exit();
        $coleccion = DB::select($sql);


        $estatus_facturas=$this->variablesglobales("estatus_facturas");

        foreach ($coleccion as $data) {

            $acciones='';
                $acciones.='<a href="'.route('facturas.visualizar',$data->id).'" class="btn btn-sm btn-info" title="Visualizar">
                    <i class="fas fa-eye"></i>
                </a>';
                // if($data->estatus=='A'){
                //     $acciones.='&nbsp';
                //     $acciones.='<form method="POST" action="'.route('facturas.eliminar',[$data->id,'logica']).'" id="FormDelete'.$data->id.'" class="d-inline">';
                //     $acciones.='<input name="_token" type="hidden" value="'.csrf_token().'"/>';
                //     $acciones.='<button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="eliminar('.$data->id.')">
                //         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                //         <i class="far fa-trash-alt"></i>
                //     </button>';
                //     $acciones.='</form>';
                // }
            $data->acciones=$acciones;

            if(isset($estatus_facturas[$data->estatus])){
                $data->estatus = $estatus_facturas[$data->estatus];
            }
        }

        $json_data = array(
            "data"  => $coleccion
        );

        echo json_encode($json_data);
    }

    /**
     * Muestra el listado
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //VERIFICA LAS FUNCIONES QUE TIENE EL USUARIO DEL CONTROLADOR CobranzaController
        $arraybtn=$this->btnfunciones(auth()->user()->idtipousuario,'FacturasController');

        $estatus_facturas=$this->variablesglobales("estatus_facturas");
        $estatus_facturas['T']='Todos';
        $tipo_facturas=$this->variablesglobales("tipo_facturas");
        $tipo_facturas['T']='Todos';

        //BUSCAR LAS SUCURSALES QUE ESTAN ASOCIADAS AL USUARIO
        $sucursales = array();
        if(auth()->user()->idsucursal!=0){
            //ES UNA SUCURSAL UNICAMENTE
            $sql = '
                select
                    sucursales.id,
                    sucursales.nombre
                from sucursales
                where
                    id= ?
            ';
            $sucursales_a = DB::select($sql,[auth()->user()->idsucursal]);
        }else{
            //TODAS LAS SUCURSALES
            $sql = '
                select
                    sucursales.id,
                    sucursales.nombre
                from sucursales
                where
                    idempresa= ?
                    AND estatus = "A"
            ';
            $sucursales_a = DB::select($sql,[auth()->user()->idempresa]);
        }
        $sucursales[0]='Todos';
        foreach ($sucursales_a as $data) {
            $sucursales[$data->id]=$data->nombre;
        }

        return view('facturas.index', [
            'arraybtn' => $arraybtn,
            'estatus_facturas' => $estatus_facturas,
            'tipo_facturas' => $tipo_facturas,
            'sucursales' => $sucursales,
            'tituloencabezado'=>'Facturas'
        ]);
    }


    //CONFIGURACION GENERAL
        public function Configuracion(){
            $empresa=Empresa::where('id','=',auth()->user()->idempresa)->first();
            $usoDeCfdi=DB::select("select * from uso_comprobante_sat");

            //COLOR DE FONDO
            $arraycolor = explode("@", $empresa->colorfondopdf);
            $empresa->colorfondopdf='rgb('.$arraycolor[0].','.$arraycolor[1].','.$arraycolor[2].')';
            $arraycolor = explode("@", $empresa->colorletrapdf);
            $empresa->colorletrapdf='rgb('.$arraycolor[0].','.$arraycolor[1].','.$arraycolor[2].')';

            return view('facturas.configuracion',[
                'empresa'=>$empresa,
                'usoCfdi'=>$usoDeCfdi,
                'tituloencabezado'=>'Configuración General'
            ]);
        }

        public function actualizarconf(Request $request){
            $empresa=Empresa::findOrFail(auth()->user()->idempresa);

            if(isset($request->mostrarformuescanear)){
                $empresa->mostrarformuescanear=$request->mostrarformuescanear;
            }
            if(isset($request->solicitaconsectivo)){
                $empresa->solicitaconsectivo=$request->solicitaconsectivo;
            }
            if(isset($request->cfdidefault)){
                if($request->cfdidefault==-1){
                    $empresa->idusocfdidefault=null;
                }else{
                    $empresa->idusocfdidefault=$request->cfdidefault;
                }
            }
            if(isset($request->avisodiamembresia)){
                if($request->avisodiamembresia!=null){
                    $empresa->avisodiamembresia=$request->avisodiamembresia;
                }
            }
            if(isset($request->avisofolio)){
                if($request->avisofolio!=null){
                    $empresa->avisofolio=$request->avisofolio;
                }
            }
            $empresa->wlogopdf=$request->wlogopdf;
            $empresa->hlogopdf=$request->hlogopdf;

            if($request->hasFile('logopdf')){
                $sitioguardar='public/logos';
                if($empresa->logopdf!=null){
                    Storage::delete('public/logos/'.$empresa->logopdf);
                }
                $formato=$this->typeFile($request->file('logopdf')->hashName());
                $archivo = $request->file('logopdf')->storeAs($sitioguardar,$empresa->id.'.'.$formato);
                $archivo = str_replace("public/", "", $archivo);
                $empresa->logopdf = $archivo;
            }

            if(isset($request->colorfondopdf)){
                $arraycolor = explode("(", $request->colorfondopdf);
                if(isset($arraycolor[1])){
                    $arraycolor = explode(",", $arraycolor[1]);
                    $arraycolor[0] = str_replace(" ", "", $arraycolor[0]);
                    $arraycolor[0] = str_replace(")", "", $arraycolor[0]);
                    $colorfondopdf= $arraycolor[0];

                    $arraycolor[1] = str_replace(" ", "", $arraycolor[1]);
                    $arraycolor[1] = str_replace(")", "", $arraycolor[1]);
                    $colorfondopdf.= '@'.$arraycolor[1];

                    $arraycolor[2] = str_replace(" ", "", $arraycolor[2]);
                    $arraycolor[2] = str_replace(")", "", $arraycolor[2]);
                    $colorfondopdf.= '@'.$arraycolor[2];

                    $empresa->colorfondopdf = $colorfondopdf;
                }
            }

            if(isset($request->colorletrapdf)){
                $arraycolor = explode("(", $request->colorletrapdf);
                if(isset($arraycolor[1])){
                    $arraycolor = explode(",", $arraycolor[1]);
                    $arraycolor[0] = str_replace(" ", "", $arraycolor[0]);
                    $arraycolor[0] = str_replace(")", "", $arraycolor[0]);
                    $colorletrapdf= $arraycolor[0];

                    $arraycolor[1] = str_replace(" ", "", $arraycolor[1]);
                    $arraycolor[1] = str_replace(")", "", $arraycolor[1]);
                    $colorletrapdf.= '@'.$arraycolor[1];

                    $arraycolor[2] = str_replace(" ", "", $arraycolor[2]);
                    $arraycolor[2] = str_replace(")", "", $arraycolor[2]);
                    $colorletrapdf.= '@'.$arraycolor[2];

                    $empresa->colorletrapdf = $colorletrapdf;
                }
            }

            if($empresa->save()){
                return redirect()->route('facturas.configuracion')->with('success','Configuración actualizada con éxito');
            }else{
                return redirect()->route('facturas.configuracion')->with('danger','Configuración no guardada');
            }
        }

    /**
     * Permite visualizar la nota
     *
     * @return \Illuminate\Http\Response
     */
    public function visualizar($id)
    {
        $objComprobante = Comprobante::where("id", "=", $id)->first();
        if(!empty($objComprobante)){
            $objRemision = Remision::where("idcomprobante", "=", $id)->first();
            return view('facturas.visualizar', [
                'objComprobante' => $objComprobante,
                'objRemision' => $objRemision,
            ]);
        }else{
            return redirect()->route('facturas.index')->with('danger','Error: Factura no encontrada');
        }
    }

        /**
         * Funcion que permite buscar las cuentas de banco para la moneda seleccionada
         * @param  Request $request [description]
         * @return json
         */
        public function buscarconceptosdenotacreada(Request $request){
            $html='';
            $error=0;

            $remision = Remision::where("id", "=", $request->idremision)->first();
            if(empty($remision)){
                $error='No se consiguio nota';
            }else{
                foreach ($remision->remisionesdetalle as $data) {
                    $nombreconcepto='Concepto no encontrado';
                    if(isset($data->concepto->codigo) || isset($data->concepto->concepto)){
                        $nombreconcepto='';
                        if(isset($data->concepto->codigo)){
                            $nombreconcepto.=$data->concepto->codigo.' ';
                        }
                        if(isset($data->concepto->concepto)){
                            $nombreconcepto.=$data->concepto->concepto;
                        }
                    }
                    if($nombreconcepto=='Concepto no encontrado'){
                        $nombreconcepto=$data->descripcion;
                    }

                    $html .= '<tr>';
                        $html .= '<td>';
                            $html .= $nombreconcepto;
                        $html .= '</td>';
                        $html .= '<td style="text-align:right;">';
                            $html .= number_format($data->importe,2,'.',',');
                        $html .= '</td>';
                    $html .= '</tr>';
                }
            }

            $data_envio['html']=$html;
            $data_envio['error']=$error;
            echo json_encode($data_envio);
        }

    /**
     * Permite eliminar un gastp
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objRemision = Remision::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();

            if(!isset($error)){
                //ELIMINACIÓN FISICA
                if(!$Remision->delete()){
                    DB::rollback();
                    $typemsj='danger';
                    $msj='No se ha podido eliminar';
                }else{
                    DB::commit();
                }
            }else{
                DB::rollback();
            }
        }else{
            DB::beginTransaction();
            if(!isset($error)){
                //ELIMINACIÓN LOGICA
                $objRemision->estatus = 'D';

                if(!$objRemision->save()){
                    DB::rollback();
                    $typemsj='danger';
                    $msj='No se ha podido eliminar';
                }else{
                    DB::commit();
                }
            }else{
                DB::rollback();
            }
        }
        return redirect()->route('facturas.index')->with($typemsj,$msj);
    }
}
