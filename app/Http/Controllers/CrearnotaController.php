<?php

namespace App\Http\Controllers;

//ANEXANDO REQUEST PARA VALIDACIONES
use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

//MODELOS USADOS
use App\Concepto;
use App\Formapago;
use App\Remision;

class CrearnotaController extends Controller
{
    public function __construct(){
    }

    /**
     * Muestra formulario para crear o editar una nota de pago
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //BUSCAR LAS SUCURSALES QUE ESTAN ASOCIADAS AL USUARIO
        $sucursales = array();
        if(auth()->user()->idsucursal!=0){
            //ES UNA SUCURSAL UNICAMENTE
            $sql = '
                select
                    sucursales.id,
                    sucursales.nombre
                from sucursales
                where
                    id= ?
            ';
            $sucursales_a = DB::select($sql,[auth()->user()->idsucursal]);
        }else{
            //TODAS LAS SUCURSALES
            $sql = '
                select
                    sucursales.id,
                    sucursales.nombre
                from sucursales
                where
                    idempresa= ?
                    AND estatus = "A"
            ';
            $sucursales_a = DB::select($sql,[auth()->user()->idempresa]);
        }
        foreach ($sucursales_a as $data) {
            $sucursales[$data->id]=$data->nombre;
        }

        //BUSCANDO FORMAS DE PAGO
        $formaspago = Formapago::where("estatus", "=", 'A')->pluck('nombre','id');

        // dd($conceptos,$formaspago,$sucursales);

        return view('notas.crearnotas', [
            'formaspago' => $formaspago,
            'sucursales' => $sucursales
        ]);
    }

        /**
         * Funcion que permite buscar las cuentas de banco para la moneda seleccionada
         * @param  Request $request [description]
         * @return json
         */
        public function buscarconceptosdenota(Request $request){
            $html='';
            $idremision = -1;
            $idformapago = -1;
            $estatus = -1;
            if($request->nuevo==1){
                //BUSCAR CONCEPTO CON IMPORTE VACIO
                $conceptos = Concepto::where("estatus", "=", 'A')
                             ->where("idempresa", "=", auth()->user()->idempresa)
                             ->orderBy('codigo')
                             ->get();

                //CREANDO HTML DE TABLA
                foreach ($conceptos as $data) {
                    $html .= '<tr>';
                        $html .= '<td>';
                            $html .= $data->codigo.' '.$data->concepto;
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= '<input style="text-align:right;" data-idconcepto="'.$data->id.'" align="right" type="text" class="form-control importes" id="importe_'.$data->id.'" name="importe_'.$data->id.'" value="0.00" onkeypress="return AcceptNumPunto(event)" onkeyup="sumarimportes();" onchange="formatonumero(\'importe_'.$data->id.'\');">';
                        $html .= '</td>';
                    $html .= '</tr>';
                }
            }else{
                //BUSCAR SI ESA REMISION A TRAVES DEL noticket YA EXISTE PARA ESA EMPRESA Y SUCURSAL
                $remision = Remision::where("idempresa", "=", auth()->user()->idempresa)
                                     ->where("idsucursal", "=", $request->idsucursal)
                                     ->where("noticket", "=", $request->noticket)
                                     ->where("estatus", "<>", "D")
                                     ->first();

                if(empty($remision)){
                    //ES NUEVA REMISION

                    //BUSCAR CONCEPTO CON IMPORTE VACIO
                    $conceptos = Concepto::where("estatus", "=", 'A')
                             ->where("idempresa", "=", auth()->user()->idempresa)
                             ->orderBy('codigo')
                             ->get();

                    //CREANDO HTML DE TABLA
                    foreach ($conceptos as $data) {
                        $html .= '<tr>';
                            $html .= '<td>';
                                $html .= $data->codigo.' '.$data->concepto;
                            $html .= '</td>';
                            $html .= '<td>';
                                $html .= '<input style="text-align:right;" data-idconcepto="'.$data->id.'" align="right" type="text" class="form-control importes" id="importe_'.$data->id.'" name="importe_'.$data->id.'" value="0.00" onkeypress="return AcceptNumPunto(event)" onkeyup="sumarimportes();" onchange="formatonumero(\'importe_'.$data->id.'\');">';
                            $html .= '</td>';
                        $html .= '</tr>';
                    }
                }else{
                    //ES UNA REMISION VIEJA
                    $idremision = $remision->id;
                    $estatus = $remision->estatus;
                    $idformapago = $remision->idformapago;
                    foreach ($remision->remisionesdetalle as $data) {
                        if(isset($data->concepto->codigo)){
                            $html .= '<tr>';
                                $html .= '<td>';
                                    $html .= $data->concepto->codigo.' '.$data->concepto->concepto;
                                $html .= '</td>';
                                $html .= '<td>';
                                    $html .= '<input style="text-align:right;" data-idconcepto="'.$data->concepto->id.'" align="right" type="text" class="form-control importes" id="importe_'.$data->concepto->id.'" name="importe_'.$data->concepto->id.'" value="'.number_format($data->importe,2,'.',',').'" onkeypress="return AcceptNumPunto(event)" onkeyup="sumarimportes();" onchange="formatonumero(\'importe_'.$data->concepto->id.'\');">';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                    }
                }
            }

            $data_envio['html']=$html;
            $data_envio['estatus']=$estatus;
            $data_envio['idremision']=$idremision;
            $data_envio['idformapago']=$idformapago;
            echo json_encode($data_envio);
        }

    /**
     * Permite actualizar los datos de una entidad del modelo en la BD
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        DB::beginTransaction();
        $conceptos=json_decode($request->conceptos_json, true);
        dd($request,$conceptos);
        if($id==-1){
            $objCobro = new Cobro;
            //BUSCANDO EL FOLIO MAXIMO
            $folio = 0;
            $sql = '
                SELECT
                    ifnull(MAX(cobros.folio),0) as folio
                FROM cobros
            ';
            $datafolio = DB::select($sql);
            if(count($datafolio)>0){
                if(isset($datafolio[0]->folio)){
                    $folio = (int)$datafolio[0]->folio;
                }
            }
            $folio++;
            $objCobro->folio= $folio;
        }else{
            $objCobro = Cobro::where("id", "=", $id)->first();
            $sql = ' DELETE FROM detallescobros WHERE idcobro = ?';
            DB::delete($sql,[$id]);
        }

        $idsucursal=-1;
        $objCobro->idempresa = auth()->user()->idempresa;
        $objCobro->idcliente = $request->idcliente;
        $objCobro->idmoneda = $request->idmoneda;
        $objCobro->idcuenta = $request->idcuenta;
        $objCobro->referencia = $request->referencia;
        $objCobro->notas = $request->notas;
        $objCobro->fecha = date('Y-m-d',strtotime($request->fecha));
        $objCobro->importe = $request->importe;
        $objCobro->estatus = 'A';

        if($objCobro->save()){
            //DETALLE COBROS
            foreach ($detallescobros as $data) {
                $objDetallecobro = new Detallecobro;
                $objDetallecobro->idcobro = $objCobro->id;
                $objDetallecobro->idcxc = $data['idcxc'];
                $objDetallecobro->idmoneda = $data['idmoneda'];
                $objDetallecobro->tipocambio = $data['tipocambio'];
                $objDetallecobro->total = $data['total'];
                $objDetallecobro->adeudo = $data['saldo'];
                $objDetallecobro->pago = $data['abono'];
                $objDetallecobro->nvosaldo = (float)$data['saldo']-(float)$data['abono'];

                if($objDetallecobro->save()){
                    //ACTUALIZAR EL SALDO A LA CUENTA POR PAGAR
                    $objCuentaporcobrar = Cuentaporcobrar::where("id", "=", $data['idcxc'])->first();
                    if(!empty($objCuentaporcobrar)){
                        $idsucursal = $objCuentaporcobrar->idsucursal;

                        //BUSCAR TODOS LOS ABONOS REALIZADOS A ESA CUENTA POR COBRAR
                        // $pago_total = $objCuentaporcobrar->abono + (float)$data['abono'];
                        $pago_total = 0;
                        $sql = '
                            SELECT
                                ifnull(SUM(detallescobros.pago),0) as pago_total
                            FROM detallescobros
                            WHERE
                                detallescobros.idcxc = ?
                                AND detallescobros.estatus = "A"
                        ';
                        $pago = DB::select($sql,[$data['idcxc']]);
                        if(count($pago)>0){
                            if($pago[0]->pago_total!=null){
                                $pago_total = $pago[0]->pago_total;
                            }
                        }
                        $objCuentaporcobrar->abono = $pago_total;
                        // dd($objCuentaporcobrar,$pago_total,$objDetallecobro);
                        if($objCuentaporcobrar->total-$objCuentaporcobrar->abono<=0){
                            $objCuentaporcobrar->estatus = 'P';
                        }else{
                            $objCuentaporcobrar->estatus = 'A';
                        }
                        if(!$objCuentaporcobrar->save()){
                            $error = 'No se pudo guadar la cuenta por cobrar con id '.$data['idcxc'];
                            break;
                        }
                    }else{
                        $error = 'No se pudo conseguir la cuenta por cobrar con id '.$data['idcxc'];
                        break;
                    }
                }else{
                    $error = 'No se pudo guardar el producto con código '.$data['codigo'];
                    break;
                }
            }

            if(!isset($error)){
                if($id==-1){
                    //CREAR UN NUEVO Kardexcte
                    $objKardexcte = new Kardexcte;
                }else{
                    //ACTUALIZAR Kardexcte
                    $objKardexcte = Kardexcte::where("idreferencia", "=", $objCobro->id)
                                             ->where("idmov", "=", 2)
                                             ->first();

                    if(empty($objKardexcte)){
                        $objKardexcte = new Kardexcte;
                    }
                }

                //KARDEXCTE
                $objKardexcte->idempresa = auth()->user()->idempresa;
                $objKardexcte->idsucursal = $idsucursal;
                $objKardexcte->idcliente = $request->idcliente;
                $objKardexcte->idmov = 2;
                $objKardexcte->idreferencia = $objCobro->id;
                $objKardexcte->idmoneda = $request->idmoneda;
                $objKardexcte->referencia = $request->referencia;
                $objKardexcte->tablareferencia = 'cobros';
                $objKardexcte->fecha = date('Y-m-d',strtotime($request->fecha));
                $objKardexcte->importe = $request->importe;

                if(!$objKardexcte->save()){
                    $error='Kardex del cliente no cobro';
                }

                if(!isset($error)){
                    DB::commit();
                    return redirect()->route('cobranzas.index')->with('success','Se ha guardado los cambios con éxito');
                }else{
                    DB::rollback();
                    return redirect()->route('cobranzas.index')->with('danger',$error);
                }
            }else{
                DB::rollback();
                return redirect()->route('cobranzas.index')->with('danger',$error);
            }
        }else{
            DB::rollback();
            return redirect()->route('cobranzas.index')->with('danger','No se pudo guardar la cobranza');
        }
    }

    /**
     * Permite eliminar un gastp
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id,$tipo='logica')
    {
        $objCobro = Cobro::where("id", "=", $id)->first();
        $typemsj='warning';
        $msj='Se ha eliminado correctamente';
        if($tipo!='logica'){
            DB::beginTransaction();
            // dd($objCobro->detallescobros);
            //VOLVER A CALCULAR LAS DEUDAS DE LAS CUENTAS POR COBRAR
            foreach ($objCobro->detallescobros as $objDetallecobro) {
                $idcxc = $objDetallecobro->idcxc;

                //ACTUALIZAR EL SALDO A LA CUENTA POR PAGAR
                $objCuentaporcobrar = Cuentaporcobrar::where("id", "=", $idcxc)->first();
                if(!empty($objCuentaporcobrar)){
                    //BUSCAR TODOS LOS ABONOS REALIZADOS A ESA CUENTA POR COBRAR
                    // $pago_total = $objCuentaporcobrar->abono + (float)$data['abono'];
                    $pago_total = 0;
                    $sql = '
                        SELECT
                            ifnull(SUM(detallescobros.pago),0) as pago_total
                        FROM detallescobros
                        WHERE
                            detallescobros.idcxc = ?
                            AND detallescobros.estatus = "A"
                    ';
                    $pago = DB::select($sql,[$idcxc]);
                    if(count($pago)>0){
                        if($pago[0]->pago_total!=null){
                            $pago_total = $pago[0]->pago_total;
                        }
                    }
                    $objCuentaporcobrar->abono = (float)$pago_total - (float)$objDetallecobro->pago;
                    // dd($objCuentaporcobrar,$pago_total,$objDetallecobro);
                    if($objCuentaporcobrar->total-$objCuentaporcobrar->abono<=0){
                        $objCuentaporcobrar->estatus = 'P';
                    }else{
                        $objCuentaporcobrar->estatus = 'A';
                    }
                    if(!$objCuentaporcobrar->save()){
                        $error = 'No se pudo guadar la cuenta por cobrar con id '.$data['idcxc'];
                        break;
                    }
                }else{
                    $error = 'No se pudo conseguir la cuenta por cobrar con id '.$data['idcxc'];
                    break;
                }
            }

            if(!isset($error)){
                //ELIMINACIÓN FISICA
                if(!$objCobro->delete()){
                    DB::rollback();
                    $typemsj='danger';
                    $msj='No se ha podido eliminar';
                }else{
                    DB::commit();
                }
            }else{
                DB::rollback();
            }
        }else{
            DB::beginTransaction();
            // dd($objCobro->detallescobros);
            //VOLVER A CALCULAR LAS DEUDAS DE LAS CUENTAS POR COBRAR
            foreach ($objCobro->detallescobros as $objDetallecobro) {
                $idcxc = $objDetallecobro->idcxc;

                //ACTUALIZAR EL SALDO A LA CUENTA POR PAGAR
                $objCuentaporcobrar = Cuentaporcobrar::where("id", "=", $idcxc)->first();
                if(!empty($objCuentaporcobrar)){
                    //BUSCAR TODOS LOS ABONOS REALIZADOS A ESA CUENTA POR COBRAR
                    // $pago_total = $objCuentaporcobrar->abono + (float)$data['abono'];
                    $pago_total = 0;
                    $sql = '
                        SELECT
                            ifnull(SUM(detallescobros.pago),0) as pago_total
                        FROM detallescobros
                        WHERE
                            detallescobros.idcxc = ?
                            AND detallescobros.estatus = "A"
                    ';
                    $pago = DB::select($sql,[$idcxc]);
                    if(count($pago)>0){
                        if($pago[0]->pago_total!=null){
                            $pago_total = $pago[0]->pago_total;
                        }
                    }
                    $objCuentaporcobrar->abono = (float)$pago_total - (float)$objDetallecobro->pago;
                    // dd($objCuentaporcobrar,$pago_total,$objDetallecobro);
                    if($objCuentaporcobrar->total-$objCuentaporcobrar->abono<=0){
                        $objCuentaporcobrar->estatus = 'P';
                    }else{
                        $objCuentaporcobrar->estatus = 'A';
                    }
                    if(!$objCuentaporcobrar->save()){
                        $error = 'No se pudo guadar la cuenta por cobrar con id '.$data['idcxc'];
                        break;
                    }
                }else{
                    $error = 'No se pudo conseguir la cuenta por cobrar con id '.$data['idcxc'];
                    break;
                }

                $objDetallecobro->estatus='D';
                if(!$objDetallecobro->save()){
                    $error = 'No se pudo conseguir la cuenta por cobrar con id '.$data['idcxc'];
                    break;
                }
            }

            if(!isset($error)){
                //ELIMINACIÓN LOGICA
                $objCobro->estatus = 'D';

                if(!$objCobro->save()){
                    DB::rollback();
                    $typemsj='danger';
                    $msj='No se ha podido eliminar';
                }else{
                    DB::commit();
                }
            }else{
                DB::rollback();
            }
        }
        return redirect()->route('cobranzas.index')->with($typemsj,$msj);
    }
}
