<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//ANEXANDO SEGURIDAD
use Illuminate\Support\Facades\Hash;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

use nusoap_client;

use App\Configuracionweb;
use App\Contactos;
use App\Afiliado;
use App\Empresa;
use App\Formapago;
use App\Orden;
use App\Ordenesdetalle;
use App\Servicio;
use App\Codigopromocion;
use App\Remision;
use App\Remisiondetalle;
use App\User;
use App\Concepto;
use \App\PaymentMethods\MercadoPago;

use App\Mail\Registrateestablecimiento;
use App\Mail\Informacionpago;
use App\Mail\Contacto;

//ENVIO DE EMAILS
use Illuminate\Support\Facades\Mail;

class ComprarplanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Permite enviar el mensaje de contacto
     *
     */
    public function enviarcontacto(Request $request){
        $data_envio['error']=0;
        $data_envio['mensaje']='';

        $objContacto = new Contactos;
        $objContacto->email=$request->email;
        $objContacto->name=$request->name;
        $objContacto->phone=$request->phone;
        $objContacto->subject=$request->subject;
        $objContacto->message=$request->message;

        if($objContacto->save()){
            $email=env('MAIL_USERNAME');
            // $email='arredondojose8@gmail.com';
            $data= array(
                'email'=>$request->email,
                'name'=>$request->name,
                'phone'=>$request->phone,
                'subject'=>$request->subject,
                'message'=>$request->message,
            );
            Mail::to($email)->send(new Contacto($data));
        }else{
            $data_envio['error']=1;
            $data_envio['mensaje']='No se pudo guardar en contacto';
        }
        echo json_encode($data_envio);
    }

    /**
     * Permite mostrar la pagina donde se muestra los planes de comprar para establecimientos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suscripciones = Servicio::where("tipo", "=", '2')->orderby('id','ASC')->get();
        $paquetes = Servicio::where("tipo", "=", '1')->orderby('id','ASC')->get();

        return view('comprarplan.index',[
            'suscripciones'=>$suscripciones,
            'paquetes'=>$paquetes,
        ]);
    }

        /**
         * Permite comprar el plan seleccionado (1.16)
         * @return \Illuminate\Http\Response
         */
        public function comprarplan(Request $request){
            $data_envio=array();

            //CONFIGURACION
            $metodopago=$request->metodopago;//MERCADO PAGO
            $ambito=env('AMBITO');//0 = SANDBOX
            $iva_incluido=1;
            $iva_porcentaje=16;


            DB::beginTransaction();

            //VERIFICAR SI POSEE CODIGO DE PROMOCION
                $idcodigopromocion=null;

            if(!isset($error)){
                //CALCULAR EL PRECIO Y COSTO
                    $subscripcion = Servicio::where("id", "=", (int)$request->idserv)->first();
                    $paquete = Servicio::where("id", "=", (int)$request->idpaquete)->first();
                    if(empty($subscripcion) && empty($paquete)){
                        $error='No se consigio la subscripcion o paquete de folio';
                    }
                    if(!isset($error)){
                        $sum_folio = 0;
                        $sum_total = 0;
                        $sum_subtotal = 0;
                        $sum_iva_monto = 0;

                        //CREAR DETALLE ORDENES
                        $duracion=0;
                        $titulo = '';
                        if(!empty($subscripcion)){
                            $ObjOrdenesdetalleServicio = New Ordenesdetalle;
                            $ObjOrdenesdetalleServicio->idservicio = $subscripcion->id;
                            if($idcodigopromocion!=null){
                                //TIENE DESCUENTO
                                $ObjOrdenesdetalleServicio->total = (float)$subscripcion->precio - (float)$subscripcion->descuento;
                            }else{
                                $ObjOrdenesdetalleServicio->total = (float)$subscripcion->precio;
                            }
                            $ObjOrdenesdetalleServicio->subtotal = round((float)$ObjOrdenesdetalleServicio->total/(($iva_porcentaje/100)+1),2);
                            $ObjOrdenesdetalleServicio->iva_incluido = $iva_incluido;
                            $ObjOrdenesdetalleServicio->iva_porcentaje = $iva_porcentaje;
                            $ObjOrdenesdetalleServicio->iva_monto = round($ObjOrdenesdetalleServicio->total - $ObjOrdenesdetalleServicio->subtotal,2);
                            $ObjOrdenesdetalleServicio->tipo = 2;//SUBSCRIPCION

                            //CARGANDO VALORES
                            $titulo .= 'Subscripción de '.$subscripcion->producto;
                            $duracion = $subscripcion->duracion;
                            $sum_folio += (int)$subscripcion->numfolios;
                            $sum_total += $ObjOrdenesdetalleServicio->total;
                            $sum_subtotal += $ObjOrdenesdetalleServicio->subtotal;
                            $sum_iva_monto += $ObjOrdenesdetalleServicio->iva_monto;
                        }

                        if(!empty($paquete)){
                            $ObjOrdenesdetallePaquete = New Ordenesdetalle;
                            $ObjOrdenesdetallePaquete->idservicio = $paquete->id;
                            $ObjOrdenesdetallePaquete->total = (float)$paquete->precio;
                            $ObjOrdenesdetallePaquete->subtotal = round((float)$ObjOrdenesdetallePaquete->total/(($iva_porcentaje/100)+1),2);
                            $ObjOrdenesdetallePaquete->iva_incluido = $iva_incluido;
                            $ObjOrdenesdetallePaquete->iva_porcentaje = $iva_porcentaje;
                            $ObjOrdenesdetallePaquete->iva_monto = round($ObjOrdenesdetallePaquete->total - $ObjOrdenesdetallePaquete->subtotal,2);
                            $ObjOrdenesdetallePaquete->tipo = 1;//PAQUETE DE FOLIO

                            //CARGANDO VALORES
                            if($titulo!=''){
                                $titulo .= ' con ';
                            }
                            $titulo .= $paquete->producto.' folios.';

                            $sum_folio += (int)$paquete->numfolios;
                            $sum_total += $ObjOrdenesdetallePaquete->total;
                            $sum_subtotal += $ObjOrdenesdetallePaquete->subtotal;
                            $sum_iva_monto += $ObjOrdenesdetallePaquete->iva_monto;
                        }
                    }

                //CREAR ORDEN
                if(!isset($error)){
                    //BUSCAR DATOS DE LA EMPRESA DEL USUARIO
                    $objEmpresa = Empresa::where("id", "=", auth()->user()->idempresa)->first();
                    if(!empty($objEmpresa)){
                        if($request->idorden=='-1'){
                            $ObjOrden = new Orden;
                            $ObjOrden->idcodigopromocion = $idcodigopromocion;
                        }else{
                            $ObjOrden = Orden::where("id", "=", (int)$request->idorden)->first();
                            if(!empty($ObjOrden)){
                                $sql = 'DELETE FROM `ordenesdetalles` WHERE idorden = ?';
                                DB::delete($sql,[$ObjOrden->id]);
                            }
                        }

                        $ObjOrden->iduser = auth()->user()->id;
                        $ObjOrden->renovacion = 1;
                        $ObjOrden->metodopago = $metodopago;
                        $ObjOrden->ambito = $ambito;
                        $ObjOrden->costometodopago = 0;
                        $ObjOrden->titulo = $titulo;
                        $ObjOrden->razonsocial = $objEmpresa->razonsocial;
                        $ObjOrden->rfc = $objEmpresa->rfc;
                        $ObjOrden->email = $objEmpresa->email;
                        $ObjOrden->tracker = bin2hex(random_bytes(32));
                        $ObjOrden->total = $sum_total;
                        $ObjOrden->folio = $sum_folio;
                        $ObjOrden->duracion = $duracion;
                        $ObjOrden->iva_incluido = $iva_incluido;
                        $ObjOrden->iva_porcentaje = $iva_porcentaje;
                        $ObjOrden->iva_monto = $sum_iva_monto;
                        $ObjOrden->subtotal = $sum_subtotal;

                        if($ObjOrden->save()){
                            if(isset($ObjOrdenesdetalleServicio)){
                                $ObjOrdenesdetalleServicio->idorden=$ObjOrden->id;
                                if(!$ObjOrdenesdetalleServicio->save()){
                                    $error='No se pudo guardar el detalle asociado al servicio';
                                }
                            }
                            if(isset($ObjOrdenesdetallePaquete)){
                                $ObjOrdenesdetallePaquete->idorden=$ObjOrden->id;
                                if(!$ObjOrdenesdetallePaquete->save()){
                                    $error='No se pudo guardar el detalle asociado al paquete de folio';
                                }
                            }
                            if(!isset($error)){
                                // dd($ObjOrden,$ObjOrdenesdetalleServicio,$ObjOrdenesdetallePaquete);
                                DB::commit();
                                $data_envio['idorden']=$ObjOrden->id;

                                //MERCADO PAGO
                                if($metodopago==1){
                                    // dd($ObjOrden,$ObjOrdenesdetalleServicio,$ObjOrdenesdetallePaquete);
                                    $method = new \App\PaymentMethods\MercadoPago;
                                    $url= $method->setupPaymentAndGetRedirectURL($ObjOrden);
                                    $data_envio['url']=$url;
                                    // return redirect()->to($url);
                                }elseif($metodopago==2){
                                    //OTRO METODO
                                    //ENVIAR CORREO CON INFORMACIÓN DE COBRO
                                    $cuentabanco="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "cuentabanco")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $cuentabanco = $ObjConfiguracionweb->parametros;
                                    }
                                    $banco="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "banco")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $banco = $ObjConfiguracionweb->parametros;
                                    }
                                    $numerodecontacto="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "numerodecontacto")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $numerodecontacto = $ObjConfiguracionweb->parametros;
                                    }
                                    $clabeinterbancaria="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "clabeinterbancaria")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $clabeinterbancaria = $ObjConfiguracionweb->parametros;
                                    }
                                    $numerotarjeta="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "numerotarjeta")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $numerotarjeta = $ObjConfiguracionweb->parametros;
                                    }
                                    $personarfc="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "personarfc")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $personarfc = $ObjConfiguracionweb->parametros;
                                    }
                                    $personanombre="";
                                    $ObjConfiguracionweb = Configuracionweb::where("nombre", "=", "personanombre")->first();
                                    if(!empty($ObjConfiguracionweb)){
                                        $personanombre = $ObjConfiguracionweb->parametros;
                                    }


                                    //ARMANDO ARRAY PARA MOSTRAR
                                    $data_envio['cuentabanco']=$cuentabanco;
                                    $data_envio['banco']=$banco;
                                    $data_envio['numerodecontacto']=$numerodecontacto;
                                    $data_envio['clabeinterbancaria']=$clabeinterbancaria;
                                    $data_envio['numerotarjeta']=$numerotarjeta;
                                    $data_envio['personarfc']=$personarfc;
                                    $data_envio['personanombre']=$personanombre;

                                    $data_envio['rfc']=$ObjOrden->rfc;
                                    $data_envio['subscripcion']='';
                                    if(isset($subscripcion)){
                                        $data_envio['subscripcion']=$subscripcion->producto.' a un precio de $'.number_format($ObjOrdenesdetalleServicio->total,2,'.',',');
                                    }
                                    $data_envio['paquete']='';
                                    if(isset($paquete)){
                                        $data_envio['paquete']=$paquete->producto.' folios a un precio de $'.number_format($ObjOrdenesdetallePaquete->total,2,'.',',');
                                    }
                                    $data_envio['total']='$'.number_format($ObjOrden->total,2,'.',',');

                                    //ENVIAR CORREO
                                    $data= array(
                                        'cuentabanco'=>$data_envio['cuentabanco'],
                                        'banco'=>$data_envio['banco'],
                                        'numerodecontacto'=>$data_envio['numerodecontacto'],
                                        'clabeinterbancaria'=>$data_envio['clabeinterbancaria'],
                                        'numerotarjeta'=>$data_envio['numerotarjeta'],
                                        'personarfc'=>$data_envio['personarfc'],
                                        'personanombre'=>$data_envio['personanombre'],
                                        'rfc'=>$data_envio['rfc'],
                                        'subscripcion'=>$data_envio['subscripcion'],
                                        'paquete'=>$data_envio['paquete'],
                                        'total'=>$data_envio['total'],
                                    );
                                    Mail::to($ObjOrden->email)->send(new Informacionpago($data));
                                }else{
                                    $error= 'No existe metodo pago disponible';
                                }
                            }else{
                                DB::rollback();
                                $error='No se pudo crear los detalles de la orden';
                            }
                        }else{
                            DB::rollback();
                            $error='No se pudo crear la orden';
                        }
                    }else{
                        DB::rollback();
                        $error='No se consiguio empresa del usuario';
                    }
                }
            }

            $data_envio['error']=0;
            $data_envio['metodopago']=$metodopago;
            if(isset($error)){
                $data_envio['error']=$error;
            }

            echo json_encode($data_envio);
        }
}
