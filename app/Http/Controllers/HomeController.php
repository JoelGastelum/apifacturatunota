<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Empresa;
use App\Certificado;
use App\Afiliado;
use App\Sucursal;
use App\Serie;
use App\Regimenfiscal;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

use nusoap_client;


//ANEXANDO STORAGE
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Vista home para usuarios idtipousuario=4
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->idtipousuario==4 || auth()->user()->idtipousuario==5){
            //CONSUMIDOR CLIENTE Y VENDEDOR
            $clientes=Cliente::where('idusuario','=',auth()->user()->id)->where('estatus','=','A')->get();
            foreach($clientes as $cliente){
                $hexa=$this->generacodigo($cliente->id);
                $cliente->hexa=$hexa;
            }
            $isAndroid=0;
            $tipodispositivo=$this->dispositivo();
            if($tipodispositivo>0){
                $navegador=$this->obtenerNavegadorWeb();
                if($navegador['isandroid']==1){
                    $isAndroid=1;
                }
            }

            return view('contribuyentes',[
                'clientes'=>$clientes,
                'isAndroid'=>$isAndroid,
                'tituloencabezado'=>'Mis Contribuyentes'
            ]);
        }elseif(auth()->user()->idtipousuario==2){
            $empresa=Empresa::where('id','=',auth()->user()->idempresa)->first();
            $regimenfiscales=Regimenfiscal::all()->pluck('descripcion','codigo');

            //MEMBRESIA VENCIDA
                $planvencido=0;
                $diadiferencias=0;
                $fecha_evaluar = $this->fechaSumaORestaDias(date("Y-m-d"),'+',$empresa->avisodiamembresia);
                $fechavigencia = $empresa->fechavigencia;
                if($fechavigencia!=null){
                    if($this->comparafecha($fecha_evaluar,$fechavigencia)==1){
                        $planvencido=1;
                        $diadiferencias=$this->diaDiferenciaFechas(date("Y-m-d"),$fechavigencia);
                    }
                }
            //FOLIOS
                $foliosvencidos=0;
                $folio_disponibles = $empresa->folioscomprados - $empresa->foliosconsumidos;
                if($folio_disponibles<=$empresa->avisofolio){
                    $foliosvencidos=1;
                }

            //ADMINISTRADOR DE EMPRESA
            return view('home',[
                'empresa'=>$empresa,
                'planvencido'=>$planvencido,
                'diadiferencias'=>$diadiferencias,
                'foliosvencidos'=>$foliosvencidos,
                'folio_disponibles'=>$folio_disponibles,
                'regimenfiscales'=>$regimenfiscales,
                'tituloencabezado'=>'Escritorio'
            ]);
        }else{
            return view('home',[
                'tituloencabezado'=>'Escritorio'
            ]);
        }
    }

    //MODAL DE CONFIGURACION
        /**
         * Pasos del modal de configuracion de empresa
         *
         */
        public function guardarpasos(Request $request){
            // dd($request);
            $dataenvio['error']=0;
            $dataenvio['msj']='';

            $empresa=Empresa::where('id','=',auth()->user()->idempresa)->first();
            if(!empty($empresa)){
                if($request->paso=="1"){
                    //GUARDA NOMBRE Y REGIMEN
                    $empresa->nombrecomercial = $request->nombrecomercial;
                    $empresa->regimen = $request->regimen;
                    $empresa->pasowizard = '1';
                    if(!$empresa->save()){
                        $dataenvio['error']=1;
                        $dataenvio['msj']='No se pudo actualizar el nombrecomercial y el regimen';
                    }
                }elseif($request->paso=="2"){
                    //GUARDAR Certificado
                    DB::beginTransaction();

                    $sitioguardarcomprobar='../storage/app/public/certificados/'.$empresa->id.'-'.$empresa->rfc;
                    $directorioexistente=1;
                    if (!file_exists($sitioguardarcomprobar)) {
                        Storage::makeDirectory('public/certificados/'.$empresa->id.'-'.$empresa->rfc);
                    }

                    if ($directorioexistente==1) {
                        $sitioguardar='public/certificados/'.$empresa->id.'-'.$empresa->rfc;
                    }

                    $certificado=Certificado::where('idempresa','=',$empresa->id)->first();
                    if(empty($certificado)){
                        $certificado=new Certificado();
                        $certificado->idempresa = $empresa->id;
                    }

                    $name = date("Ymd_His").'__';
                    if($request->hasFile('archivocer')){
                        $file = $request ->file('archivocer');
                        $nombrecer = $name.$file->getClientOriginalName();
                        $tipoarchivo = $this->typeFile($nombrecer);
                        $archivocer = $file->storeAs($sitioguardar,$nombrecer);
                    }
                    if($request->hasFile('archivokey')){
                        $file = $request ->file('archivokey');
                        $nombrekey = $name.$file->getClientOriginalName();
                        $tipoarchivo = $this->typeFile($nombrekey);
                        $archivokey = $file->storeAs($sitioguardar,$nombrekey);
                    }
                    $certificado->password= $request->password;

                    //configurando el espacio fisico en disco
                    $baseurlfisico=realpath(getcwd());
                    $direccion_cer = $archivocer;
                    $direccion_key = $archivokey;
                    $baseurlfisico = str_replace("public", "storage/app/public", $baseurlfisico);
                    $direccion_cer = str_replace("public", "", $direccion_cer);
                    $direccion_key = str_replace("public", "", $direccion_key);
                    $direccion_cer=$baseurlfisico.$direccion_cer;
                    $direccion_key=$baseurlfisico.$direccion_key;

                    $cer =  file_get_contents($direccion_cer);
                    $key =  file_get_contents($direccion_key);

                    $arrayCertificado = [
                        "id" => 0,
                        "idemisor" => $empresa->idempresaintegra,
                        "nombrecer" =>  $nombrecer,
                        "nombrekey" => $nombrekey,
                        "filecer" => base64_encode($cer),
                        "filekey" => base64_encode($key),
                        "passwordkey" => $request->password
                    ];

                    $SoapClient = new nusoap_client("http://integratucfdi.com/webservices/wsafiliados.php?wsdl", true);

                    $err = $SoapClient->getError();
                    if ($err){
                        $dataenvio['error']=1;
                        $dataenvio['msj']=$err;
                    }else{
                        $ObjAfiliado = Afiliado::where("id", "=", (int)env('IDAFILIADO'))->first();
                        if(!empty($ObjAfiliado)){
                            $respuesta = $SoapClient->call("registracertificado",array(
                                'idafiliado' => $ObjAfiliado->idafiliado,
                                'rfc' => $ObjAfiliado->rfc,
                                'usuario' => $ObjAfiliado->usuario,
                                'password' => $ObjAfiliado->password,
                                'certificado' => $arrayCertificado
                                )
                            );

                            if($respuesta['resultado']){
                                $certificado->serie = $respuesta["serie"];
                                $certificado->idintegra = $respuesta["id"];
                                $certificado->fechainicio = $respuesta["fechainicio"];
                                $certificado->fechafin = $respuesta["fechafin"];
                                $certificado->archivocer = $direccion_cer;
                                $certificado->archivokey = $direccion_key;

                                if($certificado->save()){
                                    $empresa->pasowizard = '2';
                                    if(!$empresa->save()){
                                        DB::rollback();
                                        $dataenvio['error']=1;
                                        $dataenvio['msj']='No se pudo actualizar el paso de la empresa';
                                    }else{
                                        DB::commit();
                                    }
                                }else{
                                    DB::rollback();
                                    $dataenvio['error']=1;
                                    $dataenvio['msj']='No se pudo guardar el certificado';
                                }
                            }else{
                                $dataenvio['error']=1;
                                $dataenvio['msj']=$respuesta['mensaje'];
                            }
                        }else{
                            $dataenvio['error']=1;
                            $dataenvio['msj']='Afiliado no encontrado';
                        }
                    }
                }elseif($request->paso=="3"){
                    //GUARDAR SERIE
                    $serie = Serie::where('id','=',$empresa->id)->first();
                    if(empty($serie)){
                        $serie = new Serie;
                        $serie->idempresa = $empresa->id;
                    }
                    $serie->serie = $request->serie;
                    $request->folio_actual=(int)$request->folio_actual;
                    if((int)$request->folio_actual==0){
                        $request->folio_actual=1;
                    }
                    $serie->folio_actual = $request->folio_actual;

                    $certificado=Certificado::where('idempresa','=',$empresa->id)->orderBy('id','DESC')->first();
                    if(!empty($certificado)){
                        $serie->idcertificadointegra = $certificado->idintegra;
                        if(!$serie->save()){
                            $dataenvio['error']=1;
                            $dataenvio['msj']='No se pudo guardar la serie';
                        }else{
                            $empresa->pasowizard = '3';
                            if(!$empresa->save()){
                                $dataenvio['error']=1;
                                $dataenvio['msj']='No se pudo actualizar el paso a la empresa';
                            }
                        }
                    }else{
                        $dataenvio['error']=1;
                        $dataenvio['msj']='Certificado no encontrado';
                    }
                }elseif($request->paso=="4"){
                    //GUARDAR SUCURSAL
                    $sucursal = new Sucursal;
                    $sucursal->idsucursalintegra = 0;
                    $sucursal->idempresa = $empresa->id;
                    $sucursal->nombre = $request->nombre;
                    $sucursal->domicilio = $request->domicilio;
                    $sucursal->codigopostal = $request->codigopostal;

                    $arraySucursal = [
                        "id" => $sucursal->idsucursalintegra,
                        "idemisor" => $empresa->idempresaintegra,
                        "nombre" =>  $sucursal->nombre,
                        "domicilio" => $sucursal->domicilio,
                        "codigopostal" => $sucursal->codigopostal
                    ];


                    $SoapClient = new nusoap_client("http://integratucfdi.com/webservices/wsafiliados.php?wsdl", true);
                    $err = $SoapClient->getError();
                    if ($err){
                        $dataenvio['error']=1;
                        $dataenvio['msj']=$err;
                    }else{
                        $ObjAfiliado = Afiliado::where("id", "=", (int)env('IDAFILIADO'))->first();
                        if(!empty($ObjAfiliado)){
                            $respuesta = $SoapClient->call("registrasucursal",array(
                                'idafiliado' => $ObjAfiliado->idafiliado,
                                'rfc' => $ObjAfiliado->rfc,
                                'usuario' => $ObjAfiliado->usuario,
                                'password' => $ObjAfiliado->password,
                                'sucursal' => $arraySucursal
                            ));

                            if($respuesta['resultado'] > 0){
                                $serie=Serie::where('idempresa','=',$empresa->id)->orderBy('id','DESC')->first();
                                $idserie=0;
                                $idserienc=0;
                                $idseriepago=0;
                                if(!empty($serie)){
                                    $idserie=$serie->id;
                                }

                                $sucursal->idserie=$idserie;
                                $sucursal->idserienc=$idserienc;
                                $sucursal->idseriepago=$idseriepago;
                                $sucursal->idsucursalintegra= $respuesta['id'];

                                if($sucursal->save()){
                                    $empresa->pasowizard = '4';
                                    if(!$empresa->save()){
                                        $dataenvio['error']=1;
                                        $dataenvio['msj']='No se pudo actualizar el paso a la empresa';
                                    }
                                }else{
                                    $dataenvio['error']=1;
                                    $dataenvio['msj']='No se pudo guardar la sucursal';
                                }
                            }else{
                                $dataenvio['error']=1;
                                $dataenvio['msj']=$respuesta['mensaje'];
                            }
                        }else{
                            $dataenvio['error']=1;
                            $dataenvio['msj']='No se consigue afiliado';
                        }
                    }
                }
            }else{
                $dataenvio['error']=1;
                $dataenvio['msj']='No se consiguio empresa';
            }

            echo json_encode($dataenvio);
        }
}
