<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    protected $table = 'ordenes';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordenesdetalle()
    {
        return $this->hasMany('App\Ordenesdetalle', 'idorden');
    }
}

