<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remisiondetalle extends Model
{
    //
    protected $table="remisionesdetalle";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idempresa',
        'idremision' ,
        'idconcepto',
        "codigo",
        "unidad",
        "cantidad",
        "descripcion",
        "series",
        "precio",
        "importe",
        "tasaiva",
        "tasaieps",
        "importeconimpuestos",
        "importeieps",
        "importeiva",
        "codigounidadsat",
        "codigogrupoprodsersat"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function concepto()
    {
        return $this->belongsTo('App\Concepto', 'idconcepto');
    }

}
