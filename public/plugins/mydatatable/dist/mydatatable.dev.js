"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var myDataTable =
/*#__PURE__*/
function () {
  function myDataTable(container) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, myDataTable);

    this.instancename = options.instanceVarName;
    this.container = container;
    this.columns = options.columns;
    this.height = 600;
    this.onclickrow = options.onclickrow || '';
    this.responsive = options.responsive || true;
    this.filtros = [];
    this.dataFilters = [];
    this.cantidad = 0; //VARIABLE PARA SUMAR LA CANTIDAD DEL REGISTRO SEGUN FILTRADO O NO

    var lheight = (options.height || 0) - 40; // var lheight=600-70;

    var shtml = ''; //Inicializar el arreglo de filtros y formar encabezado de la tabla

    for (var i = 0; i < this.columns.length; i++) {
    
      this.dataFilters[i] = [];
      this.filtros[i] = [];

      if (this.columns[i].visible) {
        shtml += "<th style=\"position: sticky;top: 0;box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);background: #DFE3E8;white-space: nowrap;".concat(this.columns[i].style || '', "\">").concat(this.columns[i].title || this.columns[i].data);

        if (this.columns[i].ordenar == true) {
          shtml += "<a href=\"javascript:void(0);\" onclick=\"".concat(this.instancename, ".sortTable(").concat(i, ");\" style=\"font-size:14px;color:#959596;\"><i class=\"fas fa-sort\"></i></a>");
        }

        if (this.columns[i].filter) {
          shtml += " <a href=\"#\" onclick=\"".concat(this.instancename, ".showFilter(").concat(i, ");\" style=\"font-size:8px;color:#959596;\"><i class=\"fa fa-filter\"> </i></a>\n\t\t\t\t\t<div id=\"").concat(this.container, "-filter-").concat(i, "\" class=\"").concat(this.container, "-filter\" style=\"margin-top:3px;position:absolute;border: solid 1px gray;background-color: white;display:none;z-index:100;\">\n\t\t\t\t\t\t<div id=\"").concat(this.container, "-filter-title-").concat(i, "\" style=\"height:30px;background-color: lightgray;\">Titulo</div>\n\t\t\t\t\t\t<input type=\"text\" onkeyup=\"").concat(this.instancename, ".filtrafiltro(").concat(i, ");\" class=\"form-control\" id=\"buscador-").concat(i, "\" value=\"\" style=\"width:195px;font-size: 11px;\">\n\t\t\t\t\t\t<div style=\"max-height:").concat(lheight - 200, "px;overflow-y:scroll;\">\n\t\t\t\t\t\t\t<table style=\"width:150px;font-family: calibri;font-size:14pt;text-align:left;\">\n\t\t\t\t\t\t\t\t<tbody id=\"").concat(this.container, "-filter-data-").concat(i, "\"></tbody>\n\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div style=\"text-align: center;border-top: 1px solid whitesmoke;padding: 2px;\">\n\t\t\t\t\t\t\t<input type=\"button\" class=\"btn btn-sm btn-warning\" value=\"Aceptar\"   onclick=\"filtrartabla('").concat(this.instancename, "',").concat(i, ");\">&nbsp\n\t\t\t\t\t\t\t<input type=\"button\" class=\"btn btn-sm btn-primary\" value=\"Cancelar\"  onclick=\"").concat(this.instancename, ".closeFilter(").concat(i, ",false);\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t");
        }

        shtml += "</th>";
      }
    } //Agregar encabezados de la tabla
    //onscroll="document.getElementById('${this.container}-header').style.left = (this.scrollLeft*-1)+'px';console.log(this.scrollLeft);"


    shtml = "\n\t\t<div style=\"overflow:scroll;position: absolute;top: 7px;width: 100%;height: ".concat(lheight, "px;border:1px solid;\">\n\t\t<table class=\"").concat(this.container, "-tablesinc table-bordered table-hover table-striped\" style=\"position:absolute;font-family: calibri;font-size:16pt;width: 100%;min-width:100%; \">\n\t\t\t<thead id=\"").concat(this.container, "-head\">\n\t\t\t\t<tr>").concat(shtml, "</tr>\n\t\t\t</thead>\n\t\t\t<tbody id=\"").concat(this.container, "-data\" style=\"max-height:").concat(this.height, "px;color:black;\"></tbody>\n\t\t</table>\n\t\t</div>\n\t\t<div id=\"").concat(this.container, "-tfoot\" style=\"top:").concat(lheight + 6, "px; border:solid 1px; position: absolute;background: #DFE3E8;white-space: nowrap; font-family: calibri;font-size:16pt;width: 100%;min-width:100%;\">\n\t\t</div>\n\t\t");
    document.getElementById(this.container).innerHTML = shtml;
    document.getElementById(this.container).style.position = "relative";
    document.getElementById(this.container).style.overflow = "hidden";
    document.getElementById(this.container).style.height = options.height + 'px';
    document.getElementById(this.container).classList.remove('table-responsive');
  }

  _createClass(myDataTable, [{
    key: "initializeTable",
    value: function initializeTable(data) {
      var self = this;
      self.data = data;
      self.cantidad = 0; // setTimeout(self.fillTable(true),10);
      // setTimeout(self.initializeFilters(),10);

      self.initializeFilters();
      fillTable(this.instancename, 0, true);
    }
  }, {
    key: "initializeFilters",
    value: function initializeFilters() {
      var self = this; //LLenar el arreglo de filtros con los valores unicos

      self.data.forEach(function (dataRow, indexRow) {
        for (var columnIndex = 0; columnIndex < self.columns.length; columnIndex++) {
          if (self.dataFilters[columnIndex].indexOf(dataRow[self.columns[columnIndex]['data']]) == -1) {
            if (self.columns[columnIndex].filtervalue != undefined) {
              if (dataRow[self.columns[columnIndex]['data']] == self.columns[columnIndex].filtervalue) {
                self.filtros[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
              }
            } else {
              self.filtros[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
            }

            self.dataFilters[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
          }
        }

        ;
      });
      self.dataFilters.forEach(function (dataColumn, columnIndex) {
        self.dataFilters[columnIndex].sort().reverse();
      });
    }
  }, {
    key: "fillTable",
    value: function fillTable() {
      var all = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var numberrow = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var self = this; //Agregar los datos a la tabla

      var shtml = '';
      var contador = 0;
      var fin = true;

      if (all) {
        var cantidadvisible = 0;
        var totFiltros = self.filtros.length;

        for (var i = numberrow; i < self.data.length; i++) {
          var rowIndex = i;
          var dataRow = self.data[i]; // self.data.forEach(function(dataRow,rowIndex){

          var showRow = '';

          for (var index = 0; index < totFiltros; index++) {
            if (self.filtros[index].indexOf(dataRow[self.columns[index]['data']]) == -1) {
              showRow = 'none';
              break;
            }
          }

          if (showRow == '') {
            cantidadvisible++;
          }

          self.cantidad++;

          if (self.onclickrow != '') {
            var onclick = JSON.stringify(dataRow);
            shtml += "<tr id=\"".concat(self.instancename, "-rowIndex-").concat(rowIndex, "\" onclick='").concat(self.onclickrow, "(").concat(onclick, ")' style=\"display:").concat(showRow, ";\">");
          } else {
            shtml += "<tr id=\"".concat(self.instancename, "-rowIndex-").concat(rowIndex, "\" style=\"display:").concat(showRow, ";\">");
          }

          for (var columnIndex = 0; columnIndex < self.columns.length; columnIndex++) {
           
            if (self.columns[columnIndex].visible){
              shtml += "<td";
              console.log(self.columns[columnIndex].onClick);
              if(self.columns[columnIndex].onClick!='')
               shtml=shtml+ ' onClick="'+self.columns[columnIndex].onClick+'"style=\"white-space: nowrap;'.concat(self.columns[columnIndex].style || '', "\"><a>").concat(dataRow[self.columns[columnIndex]['data']], "</a></td>");
               else
                shtml += " style=\"white-space: nowrap;".concat(self.columns[columnIndex].style || '', "\">").concat(dataRow[self.columns[columnIndex]['data']], "</td>");
              }
        
            }
          shtml += "</tr>";
          contador++; // if(contador == 5000){
          // 	fin = false;
          // 	break;
          // }
        }

        shtml += "<tr class=\"ignorar\"><td colspan=\"".concat(self.columns.length, "\"></td></tr>");
      } else {
        var totFiltros = self.filtros.length;
        var cantidadvisible = 0;

        for (var i = numberrow; i < self.data.length; i++) {
          var rowIndex = i;
          var dataRow = self.data[i];
          var showRow = true;

          for (var index = 0; index < totFiltros; index++) {
            if (self.filtros[index].indexOf(dataRow[self.columns[index]['data']]) == -1) {
              showRow = false;
              break;
            }
          }

          if (showRow == true) {
            cantidadvisible++;
          }

          document.getElementById("".concat(self.instancename, "-rowIndex-").concat(rowIndex)).style.display = showRow ? '' : 'none'; // if(showRow==false){
          // 	// self.data.forEach(function(dataRow,rowIndex){
          // 	if(self.onclickrow!=''){
          // 		var onclick=JSON.stringify(dataRow);
          // 		shtml +=`<tr id="rowIndex-${rowIndex}" onclick='${self.onclickrow}(${onclick})'>`;
          // 	}else{
          // 		shtml +=`<tr id="rowIndex-${rowIndex}">`;
          // 	}
          // 	for(var columnIndex=0;columnIndex<self.columns.length;columnIndex++){
          // 		if(self.columns[columnIndex].visible)
          // 			shtml +=`<td style="${self.columns[columnIndex].style||''}">${dataRow[self.columns[columnIndex]['data']]}</td>`;
          // 	}
          // 	shtml +=`</tr>`;
          // 	contador ++ ;
          // 	if(contador == 5000){
          // 		fin = false;
          // 		break;
          // 	}
          // }
        }
      }

      document.getElementById("".concat(self.container, "-tfoot")).innerHTML = '&nbsp;Rows: ' + cantidadvisible + ' of ' + self.cantidad;
      return [shtml, contador + numberrow, fin];
    }
  }, {
    key: "adjustColumn",
    value: function adjustColumn() {
      var self = this;

      if (self.responsive) {
        //Sincronizar el tamaño de cada columna
        var tableArr = document.getElementsByClassName("".concat(self.container, "-tablesinc"));
        var cellWidths = new Array(); // verificar cual de las 2 celdas es mayor si la del encabezado o a de los datos

        for (var i = 0; i < tableArr.length; i++) {
          for (var j = 0; j < tableArr[i].rows[0].cells.length; j++) {
            var cell = tableArr[i].rows[0].cells[j]; // console.log(cellWidths[j],cell.clientWidth,j);

            if (!cellWidths[j] || cellWidths[j] < cell.clientWidth) cellWidths[j] = cell.clientWidth;
          }
        } // asignar el tamaño de cada celda a ambas tablas


        for (var i = 0; i < tableArr.length; i++) {
          for (var j = 0; j < tableArr[i].rows[0].cells.length; j++) {
            tableArr[i].rows[0].cells[j].style.maxWidth = cellWidths[j] + 'px';
            tableArr[i].rows[0].cells[j].style.minWidth = cellWidths[j] + 'px';
          }
        }
      }
    }
  }, {
    key: "showFilter",
    value: function showFilter(column) {
      //Actualizar todos los checkbox
      document.getElementById("buscador-".concat(column)).value = '';
      var clist = document.getElementsByClassName("".concat(this.container, "-filter"));

      for (var i = 0; i < clist.length; ++i) {
        clist[i].style.display = "none";
      }

      var posX = event.clientX;
      event.preventDefault(); //var posY=0;

      document.getElementById("".concat(this.container, "-filter-title-").concat(column)).innerHTML = this.columns[column].title; //document.getElementById(`${this.container}-filter`).style.marginLeft=posX;
      //document.getElementById(`${this.container}-filter`).style.top=posY;

      document.getElementById("".concat(this.container, "-filter-").concat(column)).style.display = "block";
      var shtml = "<!--table style=\"font-size: 10px;\"-->\n\t\t\t<tr><td style=\"width:24px;\"><input type=\"checkbox\" ".concat(this.filtros[column].length == this.dataFilters[column].length ? "checked" : "", " onclick=\"").concat(this.instancename, ".checkAll(").concat(column, ",this.checked);\"></td><td>(todos)</td></tr>");

      for (var i = 0; i < this.dataFilters[column].length; i++) {
        shtml += "<tr id=\"filtrorow-".concat(column, "-").concat(i, "\"><td style=\"width:24px;\"><input type=\"checkbox\" class=\"").concat(this.container, "-chkfilters-").concat(column, "\" ").concat(this.filtros[column].indexOf(this.dataFilters[column][i]) == -1 ? "" : "checked", " value=").concat(i, "></td><td id=\"filtrocolumn-").concat(column, "-").concat(i, "\">").concat(this.dataFilters[column][i], "</td></tr>");
      } //shtml+='</table>';


      document.getElementById("".concat(this.container, "-filter-data-").concat(column)).innerHTML = shtml;
    }
  }, {
    key: "closeFilter",
    value: function closeFilter(columnindex, filtrar) {
      document.getElementById("".concat(this.container, "-filter-").concat(columnindex)).style.display = "none";

      if (filtrar) {
        //Actualizar el arreglo de filtros en base a los check
        this.filtros[columnindex] = [];
        var clist = document.getElementsByClassName("".concat(this.container, "-chkfilters-").concat(columnindex));

        for (var i = 0; i < clist.length; ++i) {
          // console.log('filtrando por for');
          if (clist[i].checked) this.filtros[columnindex].push(this.dataFilters[columnindex][clist[i].value]);
        } // console.log('terminar de filtrar');
        // setTimeout(`${this.instancename}.fillTable()`,10);

      }
    }
  }, {
    key: "checkAll",
    value: function checkAll(columnIndex, checked) {
      var self = this;
      /*
      if(checked){
      	self.filtros[columnIndex]=[];
      	self.dataFilters[columnIndex].forEach(function(value,index){
      		self.filtros[columnIndex].push(value);
      	});
      }
      else{
      	self.filtros[columnIndex]=[];
      }
      */
      //Actualizar todos los checkbox

      var clist = document.getElementsByClassName("".concat(self.container, "-chkfilters-").concat(columnIndex));

      for (var i = 0; i < clist.length; ++i) {
        clist[i].checked = checked;
      }
    }
    /**
     * Permite filtrar los filtros a traves del input
     */

  }, {
    key: "filtrafiltro",
    value: function filtrafiltro(column) {
      var buscar = document.getElementById("buscador-".concat(column)).value;

      if (buscar != '') {
        for (var i = 0; i < this.dataFilters[column].length; i++) {
          var td_valor = document.getElementById("filtrocolumn-".concat(column, "-").concat(i)).innerText;

          if (td_valor.toUpperCase().indexOf(buscar.toUpperCase()) == -1) {
            document.getElementById("filtrorow-".concat(column, "-").concat(i)).style.display = "none";
          } else {
            document.getElementById("filtrorow-".concat(column, "-").concat(i)).style.display = "";
          }
        }
      } else {
        for (var i = 0; i < this.dataFilters[column].length; i++) {
          document.getElementById("filtrorow-".concat(column, "-").concat(i)).style.display = "";
        }
      }
    }
    /**
     * Agregar fila al final
     * @use se usa en adjuste de inventario
     */

  }, {
    key: "addrow",
    value: function addrow(dataRow) {
      var self = this;

      for (var columnIndex = 0; columnIndex < self.columns.length; columnIndex++) {
        if (self.filtros[columnIndex].indexOf(dataRow[self.columns[columnIndex]['data']]) == -1) {
          self.dataFilters[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
          self.filtros[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
        }
      }

      ;
      self.data.push(dataRow);
      var totFiltros = self.filtros.length;
      var rowIndex = self.data.length - 1;
      var showRow = true;
      var shtml = '';

      for (var index = 0; index < totFiltros; index++) {
        if (self.filtros[index].indexOf(dataRow[self.columns[index]['data']]) == -1) {
          showRow = false;
          break;
        }
      }

      if (self.onclickrow != '') {
        var onclick = JSON.stringify(dataRow);
        shtml += "<tr id=\"rowIndex-".concat(rowIndex, "\" onclick='").concat(self.onclickrow, "(").concat(onclick, ")' style=\"display: ").concat(showRow ? '' : 'none', "\">");
      } else {
        shtml += "<tr id=\"rowIndex-".concat(rowIndex, "\" style=\"display: ").concat(showRow ? '' : 'none', "\">");
      }

      for (var columnIndex = 0; columnIndex < self.columns.length; columnIndex++) {
        if (self.columns[columnIndex].visible) shtml += "<td style=\"".concat(self.columns[columnIndex].style || '', "\">").concat(dataRow[self.columns[columnIndex]['data']], "</td>");
      }

      shtml += "</tr>";
      $('#tabladatos-data').append(shtml);
    }
    /**
     * Permite ordenar la tabla segun una columna
     */

  }, {
    key: "sortTable",
    value: function sortTable(column) {
      var table,
          rows,
          switching,
          i,
          x,
          y,
          shouldSwitch,
          dir,
          switchcount = 0;
      var type = this.columns[column].type || 'text';
      table = document.getElementById("".concat(this.container, "-data"));
      switching = true; // Set the sorting direction to ascending:

      dir = "asc";
      /* Make a loop that will continue until
      no switching has been done: */

      while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */

        for (i = 0; i < rows.length - 2; i++) {
          // Start by saying there should be no switching:
          shouldSwitch = false;
          /* Get the two elements you want to compare,
          one from current row and one from the next: */

          x = rows[i].getElementsByTagName("TD")[column];
          y = rows[i + 1].getElementsByTagName("TD")[column];
          /* Check if the two rows should switch place,
          based on the direction, asc or desc: */

          if (dir == "asc") {
            if (type == 'text') {
              if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            } else {
              if (Number(x.innerHTML.replace(',', '')) > Number(y.innerHTML.replace(',', ''))) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            }
          } else if (dir == "desc") {
            if (type == 'text') {
              if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            } else {
              if (Number(x.innerHTML.replace(',', '')) < Number(y.innerHTML.replace(',', ''))) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
              }
            }
          }
        }

        if (shouldSwitch) {
          /* If a switch has been marked, make the switch
          and mark that a switch has been done: */
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true; // Each time a switch is done, increase this count by 1:

          switchcount++;
        } else {
          /* If no switching has been done AND the direction is "asc",
          set the direction to "desc" and run the while loop again. */
          if (switchcount == 0 && dir == "asc") {
            dir = "desc";
            switching = true;
          }
        }
      }
    }
    /*
    check(columnIndex,index,checked){
    	if(checked){
    		//Agregar el valor a filtros
    		this.filtros[columnIndex].push(this.dataFilters[columnIndex][index]);
    	}
    	else{
    		//Eliminar el valor de filtros
    		var rowIndex=this.filtros[columnIndex].indexOf(this.dataFilters[columnIndex][index]);
    		this.filtros[columnIndex].splice(rowIndex, 1);
    	}
    }*/

  }]);

  return myDataTable;
}();
/**
* Permite rellenar las tabla por parte
*/


function fillTable(instancename, numberrow, all) {
  if (instancename == 'ObjTabla') {
    var arrayresult = ObjTabla.fillTable(all, numberrow);

    if (numberrow == 0) {
      if (all == true) {
        $('#tabladatos-data').html(arrayresult[0]);
      }
    } else {
      if (all == true) {
        $('#tabladatos-data').append(arrayresult[0]);
      }
    }

    if (arrayresult[2] == false) {
      setTimeout(fillTable, 10, instancename, arrayresult[1], all);
    } else {
      Swal.close();
      var concat = 'fincarga';

      if (typeof window[concat] == 'function') {
        window[concat](ObjTabla.container); //EJECUTA FUNCION
      }

      console.log('LISTO1');
    }
  }

  if (instancename == 'ObjTabla2') {
    var arrayresult = ObjTabla2.fillTable(all, numberrow);
    console.log(arrayresult);

    if (numberrow == 0) {
      if (all == true) {
        $('#tabladatos2-data').html(arrayresult[0]);
      }
    } else {
      if (all == true) {
        $('#tabladatos2-data').append(arrayresult[0]);
      }
    }

    if (arrayresult[2] == false) {
      setTimeout(fillTable, 10, instancename, arrayresult[1], all);
    } else {
      Swal.close();
      console.log('LISTO');
    }
  }

  if (instancename == 'ObjTabla3') {
    var arrayresult = ObjTabla3.fillTable(all, numberrow);
    console.log(arrayresult);

    if (numberrow == 0) {
      if (all == true) {
        $('#tabladatos3-data').html(arrayresult[0]);
      }
    } else {
      if (all == true) {
        $('#tabladatos3-data').append(arrayresult[0]);
      }
    }

    if (arrayresult[2] == false) {
      setTimeout(fillTable, 10, instancename, arrayresult[1], all);
    } else {
      Swal.close();
      console.log('LISTO');
    }
  }

  if (instancename == 'ObjTabla4') {
    var arrayresult = ObjTabla4.fillTable(all, numberrow);
    console.log(arrayresult);

    if (numberrow == 0) {
      if (all == true) {
        $('#tabladatos4-data').html(arrayresult[0]);
      }
    } else {
      if (all == true) {
        $('#tabladatos4-data').append(arrayresult[0]);
      }
    }

    if (arrayresult[2] == false) {
      setTimeout(fillTable, 10, instancename, arrayresult[1], all);
    } else {
      Swal.close();
      console.log('LISTO');
    }
  }

  if (instancename == 'ObjTabla5') {
    var arrayresult = ObjTabla5.fillTable(all, numberrow);
    console.log(arrayresult);

    if (numberrow == 0) {
      if (all == true) {
        $('#tabladatos5-data').html(arrayresult[0]);
      }
    } else {
      if (all == true) {
        $('#tabladatos5-data').append(arrayresult[0]);
      }
    }

    if (arrayresult[2] == false) {
      setTimeout(fillTable, 10, instancename, arrayresult[1], all);
    } else {
      Swal.close();
      console.log('LISTO');
    }
  }

  if (instancename == 'ObjTabla6') {
    var arrayresult = ObjTabla6.fillTable(all, numberrow);
    console.log(arrayresult);

    if (numberrow == 0) {
      if (all == true) {
        $('#tabladatos6-data').html(arrayresult[0]);
      }
    } else {
      if (all == true) {
        $('#tabladatos6-data').append(arrayresult[0]);
      }
    }

    if (arrayresult[2] == false) {
      setTimeout(fillTable, 10, instancename, arrayresult[1], all);
    } else {
      Swal.close();
      console.log('LISTO');
    }
  }
}
/**
* Permite filtrar tabla una vez seleccionado una opcion
*/


function filtrartabla(instancename, numbercolum) {
  if (instancename == 'ObjTabla') {
    ObjTabla.closeFilter(numbercolum, true);
    Swal.fire({
      title: 'Filtering, please wait'
    });
    Swal.showLoading(); // console.log('Antes limpiar '+Date.now());
    // document.getElementById('tabladatos-data').innerHTML = "";
    // console.log('Despues de limpiar '+Date.now());
    // $('#tabladatos-data').html('');

    setTimeout(fillTable, 1000, instancename, 0, false);
  }

  if (instancename == 'ObjTabla2') {
    ObjTabla2.closeFilter(numbercolum, true);
    Swal.fire({
      title: 'Filtering, please wait'
    });
    Swal.showLoading(); // console.log('Antes limpiar '+Date.now());
    // document.getElementById('tabladatos-data').innerHTML = "";
    // console.log('Despues de limpiar '+Date.now());
    // $('#tabladatos-data').html('');

    console.log(instancename);
    setTimeout(fillTable, 1000, instancename, 0, false);
  }

  if (instancename == 'ObjTabla3') {
    ObjTabla3.closeFilter(numbercolum, true);
    Swal.fire({
      title: 'Filtering, please wait'
    });
    Swal.showLoading(); // console.log('Antes limpiar '+Date.now());
    // document.getElementById('tabladatos-data').innerHTML = "";
    // console.log('Despues de limpiar '+Date.now());
    // $('#tabladatos-data').html('');

    console.log(instancename);
    setTimeout(fillTable, 1000, instancename, 0, false);
  }

  if (instancename == 'ObjTabla4') {
    ObjTabla4.closeFilter(numbercolum, true);
    Swal.fire({
      title: 'Filtering, please wait'
    });
    Swal.showLoading(); // console.log('Antes limpiar '+Date.now());
    // document.getElementById('tabladatos-data').innerHTML = "";
    // console.log('Despues de limpiar '+Date.now());
    // $('#tabladatos-data').html('');

    console.log(instancename);
    setTimeout(fillTable, 1000, instancename, 0, false);
  }

  if (instancename == 'ObjTabla5') {
    ObjTabla5.closeFilter(numbercolum, true);
    Swal.fire({
      title: 'Filtering, please wait'
    });
    Swal.showLoading(); // console.log('Antes limpiar '+Date.now());
    // document.getElementById('tabladatos-data').innerHTML = "";
    // console.log('Despues de limpiar '+Date.now());
    // $('#tabladatos-data').html('');

    console.log(instancename);
    setTimeout(fillTable, 1000, instancename, 0, false);
  }

  if (instancename == 'ObjTabla6') {
    ObjTabla6.closeFilter(numbercolum, true);
    Swal.fire({
      title: 'Filtering, please wait'
    });
    Swal.showLoading(); // console.log('Antes limpiar '+Date.now());
    // document.getElementById('tabladatos-data').innerHTML = "";
    // console.log('Despues de limpiar '+Date.now());
    // $('#tabladatos-data').html('');

    console.log(instancename);
    setTimeout(fillTable, 1000, instancename, 0, false);
  }
}