class myDataTable{
	constructor(container,options={})
	{
		this.instancename=options.instanceVarName;
		this.container=container;
		this.columns=options.columns;
		this.height=600;
		this.onclickrow=options.onclickrow||'';
		this.responsive=options.responsive||true;

		this.filtros=[];
		this.dataFilters=[];
		this.cantidad=0;//VARIABLE PARA SUMAR LA CANTIDAD DEL REGISTRO SEGUN FILTRADO O NO
		var lheight=(options.height||0)-40;
		// var lheight=600-70;
		var shtml='';
		//Inicializar el arreglo de filtros y formar encabezado de la tabla
		for(var i=0;i<this.columns.length;i++){
			this.dataFilters[i]=[];
			this.filtros[i]=[];
			if(this.columns[i].visible)
			{
				shtml+=`<th style="position: sticky;top: 0;box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);background: #f5f5f5;;white-space: nowrap;${this.columns[i].style||''}">${this.columns[i].title||this.columns[i].data}`;
				if(this.columns[i].ordenar==true){
					shtml+=`<a href="javascript:void(0);" onclick="${this.instancename}.sortTable(${i});" style="font-size:14px;color:#f9fafc;"><i class="fas fa-sort"></i></a>`;
				}

				if(this.columns[i].filter)
				{
					shtml+=` <a href="#" onclick="${this.instancename}.showFilter(${i});" style="font-size:8px;color:#c2c2c4;"><i class="fa fa-filter"> </i></a>
					<div id="${this.container}-filter-${i}" class="${this.container}-filter" style="margin-top:12px;position:absolute;border: solid 1px #DFE3E8;background-color: white;display:none;z-index:100;">
						<div id="${this.container}-filter-title-${i}" style="height:30px;background-color: #f5f5f5; padding:.75rem;">Titulo</div>
						<input type="text" onkeyup="${this.instancename}.filtrafiltro(${i});" class="form-control form-control-sm" id="buscador-${i}" value="" style="width:195px;font-size: font-size: 1rem;">
						<div style="max-height:${lheight-200}px;overflow-y:scroll;">
							<table style="width:150px;font-family: calibri;font-size:font-size: 1rem;color:#495057;text-align:left;border-bottom: 0px solid #e6ebf4;">
								<tbody id="${this.container}-filter-data-${i}"></tbody>
							</table>
						</div>
						<div style="text-align: center;border-top: 1px solid whitesmoke;padding: 2px;">
							<input type="button" class="btn btn-sm btn-warning" value="Aceptar"   onclick="${this.instancename}.closeFilter(${i},true);${this.instancename}.filtrardatos();">&nbsp
							<input type="button" class="btn btn-sm btn-primary" value="Cancelar"  onclick="${this.instancename}.closeFilter(${i},false);">
						</div>
					</div>
					`;
				}
				shtml+=`</th>`;
			}
		}
		//Agregar encabezados de la tabla
		//onscroll="document.getElementById('${this.container}-header').style.left = (this.scrollLeft*-1)+'px';console.log(this.scrollLeft);"
		shtml=`
		<div class="div_tabladatos" style="height: ${lheight}px;">
		<table class="${this.container}-tablesinc table-bordered table-hover table-striped" style="position:absolute;font-family: calibri;font-size:font-size: 1rem;width: 100%;min-width:100%; ">
			<thead id="${this.container}-head">
				<tr>${shtml}</tr>
			</thead>
			<tbody id="${this.container}-data" style="max-height:${this.height}px;color:black;"></tbody>
		</table>
		</div>
		<div id="${this.container}-tfoot" style="top:${lheight+6}px; border:solid 1px #DFE3E8; position: absolute;background: #f5f5f5;;white-space: nowrap; font-family: calibri;font-size: 1rem; padding:.25rem; width: 100%;min-width:100%;">
		</div>
		`;
		document.getElementById(this.container).innerHTML=shtml;
		document.getElementById(this.container).style.position="relative";
		document.getElementById(this.container).style.overflow="hidden";
		document.getElementById(this.container).style.height=options.height+'px';
		document.getElementById(this.container).classList.remove('table-responsive');
	}
	initializeTable(data){
		var self=this;
		self.data=data;
		self.cantidad=0;

		// setTimeout(self.fillTable(true),10);
		// setTimeout(self.initializeFilters(),10);
		self.initializeFilters();
		this.fillTable();
	}
	initializeFilters(){
		var self=this;
		//LLenar el arreglo de filtros con los valores unicos
		self.data.forEach(function(dataRow,indexRow){
			for(var columnIndex=0;columnIndex<self.columns.length;columnIndex++){
				if(self.dataFilters[columnIndex].indexOf(dataRow[self.columns[columnIndex]['data']])==-1)
				{
					if(self.columns[columnIndex].filtervalue!=undefined){
						if(dataRow[self.columns[columnIndex]['data']]==self.columns[columnIndex].filtervalue){
							self.filtros[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
						}
					}else{
						self.filtros[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
					}
					self.dataFilters[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
				}
			};
		});
		self.dataFilters.forEach(function(dataColumn,columnIndex){
			self.dataFilters[columnIndex].sort().reverse();
		});
	}
	fillTable(){
		var self=this;
		//Agregar los datos a la tabla
		var shtml='';
			var cantidadvisible = 0;
			var totFiltros=self.filtros.length;
			for(var i=0;i<self.data.length;i++){
				var rowIndex = i;
				var dataRow = self.data[i];
				// self.data.forEach(function(dataRow,rowIndex){
				var showRow='';
				for(var index=0;index<totFiltros;index++){
					if(self.filtros[index].indexOf(dataRow[self.columns[index]['data']])==-1)
					{
						showRow='none';
						break;
					}
				}
				if(showRow==''){
					cantidadvisible++;
				}
				self.cantidad++;


				if(self.onclickrow!=''){
					var onclick=JSON.stringify(dataRow);
					shtml +=`<tr id="${self.instancename}-rowIndex-${rowIndex}" onclick='${self.onclickrow}(${onclick})' style="display:${showRow};">`;
				}else{
					shtml +=`<tr id="${self.instancename}-rowIndex-${rowIndex}" style="display:${showRow};">`;
				}
				for(var columnIndex=0;columnIndex<self.columns.length;columnIndex++){
				/*	if(self.columns[columnIndex].visible)
						shtml +=`<td style="white-space: nowrap;${self.columns[columnIndex].style||''}">${dataRow[self.columns[columnIndex]['data']]}</td>`;
				*/
					if (self.columns[columnIndex].visible){
						shtml += "<td";

						if(self.columns[columnIndex].onClick!=null){
						shtml += " style=\"white-space: nowrap;".concat(self.columns[columnIndex].style || '', "\"><a href='#' onClick='"+self.columns[columnIndex].onClick+"'><i class='fa fa-pencil-alt'></i>").concat(dataRow[self.columns[columnIndex]['data']], "</a></td>");
						}else{
						shtml += " style=\"white-space: nowrap;".concat(self.columns[columnIndex].style || '', "\">").concat(dataRow[self.columns[columnIndex]['data']], "</td>");
						}
					}else{
						shtml += "<td style=\"display: none;".concat(self.columns[columnIndex].style || '', "\">").concat(dataRow[self.columns[columnIndex]['data']], "</td>");
					}
				}
				shtml +=`</tr>`;
			}
			shtml +=`<tr class="ignorar d-none"><td colspan="${self.columns.length}"></td></tr>`;
		$(`#${this.container}-data`).html(shtml);
		document.getElementById(`${self.container}-tfoot`).innerHTML = '&nbsp;Filas: '+cantidadvisible+' de '+self.cantidad;
		var concat = 'fincarga';
		if(typeof window[concat] == 'function'){
			window[concat](ObjTabla.container);//EJECUTA FUNCION
		}
	}

	filtrardatos(){
		self=this;
		var totFiltros=self.filtros.length;
		var cantidadvisible = 0;
		for(var i=0;i<self.data.length;i++){
			var rowIndex = i;
			var dataRow = self.data[i];

			var showRow=true;
			for(var index=0;index<totFiltros;index++){
				if(self.filtros[index].indexOf(dataRow[self.columns[index]['data']])==-1)
				{
					showRow=false;
					break;
				}
			}
			if(showRow==true){
				cantidadvisible++;
			}

			document.getElementById(`${self.instancename}-rowIndex-${rowIndex}`).style.display = showRow?'':'none';
		}
	}

	adjustColumn(){
		var self = this;
		if(self.responsive)
		{
			//Sincronizar el tamaño de cada columna
			var tableArr = document.getElementsByClassName(`${self.container}-tablesinc`);
			var cellWidths = new Array();

			// verificar cual de las 2 celdas es mayor si la del encabezado o a de los datos
			for(var i = 0; i < tableArr.length; i++)
			{
				for(var j = 0; j < tableArr[i].rows[0].cells.length; j++)
				{
				   var cell = tableArr[i].rows[0].cells[j];
					// console.log(cellWidths[j],cell.clientWidth,j);
				   if(!cellWidths[j] || cellWidths[j] < cell.clientWidth)
						cellWidths[j] = cell.clientWidth;
				}
			}

			// asignar el tamaño de cada celda a ambas tablas
			for(var i = 0; i < tableArr.length; i++)
			{
				for(var j = 0; j < tableArr[i].rows[0].cells.length; j++)
				{
					tableArr[i].rows[0].cells[j].style.maxWidth = cellWidths[j]+'px';
					tableArr[i].rows[0].cells[j].style.minWidth = cellWidths[j]+'px';
				}
			}
		}
	}
	showFilter(column){
		//Actualizar todos los checkbox
		document.getElementById(`buscador-${column}`).value='';
		var clist=document.getElementsByClassName(`${this.container}-filter`);
		for (var i = 0; i < clist.length; ++i){
			clist[i].style.display = "none";
		}
		var posX=event.clientX;
		event.preventDefault();
		//var posY=0;
		document.getElementById(`${this.container}-filter-title-${column}`).innerHTML=this.columns[column].title;
		//document.getElementById(`${this.container}-filter`).style.marginLeft=posX;
		//document.getElementById(`${this.container}-filter`).style.top=posY;
		document.getElementById(`${this.container}-filter-${column}`).style.display = "block";
		var shtml=`<!--table style="font-size: 1rem;color: #495057"-->
			<tr><td style="width:24px;"><input type="checkbox" ${this.filtros[column].length==this.dataFilters[column].length?"checked":""} onclick="${this.instancename}.checkAll(${column},this.checked);"></td><td>(todos)</td></tr>`;
		for(var i=0;i<this.dataFilters[column].length;i++){
			shtml+=`<tr id="filtrorow-${column}-${i}"><td style="width:24px;font-size: 1rem;"><input type="checkbox" class="${this.container}-chkfilters-${column}" ${this.filtros[column].indexOf(this.dataFilters[column][i])==-1?"":"checked"} value=${i}></td><td id="filtrocolumn-${column}-${i}">${this.dataFilters[column][i]}</td></tr>`;
		}
		//shtml+='</table>';
		document.getElementById(`${this.container}-filter-data-${column}`).innerHTML=shtml;
	}
	closeFilter(columnindex,filtrar){
		document.getElementById(`${this.container}-filter-${columnindex}`).style.display = "none";
		if(filtrar){

			//Actualizar el arreglo de filtros en base a los check
			this.filtros[columnindex]=[];
			var clist=document.getElementsByClassName(`${this.container}-chkfilters-${columnindex}`);
			for (var i = 0; i < clist.length; ++i){
				// console.log('filtrando por for');
				if(clist[i].checked)
					this.filtros[columnindex].push(this.dataFilters[columnindex][clist[i].value]);
			}
			// console.log('terminar de filtrar');
			// setTimeout(`${this.instancename}.fillTable()`,10);
		}
	}
	checkAll(columnIndex,checked){
		var self=this;
		/*
		if(checked){
			self.filtros[columnIndex]=[];
			self.dataFilters[columnIndex].forEach(function(value,index){
				self.filtros[columnIndex].push(value);
			});
		}
		else{
			self.filtros[columnIndex]=[];
		}
		*/
		//Actualizar todos los checkbox
		var clist=document.getElementsByClassName(`${self.container}-chkfilters-${columnIndex}`);
		for (var i = 0; i < clist.length; ++i){
			clist[i].checked = checked;
		}
	}
	/**
	 * Permite filtrar los filtros a traves del input
	 */
	filtrafiltro(column){
		var buscar =document.getElementById(`buscador-${column}`).value;
		if(buscar!=''){
			for(var i=0;i<this.dataFilters[column].length;i++){
				var td_valor = document.getElementById(`filtrocolumn-${column}-${i}`).innerText;

				if(td_valor.toUpperCase().indexOf(buscar.toUpperCase())==-1){
					document.getElementById(`filtrorow-${column}-${i}`).style.display = "none";
				}else{
					document.getElementById(`filtrorow-${column}-${i}`).style.display = "";
				}
			}
		}else{
			for(var i=0;i<this.dataFilters[column].length;i++){
				document.getElementById(`filtrorow-${column}-${i}`).style.display = "";
			}
		}
	}
	/**
	 * Agregar fila al final
	 * @use se usa en adjuste de inventario
	 */
	addrow(dataRow){
		var self = this;
		for(var columnIndex=0;columnIndex<self.columns.length;columnIndex++){
			if(self.filtros[columnIndex].indexOf(dataRow[self.columns[columnIndex]['data']])==-1)
			{
				self.dataFilters[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
				self.filtros[columnIndex].push(dataRow[self.columns[columnIndex]['data']]);
			}
		};
		self.data.push(dataRow);

		var totFiltros=self.filtros.length;
		var rowIndex = self.data.length-1;

		var showRow=true;
		var shtml = '';
		for(var index=0;index<totFiltros;index++){
			if(self.filtros[index].indexOf(dataRow[self.columns[index]['data']])==-1)
			{
				showRow=false;
				break;
			}
		}
		if(self.onclickrow!=''){
			var onclick=JSON.stringify(dataRow);
			shtml +=`<tr id="rowIndex-${rowIndex}" onclick='${self.onclickrow}(${onclick})' style="display: ${showRow?'':'none'}">`;
		}else{
			shtml +=`<tr id="rowIndex-${rowIndex}" style="display: ${showRow?'':'none'}">`;
		}
		for(var columnIndex=0;columnIndex<self.columns.length;columnIndex++){
			if(self.columns[columnIndex].visible)
				shtml +=`<td style="${self.columns[columnIndex].style||''}">${dataRow[self.columns[columnIndex]['data']]}</td>`;
		}
		shtml +=`</tr>`;
		$('#tabladatos-data').append(shtml);
	}
	/**
	 * Permite ordenar la tabla segun una columna
	 */
	sortTable(column){
		  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
		  var type = this.columns[column].type||'text';

		  table = document.getElementById(`${this.container}-data`);
		  switching = true;
		  // Set the sorting direction to ascending:
		  dir = "asc";
		  /* Make a loop that will continue until
		  no switching has been done: */
		  while (switching) {
		    // Start by saying: no switching is done:
		    switching = false;
		    rows = table.rows;
		    /* Loop through all table rows (except the
		    first, which contains table headers): */
		    for (i = 0; i < (rows.length - 2); i++) {
		      // Start by saying there should be no switching:
		      shouldSwitch = false;
		      /* Get the two elements you want to compare,
		      one from current row and one from the next: */
		      x = rows[i].getElementsByTagName("TD")[column];
		      y = rows[i + 1].getElementsByTagName("TD")[column];
		      /* Check if the two rows should switch place,
		      based on the direction, asc or desc: */
		      if (dir == "asc") {
			      	if(type=='text'){
				        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
				          // If so, mark as a switch and break the loop:
				          shouldSwitch = true;
				          break;
				        }
			      	}else{
				        if (Number(x.innerHTML.replace(',','')) > Number(y.innerHTML.replace(',',''))){
				          // If so, mark as a switch and break the loop:
				          shouldSwitch = true;
				          break;
				        }
			      	}
		      } else if (dir == "desc") {
			      	if(type=='text'){
				        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
				          // If so, mark as a switch and break the loop:
				          shouldSwitch = true;
				          break;
				        }
				    }else{
				    	if (Number(x.innerHTML.replace(',','')) < Number(y.innerHTML.replace(',',''))){
				          // If so, mark as a switch and break the loop:
				          shouldSwitch = true;
				          break;
				        }
				    }
		      	}
		    }
		    if (shouldSwitch) {
		      /* If a switch has been marked, make the switch
		      and mark that a switch has been done: */
		      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		      switching = true;
		      // Each time a switch is done, increase this count by 1:
		      switchcount ++;
		    } else {
		      /* If no switching has been done AND the direction is "asc",
		      set the direction to "desc" and run the while loop again. */
		      if (switchcount == 0 && dir == "asc") {
		        dir = "desc";
		        switching = true;
		      }
		    }
		  }
	}
	/*
	check(columnIndex,index,checked){
		if(checked){
			//Agregar el valor a filtros
			this.filtros[columnIndex].push(this.dataFilters[columnIndex][index]);
		}
		else{
			//Eliminar el valor de filtros
			var rowIndex=this.filtros[columnIndex].indexOf(this.dataFilters[columnIndex][index]);
			this.filtros[columnIndex].splice(rowIndex, 1);
		}
	}*/
}
