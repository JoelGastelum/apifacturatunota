<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

//MANEJO DE TRANSACCIONES
use Illuminate\Support\Facades\DB;

use App\Configuracionweb;
use App\Opcion;
use App\Permiso;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        	/*
	        	$sql = 'DELETE FROM configuracionweb;';
		        DB::delete($sql);
	         	//CONFIGURACIONWEB

	            Configuracionweb::create(array(
	                'nombre' => 'nombre',
	                'parametros' => 'Fe soluciones',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'titulo',
	                'parametros' => 'Mefactura',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'nombre_negocio',
	                'parametros' => 'Fe soluciones',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'abreviatura',
	                'parametros' => 'MF',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'logo',
	                'parametros' => 'img/compartidos/logo.png',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'descripcion',
	                'parametros' => 'Software para facturación eletrónica',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'palabrasclaves',
	                'parametros' => 'Facturación, XML, Notas',
	                'tipo' => 1,
	            ));
	            Configuracionweb::create(array(
	                'nombre' => 'idtipousuarioadministrador',
	                'parametros' => '2',
	                'tipo' => 2,
	            ));
	        */

			$sql = 'DELETE FROM opciones;';
	        DB::delete($sql);

	        //TIPO USUARIO =1
	        	//ORDENES POR APROBAR 1601 - 1700
		   			Opcion::create(array(
	                    'id' => 1601,
	                    'controlador' => 'OrdenesporaprobarController',
	                    'accion' => 'index',
	                    'nombreruta' => 'ordenesporaprobar.index',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-file-invoice',
	                    'nombre' => 'Ordenes por aprobar',
	                    'orden' => 900,
	                    'estatus' => 'A',
					));
	                    //OPCIONES
						Opcion::create(array(
                            'id' => 1602,
                            'controlador' => 'OrdenesporaprobarController',
                            'accion' => 'loaddata',
                            'nombreruta' => 'ordenesporaprobar.loaddata',
                            'idpadre' => 1601,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 1603,
                            'controlador' => 'OrdenesporaprobarController',
                            'accion' => 'mostrar',
                            'nombreruta' => 'ordenesporaprobar.mostrar',
                            'idpadre' => 1601,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 1604,
                            'controlador' => 'OrdenesporaprobarController',
                            'accion' => 'aprobar',
                            'nombreruta' => 'ordenesporaprobar.aprobar',
                            'idpadre' => 1601,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 1605,
                            'controlador' => 'OrdenesporaprobarController',
                            'accion' => 'saprobar',
                            'nombreruta' => 'ordenesporaprobar.saprobar',
                            'idpadre' => 1601,
                            'estatus' => 'A',
						));
						//COMISIONES 1650 - 1700
						Opcion::create(array(
							'id' => 1650,
							'controlador' => 'RecomendadoController@PagosController',
							'accion' => '',
							'nombreruta' => '',
							'idpadre' => 0,
							'icono' => 'fas fa-file-invoice-dollar',
							'nombre' => 'Comisiones',
							'orden' => 901,
							'estatus' => 'A',
						));
						//POR PAGAR
							Opcion::create(array(
								'id' => 1654,
								'controlador' => 'RecomendadoController',
								'accion' => 'comisionesadmin',
								'nombreruta' => 'recomendados.comisiones',
								'idpadre' => 1650,
								'icono' => 'fas fa-chart-bar',
								'nombre' => 'Por Pagar',
								'orden' => 902,
								'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 1651,
	                            'controlador' => 'RecomendadoController',
	                            'accion' => 'loaddata2',
	                            'nombreruta' => 'recomendados.loaddata2',
	                            'idpadre' => 1654,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 1652,
	                            'controlador' => 'RecomendadoController',
	                            'accion' => 'realizarpagos',
	                            'nombreruta' => 'recomendados.pagos',
	                            'idpadre' => 1654,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 1653,
	                            'controlador' => 'RecomendadoController',
	                            'accion' => 'savePago',
	                            'nombreruta' => 'recomendados.savepago',
	                            'idpadre' => 1654,
	                            'estatus' => 'A',
							));

						//PAGADAS
							Opcion::create(array(
								'id' => 1680,
								'controlador' => 'PagosController',
								'accion' => 'index',
								'nombreruta' => 'pagos.index',
								'idpadre' => 1650,
								'icono' => 'fas fa-money-bill-wave',
								'nombre' => 'Pagadas',
								'orden' => 800,
								'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 1681,
	                            'controlador' => 'PagosController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'pagos.loaddata',
	                            'idpadre' => 1680,
	                            'estatus' => 'A',
							));

						//PERMISOS A OPCIONES NOTAS
						Permiso::create(array(
							'idtipousuario' => 1,
							'idopcion' => 1601,
							'estatus' => 'A',
						));
		                    Permiso::create(array(
		                        'idtipousuario' => 1,
		                        'idopcion' => 1602,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 1,
		                        'idopcion' => 1603,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 1,
		                        'idopcion' => 1604,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 1,
		                        'idopcion' => 1605,
		                        'estatus' => 'A',
							));
							//COMISIONES
							Permiso::create(array(
		                        'idtipousuario' => 1,
		                        'idopcion' => 1650,
		                        'estatus' => 'A',
							));
							//POR PAGAR
								Permiso::create(array(
			                        'idtipousuario' => 1,
			                        'idopcion' => 1651,
			                        'estatus' => 'A',
								));
								Permiso::create(array(
			                        'idtipousuario' => 1,
			                        'idopcion' => 1652,
			                        'estatus' => 'A',
								));
								Permiso::create(array(
			                        'idtipousuario' => 1,
			                        'idopcion' => 1653,
			                        'estatus' => 'A',
								));
								Permiso::create(array(
			                        'idtipousuario' => 1,
			                        'idopcion' => 1654,
			                        'estatus' => 'A',
			                    ));
							//PAGADAS
								Permiso::create(array(
			                        'idtipousuario' => 1,
			                        'idopcion' => 1680,
			                        'estatus' => 'A',
								));
								Permiso::create(array(
			                        'idtipousuario' => 1,
			                        'idopcion' => 1681,
			                        'estatus' => 'A',
								));

	        //TIPO USUARIO  = 2
	        	//MODAL CONFIGURACION 550
	       			Opcion::create(array(
                        'id' => 550,
                        'controlador' => 'HomeController',
                        'accion' => 'guardarpasos',
                        'nombreruta' => 'home.guardarpasos',
                        'idpadre' => 0,
                        'orden' => 0,
                        'estatus' => 'A',
                    ));
                    Permiso::create(array(
                        'idtipousuario' => 2,
                        'idopcion' => 550,
                        'estatus' => 'A',
                    ));

		   		//CAPTURAS 501 - 1000
		   			Opcion::create(array(
	                    'id' => 501,
	                    'controlador' => 'CrearnotaController',
	                    'accion' => '',
	                    'nombreruta' => 'home',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-clone',
	                    'nombre' => 'Capturas',
	                    'orden' => 140,
	                    'estatus' => 'D',
	                ));
	                //PERMISOS A CAPTURAS
	                    // Permiso::create(array(
	                    //     'idtipousuario' => 2,
	                    //     'idopcion' => 501,
	                    //     'estatus' => 'A',
	                    // ));

		                //NOTA 502
	                        Opcion::create(array(
	                            'id' => 502,
	                    		'controlador' => 'CrearnotaController',
	                            'accion' => 'index',
	                            'nombreruta' => 'crearnotas.index',
	                            'idpadre' => 501,
	                            'icono' => 'fas fa-receipt',
	                            'nombre' => 'Captura nota',
	                            'nombre_permiso' => 'Crear nota',
	                            'descripcion' => 'Permite crear nota.',
	                            'orden' => 99,
	                            'estatus' => 'D',
							));
							Opcion::create(array(
	                            'id' => 503,
	                            'controlador' => 'CrearnotaController',
	                            'accion' => 'update',
	                            'nombreruta' => 'crearnotas.update',
	                            'idpadre' => 502,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 504,
	                            'controlador' => 'CrearnotaController',
	                            'accion' => 'buscarconceptosdenota',
	                            'nombreruta' => 'crearnotas.buscarconceptosdenota',
	                            'idpadre' => 502,
	                            'estatus' => 'A',
	                        ));

	                        //PERMISOS A OPCIONES NOTAS
			                    // Permiso::create(array(
			                    //     'idtipousuario' => 2,
			                    //     'idopcion' => 502,
			                    //     'estatus' => 'A',
			                    // ));
			                    // Permiso::create(array(
			                    //     'idtipousuario' => 2,
			                    //     'idopcion' => 503,
			                    //     'estatus' => 'A',
			                    // ));
			                    // Permiso::create(array(
			                    //     'idtipousuario' => 2,
			                    //     'idopcion' => 504,
			                    //     'estatus' => 'A',
			                    // ));

		        //LISTADOS 1 - 199
	                Opcion::create(array(
	                    'id' => 1,
	                    'controlador' => 'NotasController@FacturasController',
	                    'accion' => '',
	                    'nombreruta' => 'home',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-list-ul',
	                    'nombre' => 'Listados',
	                    'orden' => 120,
	                    'estatus' => 'A',
	                ));
	                //PERMISOS A OPERACIONES
	                    Permiso::create(array(
	                        'idtipousuario' => 2,
	                        'idopcion' => 1,
	                        'estatus' => 'A',
	                    ));

		                //NOTAS 2 a 100
	                        Opcion::create(array(
	                            'id' => 2,
	                    		'controlador' => 'NotasController',
	                            'accion' => 'index',
	                            'nombreruta' => 'notas.index',
	                            'idpadre' => 1,
	                            'icono' => 'fas fa-receipt',
	                            'nombre' => 'Notas',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar las notas.',
	                            'orden' => 99,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 3,
								'controlador' => 'NotasController',
								'accion' => 'create',
								'nombreruta' => 'notas.create',
								'idpadre' => 2,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear notas.',
								'estatus' => 'D',
							));
							Opcion::create(array(
								'id' => 4,
								'controlador' => 'NotasController',
								'accion' => 'visualizar',
								'nombreruta' => 'notas.visualizar',
								'idpadre' => 2,
								'nombre_permiso' => 'Visualizar',
								'descripcion' => 'Permite visualizar los datos de la nota.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 5,
	                            'controlador' => 'NotasController',
	                            'accion' => 'update',
	                            'nombreruta' => 'notas.update',
	                            'idpadre' => 4,
	                            'orden' => 97,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 6,
	                            'controlador' => 'NotasController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'notas.loaddata',
	                            'idpadre' => 2,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 7,
	                            'controlador' => 'NotasController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'notas.eliminar',
	                            'idpadre' => 2,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar una nota',
	                            'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 8,
	                            'controlador' => 'NotasController',
	                            'accion' => 'buscarconceptosdenotacreada',
	                            'nombreruta' => 'notas.buscarconceptosdenotacreada',
	                            'idpadre' => 4,
	                            'estatus' => 'A',
	                        ));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 2,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 3,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 4,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 5,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 6,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 7,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 8,
			                        'estatus' => 'A',
								));

		                //FACTURAS 101 a 199
	                        Opcion::create(array(
	                            'id' => 101,
	                    		'controlador' => 'FacturasController',
	                            'accion' => 'index',
	                            'nombreruta' => 'facturas.index',
	                            'idpadre' => 1,
	                            'icono' => 'fas fa-file-invoice',
	                            'nombre' => 'Facturas',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar las facturas.',
	                            'orden' => 90,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 102,
								'controlador' => 'FacturasController',
								'accion' => 'create',
								'nombreruta' => 'facturas.create',
								'idpadre' => 101,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear factura.',
								'estatus' => 'D',
							));
							Opcion::create(array(
								'id' => 103,
								'controlador' => 'FacturasController',
								'accion' => 'visualizar',
								'nombreruta' => 'facturas.visualizar',
								'idpadre' => 101,
								'nombre_permiso' => 'Visualizar',
								'descripcion' => 'Permite visualizar los datos de la factura.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 104,
	                            'controlador' => 'FacturasController',
	                            'accion' => 'update',
	                            'nombreruta' => 'facturas.update',
	                            'idpadre' => 103,
	                            'orden' => 97,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 105,
	                            'controlador' => 'FacturasController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'facturas.loaddata',
	                            'idpadre' => 101,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 106,
	                            'controlador' => 'FacturasController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'facturas.eliminar',
	                            'idpadre' => 101,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar una factura',
	                            'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 107,
	                            'controlador' => 'CrearnotaController',
	                            'accion' => 'buscarconceptosdenotacreada',
	                            'nombreruta' => 'notas.buscarconceptosdenotacreada',
	                            'idpadre' => 103,
	                            'estatus' => 'A',
	                        ));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 101,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 102,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 103,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 104,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 105,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 106,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 107,
			                        'estatus' => 'A',
								));

		        //CONFIGURACIONES 1001 - 1599
	                Opcion::create(array(
	                    'id' => 1001,
	                    'controlador' => 'ConceptosController@SeriesController@SucursalesController@CertificadosController@UsuariosController@ComprarplanController',
	                    'accion' => '',
	                    'nombreruta' => 'home',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-tools',
	                    'nombre' => 'Configuraciones',
	                    'orden' => 62,
	                    'estatus' => 'A',
	                ));
	                //PERMISOS A OPERACIONES
	                    Permiso::create(array(
	                        'idtipousuario' => 2,
	                        'idopcion' => 1001,
	                        'estatus' => 'A',
	                    ));

						//CONFIGURACIONES GENERALES 1501 a 1600
							Opcion::create(array(
								'id' => 1501,
								'controlador' => 'FacturasController',
								'accion' => 'Configuracion',
								'nombreruta' => 'facturas.configuracion',
								'idpadre' => 1001,
								'icono' => 'fas fa-cogs',
								'nombre' => 'General',
								'nombre_permiso' => 'Configuracion',
								'descripcion' => 'Permite configurar.',
								'orden' => 120,
								'estatus' => 'A',
							));

							Opcion::create(array(
								'id' => 1502,
								'controlador' => 'FacturasController',
								'accion' => 'actualizarconf',
								'nombreruta' => 'facturas.actualizarconf',
								'idpadre' => 1501,
								'nombre_permiso' => 'actualizarconf',
								'descripcion' => 'Permite actualizar',
								'estatus' => 'A',
							));

							//PERMISOS CONFIGURACIONES
								Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1501,
			                        'estatus' => 'A',
								));
								Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1502,
			                        'estatus' => 'A',
								));

		                //CONCEPTOS 1002 a 1100
	                        Opcion::create(array(
	                            'id' => 1002,
	                    		'controlador' => 'ConceptosController',
	                            'accion' => 'index',
	                            'nombreruta' => 'conceptos.index',
	                            'idpadre' => 1001,
	                            'icono' => 'fas fa-file-invoice-dollar',
	                            'nombre' => 'Conceptos',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar los conceptos.',
	                            'orden' => 99,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1003,
								'controlador' => 'ConceptosController',
								'accion' => 'create',
								'nombreruta' => 'conceptos.create',
								'idpadre' => 1002,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear concepto.',
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1004,
								'controlador' => 'ConceptosController',
								'accion' => 'edit',
								'nombreruta' => 'conceptos.edit',
								'idpadre' => 1002,
								'nombre_permiso' => 'Edicion',
								'descripcion' => 'Permite editar concepto.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 1005,
	                            'controlador' => 'ConceptosController',
	                            'accion' => 'update',
	                            'nombreruta' => 'conceptos.update',
	                            'idpadre' => 1002,
	                            'orden' => 97,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1006,
	                            'controlador' => 'ConceptosController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'conceptos.loaddata',
	                            'idpadre' => 1002,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1007,
	                            'controlador' => 'ConceptosController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'conceptos.eliminar',
	                            'idpadre' => 1002,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar concepto',
	                            'estatus' => 'A',
							));
		                    Opcion::create(array(
	                            'id' => 1008,
	                            'controlador' => 'ConceptosController',
	                            'accion' => 'buscarunidadessat',
	                            'nombreruta' => 'conceptos.buscarunidadessat',
	                            'idpadre' => 1002,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1009,
	                            'controlador' => 'ConceptosController',
	                            'accion' => 'buscarcodigoproductosat',
	                            'nombreruta' => 'conceptos.buscarcodigoproductosat',
	                            'idpadre' => 1002,
	                            'estatus' => 'A',
	                        ));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1002,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1003,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1004,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1005,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1006,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1007,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1008,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1009,
			                        'estatus' => 'A',
								));

		                //SERIES 1101 a 1200
	                        Opcion::create(array(
	                            'id' => 1101,
	                    		'controlador' => 'SeriesController',
	                            'accion' => 'index',
	                            'nombreruta' => 'series.index',
	                            'idpadre' => 1001,
	                            'icono' => 'fas fa-server',
	                            'nombre' => 'Series',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar las series.',
	                            'orden' => 90,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1102,
								'controlador' => 'SeriesController',
								'accion' => 'create',
								'nombreruta' => 'series.create',
								'idpadre' => 1101,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear serie.',
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1103,
								'controlador' => 'SeriesController',
								'accion' => 'edit',
								'nombreruta' => 'series.edit',
								'idpadre' => 1101,
								'nombre_permiso' => 'Edicion',
								'descripcion' => 'Permite editar serie.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 1104,
	                            'controlador' => 'SeriesController',
	                            'accion' => 'update',
	                            'nombreruta' => 'series.update',
	                            'idpadre' => 1101,
	                            'orden' => 97,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1105,
	                            'controlador' => 'SeriesController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'series.loaddata',
	                            'idpadre' => 1101,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1106,
	                            'controlador' => 'SeriesController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'series.eliminar',
	                            'idpadre' => 1101,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar serie',
	                            'estatus' => 'A',
							));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1101,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1102,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1103,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1104,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1105,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1106,
			                        'estatus' => 'A',
								));

		                //CERTIFICADOS 1201 a 1300
	                        Opcion::create(array(
	                            'id' => 1201,
	                    		'controlador' => 'CertificadosController',
	                            'accion' => 'index',
	                            'nombreruta' => 'certificados.index',
	                            'idpadre' => 1001,
	                            'icono' => 'fas fa-stamp',
	                            'nombre' => 'Certificados',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar los certificados.',
	                            'orden' => 80,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1202,
								'controlador' => 'CertificadosController',
								'accion' => 'create',
								'nombreruta' => 'certificados.create',
								'idpadre' => 1201,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear certificado.',
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1203,
								'controlador' => 'CertificadosController',
								'accion' => 'edit',
								'nombreruta' => 'certificados.edit',
								'idpadre' => 1201,
								'nombre_permiso' => 'Edicion',
								'descripcion' => 'Permite editar certificado.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 1204,
	                            'controlador' => 'CertificadosController',
	                            'accion' => 'update',
	                            'nombreruta' => 'certificados.update',
	                            'idpadre' => 1201,
	                            'orden' => 97,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1205,
	                            'controlador' => 'CertificadosController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'certificados.loaddata',
	                            'idpadre' => 1201,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1206,
	                            'controlador' => 'CertificadosController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'certificados.eliminar',
	                            'idpadre' => 1201,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar certificado',
	                            'estatus' => 'A',
							));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1201,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1202,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1203,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1204,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1205,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1206,
			                        'estatus' => 'A',
								));

		                //SUCURSALES 1301 a 1400
	                        Opcion::create(array(
	                            'id' => 1301,
	                    		'controlador' => 'SucursalesController',
	                            'accion' => 'index',
	                            'nombreruta' => 'sucursales.index',
	                            'idpadre' => 1001,
	                            'icono' => 'fas fa-laptop-house',
	                            'nombre' => 'Sucursales',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar sucursales.',
	                            'orden' => 70,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1302,
								'controlador' => 'SucursalesController',
								'accion' => 'create',
								'nombreruta' => 'sucursales.create',
								'idpadre' => 1301,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear sucursal.',
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1303,
								'controlador' => 'SucursalesController',
								'accion' => 'edit',
								'nombreruta' => 'sucursales.edit',
								'idpadre' => 1301,
								'nombre_permiso' => 'Edicion',
								'descripcion' => 'Permite editar sucursal.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 1304,
	                            'controlador' => 'SucursalesController',
	                            'accion' => 'update',
	                            'nombreruta' => 'sucursales.update',
	                            'idpadre' => 1301,
	                            'orden' => 97,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1305,
	                            'controlador' => 'SucursalesController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'sucursales.loaddata',
	                            'idpadre' => 1301,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1306,
	                            'controlador' => 'SucursalesController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'sucursales.eliminar',
	                            'idpadre' => 1301,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar certificado',
	                            'estatus' => 'A',
							));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1301,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1302,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1303,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1304,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1305,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1306,
			                        'estatus' => 'A',
								));

		                //USUARIOS 1401 a 1500
	                        Opcion::create(array(
	                            'id' => 1401,
	                    		'controlador' => 'UsuariosController',
	                            'accion' => 'index',
	                            'nombreruta' => 'usuarios.index',
	                            'idpadre' => 1001,
	                            'icono' => 'fas fa-users',
	                            'nombre' => 'Usuarios',
	                            'nombre_permiso' => 'Listado',
	                            'descripcion' => 'Permite listar usuarios.',
	                            'orden' => 60,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1402,
								'controlador' => 'UsuariosController',
								'accion' => 'create',
								'nombreruta' => 'usuarios.create',
								'idpadre' => 1401,
								'nombre_permiso' => 'Creacion',
								'descripcion' => 'Permite crear usuario.',
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1403,
								'controlador' => 'UsuariosController',
								'accion' => 'edit',
								'nombreruta' => 'usuarios.edit',
								'idpadre' => 1401,
								'nombre_permiso' => 'Edicion',
								'descripcion' => 'Permite editar usuario.',
								'estatus' => 'A',
							));
	                        Opcion::create(array(
	                            'id' => 1404,
	                            'controlador' => 'UsuariosController',
	                            'accion' => 'update',
	                            'nombreruta' => 'usuarios.update',
	                            'idpadre' => 1401,
	                            'orden' => 97,
	                            'estatus' => 'A',
							));
		                    Opcion::create(array(
	                            'id' => 1405,
	                            'controlador' => 'UsuariosController',
	                            'accion' => 'loaddata',
	                            'nombreruta' => 'usuarios.loaddata',
	                            'idpadre' => 1401,
	                            'estatus' => 'A',
	                        ));
		                    Opcion::create(array(
	                            'id' => 1406,
	                            'controlador' => 'UsuariosController',
	                            'accion' => 'eliminar',
	                            'nombreruta' => 'usuarios.eliminar',
	                            'idpadre' => 1401,
	                            'nombre_permiso' => 'Eliminar',
	                            'descripcion' => 'Permite eliminar usuario',
	                            'estatus' => 'A',
							));

							//PERMISOS A OPCIONES NOTAS
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1401,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1402,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1403,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1404,
			                        'estatus' => 'A',
			                    ));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1405,
			                        'estatus' => 'A',
								));
			                    Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1406,
			                        'estatus' => 'A',
								));

						//COMPRAR PLAN 1701 a 1699
							Opcion::create(array(
								'id' => 1701,
								'controlador' => 'ComprarplanController',
								'accion' => 'index',
								'nombreruta' => 'comprarplan.index',
								'idpadre' => 1001,
								'icono' => 'fas fa-shopping-basket',
								'nombre' => 'Comprar plan',
								'nombre_permiso' => 'Comprar plan',
								'descripcion' => 'Permite comprar un plan a la empresa',
								'orden' => 20,
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 1702,
								'controlador' => 'ComprarplanController',
								'accion' => 'comprarplan',
								'nombreruta' => 'comprarplan.comprarplan',
								'idpadre' => 1701,
								'estatus' => 'A',
							));

							//PERMISOS CONFIGURACIONES
								Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1701,
			                        'estatus' => 'A',
								));
								Permiso::create(array(
			                        'idtipousuario' => 2,
			                        'idopcion' => 1702,
			                        'estatus' => 'A',
								));

			//TIPO USUARIO = 4
				//GANAR DINERO CON NOSOTROS 350 - 400
		   			Opcion::create(array(
	                    'id' => 350,
	                    'controlador' => 'GanardineroController',
	                    'accion' => 'index',
	                    'nombreruta' => 'ganardinero.index',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-hand-holding-usd',
	                    'nombre' => 'Gana dinero con nosotros',
	                    'orden' => 800,
	                    'estatus' => 'A',
	                ));
	                //PERMISO
	                	Permiso::create(array(
	                        'idtipousuario' => 4,
	                        'idopcion' => 350,
	                        'estatus' => 'A',
	                    ));
	                    //OPCIONES
						Opcion::create(array(
                            'id' => 351,
                            'controlador' => 'GanardineroController',
                            'accion' => 'terminoscondiciones',
                            'nombreruta' => 'ganardinero.terminoscondiciones',
                            'idpadre' => 350,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 352,
                            'controlador' => 'GanardineroController',
                            'accion' => 'sterminoscondiciones',
                            'nombreruta' => 'ganardinero.sterminoscondiciones',
                            'idpadre' => 351,
                            'estatus' => 'A',
                        ));
                        //PERMISOS
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 351,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 352,
		                        'estatus' => 'A',
		                    ));

			//TIPO USUARIO = 4 Y 5
				//CONTRIBUYENTES 200 - 300
		   			Opcion::create(array(
	                    'id' => 200,
	                    'controlador' => 'HomeController',
	                    'accion' => 'index',
	                    'nombreruta' => 'contribuyentes',
	                    'idpadre' => 0,
	                    'icono' => 'fa fa-address-card',
	                    'nombre' => 'Mis Contribuyentes',
	                    'orden' => 1000,
	                    'estatus' => 'A',
	                ));
	                //PERMISOS A OPCIONES CONTRIBUYENTES
	                    Permiso::create(array(
	                        'idtipousuario' => 4,
	                        'idopcion' => 200,
	                        'estatus' => 'A',
	                    ));
	                    Permiso::create(array(
	                        'idtipousuario' => 5,
	                        'idopcion' => 200,
	                        'estatus' => 'A',
	                    ));

	                    //OPCIONES
							Opcion::create(array(
	                            'id' => 201,
	                            'controlador' => 'ContribuyentesController',
	                            'accion' => 'crearqr',
	                            'nombreruta' => 'contribuyentes.crearqr',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 202,
	                            'controlador' => 'ContribuyentesController',
	                            'accion' => 'eliminarqr',
	                            'nombreruta' => 'contribuyentes.eliminarqr',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 203,
	                            'controlador' => 'ContribuyentesController',
	                            'accion' => 'buscarviajes',
	                            'nombreruta' => 'contribuyentes.buscarviajes',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 204,
	                            'controlador' => 'ContribuyentesController',
	                            'accion' => 'cerrarviaje',
	                            'nombreruta' => 'contribuyentes.cerrarviaje',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 205,
	                            'controlador' => 'ContribuyentesController',
	                            'accion' => 'eliminarviaje',
	                            'nombreruta' => 'contribuyentes.eliminarviaje',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 206,
	                            'controlador' => 'ContribuyentesController',
	                            'accion' => 'editarviaje',
	                            'nombreruta' => 'contribuyentes.editarviaje',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 207,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'create',
	                            'nombreruta' => 'clientes.create',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 208,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'update',
	                            'nombreruta' => 'clientes.update',
	                            'idpadre' => 207,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 209,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'edit',
	                            'nombreruta' => 'clientes.edit',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 210,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'DeleteCliente',
	                            'nombreruta' => 'clientes.delete',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 211,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'verfacturas',
	                            'nombreruta' => 'clientes.misfacturas',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 212,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'misfacturas',
	                            'nombreruta' => 'clientes.facturas',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 213,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'facturaexterna',
	                            'nombreruta' => 'clientes.facturaexterna',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 214,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'saveExterno',
	                            'nombreruta' => 'clientes.saveexterno',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 215,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'ActivarViaje',
	                            'nombreruta' => 'clientes.activarviajeWeb',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
	                        Opcion::create(array(
	                            'id' => 216,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'buscarfacturas',
	                            'nombreruta' => 'clientes.buscarfacturas',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 217,
								'controlador' => 'ContribuyentesController',
								'accion' => 'enviarqr',
								'nombreruta' => 'contribuyentes.enviarqr',
								'idpadre' => 1,
								'estatus' => 'A',
							));
							Opcion::create(array(
								'id' => 221,
								'controlador' => 'ClienteController',
								'accion' => 'buscarfacturas',
								'nombreruta' => 'clientes.buscarfacturas',
								'idpadre' => 1,
								'estatus' => 'A',
							));
							Opcion::create(array(
	                            'id' => 222,
	                            'controlador' => 'ClienteController',
	                            'accion' => 'Viajes',
	                            'nombreruta' => 'clientes.Viajes',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));
							Opcion::create(array(
	                            'id' => 223,
	                            'controlador' => 'EmpresaController',
	                            'accion' => 'PdfListo',
	                            'nombreruta' => 'empresa.descargar',
	                            'idpadre' => 200,
	                            'estatus' => 'A',
	                        ));


                        //PERMISOS
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 201,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 202,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 203,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 204,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 205,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 206,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 207,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 208,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 209,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 210,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 211,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 212,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 213,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 214,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 215,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 216,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 201,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 202,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 203,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 204,
		                        'estatus' => 'A',
							));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 205,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 206,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 207,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 208,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 209,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 210,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 211,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 212,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 213,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 214,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 215,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 216,
		                        'estatus' => 'A',
							));
							Permiso::create(array(
		                        'idtipousuario' => 2,
		                        'idopcion' => 217,
		                        'estatus' => 'A',
							));
							Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 217,
		                        'estatus' => 'A',
							));
							Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 217,
		                        'estatus' => 'A',
		                    ));
							Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 222,
		                        'estatus' => 'A',
							));	Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 222,
		                        'estatus' => 'A',
							));
							Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 223,
		                        'estatus' => 'A',
							));	Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 223,
		                        'estatus' => 'A',
							));

		        //RECOMENDAR AMIGOS 301 - 349
		   			Opcion::create(array(
	                    'id' => 301,
	                    'controlador' => 'RecomendadoController',
	                    'accion' => 'amigos',
	                    'nombreruta' => 'recomendados.amigos',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-user-friends',
	                    'nombre' => 'Recomendar amigos',
	                    'orden' => 900,
	                    'estatus' => 'A',
	                ));
	                //PERMISO
	                	Permiso::create(array(
	                        'idtipousuario' => 4,
	                        'idopcion' => 301,
	                        'estatus' => 'A',
	                    ));
	                	Permiso::create(array(
	                        'idtipousuario' => 5,
	                        'idopcion' => 301,
	                        'estatus' => 'A',
	                    ));
	                    //OPCIONES
						Opcion::create(array(
                            'id' => 302,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'loaddata',
                            'nombreruta' => 'recomendados.loaddata',
                            'idpadre' => 301,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 303,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'mislinks',
                            'nombreruta' => 'recomendados.mislinks',
                            'idpadre' => 301,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 304,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'misbanner',
                            'nombreruta' => 'recomendados.misbanner',
                            'idpadre' => 301,
                            'estatus' => 'A',
                        ));
                        //PERMISOS
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 302,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 302,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 303,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 303,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 304,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 304,
		                        'estatus' => 'A',
		                    ));

		    //TIPO USUARIO = 5
				//RECOMENDAR ESTABLECIMIENTO 400 - 500
		   			Opcion::create(array(
	                    'id' => 400,
	                    'controlador' => 'RecomendadoController',
	                    'accion' => 'verfacturas',
	                    'nombreruta' => 'recomendados.index',
	                    'idpadre' => 0,
	                    'icono' => 'fas fa-hand-holding-medical',
	                    'nombre' => 'Recomendar establecimiento',
	                    'orden' => 800,
	                    'estatus' => 'A',
	                ));
	                //PERMISO
	                	Permiso::create(array(
	                        'idtipousuario' => 5,
	                        'idopcion' => 400,
	                        'estatus' => 'A',
	                    ));
	                    //OPCIONES
						Opcion::create(array(
                            'id' => 401,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'store',
                            'nombreruta' => 'clientes.saverecomendado',
                            'idpadre' => 400,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 402,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'verfacturas',
                            'nombreruta' => 'recomendados.index',
                            'idpadre' => 400,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 403,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'buscarRecomendados',
                            'nombreruta' => 'clientes.misrecomendadosweb',
                            'idpadre' => 400,
                            'estatus' => 'A',
                        ));
						Opcion::create(array(
                            'id' => 404,
                            'controlador' => 'ContribuyentesController',
                            'accion' => 'crearqr',
                            'nombreruta' => 'contribuyentes.crearqr',
                            'idpadre' => 400,
                            'estatus' => 'A',
                        ));
                        Opcion::create(array(
                            'id' => 405,
                            'controlador' => 'ContribuyentesController',
                            'accion' => 'seditarviaje',
                            'nombreruta' => 'contribuyentes.seditarviaje',
                            'idpadre' => 400,
                            'estatus' => 'A',
                        ));

                        //PERMISOS
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 401,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 402,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 403,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 404,
		                        'estatus' => 'A',
		                    ));
		                    Permiso::create(array(
		                        'idtipousuario' => 5,
		                        'idopcion' => 405,
		                        'estatus' => 'A',
							));

							Permiso::create(array(
		                        'idtipousuario' => 4,
		                        'idopcion' => 405,
		                        'estatus' => 'A',
							));


				//RECOMENDAR GENERICO //601 - 700
					Opcion::create(array(
						'id' => 601,
						'controlador' => 'RecomendadoController',
						'accion' => 'codigogenerico',
						'nombreruta' => 'recomendados.codigogenerico',
						'idpadre' => 0,
						'icono' => 'fab fa-slideshare',
						'nombre' => 'Código único',
						'orden' => 700,
						'estatus' => 'A',
					));
					//PERMISO
						Permiso::create(array(
	                        'idtipousuario' => 5,
	                        'idopcion' => 601,
	                        'estatus' => 'A',
						));
						Opcion::create(array(
                            'id' => 602,
                            'controlador' => 'RecomendadoController',
                            'accion' => 'misbannergenerico',
                            'nombreruta' => 'recomendados.misbannergenerico',
                            'idpadre' => 601,
                            'estatus' => 'A',
                        ));
						Permiso::create(array(
	                        'idtipousuario' => 5,
	                        'idopcion' => 602,
	                        'estatus' => 'A',
						));


        Model::reguard();
    }
}
?>
