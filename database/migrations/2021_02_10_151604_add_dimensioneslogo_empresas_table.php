<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDimensioneslogoEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('wlogopdf',250)->default(null)->nullable()->after('logopdf')->comment("Width del logo");
            $table->string('hlogopdf',250)->default(null)->nullable()->after('wlogopdf')->comment("Height del logo");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('wlogopdf');
            $table->dropColumn('hlogopdf');
        });
    }
}
