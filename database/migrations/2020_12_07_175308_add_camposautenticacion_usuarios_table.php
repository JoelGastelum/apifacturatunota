<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposautenticacionUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuarios', function (Blueprint $table) {
            $table->string('password',255)->after('nombre')->nullable()->default(null);
            $table->string('email', 150)->after('password')->nullable()->default(null);
            $table->string('remember_token',255)->after('tokenlogin')->nullable()->default(null);
            $table->string('foto_perfil',255)->after('tokenlogin')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuarios', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('email');
            $table->dropColumn('remember_token');
            $table->dropColumn('foto_perfil');
        });
    }
}
