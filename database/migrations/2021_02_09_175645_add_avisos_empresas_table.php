<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvisosEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->integer('avisodiamembresia')->default(15)->nullable()->after('foliosconsumidos')->comment("Dias que falta por vencer");
            $table->integer('avisofolio')->default(25)->nullable()->after('avisodiamembresia')->comment("Folios que falta por vencer");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('avisodiamembresia');
            $table->dropColumn('avisofolio');
        });
    }
}
