<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permisos', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('idtipousuario')->nullable()->index('PERMISOS_TIPOUSUARIO_FK');
            $table->integer('idopcion')->index('PERMISOS_OPCIONES_FK');
            $table->char('estatus', 1)->nullable()->default("A");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permisos');
    }
}
