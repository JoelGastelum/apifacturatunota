<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('iduser')->nullable()->default(null);
            $table->integer('idcodigopromocion')->nullable()->default(null);
            $table->integer('metodopago')->nullable()->default(1);
            $table->integer('ambito')->nullable()->default(1)->comment('0 = sandbox, 1 = production');
            $table->decimal('costometodopago',16,4)->nullable()->default(0);
            $table->string('titulo', 500)->nullable()->default(null);
            $table->string('razonsocial', 250)->nullable()->default(null);
            $table->string('rfc', 150)->nullable()->default(null);
            $table->string('email', 150)->nullable()->default(null);
            $table->string('tracker', 150)->nullable()->default(null);
            $table->decimal('subtotal',16,4)->nullable()->default(0);
            $table->integer('iva_incluido')->nullable()->default(1);
            $table->decimal('iva_porcentaje',8,2)->nullable()->default(16);
            $table->decimal('iva_monto',16,4)->nullable()->default(0);
            $table->decimal('total',16,4)->nullable()->default(0);
            $table->integer('folio')->nullable()->default(0)->comment('cantidad de folio');
            $table->date('fechapagado')->nullable()->default(null);
            $table->date('fecharechazo')->nullable()->default(null);
            $table->string('collection_id', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('collection_status', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('payment_id', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('status', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('payment_type', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('merchant_order_id', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('preference_id', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('site_id', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('processing_mode', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('merchant_account_id', 150)->nullable()->default(null)->comment('campos mp');
            $table->string('estatus', 2)->nullable()->default('0')->comment('-1=intento de pago realizado, 0=En proceso,1=espera del metodo de pago,2=pagado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordenes');
    }
}
