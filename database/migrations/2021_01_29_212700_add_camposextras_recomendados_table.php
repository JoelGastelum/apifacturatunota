<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposextrasRecomendadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recomendados', function (Blueprint $table) {
            $table->integer('idrecomendado')->default(null)->nullable()->after('idusuario');
            $table->integer('idempresacomprador')->default(null)->nullable()->after('idrecomendado');
            $table->integer('idpago')->default(null)->nullable()->after('idempresacomprador');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recomendados', function (Blueprint $table) {
            $table->dropColumn('idrecomendado');
            $table->dropColumn('idempresacomprador');
            $table->dropColumn('idpago');
        });
    }
}
