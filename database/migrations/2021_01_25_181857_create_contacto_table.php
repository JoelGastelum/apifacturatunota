<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacto', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name', 250)->nullable()->default(null);
            $table->string('email', 250)->nullable()->default(null);
            $table->string('phone', 250)->nullable()->default(null);
            $table->string('subject', 250)->nullable()->default(null);
            $table->longText('message')->nullable()->default(null);
            $table->integer('leido')->nullable()->default(0)->comment('0 = No, 1 = Si');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacto');
    }
}
