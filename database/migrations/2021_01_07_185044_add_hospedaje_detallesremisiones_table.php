<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHospedajeDetallesRemisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remisionesdetalle', function (Blueprint $table) {
            $table->decimal('importehospedaje',16,4)->nullable()->default(0)->after('importeiva');
            $table->decimal('tasahospedaje',6,2)->nullable()->default(0)->after('tasaieps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remisionesdetalle', function (Blueprint $table) {
            $table->dropColumn('importehospedaje');
            $table->dropColumn('tasahospedaje');
        });
    }
}
