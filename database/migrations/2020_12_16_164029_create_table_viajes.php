<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableViajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('viajes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('descripcion', 100)->nullable();
            $table->decimal('presupuesto', 18,2)->nullable()->default(0);
            $table->integer('activo')->default(0)->nullable();
            $table->integer('idusuario')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('viajes');
    }
}
