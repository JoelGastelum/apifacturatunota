<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposCodigopromocionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('codigopromociones', function (Blueprint $table) {
            $table->integer('idrecomendado')->nullable()->default(null)->after('iduser');
            $table->integer('tipo')->nullable()->default(1)->after('fechavigencia')->commet('1 = general, 2 = establecimiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('codigopromociones', function (Blueprint $table) {
            $table->dropColumn('idusuariorecomendon');
            $table->dropColumn('tipo');
        });
    }
}
