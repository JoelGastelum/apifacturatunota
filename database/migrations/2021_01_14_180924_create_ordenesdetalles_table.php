<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesdetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenesdetalles', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('idorden')->nullable()->default(null);
            $table->integer('idservicio')->nullable()->default(null);
            $table->decimal('subtotal',16,4)->nullable()->default(0);
            $table->integer('iva_incluido')->nullable()->default(1);
            $table->decimal('iva_porcentaje',8,2)->nullable()->default(16);
            $table->decimal('iva_monto',16,4)->nullable()->default(0);
            $table->decimal('total',16,4)->nullable()->default(0);
            $table->integer('tipo')->nullable()->default('2')->comment('1=paquete 2=suscripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordenesdetalles');
    }
}
