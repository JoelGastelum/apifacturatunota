<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTasaivatasahConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conceptosxdefault', function (Blueprint $table) {
            $table->decimal('tasaiva',16,2)->nullable()->default('0.16');
            $table->decimal('tasahospedaje',16,2)->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conceptosxdefault', function (Blueprint $table) {
            $table->dropColumn('tasaiva');
            $table->dropColumn('tasahospedaje');
        });
    }
}
