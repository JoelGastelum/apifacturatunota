<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdduenoCodigopromocionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('codigopromociones', function (Blueprint $table) {
            $table->integer('iddueno')->default(null)->nullable()->after('idrecomendado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('codigopromociones', function (Blueprint $table) {
            $table->dropColumn('iddueno');
        });
    }
}
