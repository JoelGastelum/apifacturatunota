<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdusocfdiRemisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('remisiones', function (Blueprint $table) {
            $table->integer('idusocfdi')->nullable()->default(null)->after('idconciliacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('remisiones', function (Blueprint $table) {
            $table->dropColumn('idusocfdi');
        });
    }
}
