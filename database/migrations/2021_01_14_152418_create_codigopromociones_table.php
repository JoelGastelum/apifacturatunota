<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodigopromocionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codigopromociones', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('iduser')->nullable()->default(null);
            $table->string('codigo', 150)->nullable()->default(null);
            $table->date('fechavigencia')->nullable()->default(null);
            $table->string('estatus', 1)->nullable()->default('H')->comment('H = Habilitado, T = Tomado pero no listo, A = Aplicado, D = Desactivado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codigopromociones');
    }
}
