<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellosComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->mediumText('cadenaoriginal')->default(null)->nullable()->after('idusocfdi');
            $table->mediumText('sellodigitalemisor')->default(null)->nullable()->after('tiporelacion');
            $table->mediumText('sellodigitalsat')->default(null)->nullable()->after('tiporelacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('comprobantes', function (Blueprint $table) {
            $table->dropColumn('cadenaoriginal');
            $table->dropColumn('sellodigitalemisor');
            $table->dropColumn('sellodigitalsat');
        });
    }
}
