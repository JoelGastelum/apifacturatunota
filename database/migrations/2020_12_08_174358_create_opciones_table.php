<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('controlador',150)->nullable();
            $table->string('accion',150)->nullable();
            $table->string('nombreruta',150)->nullable();
            $table->integer('idpadre')->nullable()->default(0)->comment('Campo usado para recursividad del opción. ');
            $table->string('icono', 100)->nullable()->comment('Icono visible en el menu')->default(null);
            $table->string('nombre', 150)->nullable()->comment('Nombre visible en el menu')->default(null);
            $table->string('color_permiso', 50)->nullable();
            $table->string('background_permiso', 50)->nullable();
            $table->string('nombre_permiso', 250)->nullable();
            $table->string('descripcion', 250)->nullable();
            $table->integer('orden')->nullable();
            $table->string('estatus', 1)->nullable()->default("A")->comment('A = Activo, D = Desactivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones');
    }
}
