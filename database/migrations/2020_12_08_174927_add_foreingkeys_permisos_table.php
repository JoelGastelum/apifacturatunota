<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeingkeysPermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permisos', function(Blueprint $table)
        {
            $table->foreign('idtipousuario', 'PERMISOS_TIPOUSUARIO_FK')->references('id')->on('tiposusuario')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('idopcion', 'PERMISOS_OPCIONES_FK')->references('id')->on('opciones')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permisos', function(Blueprint $table)
        {
            $table->dropForeign('PERMISOS_TIPOUSUARIO_FK');
            $table->dropForeign('PERMISOS_OPCIONES_FK');
        });
    }
}
