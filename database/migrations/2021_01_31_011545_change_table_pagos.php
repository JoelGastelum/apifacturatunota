<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTablePagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('pagos');
        Schema::create('pagos', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('iduser')->nullable()->default(null);
            $table->date('fecha')->nullable()->default(null);
            $table->decimal('importe',16,2)->nullable()->default(null);
            $table->string('cuenta', 250)->nullable()->default(null);
            $table->string('referencia',70)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        
    }
}
