<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCaracteristaspdfEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('logopdf',250)->default(null)->nullable()->after('avisofolio')->comment("Logo que se mostrara en los pdf factura");
            $table->string('colorfondopdf',250)->default('238@238@238')->nullable()->after('logopdf')->comment("Color de fondo de los recuadros pdf");
            $table->string('colorletrapdf',250)->default('0@0@0')->nullable()->after('colorfondopdf')->comment("Color de letra de pdf");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('logopdf');
            $table->dropColumn('colorfondopdf');
            $table->dropColumn('colorletrapdf');
        });
    }
}
