<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigopromocionUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuarios', function (Blueprint $table) {
            $table->string('codigopromocion',50)->nullable()->default(null)->after('idsucursal');
            // $table->integer('idusuariorecomendon')->nullable()->default(null)->after('codigopromocion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuarios', function (Blueprint $table) {
            $table->dropColumn('codigopromocion');
            // $table->dropColumn('idusuariorecomendon');
        });
    }
}
