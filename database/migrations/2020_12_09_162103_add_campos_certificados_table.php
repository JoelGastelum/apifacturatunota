<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposCertificadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('certificados', function (Blueprint $table) {
            $table->string('archivokey',255)->after('fechafin')->nullable()->default(null);
            $table->string('archivocer',255)->after('archivokey')->nullable()->default(null);
            $table->string('password',255)->after('archivocer')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('certificados', function (Blueprint $table) {
            $table->dropColumn('archivokey');
            $table->dropColumn('archivocer');
            $table->dropColumn('password');
        });
    }
}
