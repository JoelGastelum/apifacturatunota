<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposconfigEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->integer('idusocfdidefault')->nullable()->default(null)->after('foliosconsumidos')->comment('null = preguntar uso, null<> idusosat');
            $table->integer('mostrarformuescanear')->nullable()->default(1)->after('idusocfdidefault')->comment('0 = No mostrar formulario, 1 = formulario');
            $table->integer('solicitaconsectivo')->nullable()->default(1)->after('mostrarformuescanear')->comment('0 = No solicitar consecutivo sacar consecutivo de idsucursal, 1 = Solicitar campo de nota');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('idusocfdidefault');
            $table->dropColumn('mostrarformuescanear');
            $table->dropColumn('solicitaconsectivo');
        });
    }
}
